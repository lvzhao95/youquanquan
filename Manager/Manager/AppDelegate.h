//
//  AppDelegate.h
//  Manager
//
//  Created by lvzhao on 2020/7/29.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

