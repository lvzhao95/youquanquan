//
//  PMScreeningViewController.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMScreeningViewController.h"
#import "PMScreeningView.h"
#import "PMScreeningViewModel.h"


@interface PMScreeningViewController ()
 
@property (nonatomic,strong) PMScreeningViewModel *viewModel;

@property (nonatomic,strong) PMScreeningView *screeningView;

@end

@implementation PMScreeningViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"选择时间";
    // Do any additional setup after loading the view.
    [self setupUI];
    
}

- (void)setupUI{
    
    [self.view addSubview:self.screeningView];
    [self.screeningView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

#pragma mark -懒加载
- (PMScreeningView *)screeningView{
    if(!_screeningView) {
        _screeningView = [[PMScreeningView alloc] initViewModel:self.viewModel];
    }
    return _screeningView;
}

- (PMScreeningViewModel *)viewModel{
    if(!_viewModel) {
        _viewModel = [[PMScreeningViewModel alloc] init];
        _viewModel.pickerModel = self.pickerModel;
        _viewModel.reloadSubject = self.reloadSubject;

    }
    return _viewModel;
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
