//
//  PMScreeningViewController.h
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZBaseViewController.h"
#import "PMScreeningView.h"

NS_ASSUME_NONNULL_BEGIN

@interface PMScreeningViewController : LZBaseViewController

//选择时间的类型
@property (nonatomic,assign) PMDatePickerModel pickerModel;

@property (nonatomic,strong) RACSubject *reloadSubject;

@end

NS_ASSUME_NONNULL_END
