//
//  PMScreeningView.h
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZBaseView.h"

/// 弹出日期类型
typedef NS_ENUM(NSInteger, PMDatePickerModel) {
    // 年月
    PMDatePickerModelYM = 0,         // yyyy-MM
    // 年月日
    PMDatePickerModelYMD = 1,        // yyyy-MM-dd
    // 年月日 时 分
    PMDatePickerModelYMDHM = 2,        // yyyy-MM-dd HH:mm
};
NS_ASSUME_NONNULL_BEGIN

@interface PMScreeningView : LZBaseView

@end

NS_ASSUME_NONNULL_END
