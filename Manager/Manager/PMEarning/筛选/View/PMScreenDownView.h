//
//  PMScreenDownView.h
//  Manager
//
//  Created by lvzhao on 2020/7/31.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PMScreenDownView : UIView
/**
 *  returns Alert.
 *  title NSString
 *  message NSString
 *  cancel NSString
 *  sure NSString
 *  dictionary @{};里面的参数自定义    key:subTitles||image||cancelTitle||sureTitle||pointString;
 *  cancelBlock & sureBlock 回调
 *  returns Alert.
 */
+ (void)showScreenAlertType:(LZAlertType)alertType cancel:(NSString *__nullable)cancel sure:(NSString *__nullable)sure objectDict:(NSDictionary *__nullable)dictionary cancelBlock:(void(^)(__nullable id object))cancelBlock sureBlock:(void(^)(__nullable id object))sureBlock;

+ (void)dismiss;

@end

NS_ASSUME_NONNULL_END
