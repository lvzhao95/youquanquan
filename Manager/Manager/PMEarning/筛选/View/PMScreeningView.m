//
//  PMScreeningView.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMScreeningView.h"
#import "PMScreeningViewModel.h"
#import "LZBaseViewController.h"



@interface PMScreeningView ()<UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource>{
    
    // 记录 年、月、日,时,分当前选择的位置
     NSInteger _yearIndex;
     NSInteger _monthIndex;
     NSInteger _dayIndex;
     NSInteger _hourIndex;
     NSInteger _minIndex;

}

@property (nonatomic,strong) PMScreeningViewModel *viewModel;
///选择月日
@property (nonatomic,strong) UIButton *selectBtn;

///按日选择 开始
@property (nonatomic,strong) UITextField *startDayTF;
///按日选择 结束
@property (nonatomic,strong) UITextField *endDayTF;
///按月选择 开始
@property (nonatomic,strong) UITextField *monthTF;


@property (nonatomic,strong) UIView *dayView;
@property (nonatomic,strong) UIView *monthView;



@property (strong,nonatomic) UIPickerView *YMpickView;//年月
@property (assign,nonatomic) NSInteger indexTag;//记录年月日的开始时间和结束时间TF

/// 日期存储数组
@property(nonatomic, strong) NSArray *yearArr;
@property(nonatomic, strong) NSArray *monthArr;
@property(nonatomic, strong) NSArray *dayArr;
@property(nonatomic, strong) NSArray *hourArr;
@property(nonatomic, strong) NSArray *minArr;

/** 限制最小日期 */
@property (nonatomic, strong) NSDate *minLimitDate;
/** 限制最大日期 */
@property (nonatomic, strong) NSDate *maxLimitDate;
/** 当前选择的日期 */
@property (nonatomic, strong) NSDate *selectDate;
/** 选择的日期的格式 */
@property (nonatomic, strong) NSString *selectDateFormatter;
@end

@implementation PMScreeningView


- (instancetype)initViewModel:(LZBaseViewModel *)viewModel
{
    self = [super init];
    if (self) {
        self.backgroundColor = COLOR_cellbackground;
        self.viewModel = (PMScreeningViewModel *)viewModel;
        [self setupView];
    }
    return self;
}

- (void) setupView{
    
    ///!!!:完成按钮
    UIButton *finishBtn = [UIButton lz_buttonTitle:@"完成" titleColor:COLOR_appColor fontSize:15];
    finishBtn.frame = CGRectMake(0, 0, 44, 44);
    LZBaseViewController *currenttVC = (LZBaseViewController *)LZTool.currentViewController;
    [currenttVC initBarItem:finishBtn withType:1];
    
    if(self.viewModel.pickerModel != PMDatePickerModelYMDHM) {
        ///!!!:按月选择
        UIButton *monthBtn = [UIButton lz_buttonTitle:@"按月选择" titleColor:COLOR_textColor fontSize:15];
        [monthBtn setTitleColor:UIColor.whiteColor forState:UIControlStateSelected];
        monthBtn.backgroundColor = COLOR_appColor;
        monthBtn.cornerRadius = 5;
        monthBtn.selected = YES;
        self.selectBtn = monthBtn;
        [self addSubview:monthBtn];
        [monthBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(90);
            make.top.mas_equalTo(50);
            make.left.mas_equalTo((K_SCREENWIDTH - 180 - 20)/2.0);
            make.height.mas_equalTo(35);
        }];
        [monthBtn addTarget:self action:@selector(screenClickDate:) forControlEvents:UIControlEventTouchUpInside];
        
        ///!!!:按日选择
        UIButton *dayBtn = [UIButton lz_buttonTitle:@"按日选择" titleColor:COLOR_textColor fontSize:15];
        [dayBtn setTitleColor:UIColor.whiteColor forState:UIControlStateSelected];
        dayBtn.cornerRadius = 5;
        [self addSubview:dayBtn];
        [dayBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(90);
            make.top.mas_equalTo(50);
            make.right.mas_equalTo(-(K_SCREENWIDTH - 180 - 20)/2.0);
            make.height.mas_equalTo(35);
        }];
        [dayBtn addTarget:self action:@selector(screenClickDate:) forControlEvents:UIControlEventTouchUpInside];

        
        
        ///!!!:选择月选择
        [self.monthView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(15);
            make.right.mas_equalTo(-15);
            make.top.mas_equalTo(120);
            make.height.mas_equalTo(32);
        }];
        [self.monthTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(0);
        }];
        self.monthTF.tag = 100;
        
        
    }
    

    ///!!!:日选择
    [self.dayView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.top.mas_equalTo(120);
        make.height.mas_equalTo(32);
    }];
    
    //开始日期
    [self.startDayTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.width.mas_equalTo((K_SCREENWIDTH - 30 - 30)/2.0);
        make.height.mas_equalTo(32);
        make.top.mas_equalTo(0);
    }];
    self.startDayTF.tag = 101;
    
    //至
    UILabel *toLab = [UILabel lz_labelWithText:@"至" fontSize:PingFangSC(15) color:COLOR_textColor];
    [self.dayView addSubview:toLab];
    [toLab lz_centerAlignment];
    [toLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.dayView);
        make.height.mas_equalTo(15);
        make.width.mas_equalTo(20);
        make.centerX.mas_equalTo(self.dayView);
    }];

    //结束日期
    [self.endDayTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.width.mas_equalTo((K_SCREENWIDTH - 30 - 30)/2.0);
        make.height.mas_equalTo(32);
        make.top.mas_equalTo(0);
    }];
    self.endDayTF.tag = 102;
    UIView *lineView = [self.endDayTF viewWithTag:88];
    lineView.backgroundColor = COLOR_cellLine;
    
    [self addSubview:self.YMpickView];
    [self.YMpickView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(200);
    }];
    
    ///YMpickView
    self.YMpickView.showsSelectionIndicator = YES;
    // 设置子视图的大小随着父视图变化
    self.YMpickView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;

    //配置数据
    [self setupConfig];
    
    
    @weakify(self);
    [[finishBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);
   
        NSString *startTime = @"";
        NSString *endTime = @"";
        
        switch (self.viewModel.pickerModel) {
            case PMDatePickerModelYM:
            {
                NSRange daysInLastMonth = [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:self.selectDate];
                NSInteger dayCountOfThisMonth = daysInLastMonth.length;
                
                startTime = [NSString stringWithFormat:@"%@-01 00:00",self.monthTF.text];
                endTime = [NSString stringWithFormat:@"%@-%ld 23:59",self.monthTF.text,dayCountOfThisMonth];
            }
                break;
            case PMDatePickerModelYMD:{
                startTime = [NSString stringWithFormat:@"%@ 00:00",self.startDayTF.text];
                endTime = [NSString stringWithFormat:@"%@ 23:59",self.endDayTF.text];
            }
                break;
            case PMDatePickerModelYMDHM:
            {
               startTime = [NSString stringWithFormat:@"%@",self.startDayTF.text];
               endTime = [NSString stringWithFormat:@"%@",self.endDayTF.text];
            }
         
                break;
            default:
                break;
        }
        
        NSInteger startTimestamp = [NSDate cTimestampFromDate:[NSDate dateFromString:startTime format:@"yyyy-MM-dd HH:mm"]];
        NSInteger endTimestamp = [NSDate cTimestampFromDate:[NSDate dateFromString:endTime format:@"yyyy-MM-dd HH:mm"]];
        if(startTimestamp > endTimestamp){
            showDelayedDismissTitle(@"开始时间不得大于结束时间！", nil);
            return;
        }
       
        if(self.viewModel.reloadSubject && startTime.length > 0 && endTime.length > 0){
            [self.viewModel.reloadSubject sendNext:@{@"startTime":startTime,
                                                     @"endTime":endTime}];
        } else {
            showLoadingTitle(@"请选择时间", nil);
        }
        [LZTool.currentViewController.navigationController popViewControllerAnimated:YES];
        
        
    }];
}

- (void)setupConfig{
    //默认按月筛选
    self.dayView.hidden = (self.viewModel.pickerModel == PMDatePickerModelYM);
    self.monthView.hidden = !(self.viewModel.pickerModel == PMDatePickerModelYM);
    if(self.viewModel.pickerModel == PMDatePickerModelYMDHM){
        self.dayView.hidden = NO;
        self.monthView.hidden = YES;
    }
    self.selectDate = [NSDate date];
    
    //配置属性
    [self setupMinMaxValue];
    
    //初始化数据
    [self initDefaultDateArray];
    
    //滚到今日的日期
    [self scrollToSelectDate:self.selectDate animated:YES];

}
///配置
- (void)setupMinMaxValue {
    NSDate *now = [NSDate date];

    switch (self.viewModel.pickerModel) {
        case PMDatePickerModelYM:
        {
            self.indexTag = 100;
            self.selectDateFormatter = @"yyyy-MM";
            self.minLimitDate = [NSDate dateFromString:@"2000-08" format:self.selectDateFormatter];
            self.maxLimitDate = now;
        }
            break;

        case PMDatePickerModelYMD:
        {
            self.indexTag = 101;
            self.selectDateFormatter = @"yyyy-MM-dd";
            self.minLimitDate = [NSDate dateFromString:@"2000-08-08" format:self.selectDateFormatter];
            self.maxLimitDate = now;
        }
            break;
        case PMDatePickerModelYMDHM:
        {
            self.indexTag = 101;
            self.selectDateFormatter = @"yyyy-MM-dd HH:mm";
            self.minLimitDate = [NSDate dateFromString:@"2000-08-08 00:00" format:self.selectDateFormatter];
            self.maxLimitDate = now;
        }
            break;


        default:
            break;
    }
}

#pragma mark - 设置日期数据源数组
- (void)initDefaultDateArray {
    // 1. 设置 yearArr 数组
    [self setupYearArr];
    // 2.设置 monthArr 数组
    [self setupMonthArr:self.selectDate.lz_year];
    // 3.设置 dayArr 数组
    [self setupDayArr:self.selectDate.lz_year month:self.selectDate.lz_month];
    
    // 4.设置 小时 分钟 数组
    [self setupHourMinArray];
    
    //刷新界面
    [self.YMpickView reloadAllComponents];
    
    // 根据 默认选择的日期 计算出 对应的索引
    _yearIndex = self.selectDate.lz_year - self.minLimitDate.lz_year;
    _monthIndex = self.selectDate.lz_month - ((_yearIndex == 0) ? self.minLimitDate.lz_month : 1);
    _dayIndex = self.selectDate.lz_day - ((_yearIndex == 0 && _monthIndex == 0) ? self.minLimitDate.lz_day : 1);
    _hourIndex = self.selectDate.lz_hour;
    _minIndex = self.selectDate.lz_min;

}

#pragma mark - 更新日期数据源数组
- (void)updateDateArray {
    NSInteger year = [self.yearArr[_yearIndex] integerValue];
    // 1.设置 monthArr 数组
    [self setupMonthArr:year];
    // 更新索引：防止更新 monthArr 后数组越界
    _monthIndex = (_monthIndex > self.monthArr.count - 1) ? (self.monthArr.count - 1) : _monthIndex;
    
    NSInteger month = [self.monthArr[_monthIndex] integerValue];
    // 2.设置 dayArr 数组
    [self setupDayArr:year month:month];
    // 更新索引：防止更新 dayArr 后数组越界
    _dayIndex = (_dayIndex > self.dayArr.count - 1) ? (self.dayArr.count - 1) : _dayIndex;
}

// 设置 monthArr 数组
- (void)setupMonthArr:(NSInteger)year {
    NSInteger startMonth = 1;
    NSInteger endMonth = 12;
    if (year == self.minLimitDate.lz_year) {
        startMonth = self.minLimitDate.lz_month;
    }
    if (year == self.maxLimitDate.lz_year) {
        endMonth = self.maxLimitDate.lz_month;
    }
    NSMutableArray *tempArr = [NSMutableArray arrayWithCapacity:(endMonth - startMonth + 1)];
    for (NSInteger i = startMonth; i <= endMonth; i++) {
        [tempArr addObject:[@(i) stringValue]];
    }
    self.monthArr = [tempArr copy];
}

// 设置 dayArr 数组
- (void)setupDayArr:(NSInteger)year month:(NSInteger)month {
    NSInteger startDay = 1;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *endDate = [NSDate dateFromString:[NSString stringWithFormat:@"%ld-%ld",(long)year,(long)month] format:@"yyyy-MM"];
    NSRange range = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:endDate];
    NSInteger endDay = range.length;//;
    if (year == self.minLimitDate.lz_year && month == self.minLimitDate.lz_month) {
        startDay = self.minLimitDate.lz_day;
    }
    if (year == self.maxLimitDate.lz_year && month == self.maxLimitDate.lz_month) {
        endDay = self.maxLimitDate.lz_day;
    }
    NSMutableArray *tempArr = [NSMutableArray array];
    for (NSInteger i = startDay; i <= endDay; i++) {
        [tempArr addObject:[NSString stringWithFormat:@"%zi",i]];
    }
    self.dayArr = [tempArr copy];
}

// 设置 hour min数组
- (void)setupHourMinArray {
    NSMutableArray *tempArr = [NSMutableArray array];
    for (NSInteger i = 0; i < 24; i++) {
        [tempArr addObject:[NSString stringWithFormat:@"%zi",i]];
    }
    self.hourArr = [tempArr copy];
    
    NSMutableArray *tempMinArr = [NSMutableArray array];
    for (NSInteger i = 0; i < 60; i++) {
        [tempMinArr addObject:[NSString stringWithFormat:@"%zi",i]];
    }
    self.minArr = [tempMinArr copy];
    
}

#pragma mark - UIPickerViewDataSource
// 1.指定pickerview有几个表盘(几列)
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    if (self.viewModel.pickerModel == PMDatePickerModelYMD) {
        return 3;
    } else if (self.viewModel.pickerModel == PMDatePickerModelYM) {
        return 2;
    } else if (self.viewModel.pickerModel == PMDatePickerModelYMDHM) {
        return 5;
    }
    return 0;
}

// 2.指定每个表盘上有几行数据
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSArray *rowsArr = [NSArray array];
    if (self.viewModel.pickerModel == PMDatePickerModelYMD) {
        rowsArr = @[@(self.yearArr.count), @(self.monthArr.count), @(self.dayArr.count)];
    } else if (self.viewModel.pickerModel == PMDatePickerModelYM) {
        rowsArr = @[@(self.yearArr.count), @(self.monthArr.count)];
    }  else if (self.viewModel.pickerModel == PMDatePickerModelYMDHM) {
        rowsArr = @[@(self.yearArr.count), @(self.monthArr.count), @(self.dayArr.count), @(self.hourArr.count), @(self.minArr.count)];
    }
    return [rowsArr[component] integerValue];
}
#pragma mark - UIPickerViewDelegate
// 3.设置 pickerView 的 显示内容
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(nullable UIView *)view {
    // 设置分割线的颜色
    ((UIView *)[pickerView.subviews objectAtIndex:1]).backgroundColor = COLOR_cellLine;
    ((UIView *)[pickerView.subviews objectAtIndex:2]).backgroundColor = COLOR_cellLine;
    
    UILabel *label = (UILabel *)view;
    if (!label) {
        label = [[UILabel alloc]init];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = PingFangSC(21);
        label.textColor = COLOR_textColor;
        // 字体自适应属性
        label.adjustsFontSizeToFitWidth = YES;
        // 自适应最小字体缩放比例
        label.minimumScaleFactor = 0.5f;
    }
    // 给选择器上的label赋值
    [self setDateLabelText:label component:component row:row];
    return label;
}

// 4.选中时回调的委托方法，
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    // 获取滚动后选择的日期
    self.selectDate = [self getDidSelectedDate:component row:row];
    NSString *selectDateValue = [NSDate datestrFromDate:self.selectDate withDateFormat:self.selectDateFormatter];
    switch (self.viewModel.pickerModel) {
        case PMDatePickerModelYMD:
        case PMDatePickerModelYMDHM:
        {
            if(self.indexTag == 101){
                self.startDayTF.text = selectDateValue;
            }else if(self.indexTag == 102){
                self.endDayTF.text = selectDateValue;
            }
        }
            break;
        case PMDatePickerModelYM:{
            self.monthTF.text = selectDateValue;
        }
            break;
            
        default:
            break;
    }
    
}

// 设置行高
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 35.0f;
}

- (void)setDateLabelText:(UILabel *)label component:(NSInteger)component row:(NSInteger)row {
    
    switch (component) {
            case 0:
                label.text = [NSString stringWithFormat:@"%@年", self.yearArr[row]];
                break;
            case 1:
                label.text = [NSString stringWithFormat:@"%@月", self.monthArr[row]];
                break;
            case 2:
                label.text = [NSString stringWithFormat:@"%@日", self.dayArr[row]];
                break;
            case 3:
                label.text = [NSString stringWithFormat:@"%@时", self.hourArr[row]];
                break;
            case 4:
                label.text = [NSString stringWithFormat:@"%@分", self.minArr[row]];
                break;
        default:
            break;
    }
    
    
  
}

- (NSDate *)getDidSelectedDate:(NSInteger)component row:(NSInteger)row {
    NSString *selectDateValue = nil;
    if (self.viewModel.pickerModel == PMDatePickerModelYMD) {
        if (component == 0) {
            _yearIndex = row;
            [self updateDateArray];
            [self.YMpickView reloadComponent:1];
            [self.YMpickView reloadComponent:2];
        } else if (component == 1) {
            _monthIndex = row;
            [self updateDateArray];
            [self.YMpickView reloadComponent:2];
        } else if (component == 2) {
            _dayIndex = row;
        }
        if(self.yearArr.count > _yearIndex){
            selectDateValue = [NSString stringWithFormat:@"%@-%02ld-%02ld", self.yearArr[_yearIndex], [self.monthArr[_monthIndex] integerValue], [self.dayArr[_dayIndex] integerValue]];
        }
        
    } else if (self.viewModel.pickerModel == PMDatePickerModelYM) {
        if (component == 0) {
            _yearIndex = row;
            [self updateDateArray];
            [self.YMpickView reloadComponent:1];
        } else if (component == 1) {
            _monthIndex = row;
        }
        if(self.yearArr.count > _yearIndex){
            selectDateValue = [NSString stringWithFormat:@"%@-%02ld", self.yearArr[_yearIndex], [self.monthArr[_monthIndex] integerValue]];
        }
    } else if (self.viewModel.pickerModel == PMDatePickerModelYMDHM) {
        if (component == 0) {
            _yearIndex = row;
            [self updateDateArray];
            [self.YMpickView reloadComponent:1];
            [self.YMpickView reloadComponent:2];
        } else if (component == 1) {
            _monthIndex = row;
            [self updateDateArray];
            [self.YMpickView reloadComponent:2];
        } else if (component == 2) {
            _dayIndex = row;
        } else if (component == 3) {
            _hourIndex = row;
        } else if (component == 4) {
            _minIndex = row;
        }
        if(self.yearArr.count > _yearIndex){
            selectDateValue = [NSString stringWithFormat:@"%@-%02ld-%02ld %@:%@", self.yearArr[_yearIndex], [self.monthArr[_monthIndex] integerValue], [self.dayArr[_dayIndex] integerValue],self.hourArr[_hourIndex],self.minArr[_minIndex]];
        }
        
    }
    return  [NSDate dateFromString:selectDateValue format:self.selectDateFormatter];
}


#pragma mark - getter 方法
- (NSArray *)yearArr {
    if (!_yearArr) {
        _yearArr = [NSArray array];
    }
    return _yearArr;
}

- (NSArray *)monthArr {
    if (!_monthArr) {
        _monthArr = [NSArray array];
    }
    return _monthArr;
}

- (NSArray *)dayArr {
    if (!_dayArr) {
        _dayArr = [NSArray array];
    }
    return _dayArr;
}

- (NSArray *)hourArr {
    if (!_hourArr) {
        _hourArr = [NSArray array];
    }
    return _hourArr;
}
- (NSArray *)minArr {
    if (!_minArr) {
        _minArr = [NSArray array];
    }
    return _minArr;
}
#pragma mark - 滚动到指定的时间位置
- (void)scrollToSelectDate:(NSDate *)selectDate animated:(BOOL)animated {
    // 根据 当前选择的日期 计算出 对应的索引
    NSInteger yearIndex = selectDate.lz_year - self.minLimitDate.lz_year;
    NSInteger monthIndex = selectDate.lz_month - ((yearIndex == 0) ? self.minLimitDate.lz_month : 1);
    NSInteger dayIndex = selectDate.lz_day - ((yearIndex == 0 && monthIndex == 0) ? self.minLimitDate.lz_day : 1);
    NSInteger hourIndex = selectDate.lz_hour;
    NSInteger minIndex = selectDate.lz_min;


    NSArray *indexArr = [NSArray array];
    if (self.viewModel.pickerModel == PMDatePickerModelYMD) {
        indexArr = @[@(yearIndex), @(monthIndex), @(dayIndex)];
    } else if (self.viewModel.pickerModel == PMDatePickerModelYM) {
        indexArr = @[@(yearIndex), @(monthIndex)];
    } else if (self.viewModel.pickerModel == PMDatePickerModelYMDHM) {
        indexArr = @[@(yearIndex), @(monthIndex), @(dayIndex),@(hourIndex),@(minIndex)];
    }
    for (NSInteger i = 0; i < indexArr.count; i++) {
        [self.YMpickView selectRow:[indexArr[i] integerValue] inComponent:i animated:animated];
    }
}

// 设置 yearArr 数组
- (void)setupYearArr {
    NSMutableArray *tempArr = [NSMutableArray array];
    for (NSInteger i = self.minLimitDate.lz_year; i <= self.maxLimitDate.lz_year; i++) {
        [tempArr addObject:[@(i) stringValue]];
    }
    self.yearArr = [tempArr copy];
}





#pragma mark -UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    self.indexTag = textField.tag;
    UIView *lineView = [textField viewWithTag:88];
    lineView.backgroundColor = COLOR_appColor;
    
    switch (self.indexTag) {
        case 101:
        {
            UIView *tempLineView = [self.endDayTF viewWithTag:88];
            tempLineView.backgroundColor = COLOR_cellLine;
        }
            break;
            
        case 102:
        {
            UIView *tempLineView = [self.startDayTF viewWithTag:88];
            tempLineView.backgroundColor = COLOR_cellLine;
        }
        break;
                       
        default:
            break;
    }
    
    return NO;
}

#pragma mark -选择条件
- (void)screenClickDate:(UIButton *)sender{
    self.selectBtn.backgroundColor = UIColor.whiteColor;
    self.selectBtn.selected = NO;
    
    sender.backgroundColor = COLOR_appColor;
    sender.selected = YES;
    self.selectBtn = sender;
    if ([sender.currentTitle isEqualToString:@"按月选择"]) {
        self.viewModel.pickerModel = PMDatePickerModelYM;
        self.indexTag = 100;
    } else {
        self.viewModel.pickerModel = PMDatePickerModelYMD;
        self.indexTag = 101;
        [self textFieldShouldBeginEditing:self.startDayTF];
    }
    
    //配置
    [self setupConfig];
}
#pragma mark -创建视图
- (UIView *) createTextField:(NSString *)placeholder{
    
    UITextField *textField = [[UITextField alloc] init];
    textField.placeholder = placeholder;
    textField.delegate = self;
    textField.lz_placeholderColor = COLOR_placeholderColor;
    textField.textColor = COLOR_textColor;
    textField.textAlignment = NSTextAlignmentCenter;
    [self addSubview:textField];
    
    UIView *lineView = [[UIView alloc] init];
    lineView.tag = 88;
    lineView.backgroundColor = COLOR_appColor;
    [textField addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
    
    return textField;
    
}
- (UIView *)dayView{
    if(!_dayView){
        _dayView = [[UIView alloc] init];
        _dayView.hidden = YES;
        [self addSubview:_dayView];
        self.startDayTF = (UITextField *)[self createTextField:@"开始日期"];
        self.endDayTF = (UITextField *)[self createTextField:@"结束日期"];
        [_dayView addSubview:self.startDayTF];
        [_dayView addSubview:self.endDayTF];

    }
    return _dayView;
}


- (UIView *)monthView{
    if(!_monthView){
        _monthView = [[UIView alloc] init];
        [self addSubview:_monthView];
        self.monthTF = (UITextField *)[self createTextField:@"按月选择"];
        [_monthView addSubview:self.monthTF];
    }
    return _monthView;
}

//时间选择器
- (UIPickerView *)YMpickView{
    if(!_YMpickView){
        _YMpickView = [[UIPickerView alloc] init];
        _YMpickView.delegate = self;
        _YMpickView.dataSource = self;
    }
    return _YMpickView;
}


@end
