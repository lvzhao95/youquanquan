//
//  PMEarningViewController.m
//  Manager
//
//  Created by lvzhao on 2020/7/29.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMEarningViewController.h"
#import "PMEarningViewModel.h"
#import "PMEarningView.h"
#import "PMScreeningViewController.h"
#import "LZOptionView.h"
#import "PMPDDOrderModel.h"



@interface PMEarningViewController ()
 
@property (nonatomic,strong) PMEarningViewModel *viewModel;

@property (nonatomic,strong) PMEarningView *earningView;

@end

@implementation PMEarningViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)setupUI{
    
    [self.view addSubview:self.earningView];
    [self.earningView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    
    
    ///!!!:筛选
    UIButton *screeningBtn = [UIButton lz_buttonImageName:@"pm_shaixuan" backgroundImageName:@""];
    screeningBtn.frame = CGRectMake(0, 0, 44, 44);
    [self initBarItem:screeningBtn withType:1];
    @weakify(self);
    [[screeningBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);
        RACSubject *subject = [RACSubject subject];
        PMScreeningViewController *screeningVC = [[PMScreeningViewController alloc] init];
        screeningVC.pickerModel = PMDatePickerModelYM;
        screeningVC.reloadSubject = subject;
        [LZTool.currentViewController.navigationController pushViewController:screeningVC animated:YES];
        [subject subscribeNext:^(id  _Nullable x) {
            NSDictionary *dict = x;
            NSLog(@"%@",x);
            [self.viewModel getEarnDetail:dict[@"startTime"] endTime:dict[@"endTime"]];
        }];

    }];
    
  
    
    NSLog(@"----");
//    LZOptionView *view = [[LZOptionView alloc] initWithFrame:CGRectMake(100, 300, 200, 40)];
//    view.placeholder = @"请选择你的选择";
//    NSArray *titles = @[@"111",@"222",@"333",@"444",@"555",@"55",@"111",@"222",@"333",@"444",@"555",@"55"];
//    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
//    for (int i = 0; i < titles.count; i++){
//        LZOptionModel *model = [LZOptionModel new];
//        model.title = titles[i];
//        model.detail = [NSString stringWithFormat:@"%d",i];
//        [dataArray addObject:model];
//    }
//    view.dataSource = dataArray;
//    [view setSelectedBlock:^(LZOptionView * _Nonnull optionView, LZOptionModel * _Nonnull optionModel) {
//        NSLog(@"detail == %@",optionModel.detail);
//    }];
//    [self.view addSubview:view];
//    return;
    
    
    
    
    
    
    
    
    
}

#pragma mark -懒加载
- (PMEarningView *)earningView{
    if(!_earningView) {
        _earningView = [[PMEarningView alloc] initViewModel:self.viewModel];
    }
    return _earningView;
}

- (PMEarningViewModel *)viewModel{
    if(!_viewModel) {
        _viewModel = [[PMEarningViewModel alloc] init];
    }
    return _viewModel;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
