//
//  PMEarningViewModel.m
//  Manager
//
//  Created by lvzhao on 2020/7/29.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMEarningViewModel.h"

@implementation PMEarningViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        NSArray *titles = @[@"总收入",@"支付宝收入",@"微信收入"];
        NSArray *iamgeNames = @[@"pm_earning_bg",@"pm_bg_miaobian",@"pm_bg_miaobian"];
        for (int i = 0; i < 3; i++) {
            
            PMEarningModel *earningModel = [[PMEarningModel alloc] init];
            earningModel.imageName = iamgeNames[i];
            earningModel.title = titles[i];
            earningModel.sumAmountSettle = @(0);
            earningModel.sumAmount = @(0);
            earningModel.sumTotal = @(0);
            earningModel.profits = @(0);
            [self.dataArray addObject:earningModel];
        }
        
        
    }
    return self;
}
/** 获取利益明细*/
- (void) getEarnDetail:(NSString *)startTime endTime:(NSString *)endTime{
 
//    NSDictionary *params = @{@"is_transfer":@(1)};
//    [LZNetworkingManager lz_request:@"post" url:kGetPayChannel params:params
//         success:^(id  _Nullable responseObject) {
//
//    } failure:^(NSError * _Nullable error) {} isLoading:YES isFailTip:YES];
    if(startTime.length == 0){
        startTime = [NSDate datestrFromDate:[NSDate date] withDateFormat:@"yyyy-MM-dd"];
        startTime = [NSString stringWithFormat:@"%@ 00:00",startTime];
    }
    if(endTime.length == 0){
         endTime = [NSDate datestrFromDate:[NSDate date] withDateFormat:@"yyyy-MM-dd"];
         endTime = [NSString stringWithFormat:@"%@ 23:59",endTime];
         
     }
    
    NSDictionary *params = @{@"startTime":startTime,
                             @"endTime":endTime};
    [LZNetworkingManager lz_request:@"post" url:kSummaryCharge params:params
    success:^(id  _Nullable responseObject) {
        if([responseObject[kResultStatus] isEqualToString:kCode]){
            NSArray * dataArray = responseObject[@"sums"];
            
            CGFloat alipaySum_amount = 0;
            CGFloat alipaySum_amount_settle = 0;
            NSInteger alipayTotal = 0;
            
            
            CGFloat wxSum_amount = 0;
            CGFloat wxSum_amount_settle = 0;
            NSInteger wxTotal = 0;

            for (int i = 0; i < dataArray.count; i++){
                PMEarningModel *earningModel = [PMEarningModel modelWithJSON:dataArray[i]];
                if([earningModel.channel containsString:@"alipay"]){
                    alipaySum_amount_settle += earningModel.amountSettle;
                    alipaySum_amount += earningModel.amount;
                    alipayTotal += earningModel.total;
                } else if([earningModel.channel containsString:@"wx"]){
                    wxSum_amount_settle += earningModel.amountSettle;
                    wxSum_amount += earningModel.amount;
                    wxTotal += earningModel.total;

                }
            }
            
            NSArray *titles = @[@"总收入",@"支付宝收入",@"微信收入"];
            NSArray *iamgeNames = @[@"pm_earning_bg",@"pm_bg_miaobian",@"pm_bg_miaobian"];
            NSArray *sumAmountSettles = @[@(alipaySum_amount_settle + wxSum_amount_settle),@(alipaySum_amount_settle),@(wxSum_amount_settle)];
            NSArray *sumAmounts = @[@(wxSum_amount + alipaySum_amount),@(alipaySum_amount),@(wxSum_amount)];
            NSArray *profits = @[@((alipaySum_amount + wxSum_amount) - (alipaySum_amount_settle + wxSum_amount_settle)),@(alipaySum_amount - alipaySum_amount_settle),@(wxSum_amount - wxSum_amount_settle)];
            NSArray *totals = @[@(alipayTotal + wxTotal),@(alipayTotal),@(wxTotal)];
            
            
            [self.dataArray removeAllObjects];
            for (int i = 0; i < titles.count; i++){
                
                NSDictionary *dictionary = @{@"imageName":iamgeNames[i],
                                             @"title":titles[i],
                                             @"sumAmountSettle":sumAmountSettles[i],
                                             @"sumAmount":sumAmounts[i],
                                             @"total":totals[i],
                                             @"profits":profits[i]};
                PMEarningModel *earningModel = [PMEarningModel modelWithJSON:dictionary];
                [self.dataArray addObject:earningModel];
            
            }
            [self.reloadSubject sendNext:@(1)];
        }

    } failure:^(NSError * _Nullable error) {
        [self.reloadSubject sendNext:@(1)];
    } isLoading:NO isFailTip:NO];
}



- (NSDictionary *)dataDictionary{
    if(!_dataDictionary){
        _dataDictionary = [[NSMutableDictionary alloc] init];
    }
    return _dataDictionary;
}
@end
