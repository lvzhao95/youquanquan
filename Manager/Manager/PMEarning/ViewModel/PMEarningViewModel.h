//
//  PMEarningViewModel.h
//  Manager
//
//  Created by lvzhao on 2020/7/29.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZBaseViewModel.h"
#import "PMEarningModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PMEarningViewModel : LZBaseViewModel

@property (nonatomic,strong) NSDictionary *dataDictionary;

/** 获取利益明细*/
- (void) getEarnDetail:(NSString *)startTime endTime:(NSString *)endTime;


@end

NS_ASSUME_NONNULL_END
