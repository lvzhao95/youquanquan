//
//  PMEarningModel.h
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PMEarningModel : NSObject


@property (nonatomic,assign) CGFloat amountSettle;
@property (nonatomic,assign) CGFloat amount;
@property (nonatomic,strong) NSString *channel;
@property (nonatomic,assign) NSInteger total;



@property (nonatomic,strong) NSString *imageName;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSNumber *sumAmountSettle;
@property (nonatomic,strong) NSNumber *sumAmount;
@property (nonatomic,strong) NSNumber *sumTotal;
@property (nonatomic,assign) NSNumber *profits;




@end

NS_ASSUME_NONNULL_END
