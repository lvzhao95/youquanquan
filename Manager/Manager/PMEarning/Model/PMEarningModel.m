//
//  PMEarningModel.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMEarningModel.h"

@implementation PMEarningModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"amountSettle" : @"sum_amount_settle",
             @"amount": @"sum_amount"};
}

@end
