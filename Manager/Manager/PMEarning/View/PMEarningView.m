//
//  PMEarningView.m
//  Manager
//
//  Created by lvzhao on 2020/7/29.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMEarningView.h"
#import "PMEarningViewModel.h"
#import "LZMJDIYRefreshHeader.h"

@interface PMEarningView ()

@property (nonatomic,strong) PMEarningViewModel *viewModel;

@end

@implementation PMEarningView


- (instancetype)initViewModel:(LZBaseViewModel *)viewModel
{
    self = [super init];
    if (self) {
        
        self.backgroundColor = COLOR_background;
        self.viewModel = (PMEarningViewModel *)viewModel;
        [self setupView];
    }
    return self;
}

- (void) setupView{
    
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.showsVerticalScrollIndicator = NO;
    [self addSubview:scrollView];
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.right.bottom.mas_equalTo(0);
    }];

    //内容
    UIView *contentView = [[UIView alloc] init];
    contentView.backgroundColor = COLOR_background;
    [scrollView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.and.right.equalTo(scrollView).with.insets(UIEdgeInsetsZero);
        make.width.equalTo(scrollView);

    }];

    
    NSMutableArray *detailViews = [[NSMutableArray alloc] init];
    UIView *lastView = nil;
    for (int i = 0; i < self.viewModel.dataArray.count; i++){
        UIButton *contentBtn = [self createEarningView:self.viewModel.dataArray[i]];
        [contentView addSubview:contentBtn];
        [contentBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(3.5);
            make.right.mas_equalTo(-3.5);
            make.height.mas_equalTo(178);
            if(lastView){
                make.top.mas_equalTo(lastView.mas_bottom);
            } else {
                make.top.mas_equalTo(0);
            }
        }];
        lastView = contentBtn;
        [detailViews addObject:lastView];
    }
    
    [contentView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(lastView.mas_bottom).mas_offset(34);
    }];
    
    //下拉刷新
    @weakify(self);
    LZMJDIYRefreshHeader *header = [LZMJDIYRefreshHeader headerWithRefreshingBlock:^{
    @strongify(self);
        [self.viewModel getEarnDetail:@"" endTime:@""];
    }];
    scrollView.mj_header = header;
    
    [self.viewModel.reloadSubject subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        dismiss(nil);
        scrollView.mj_header.state = MJRefreshStateIdle;
        for (int i = 0; i < self.viewModel.dataArray.count; i++){
            [self reloadEarning:detailViews[i] model:self.viewModel.dataArray[i]];
        }
    }];
    
    
    ///获取收益明细
    showLoadingTitle(@"", nil);
    [self.viewModel getEarnDetail:@"" endTime:@""];
    
}


//创建UI
- (UIButton *)createEarningView:(PMEarningModel *)earningModel{
    
    UIButton *contentBtn = [UIButton lz_buttonImageName:@"" backgroundImageName:earningModel.imageName];
    contentBtn.adjustsImageWhenHighlighted = NO;
    
    
    BOOL isTotal = [earningModel.imageName isEqualToString:@"pm_earning_bg"];
    
    UIColor * textColor = isTotal ? UIColor.whiteColor : COLOR_textColor;
    
    ///!!!:总收入
    UILabel *totalLab = [UILabel lz_labelWithText:earningModel.title fontSize:PingFangSC_M(15) color:textColor];
    [contentBtn addSubview:totalLab];
    [totalLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.mas_equalTo(20);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(21);
    }];
    
    UILabel *totalNumLab = [UILabel lz_labelWithText:[NSString stringWithFormat:@"￥ %@",earningModel.sumAmount] fontSize:PingFangSC_M(15) color:textColor];
    totalNumLab.tag = 100;
    [contentBtn addSubview:totalNumLab];
    [totalNumLab lz_rightAlignment];
    [totalNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.top.mas_equalTo(20);
        make.width.mas_equalTo(170);
        make.height.mas_equalTo(21);
    }];
    
    
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = isTotal ? UIColor.whiteColor : COLOR_cellLine;
    [contentBtn addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.mas_equalTo(15);
         make.top.mas_equalTo(54);
         make.right.mas_equalTo(-15);
        make.height.mas_equalTo(0.5);
     }];
    
    ///!!!: 平台利润
    UILabel *profitLab = [UILabel lz_labelWithText:@"平台利润:" fontSize:PingFangSC(15) color:textColor];
    [contentBtn addSubview:profitLab];
    [profitLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.mas_equalTo(lineView.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(21);
    }];
    
    
    UILabel *profitNumLab = [UILabel lz_labelWithText:[NSString stringWithFormat:@"￥ %@",earningModel.profits]  fontSize:PingFangSC_M(15) color:textColor];
    [contentBtn addSubview:profitNumLab];
    profitNumLab.tag = 101;
    [profitNumLab lz_rightAlignment];
    [profitNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.centerY.mas_equalTo(profitLab.mas_centerY);
        make.width.mas_equalTo(170);
        make.height.mas_equalTo(21);
    }];
    
    
    ///!!!: 成交笔数
    UILabel *turnoverNumLab = [UILabel lz_labelWithText:[NSString stringWithFormat:@"%ld\n成交笔数",(long)earningModel.total] fontSize:PingFangSC(13) color:textColor];
    [contentBtn addSubview:turnoverNumLab];
    turnoverNumLab.tag = 102;
    [turnoverNumLab lz_centerAlignment];
    [turnoverNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.mas_equalTo(profitNumLab.mas_bottom).mas_offset(25);
        make.width.mas_equalTo((kScreenWidth - 7 - 40 - 10)/2.0);
        make.height.mas_equalTo(40);
    }];
    
    
    ///!!!: 结算金额
    UILabel *amountNumLab = [UILabel lz_labelWithText:[NSString stringWithFormat:@"￥%@\n结算金额",earningModel.sumAmountSettle] fontSize:PingFangSC(13) color:textColor];
    amountNumLab.tag = 103;
    [contentBtn addSubview:amountNumLab];
   [amountNumLab lz_centerAlignment];
   [amountNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
       make.right.mas_equalTo(-20);
       make.top.mas_equalTo(turnoverNumLab);
       make.width.mas_equalTo((kScreenWidth - 7 - 40 - 10)/2.0);
       make.height.mas_equalTo(40);
   }];
    
    return contentBtn;
}


///刷新数据
- (void)reloadEarning:(UIView *)contentView model:(PMEarningModel *)earningModel{
    
    UILabel *totalNumLab = [contentView viewWithTag:100];
    NSString *sumAmount = [[NSString stringWithFormat:@"%@",earningModel.sumAmount] yw_stringByDividingBy:@"100"];
    totalNumLab.text = [NSString stringWithFormat:@"￥ %@",sumAmount];
    
    UILabel *profitNumLab = [contentView viewWithTag:101];
    NSString *profits = [[NSString stringWithFormat:@"%@",earningModel.profits] yw_stringByDividingBy:@"100"];
    profitNumLab.text = [NSString stringWithFormat:@"￥ %@",profits];
    
    UILabel *turnoverNumLab = [contentView viewWithTag:102];
    turnoverNumLab.text = [NSString stringWithFormat:@"%ld\n成交笔数",(long)earningModel.total];
    
    UILabel *amountNumLab = [contentView viewWithTag:103];
    NSString *sumAmountSettle = [[NSString stringWithFormat:@"%@",earningModel.sumAmountSettle] yw_stringByDividingBy:@"100"];
    amountNumLab.text = [NSString stringWithFormat:@"￥%@\n结算金额",sumAmountSettle];
}


@end
