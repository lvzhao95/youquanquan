//
//  LZToolCache.h
//  ZeroCloud
//
//  Created by 微微 on 2019/5/14.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LZUserDetailModel.h"





//获取用的Id
#define k_userId LZToolCache.locationUserId

//获取用户的token
#define k_userToken LZToolCache.locationUserToken


extern NSString * _Nonnull k_appVersion;
//本地语言
extern NSString * _Nonnull k_locationLanguge;

//本地用户的token
extern NSString * _Nonnull k_locationUserToken;

//本地用户的id
extern NSString * _Nonnull k_locationUserId;

NS_ASSUME_NONNULL_BEGIN

@interface LZToolCache : NSObject

#pragma mark - 本地存值
//存放字符串.
+ (void)setObject:(nullable id)value forKey:(NSString *_Nullable)defaultName;

//取值
+  (id _Nullable)objectForKey:(NSString *_Nonnull)defaultName;

//存放bool值
+ (void)setBool:(BOOL)value forKey:(NSString *_Nullable)defaultName;
//获取bool 值
+ (BOOL)boolForKey:(NSString *_Nullable)defaultName;

/* 清除本地的值
 * defaultName 对应的Key
 * 传 nil 删除全部
 */
+ (void)clearObjectKey:(NSString *_Nullable)defaultName;


#pragma mark - 保存用户信息
+ (void)saveUserInfo:(LZUserDetailModel * _Nonnull)model;

#pragma mark - 获取用户信息
+ (LZUserDetailModel * _Nonnull)getUserInfo;

//直接获取版本号
+ (NSString *_Nullable)appVersion;

//本地语言
+ (NSString *_Nullable)locationLanguge;

//本地用户id
+ (NSString *_Nullable)locationUserId;

//本地用户token
+ (NSString *_Nullable)locationUserToken;


@end

NS_ASSUME_NONNULL_END
