//
//  LZTool.h
//  ZeroCloud
//
//  Created by 微微 on 2019/5/15.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LZBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LZTool : NSObject

#pragma mark - 获取当前页面的ViewController
+ (LZBaseViewController *)currentViewController;


+ (NSString *)getCurrentDeviceModel;
@end

NS_ASSUME_NONNULL_END
