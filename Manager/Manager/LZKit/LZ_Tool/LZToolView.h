//
//  LZToolView.h
//  ZeroCloud
//
//  Created by 微微 on 2019/5/15.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import <UIKit/UIKit.h>


//alertViewtype
typedef NS_ENUM(NSInteger,LZAlertType){
    LZAlertTypeActionSheet,                 //Sheet
    LZAlertTypeTipAlter,                    //tipAlter
    LZAlertTypeReplacePerson,               //更换刷手
    LZAlertTypeAuditStatus,                 //审核状态
    LZAlertTypeMerchantAudit,                 //商家审核
    LZAlertTypeEditAmout,                      //编辑输入框
    LZAlertTypeAmountDeducted,                 //划扣金额
    LZAlertTypeModification,                    //修改
    LZAlertTypeModificaPassword,                 //修改密码
    LZAlertTypeScanLogin,                       //扫描登录

    LZAlertTypeAddLink,                       //添加链接
    LZAlertTypeAddress,                       //地址相关

    LZAlertTypeEmailLogin,                       //短信登录
    LZAlertTypeProductionAccount,                       //批量生产
    LZAlertTypeModificationAccount,                       //修改账户

    
    LZAlertTypeTradeScreen,                      //交易筛选
    LZAlertTypeMerchantScreen,                      //商户筛选

    
    

};

NS_ASSUME_NONNULL_BEGIN

@interface LZToolView : UIView

// 系统的alertView 回调
/**
 *  returns Alert.
 *  title NSString
 *  message NSString
 *  cancel NSString
 *  sure NSString
 *  dictionary @{};里面的参数自定义    key:subTitles||image||cancelTitle||sureTitle||pointString;
 *  cancelBlock & sureBlock 回调
 *  returns Alert.
 */
+ (void)showAlertType:(LZAlertType)alertType withTitle:(NSString *__nullable)title message:(NSString *__nullable)message cancel:(NSString *__nullable)cancel sure:(NSString *__nullable)sure objectDict:(NSDictionary *__nullable)dictionary cancelBlock:(void(^)(__nullable id object))cancelBlock sureBlock:(void(^)(__nullable id object))sureBlock;


#pragma mark - 取消
+ (void)dismiss;



/**
 [LZToolView showAlertType:LZAlertViewTypeOrderPay withTitle:LZLocalizedString(@"订单支付") message:LZLocalizedString(@"请输入资金密码") cancel:nil sure:LZLocalizedString(@"确认") objectDict:@{} cancelBlock:^(id  _Nullable object) {
              
          } sureBlock:^(id  _Nullable object) {
              
          }];
       [LZToolView showAlertType:LZAlertViewTypeActionSheet withTitle:@"" message:@"" cancel:@"" sure:@"" objectDict:@{@"subtitles":@[@"拍照",@"从相册选择",@"取消"]} cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {

       
       }];
         
         
 //        [LZToolView showAlertType:LZAlertViewTypeTipAlter withTitle:LZLocalizedString(@"确认执行当前操作吗？确认执行当前操作吗？确认执行当前操作吗？") message:nil cancel:LZLocalizedString(@"取消") sure:LZLocalizedString(@"确认") objectDict:nil cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
 //
 //
 //        }];
 
 */

@end

NS_ASSUME_NONNULL_END
