//
//  LZToolCache.m
//  ZeroCloud
//
//  Created by 微微 on 2019/5/14.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import "LZToolCache.h"


NSString * k_appVersion = @"k_appVersion";

//本地语言
NSString *k_locationLanguge = @"k_locationLanguge";

//本地用户的token
NSString *k_locationUserToken = @"k_locationUserToken";

//本地用户的id
NSString *k_locationUserId = @"k_locationUserId";

@implementation LZToolCache
///存放字符串.
+ (void)setObject:(nullable id)value forKey:(NSString *)defaultName
{
    if([value isKindOfClass:[NSNull class]]){
        value = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:defaultName];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

///取值
+ (id)objectForKey:(NSString *)defaultName
{
    return [[NSUserDefaults standardUserDefaults]objectForKey:defaultName];
}

///存放bool值
+ (void)setBool:(BOOL)value forKey:(NSString *)defaultName
{
    [[NSUserDefaults standardUserDefaults]setBool:value forKey:defaultName];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

///获取bool 值
+ (BOOL)boolForKey:(NSString *)defaultName
{
    return [[NSUserDefaults standardUserDefaults]boolForKey:defaultName];
}

+ (void)clearObjectKey:(NSString *)defaultName
{
    if (defaultName)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:defaultName];
        
    } else {
        ///删除全部 本地缓存的值
        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    }
}

#pragma mark - 保存用户信息
+ (void)saveUserInfo:(LZUserDetailModel* )model{
    if(model.token.length == 0){
        [LZToolCache clearObjectKey:k_locationUserToken];
    }
    BOOL isSave = [NSKeyedArchiver archiveRootObject:model toFile:LZUserInfoPlist];
    if(isSave){
        LZLog(@"保存用户信息成功");
        //保存一些常用用的信息
        [LZToolCache setObject:model.token forKey:k_locationUserToken];
    } else  {
        LZLog(@"保存用户信息失败");
    }
}
#pragma mark - 获取用户信息
+ (LZUserDetailModel* )getUserInfo{
    LZUserDetailModel* model = [NSKeyedUnarchiver unarchiveObjectWithFile:LZUserInfoPlist];
    if(!model) model = [LZUserDetailModel new];
    return model;
}


#pragma mark - 直接获取值
//获取版本号
+ (NSString *)appVersion{
    return [LZToolCache objectForKey:k_appVersion]?:@"1.0.0";
}


//获取本地语言
+ (NSString *)locationLanguge{
    return [LZToolCache objectForKey:k_locationLanguge]?:@"";
}


//本地用户id
+ (NSString *_Nullable)locationUserId{
    
    NSString *userId = [LZToolCache objectForKey:k_locationUserId];
    userId = [self lz_checkNULLString:userId];
    return userId ;
}

//本地用户token
+ (NSString *_Nullable)locationUserToken{
    NSString *userToken = [LZToolCache objectForKey:k_locationUserToken];
    userToken = [self lz_checkNULLString:userToken];
    return userToken;
}
@end
