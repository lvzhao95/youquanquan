//
//  LZToolView.m
//  ZeroCloud
//
//  Created by 微微 on 2019/5/15.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import "LZToolView.h"
#import "LZOptionView.h"

@interface LZToolView ()<UIGestureRecognizerDelegate>

//背景
@property (nonatomic,strong)UIView *backgroundView; //
@property (nonatomic,copy) void(^cancelBlock) (id object);
@property (nonatomic,copy) void(^sureBlock)(id object);
@property (nonatomic,strong) UIButton *sureBtn;
@property (nonatomic,strong) UIButton *cancelBtn;

@property (nonatomic,strong) NSDictionary *dictionary;
@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *message;
@property (nonatomic,copy)NSString *cancel;
@property (nonatomic,copy)NSString *sure;







@end


@implementation LZToolView
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    LZLog(@"LZToolView");
    
}


+ (void)showAlertType:(LZAlertType)alertType withTitle:(NSString *__nullable)title message:(NSString *__nullable)message cancel:(NSString *__nullable)cancel sure:(NSString *__nullable)sure objectDict:(NSDictionary *__nullable)dictionary cancelBlock:(void(^)(id object))cancelBlock sureBlock:(void(^)(id object))sureBlock{
             
    UIWindow * window = [UIApplication sharedApplication].keyWindow;
    for (UIView * aView in window.subviews) {
          if ([aView isKindOfClass:[LZToolView class]]){
              return;
          }
      }
      LZToolView *alertView = [[LZToolView alloc]initWithFrame:CGRectMake(0, 0,K_SCREENWIDTH , K_SCREENHEIGHT) alertType:alertType withTitle:title message:message cancel:cancel sure:sure objectDict:dictionary cancelBlock:cancelBlock sureBlock:sureBlock];
      [window addSubview:alertView];
      return;
}

+ (void)dismiss{
    UIWindow * window = [UIApplication sharedApplication].keyWindow;
    for (UIView * aView in window.subviews) {
        if ([aView isKindOfClass:[LZToolView class]]){
            [aView removeFromSuperview];
        }
    }
}


#pragma mark - 取消
- (void)dismissAnimate:(BOOL)animate{
    if(animate){
        [UIView animateWithDuration:0.3 animations:^{
            self.backgroundView.frame = CGRectMake(self.backgroundView.x, K_SCREENHEIGHT, self.backgroundView.width, self.backgroundView.height);
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    } else {
        [UIView animateWithDuration:0.2 animations:^{
            self.hidden = YES;
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }

}

//手势
- (void)gestureRecognizerDismiss{
    [self dismissAnimate:YES];
}

- (void)dismiss{
    [self dismissAnimate:NO];
}

#pragma mark - 增加键盘的通知
- (void)addKeyboardWillNotification
{
    @weakify(self);
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillShowNotification object:nil] subscribeNext:^(NSNotification * _Nullable x) {
        @strongify(self);
        NSDictionary *userInfo = x.userInfo;
        NSValue *value = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
        CGRect keyboardRect = [value CGRectValue];
        
        [UIView animateWithDuration:0.4 animations:^{
            self.backgroundView.y = K_SCREENHEIGHT - keyboardRect.size.height -  self.backgroundView.height - 10;
        } completion:^(BOOL finished) {
            
        }];
    }];
    
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillHideNotification object:nil] subscribeNext:^(NSNotification * _Nullable x) {
        @strongify(self);
        self.backgroundView.y = (K_SCREENHEIGHT -  self.backgroundView.height )/2.0;
    }];
}

#pragma mark - 修改取消, 确认按钮的样式
- (void)refreshUI{
    ///!!!:确定
    [self.sureBtn setTitleColor:COLOR_appColor forState:UIControlStateNormal];
    [self.sureBtn setTitle:self.sure forState:UIControlStateNormal];
    [self.sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.width.mas_equalTo(280/2.0);
        make.height.mas_equalTo(50);
    }];
           
    
    
    self.cancelBtn.titleLabel.font = PingFangSC(17);
    [self.cancelBtn setTitleColor:UIColorHex(0x666666) forState:UIControlStateNormal];
    [self.cancelBtn setTitle:self.cancel forState:UIControlStateNormal];
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.width.mas_equalTo(self.sureBtn);
        make.height.mas_equalTo(50);
    }];

    
    UIView *lineView = [[UIView alloc] init];
      lineView.backgroundColor = COLOR_cellLine;
      [self.backgroundView addSubview:lineView];
      [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
          make.bottom.mas_equalTo(-50);
          make.left.mas_equalTo(0);
          make.right.mas_equalTo(0);
          make.height.mas_equalTo(0.5);
      }];
        
        
        
      UIView *lineView1 = [[UIView alloc] init];
      lineView1.backgroundColor = COLOR_cellLine;
      [self.backgroundView addSubview:lineView1];
      [lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
          make.bottom.mas_equalTo(0);
          make.centerX.mas_equalTo(self.backgroundView.mas_centerX);
          make.width.mas_equalTo(1);
          make.height.mas_equalTo(50);
      }];
    
}



///block 回调 AlertView
- (id)initWithFrame:(CGRect)frame alertType:(LZAlertType)alertType withTitle:(NSString *)title message:(NSString *)message  cancel:(NSString *)cancel sure:(NSString *)sure objectDict:(NSDictionary *)dictionary cancelBlock:(void(^)(id object))cancelBlock sureBlock:(void(^)(id object))sureBlock{
    
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColorHex(0x000000) colorWithAlphaComponent:0.5];
        self.cancelBlock = cancelBlock;
        self.sureBlock = sureBlock;
        self.dictionary = dictionary;
        self.title = title;
        self.message = message;
        self.cancel = cancel;
        self.sure = sure;
        [self setupView];
        
        switch (alertType) {
            case LZAlertTypeActionSheet:
                [self initSubActionSheetView];
                break;
                
            case LZAlertTypeTipAlter:
                [self initSubTipAlterView];
                break;
                
            case LZAlertTypeReplacePerson:
                [self initReplacePersonView];
                break;

            case LZAlertTypeAuditStatus:
                [self initAuditStatusView];
                break;

            case LZAlertTypeMerchantAudit:
                [self initMerchantAuditView];
                break;
                
            case LZAlertTypeEditAmout:
                [self initEditAmoutView];
                break;
                
            case LZAlertTypeAmountDeducted:
                [self initAmountDeductedView];
                break;
                
            case LZAlertTypeModification:
                [self initModificationView];
                break;
                
            case LZAlertTypeModificaPassword:
                [self initModificaPasswordView];
                break;
                
            case LZAlertTypeScanLogin:
                [self initScanLoginView];
                break;
                
            case LZAlertTypeAddLink:
                [self initAddLinkView];
                break;
                
            case LZAlertTypeAddress:
                [self initAddressView];
                break;
             

            case LZAlertTypeEmailLogin:
                [self initEmailLoginView];
                break;
                
                
            case LZAlertTypeProductionAccount:
                [self initProductionAccountView];
                break;
                
            case LZAlertTypeModificationAccount:
                [self initModificationAccountView];
                break;
                
                
                
            default:
                
                break;
        }
        
    }
    return self;
}
#pragma mark - setupView
- (void) setupView{
    self.backgroundView = [[UIView alloc]init];
    self.backgroundView.backgroundColor = COLOR_nav;
    self.backgroundView.frame = CGRectZero;
    [self addSubview:self.backgroundView];
    self.backgroundView.layer.cornerRadius = 13;
    self.backgroundView.clipsToBounds = YES;
    
    //确定
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.titleLabel.font = PingFangSC_M(17);
    [sureBtn setTitle:self.sure forState:UIControlStateNormal];
    [sureBtn setTitleColor:COLOR_nav forState:UIControlStateNormal];
    self.sureBtn = sureBtn;
    [self.backgroundView addSubview:sureBtn];
    
    //取消
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.titleLabel.font = PingFangSC(17);
    [cancelBtn setTitle:self.cancel forState:UIControlStateNormal];
    [cancelBtn setTitleColor:COLOR_nav forState:UIControlStateNormal];
    self.cancelBtn = cancelBtn;
    [self.backgroundView addSubview:cancelBtn];
    
}
#pragma mark - LZAlertTypeModificationAccount
- (void)initModificationAccountView{
    
    [self.backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.width.mas_offset(280);
        make.height.mas_equalTo(330);
        make.centerY.mas_equalTo(self.mas_centerY);
    }];
            
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = PingFangSC_M(18);
    titleLabel.text = self.title;
    titleLabel.numberOfLines = 0;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = COLOR_textColor;
    CGFloat titleHeight = [titleLabel.text lz_textHeightWithFontSize:titleLabel.font withMaxWidth:250];
    [self.backgroundView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(titleHeight);
    }];
    
    
    NSArray *titles = @[@"账号：",@"名称："];
    NSArray *placeholders = @[@"请输入账号",@"请输入名称"];
      
    for (int i= 0; i < 2; i++) {

        UILabel *detailLab = [UILabel lz_labelWithText:titles[i] fontSize:PingFangSC(15) color:COLOR_textColor];
        [self.backgroundView addSubview:detailLab];
        [detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(65 + i * 50);
            make.left.mas_equalTo(15);
            make.width.mas_equalTo(45);
            make.height.mas_equalTo(21);
        }];
        
        UITextField *textField = [[UITextField alloc] init];
        textField.font = PingFangSC(15);
        textField.keyboardType = UIKeyboardTypeDecimalPad;
        textField.placeholder = placeholders[i];
        textField.cornerRadius = 6;
        textField.bWidth = 0.5;
        textField.bColor = COLOR_bColor;
        {
            UIView *leftView = [UIView new];
            leftView.frame = CGRectMake(0, 0, 10, 35);
            textField.leftView = leftView;
        }

        //设置显示模式为永远显示(默认不显示)
        textField.leftViewMode = UITextFieldViewModeAlways;
        [self.backgroundView addSubview:textField];
        [textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(detailLab.mas_centerY);
            make.left.mas_equalTo(detailLab.mas_right).mas_offset(3);
            make.right.mas_equalTo(-15);
            make.height.mas_equalTo(34);
        }];
    }
      
    ///!!!:refer_page_id
    UILabel *referIdLab = [UILabel lz_labelWithText:@"refer_page_id:" fontSize:PingFangSC(15) color:COLOR_textColor];
    [self.backgroundView addSubview:referIdLab];
    [referIdLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(155);
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(265);
        make.height.mas_equalTo(21);
    }];
        
        
    YYTextView *referIdTextView = [[YYTextView alloc] init];
    referIdTextView.placeholderText = @"请输入refer_page_id";
    referIdTextView.placeholderFont = PingFangSC(15);
    referIdTextView.font = PingFangSC(15);
    referIdTextView.placeholderTextColor = COLOR_placeholderColor;
    referIdTextView.cornerRadius = 6;
    referIdTextView.bWidth = 0.5;
    referIdTextView.bColor = COLOR_bColor;
    [self.backgroundView addSubview:referIdTextView];
    [referIdTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(referIdLab.mas_bottom).mas_offset(3);
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(280 - 30);
        make.height.mas_equalTo(70);
    }];
    
    [self refreshUI];
    
    [self addKeyboardWillNotification];
       
       @weakify(self);
       [[self.sureBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
       @strongify(self);
           if(self.sureBlock){
               self.sureBlock(@"来我看");
           }
           [self dismissAnimate:NO];
       }];

       [[self.cancelBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
       @strongify(self);
           [self dismissAnimate:NO];
       }];


       UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismiss)];
       tap.delegate = self;
       [self addGestureRecognizer:tap];
    
    
    
    
    
    
}

#pragma mark LZAlertTypeProductionAccount
- (void)initProductionAccountView{
    

    [self.backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.width.mas_offset(280);
        make.height.mas_equalTo(280);
        make.centerY.mas_equalTo(self.mas_centerY);
    }];
            
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = PingFangSC_M(18);
    titleLabel.text = self.title;
    titleLabel.numberOfLines = 0;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = COLOR_textColor;
    CGFloat titleHeight = [titleLabel.text lz_textHeightWithFontSize:titleLabel.font withMaxWidth:250];
    [self.backgroundView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(titleHeight);
    }];

    
    ///!!!:数量
    UILabel *numLab = [UILabel lz_labelWithText:@"生产数量:" fontSize:PingFangSC(15) color:COLOR_textColor];
    [self.backgroundView addSubview:numLab];
    [numLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(75);
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(88);
        make.height.mas_equalTo(21);
    }];
    
    UITextField *numtTextField = [[UITextField alloc] init];
    numtTextField.font = PingFangSC(15);
    numtTextField.keyboardType = UIKeyboardTypeDecimalPad;
    numtTextField.placeholder = @"数量";
    numtTextField.cornerRadius = 6;
    numtTextField.bWidth = 0.5;
    numtTextField.bColor = COLOR_cellLine;
    {
        UIView *leftView = [UIView new];
        leftView.frame = CGRectMake(0, 0, 10, 35);
        numtTextField.leftView = leftView;
    }

    //设置显示模式为永远显示(默认不显示)
    numtTextField.leftViewMode = UITextFieldViewModeAlways;
    [self.backgroundView addSubview:numtTextField];
    [numtTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(numLab.mas_centerY);
        make.left.mas_equalTo(88);
        make.right.mas_equalTo(-60);
        make.height.mas_equalTo(34);
    }];

    
    UILabel *unitLab = [UILabel lz_labelWithText:@"个" fontSize:PingFangSC(15) color:COLOR_textColor];
    [self.backgroundView addSubview:unitLab];
    [unitLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(numLab.mas_centerY);
        make.left.mas_equalTo(numtTextField.mas_right).mas_offset(8);
        make.width.mas_equalTo(20);
        make.height.mas_equalTo(21);
    }];
    
    
    NSArray *titles = @[@"目标省份:",@"目标城市:"];
    NSArray *subTitles = self.dictionary[@"subTitles"];
    NSArray *placeholders = @[@"请选择省份",@"请选择城市"];

    for (int i= 0; i < 2; i++) {

        LZOptionView *optionView = [[LZOptionView alloc] initWithFrame:CGRectMake( 0, 0, 175, 34) dataSource:subTitles[i]];
        optionView.tag = 10 + i;
        optionView.title = placeholders[i];
        [self.backgroundView addSubview:optionView];
        [optionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(120 + i * 50);
            make.left.mas_equalTo(88);
            make.right.mas_equalTo(-15);
            make.height.mas_equalTo(34);
        }];
//        [optionView setSelectedBlock:^(LZOptionView * _Nonnull optionView, NSInteger selectedIndex) {
//
//
//        }];

        UILabel *detailLab = [UILabel lz_labelWithText:titles[i] fontSize:PingFangSC(15) color:COLOR_textColor];
        [self.backgroundView addSubview:detailLab];
        [detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(optionView.mas_centerY);
            make.left.mas_equalTo(15);
            make.width.mas_equalTo(75);
            make.height.mas_equalTo(21);
        }];
    }
        
    [self refreshUI];
    
    [self addKeyboardWillNotification];
       
    @weakify(self);
    [[self.sureBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);
        if(self.sureBlock){
            self.sureBlock(@"来我看");
        }
        [self dismissAnimate:NO];
    }];

    [[self.cancelBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);
        [self dismissAnimate:NO];
    }];
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismiss)];
    tap.delegate = self;
    [self addGestureRecognizer:tap];
}


#pragma mark -    LZAlertTypeEmailLogin:
- (void)initEmailLoginView{

    [self.backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
         make.centerX.mas_equalTo(self.mas_centerX);
         make.width.mas_offset(280);
         make.height.mas_equalTo(230);
         make.centerY.mas_equalTo(self.mas_centerY);
     }];
        
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = PingFangSC_M(18);
    titleLabel.text = self.title;
    titleLabel.numberOfLines = 0;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = COLOR_textColor;
    CGFloat titleHeight = [titleLabel.text lz_textHeightWithFontSize:titleLabel.font withMaxWidth:250];
    [self.backgroundView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(titleHeight);
    }];

    NSArray *titles = @[@"手机号",@"验证码"];
    NSArray *placeholders = @[@"请输入手机号",@"请输入验证码"];
      
    for (int i= 0; i < 2; i++) {

        UILabel *detailLab = [UILabel lz_labelWithText:titles[i] fontSize:PingFangSC(15) color:COLOR_textColor];
        [self.backgroundView addSubview:detailLab];
        [detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(65 + i * 50);
            make.left.mas_equalTo(15);
            make.width.mas_equalTo(55);
            make.height.mas_equalTo(21);
        }];
        
        UITextField *textField = [[UITextField alloc] init];
        textField.font = PingFangSC(15);
        textField.keyboardType = UIKeyboardTypeDecimalPad;
        textField.placeholder = placeholders[i];
        textField.cornerRadius = 6;
        textField.bWidth = 0.5;
        textField.bColor = COLOR_cellLine;
        {
            UIView *leftView = [UIView new];
            leftView.frame = CGRectMake(0, 0, 10, 35);
            textField.leftView = leftView;
        }

        //设置显示模式为永远显示(默认不显示)
        textField.leftViewMode = UITextFieldViewModeAlways;
        [self.backgroundView addSubview:textField];
        [textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(detailLab.mas_centerY);
            make.left.mas_equalTo(detailLab.mas_right).mas_offset(3);
            if(i== 0){
                make.right.mas_equalTo(-100);
            } else {
                make.right.mas_equalTo(-15);
            }
            make.height.mas_equalTo(34);
        }];

        
        if(i == 0){
            UIButton *codeBtn = [UIButton lz_buttonTitle:@"获取验证码" titleColor:COLOR_appColor fontSize:15];
            codeBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
            [self.backgroundView addSubview:codeBtn];
            [codeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(detailLab.mas_centerY);
                make.left.mas_equalTo(textField.mas_right).mas_offset(3);
                make.width.mas_equalTo(80);
                make.height.mas_equalTo(34);
            }];
        }
    }
      
    [self refreshUI];
    
    [self addKeyboardWillNotification];
       
       @weakify(self);
       [[self.sureBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
       @strongify(self);
           if(self.sureBlock){
               self.sureBlock(@"来我看");
           }
           [self dismissAnimate:NO];
       }];

       [[self.cancelBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
       @strongify(self);
           [self dismissAnimate:NO];
       }];


       UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismiss)];
       tap.delegate = self;
       [self addGestureRecognizer:tap];
    
}
#pragma mark -   LZAlertTypeAddress:
- (void)initAddressView{
    [self.backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
          make.centerX.mas_equalTo(self.mas_centerX);
          make.width.mas_offset(280);
          make.height.mas_equalTo(230);
          make.centerY.mas_equalTo(self.mas_centerY);
      }];
         
     UILabel *titleLabel = [[UILabel alloc] init];
     titleLabel.font = PingFangSC_M(18);
     titleLabel.text = self.title;
     titleLabel.numberOfLines = 0;
     titleLabel.textAlignment = NSTextAlignmentCenter;
     titleLabel.textColor = COLOR_textColor;
     CGFloat titleHeight = [titleLabel.text lz_textHeightWithFontSize:titleLabel.font withMaxWidth:250];
     [self.backgroundView addSubview:titleLabel];
     [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
         make.top.mas_equalTo(30);
         make.left.mas_equalTo(10);
         make.right.mas_equalTo(-10);
         make.height.mas_equalTo(titleHeight);
     }];

    
    NSArray *titles = @[@"目标省份",@"目标城市"];
    NSArray *subTitles = self.dictionary[@"subTitles"];
    NSArray *placeholders = @[@"请选择省份",@"请选择城市"];
    
    for (int i= 0; i < 2; i++) {
        
        LZOptionView *optionView = [[LZOptionView alloc] initWithFrame:CGRectMake( 0, 0, 175, 34) dataSource:subTitles[i]];
        optionView.tag = 10 + i;
        optionView.title = placeholders[i];
        [self.backgroundView addSubview:optionView];
        [optionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(65 + i * 50);
            make.left.mas_equalTo(88);
            make.right.mas_equalTo(-15);
            make.height.mas_equalTo(34);
        }];
//        [optionView setSelectedBlock:^(LZOptionView * _Nonnull optionView, NSInteger selectedIndex) {
//
//
//        }];
        
        UILabel *detailLab = [UILabel lz_labelWithText:titles[i] fontSize:PingFangSC(15) color:COLOR_textColor];
        [self.backgroundView addSubview:detailLab];
        [detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(optionView.mas_centerY);
            make.left.mas_equalTo(15);
            make.width.mas_equalTo(75);
            make.height.mas_equalTo(21);
        }];
    }
    
    [self refreshUI];
   
     
     @weakify(self);
     [[self.sureBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
     @strongify(self);
         if(self.sureBlock){
             self.sureBlock(@"来我看");
         }
         [self dismissAnimate:NO];
     }];

     [[self.cancelBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
     @strongify(self);
         [self dismissAnimate:NO];
     }];


     UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismiss)];
     tap.delegate = self;
     [self addGestureRecognizer:tap];
    
    
}



#pragma mark -   LZAlertTypeAddLink:
- (void)initAddLinkView{
    
    [self.backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.width.mas_offset(280);
        make.height.mas_equalTo(345);
        make.centerY.mas_equalTo(self.mas_centerY);
    }];
       
   UILabel *titleLabel = [[UILabel alloc] init];
   titleLabel.font = PingFangSC_M(18);
   titleLabel.text = self.title;
   titleLabel.numberOfLines = 0;
   titleLabel.textAlignment = NSTextAlignmentCenter;
   titleLabel.textColor = COLOR_textColor;
   CGFloat titleHeight = [titleLabel.text lz_textHeightWithFontSize:titleLabel.font withMaxWidth:250];
   [self.backgroundView addSubview:titleLabel];
   [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       make.top.mas_equalTo(30);
       make.left.mas_equalTo(10);
       make.right.mas_equalTo(-10);
       make.height.mas_equalTo(titleHeight);
   }];


    ///!!!:商品ID
    NSString *pID = @"148857786078";//self.dictionary[@"productID"];
    NSString *name = @"手提式家居清洁用蒸汽清洗机 电加热型蒸汽清洁机 酒店地毯清洗机";//self.dictionary[@"productName"];


    UILabel *productIDLab = [UILabel lz_labelWithText:@"商品ID:" fontSize:PingFangSC(15) color:COLOR_textColor];
    [productIDLab lz_rightAlignment];
    [self.backgroundView addSubview:productIDLab];
    [productIDLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(70);
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(65);
        make.height.mas_equalTo(21);
     }];
    
    UILabel *pIDLab = [UILabel lz_labelWithText:pID fontSize:PingFangSC(15) color:COLOR_textColor];
    [self.backgroundView addSubview:pIDLab];
    [pIDLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(productIDLab.mas_centerY);
        make.left.mas_equalTo(productIDLab.mas_right);
        make.width.mas_equalTo(150);
        make.height.mas_equalTo(21);
     }];
    
    
    
    ///!!!:商品名称
    UILabel *produceNameLab = [UILabel lz_labelWithText:@"商品名称:" fontSize:PingFangSC(15) color:COLOR_textColor];
    [produceNameLab lz_rightAlignment];
    [self.backgroundView addSubview:produceNameLab];
    [produceNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(productIDLab.mas_bottom).mas_offset(15);
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(65);
        make.height.mas_equalTo(21);
    }];
    
    UILabel *nameLab = [UILabel lz_labelWithText:name fontSize:PingFangSC(15) color:COLOR_textColor];
    [self.backgroundView addSubview:nameLab];
    CGFloat nameHeight = [nameLab.text lz_textHeightWithFontSize:nameLab.font withMaxWidth:280 - 30 - 65];
    [nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(produceNameLab);
        make.width.mas_equalTo(280 - 30 - 65);
        make.left.mas_equalTo(produceNameLab.mas_right);
        make.height.mas_equalTo(nameHeight);
    }];
    
    
  
       ///!!!:推广链接
    UILabel *promoteLinkLab = [UILabel lz_labelWithText:@"推广链接:" fontSize:PingFangSC(15) color:COLOR_textColor];
    [self.backgroundView addSubview:promoteLinkLab];
    [promoteLinkLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(nameLab.mas_bottom).mas_offset(15);
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(65);
        make.height.mas_equalTo(21);
    }];
       
       
   YYTextView *linkTextView = [[YYTextView alloc] init];
   linkTextView.placeholderText = @"请输入推广链接";
   linkTextView.placeholderFont = PingFangSC(15);
   linkTextView.font = PingFangSC(15);
   linkTextView.placeholderTextColor = COLOR_placeholderColor;
   linkTextView.cornerRadius = 6;
   linkTextView.bWidth = 0.5;
   linkTextView.bColor = COLOR_cellLine;
   [self.backgroundView addSubview:linkTextView];
   [linkTextView mas_makeConstraints:^(MASConstraintMaker *make) {
       make.top.mas_equalTo(promoteLinkLab);
       make.left.mas_equalTo(promoteLinkLab.mas_right);
       make.width.mas_equalTo(280 - 30 - 65);
       make.height.mas_equalTo(78);
   }];

   [self refreshUI];
   
   [self addKeyboardWillNotification];
     
     @weakify(self);
     [[self.sureBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
     @strongify(self);
         if(self.sureBlock){
             self.sureBlock(@"来我看");
         }
         [self dismissAnimate:NO];
     }];

     [[self.cancelBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
     @strongify(self);
         [self dismissAnimate:NO];
     }];


     UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismiss)];
     tap.delegate = self;
     [self addGestureRecognizer:tap];
     
    
    
}

#pragma mark -  LZAlertTypeScanLogin
- (void)initScanLoginView{
    
    [self.backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.width.mas_offset(280);
        make.height.mas_equalTo(445);
        make.centerY.mas_equalTo(self.mas_centerY);
    }];

    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = PingFangSC_M(18);
    titleLabel.text = self.title;
    titleLabel.numberOfLines = 0;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = COLOR_textColor;
    CGFloat titleHeight = [titleLabel.text lz_textHeightWithFontSize:titleLabel.font withMaxWidth:250];
    [self.backgroundView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(titleHeight);
    }];

    UILabel *messageLab = [[UILabel alloc] init];
    messageLab.font = PingFangSC(15);
    messageLab.text = self.message;
    messageLab.numberOfLines = 0;
    messageLab.textColor = COLOR_textColor;
    [self.backgroundView addSubview:messageLab];
    [messageLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(53);
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(250);
        make.height.mas_equalTo(21);
    }];
    
    
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.backgroundColor = UIColor.lz_randomColor;
    [self.backgroundView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(240);
        make.width.mas_equalTo(240);
        make.centerX.mas_equalTo(self.backgroundView.mas_centerX);
        make.top.mas_equalTo(messageLab.mas_bottom).mas_offset(20);
    }];
    
    
    ///!!!:持有人
    UILabel *holderLab = [UILabel lz_labelWithText:@"持有人" fontSize:PingFangSC(15) color:COLOR_textColor];
    [self.backgroundView addSubview:holderLab];
    [holderLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(21);
        make.width.mas_equalTo(55);
        make.top.mas_equalTo(imageView.mas_bottom).mas_offset(20);
        make.left.mas_equalTo(15);
    }];
    
    
    UITextField *holderTextField = [[UITextField alloc] init];
    holderTextField.font = PingFangSC(15);
    holderTextField.keyboardType = UIKeyboardTypeDecimalPad;
    holderTextField.placeholder = @"请输入持有人";
    holderTextField.cornerRadius = 6;
    holderTextField.bWidth = 0.5;
    holderTextField.bColor = COLOR_cellLine;
    {
        UIView *leftView = [UIView new];
        leftView.frame = CGRectMake(0, 0, 10, 35);
        holderTextField.leftView = leftView;
    }

    //设置显示模式为永远显示(默认不显示)
    holderTextField.leftViewMode = UITextFieldViewModeAlways;
    [self.backgroundView addSubview:holderTextField];
    [holderTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(holderLab.mas_centerY);
        make.left.mas_equalTo(holderLab.mas_right).mas_offset(3);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(34);
    }];
    
    
    
    [self refreshUI];


    [self addKeyboardWillNotification];

    @weakify(self);
    [[self.sureBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
    @strongify(self);
        if(self.sureBlock){
            self.sureBlock(@"");
        }
        [self dismissAnimate:NO];
    }];

    [[self.cancelBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);
        [self dismissAnimate:NO];
    }];


    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismiss)];
    tap.delegate = self;
    [self addGestureRecognizer:tap];
    
}

#pragma mark -  LZAlertTypeModificaPassword
- (void)initModificaPasswordView{
    
    [self.backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.width.mas_offset(280);
        make.height.mas_equalTo(228);
        make.centerY.mas_equalTo(self.mas_centerY);
    }];

    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = PingFangSC_M(18);
    titleLabel.text = self.title;
    titleLabel.numberOfLines = 0;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = COLOR_textColor;
    CGFloat titleHeight = [titleLabel.text lz_textHeightWithFontSize:titleLabel.font withMaxWidth:250];
    [self.backgroundView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(30);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(titleHeight);
    }];
    
    
    ///!!!:原密码
    UILabel *oldPasLab = [UILabel lz_labelWithText:@"原密码：" fontSize:PingFangSC(15) color:COLOR_textColor];
    [self.backgroundView addSubview:oldPasLab];
    [oldPasLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(65);
        make.height.mas_equalTo(21);
        make.top.mas_equalTo(73);
    }];

    
    UITextField *oldTextField = [[UITextField alloc] init];
    oldTextField.font = PingFangSC(15);
    oldTextField.keyboardType = UIKeyboardTypeDecimalPad;
    oldTextField.placeholder = @"请输入原密码";
    oldTextField.cornerRadius = 6;
    oldTextField.bWidth = 0.5;
    oldTextField.bColor = COLOR_cellLine;
    {
        UIView *leftView = [UIView new];
        leftView.frame = CGRectMake(0, 0, 10, 35);
        oldTextField.leftView = leftView;
    }

    //设置显示模式为永远显示(默认不显示)
    oldTextField.leftViewMode = UITextFieldViewModeAlways;
    [self.backgroundView addSubview:oldTextField];
    [oldTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(oldPasLab.mas_centerY);
        make.left.mas_equalTo(oldPasLab.mas_right).mas_offset(3);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(34);
    }];
    
    
    ///!!!:新密码
    UILabel *newPasLab = [UILabel lz_labelWithText:@"新密码：" fontSize:PingFangSC(15) color:COLOR_textColor];
    [self.backgroundView addSubview:newPasLab];
    [newPasLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(65);
        make.height.mas_equalTo(21);
        make.top.mas_equalTo(oldPasLab.mas_bottom).mas_offset(28);
    }];

    
    UITextField *newTextField = [[UITextField alloc] init];
    newTextField.font = PingFangSC(15);
    newTextField.keyboardType = UIKeyboardTypeDecimalPad;
    newTextField.placeholder = @"请输入新密码";
    newTextField.cornerRadius = 6;
    newTextField.bWidth = 0.5;
    newTextField.bColor = COLOR_cellLine;
    {
        UIView *leftView = [UIView new];
        leftView.frame = CGRectMake(0, 0, 10, 35);
        newTextField.leftView = leftView;
    }
    //设置显示模式为永远显示(默认不显示)
    newTextField.leftViewMode = UITextFieldViewModeAlways;
    [self.backgroundView addSubview:newTextField];
    [newTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(newPasLab.mas_centerY);
        make.left.mas_equalTo(newPasLab.mas_right).mas_offset(3);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(34);
    }];
    
    [self refreshUI];
     
     
     [self addKeyboardWillNotification];
        
    @weakify(self);
    [[self.sureBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
    @strongify(self);
        if(self.sureBlock){
            self.sureBlock(@"");
        }
        [self dismissAnimate:NO];
    }];

    [[self.cancelBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
    @strongify(self);
        [self dismissAnimate:NO];
    }];


    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismiss)];
    tap.delegate = self;
    [self addGestureRecognizer:tap];
    
    
}
#pragma mark -  LZAlertTypeAmountDeducted:
- (void)initModificationView{

    NSArray *subTitles = self.dictionary[@"subTitles"];
    NSArray *keys = self.dictionary[@"keys"];
    NSArray *contents = self.dictionary[@"contents"];

    NSInteger height = subTitles.count * 50 + 180;
    
    
    [self.backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.width.mas_offset(280);
        make.height.mas_equalTo(height);
        make.centerY.mas_equalTo(self.mas_centerY);
    }];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = PingFangSC_M(18);
    titleLabel.text = self.title;
    titleLabel.numberOfLines = 0;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = COLOR_textColor;
    CGFloat titleHeight = [titleLabel.text lz_textHeightWithFontSize:titleLabel.font withMaxWidth:250];
    [self.backgroundView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(30);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(titleHeight);
    }];

    UILabel *messageLab = [[UILabel alloc] init];
    messageLab.font = PingFangSC(15);
    messageLab.text = self.message;
    messageLab.numberOfLines = 0;
    messageLab.textColor = COLOR_textColor;
    [self.backgroundView addSubview:messageLab];
    [messageLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(70);
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(21);
    }];
    
    NSMutableDictionary *dataDictionary = [[NSMutableDictionary alloc] init];
    
    for (int i = 0; i < subTitles.count; i++){
        UIView *contentView = [[UIView alloc] init];
        [self.backgroundView addSubview:contentView];
        [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(15);
            make.width.mas_equalTo(250);
            make.height.mas_equalTo(50);
            make.top.mas_equalTo(100 + i * 50);
        }];
        
        
        UILabel *titleLab = [UILabel lz_labelWithText:subTitles[i] fontSize:PingFangSC(15) color:COLOR_textColor];
        [contentView addSubview:titleLab];
        [titleLab lz_rightAlignment];
        [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.width.mas_equalTo(70);
            make.height.mas_equalTo(21);
            make.bottom.mas_equalTo(-6);
        }];

        UITextField *numberTextField = [[UITextField alloc] init];
        numberTextField.text = contents[i];
        numberTextField.font = PingFangSC(15);
        numberTextField.keyboardType = UIKeyboardTypeDecimalPad;
        numberTextField.cornerRadius = 6;
        numberTextField.bWidth = 0.5;
        numberTextField.bColor = COLOR_cellLine;
        UIView *leftView = [UIView new];
        leftView.frame = CGRectMake(0, 0, 10, 35);
        numberTextField.leftView = leftView;
        //设置显示模式为永远显示(默认不显示)
        numberTextField.leftViewMode = UITextFieldViewModeAlways;
        [contentView addSubview:numberTextField];
        [numberTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(titleLab.mas_centerY);
            make.left.mas_equalTo(titleLab.mas_right).mas_offset(3);
            make.right.mas_equalTo(0);
            make.height.mas_equalTo(34);
        }];
        
        [[numberTextField rac_textSignal] subscribeNext:^(NSString * _Nullable x) {
            
            dataDictionary[keys[i]] = x;
        }];
        
    }
    
    
    [self refreshUI];
    
    
    [self addKeyboardWillNotification];
       
   @weakify(self);
   [[self.sureBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
   @strongify(self);
       if(self.sureBlock){
           self.sureBlock(dataDictionary);
       }
       [self dismissAnimate:NO];
   }];

   [[self.cancelBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
   @strongify(self);
       [self dismissAnimate:NO];
   }];


   UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismiss)];
   tap.delegate = self;
   [self addGestureRecognizer:tap];

    
    
    
}

#pragma mark - LZAlertTypeAmountDeducted
- (void)initAmountDeductedView{
    
    [self.backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.width.mas_offset(280);
        make.height.mas_equalTo(228);
        make.centerY.mas_equalTo(self.mas_centerY);
    }];

    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = PingFangSC_M(18);
    titleLabel.text = self.title;
    titleLabel.numberOfLines = 0;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = COLOR_textColor;
    CGFloat titleHeight = [titleLabel.text lz_textHeightWithFontSize:titleLabel.font withMaxWidth:250];
    [self.backgroundView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(30);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(titleHeight);
    }];

    UILabel *messageLab = [[UILabel alloc] init];
    messageLab.font = PingFangSC(15);
    messageLab.text = self.message;
    messageLab.numberOfLines = 0;
    messageLab.textColor = COLOR_textColor;
    [self.backgroundView addSubview:messageLab];
    [messageLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(65);
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(21);
    }];

    ///金额
    NSArray *titles = self.dictionary[@"subTitles"];
    UILabel *amountLab = [[UILabel alloc] init];
    amountLab.font = PingFangSC(15);
    amountLab.text = titles[0];
    amountLab.textColor = COLOR_textColor;
    [self.backgroundView addSubview:amountLab];
    [amountLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(messageLab.mas_bottom).mas_offset(35);
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(70);
        make.height.mas_equalTo(21);
    }];
    
    UITextField *numberTextField = [[UITextField alloc] init];
    numberTextField.font = PingFangSC(15);
    numberTextField.keyboardType = UIKeyboardTypeDecimalPad;
    numberTextField.cornerRadius = 6;
    numberTextField.bWidth = 0.5;
    numberTextField.bColor = COLOR_cellLine;
    UIView *leftView = [UIView new];
    leftView.frame = CGRectMake(0, 0, 10, 35);
    numberTextField.leftView = leftView;
    //设置显示模式为永远显示(默认不显示)
    numberTextField.leftViewMode = UITextFieldViewModeAlways;
          
       
    [self.backgroundView addSubview:numberTextField];
    [numberTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(amountLab.mas_centerY);
        make.left.mas_equalTo(amountLab.mas_right).mas_offset(3);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(34);
    }];
    
    [self refreshUI];
       
    [self addKeyboardWillNotification];
       
       @weakify(self);
       [[self.sureBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
       @strongify(self);
           if(self.sureBlock){
               self.sureBlock(numberTextField.text);
           }
           [self dismissAnimate:NO];
       }];

       [[self.cancelBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
       @strongify(self);
           [self dismissAnimate:NO];
       }];


       UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismiss)];
       tap.delegate = self;
       [self addGestureRecognizer:tap];
}

#pragma mark - LZAlertTypeMerchantAudit
- (void)initMerchantAuditView{
    [self.backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
         make.centerX.mas_equalTo(self.mas_centerX);
         make.width.mas_offset(280);
         make.height.mas_equalTo(331);
         make.centerY.mas_equalTo(self.mas_centerY);
     }];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = PingFangSC_M(18);
    titleLabel.text = self.title;
    titleLabel.numberOfLines = 0;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = COLOR_textColor;
    CGFloat titleHeight = [titleLabel.text lz_textHeightWithFontSize:titleLabel.font withMaxWidth:250];
    [self.backgroundView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(30);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(titleHeight);
    }];
    
    UILabel *messageLab = [[UILabel alloc] init];
    messageLab.font = PingFangSC(15);
    messageLab.text = self.message;
    messageLab.numberOfLines = 0;
    messageLab.textColor = COLOR_textColor;
    [self.backgroundView addSubview:messageLab];
    [messageLab mas_makeConstraints:^(MASConstraintMaker *make) {
       make.top.mas_equalTo(70);
       make.left.mas_equalTo(15);
       make.width.mas_equalTo(80);
       make.height.mas_equalTo(21);
    }];
    
    
    UIImage *norImage = [UIImage lz_createImage:UIColor.whiteColor];
    UIImage *selImage = [UIImage lz_createImage:COLOR_appColor];

    NSMutableDictionary *dataDictionary = [[NSMutableDictionary alloc] init];
    dataDictionary[@"status"] = @"NORMAL";

    ///!!!:状态
    UIButton *passBtn = [UIButton lz_buttonTitle:@"通过" titleColor:COLOR_textColor fontSize:15];
    [passBtn setTitleColor:UIColor.whiteColor forState:UIControlStateSelected];
    passBtn.cornerRadius = 5;
    [passBtn setBackgroundImage:norImage forState:UIControlStateNormal];
    [passBtn setBackgroundImage:selImage forState:UIControlStateSelected];
    passBtn.selected = YES;
    [self.backgroundView addSubview:passBtn];
    [passBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(messageLab.mas_right).mas_offset(15);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(25);
        make.centerY.mas_equalTo(messageLab.mas_centerY);
    }];
    
    UIButton *noPassBtn = [UIButton lz_buttonTitle:@"不通过" titleColor:COLOR_textColor fontSize:15];
    [noPassBtn setTitleColor:UIColor.whiteColor forState:UIControlStateSelected];
    noPassBtn.cornerRadius = 5;
    [noPassBtn setBackgroundImage:norImage forState:UIControlStateNormal];
    [noPassBtn setBackgroundImage:selImage forState:UIControlStateSelected];
    [self.backgroundView addSubview:noPassBtn];
    [noPassBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(passBtn.mas_right).mas_offset(15);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(25);
        make.centerY.mas_equalTo(messageLab.mas_centerY);
    }];
    
    [[passBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        x.selected = YES;
        noPassBtn.selected = NO;
        dataDictionary[@"status"] = @"NORMAL";
    }];
    
    [[noPassBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        x.selected = YES;
        passBtn.selected = NO;
        dataDictionary[@"status"] = @"REGJECT";
         
     }];
    
    ///!!!:结果
    UILabel *resultLab = [UILabel lz_labelWithText:@"审核结果:" fontSize:PingFangSC(15) color:COLOR_textColor];
    [self.backgroundView addSubview:resultLab];
    [resultLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(messageLab.mas_bottom).mas_offset(22);
       make.left.mas_equalTo(15);
       make.width.mas_equalTo(80);
       make.height.mas_equalTo(21);
    }];
    
    
    YYTextView *resultTextView = [[YYTextView alloc] init];
    resultTextView.placeholderText = @"请输入审核结果/意见";
    resultTextView.placeholderFont = PingFangSC(15);
    resultTextView.font = PingFangSC(15);
    resultTextView.placeholderTextColor = COLOR_placeholderColor;
    resultTextView.cornerRadius = 6;
    resultTextView.bWidth = 0.5;
    resultTextView.bColor = COLOR_cellLine;
    [self.backgroundView addSubview:resultTextView];
    [resultTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(resultLab.mas_bottom).mas_equalTo(10);
        make.left.mas_equalTo(messageLab);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(110);
    }];

    [self refreshUI];
    
    [self addKeyboardWillNotification];
      
      @weakify(self);
      [[self.sureBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
      @strongify(self);
          
          dataDictionary[@"auditMsg"] = resultTextView.text?:@"";
          if(self.sureBlock){
              self.sureBlock(dataDictionary);
          }
          [self dismissAnimate:NO];
      }];

      [[self.cancelBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
      @strongify(self);
          [self dismissAnimate:NO];
      }];


      UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismiss)];
      tap.delegate = self;
      [self addGestureRecognizer:tap];
      
    
    
}
#pragma mark - LZAlertTypeEditAmout
- (void)initEditAmoutView{
    [self.backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.width.mas_offset(280);
        make.height.mas_equalTo(180);
        make.centerY.mas_equalTo(self.mas_centerY);
    }];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = PingFangSC_M(18);
    titleLabel.text = self.title;
    titleLabel.numberOfLines = 0;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = COLOR_textColor;
    CGFloat titleHeight = [titleLabel.text lz_textHeightWithFontSize:titleLabel.font withMaxWidth:250];
    [self.backgroundView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(30);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(titleHeight);
    }];
         
    
    NSArray *subTitles = self.dictionary[@"subTitles"];
    for (int i = 0; i < subTitles.count; i++){
    UIView *contentView = [[UIView alloc] init];
    [self.backgroundView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.mas_equalTo(15);
         make.width.mas_equalTo(250);
         make.height.mas_equalTo(50);
         make.top.mas_equalTo(50 + i * 50);
    }];

         
     UILabel *titleLab = [UILabel lz_labelWithText:subTitles[i] fontSize:PingFangSC(15) color:COLOR_textColor];
     [contentView addSubview:titleLab];
     [titleLab lz_rightAlignment];
     [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.mas_equalTo(0);
         make.width.mas_equalTo(70);
         make.height.mas_equalTo(21);
         make.bottom.mas_equalTo(-6);
     }];

     UITextField *numberTextField = [[UITextField alloc] init];
     numberTextField.font = PingFangSC(15);
     numberTextField.keyboardType = UIKeyboardTypeDecimalPad;
     numberTextField.cornerRadius = 6;
     numberTextField.bWidth = 0.5;
     numberTextField.bColor = COLOR_cellLine;
     UIView *leftView = [UIView new];
     leftView.frame = CGRectMake(0, 0, 10, 35);
     numberTextField.leftView = leftView;
     //设置显示模式为永远显示(默认不显示)
     numberTextField.leftViewMode = UITextFieldViewModeAlways;
     [contentView addSubview:numberTextField];
     [numberTextField mas_makeConstraints:^(MASConstraintMaker *make) {
         make.centerY.mas_equalTo(titleLab.mas_centerY);
         make.left.mas_equalTo(titleLab.mas_right).mas_offset(3);
         make.right.mas_equalTo(0);
         make.height.mas_equalTo(34);
     }];
         
         
     }
     
     
     [self refreshUI];
     
     
     [self addKeyboardWillNotification];
        
    @weakify(self);
    [[self.sureBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
    @strongify(self);
        if(self.sureBlock){
            self.sureBlock(@"");
        }
        [self dismissAnimate:NO];
    }];

    [[self.cancelBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
    @strongify(self);
        [self dismissAnimate:NO];
    }];


    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismiss)];
    tap.delegate = self;
    [self addGestureRecognizer:tap];

    
    
}
#pragma mark - LZAlertTypeAuditStatus
- (void)initAuditStatusView{
    
    [self.backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.width.mas_offset(280);
        make.height.mas_equalTo(280);
        make.centerY.mas_equalTo(self.mas_centerY);
    }];

    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = PingFangSC_M(18);
    titleLabel.text = self.title;
    titleLabel.numberOfLines = 0;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = COLOR_textColor;
    CGFloat titleHeight = [titleLabel.text lz_textHeightWithFontSize:titleLabel.font withMaxWidth:250];
    [self.backgroundView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(30);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(titleHeight);
    }];
      
    UILabel *messageLab = [[UILabel alloc] init];
    messageLab.font = PingFangSC(15);
    messageLab.text = self.message;
    messageLab.numberOfLines = 0;
    messageLab.textColor = COLOR_textColor;
    [self.backgroundView addSubview:messageLab];
    [messageLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(70);
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(21);
    }];
    
    NSMutableDictionary *dataDictionary = [[NSMutableDictionary alloc] init];
    NSArray *subTitles = self.dictionary[@"subTitles"];
    NSString *placeholder = self.dictionary[@"placeholder"];

    LZOptionView *optionView = [[LZOptionView alloc] initWithFrame:CGRectMake( 0, 0, 175, 34) dataSource:subTitles];
    optionView.placeholder = placeholder;
    [self.backgroundView addSubview:optionView];
    [optionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(messageLab.mas_centerY);
        make.left.mas_equalTo(messageLab.mas_right).mas_offset(5);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(34);
    }];
    @weakify(self);
    [optionView setSelectedBlock:^(LZOptionView * _Nonnull optionView, LZOptionModel * _Nonnull optionModel) {
        dataDictionary[optionModel.key] = optionModel.detail?:@"";
    }];
    
    ///!!!:审核结果
    UILabel *resultLab = [[UILabel alloc] init];
    resultLab.font = PingFangSC(15);
    resultLab.text = @"审核结果";
    resultLab.textColor = COLOR_textColor;
    [self.backgroundView addSubview:resultLab];
    [resultLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(messageLab.mas_bottom).mas_equalTo(28);
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(21);
    }];
    
    YYTextView *resultTextView = [[YYTextView alloc] init];
    resultTextView.placeholderText = @"请输入审核结果/意见";
    resultTextView.placeholderFont = PingFangSC(15);
    resultTextView.font = PingFangSC(15);
    resultTextView.placeholderTextColor = COLOR_placeholderColor;
    resultTextView.cornerRadius = 6;
    resultTextView.bWidth = 0.5;
    resultTextView.bColor = COLOR_cellLine;
    [self.backgroundView addSubview:resultTextView];
    [resultTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(optionView.mas_bottom).mas_equalTo(21);
        make.left.mas_equalTo(messageLab.mas_right).mas_offset(5);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(78);
    }];
    
    
    [self refreshUI];
    
    [self addKeyboardWillNotification];
    
    [[self.sureBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
    @strongify(self);
        if(resultTextView.text.length > 0){
            dataDictionary[@"result"] = resultTextView.text;
        }
        
        if(self.sureBlock){
            self.sureBlock(dataDictionary);
        }
        [self dismissAnimate:NO];
    }];

    [[self.cancelBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
    @strongify(self);
        [self dismissAnimate:NO];
    }];


    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismiss)];
    tap.delegate = self;
    [self addGestureRecognizer:tap];
}


#pragma mark - LZAlertTypeReplacePerson
- (void)initReplacePersonView{
    
    [self.backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.width.mas_offset(280);
        make.height.mas_equalTo(175);
        make.centerY.mas_equalTo(self.mas_centerY);
    }];
        
        
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = PingFangSC_M(18);
    titleLabel.text = self.title;
    titleLabel.numberOfLines = 0;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = COLOR_textColor;
    CGFloat titleHeight = [titleLabel.text lz_textHeightWithFontSize:titleLabel.font withMaxWidth:250];
    [self.backgroundView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(30);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(titleHeight);
    }];
      
    UILabel *messageLab = [[UILabel alloc] init];
    messageLab.font = PingFangSC(15);
    messageLab.text = self.message;
    messageLab.numberOfLines = 0;
    messageLab.textAlignment = NSTextAlignmentCenter;
    messageLab.textColor = COLOR_textColor;
    [self.backgroundView addSubview:messageLab];
    [messageLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(70);
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(69);
        make.height.mas_equalTo(21);
    }];

    NSArray *subTitles = self.dictionary[@"subTitles"];
    LZOptionView *optionView = [[LZOptionView alloc] initWithFrame:CGRectMake( 0, 0, 175, 34) dataSource:subTitles];
    optionView.title = subTitles.firstObject;
    [self.backgroundView addSubview:optionView];
    [optionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(messageLab.mas_centerY);
        make.left.mas_equalTo(messageLab.mas_right).mas_offset(5);
        make.width.mas_equalTo(175);
        make.height.mas_equalTo(34);
    }];
    
    [self refreshUI];
    
    @weakify(self);
    [[self.sureBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
    @strongify(self);
        if(self.sureBlock){
            self.sureBlock(optionView.title);
        }
        [self dismissAnimate:NO];
    }];
    [[self.cancelBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
    @strongify(self);
        [self dismissAnimate:NO];
    }];

    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismiss)];
    [self addGestureRecognizer:tap];
    
}

#pragma mark - LZAlertTypeTipAlter
- (void)initSubTipAlterView
{
    //280
    [self.backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
          make.centerX.mas_equalTo(self.mas_centerX);
          make.width.mas_offset(280);
          make.height.mas_equalTo(164);
          make.centerY.mas_equalTo(self.mas_centerY);
      }];
      
      
      UILabel *titleLabel = [[UILabel alloc] init];
      titleLabel.font = PingFangSC_M(18);
      titleLabel.text = self.title;
      titleLabel.numberOfLines = 0;
      titleLabel.textAlignment = NSTextAlignmentCenter;
      titleLabel.textColor = COLOR_textColor;
      CGFloat titleHeight = [titleLabel.text lz_textHeightWithFontSize:titleLabel.font withMaxWidth:250];
      [self.backgroundView addSubview:titleLabel];
      [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
          make.top.mas_equalTo(20);
          make.left.mas_equalTo(10);
          make.right.mas_equalTo(-10);
          make.height.mas_equalTo(titleHeight);
      }];
    
    
    
      UILabel *messageLab = [[UILabel alloc] init];
      messageLab.font = PingFangSC(15);
      messageLab.text = self.message;
      messageLab.numberOfLines = 0;
      messageLab.textAlignment = NSTextAlignmentCenter;
      messageLab.textColor = COLOR_textColor;
      CGFloat messageHeight = [messageLab.text lz_textHeightWithFontSize:messageLab.font withMaxWidth:250];
      [self.backgroundView addSubview:messageLab];
      [messageLab mas_makeConstraints:^(MASConstraintMaker *make) {
          make.top.mas_equalTo(titleLabel.mas_bottom).mas_offset(10);
          make.left.mas_equalTo(10);
          make.right.mas_equalTo(-10);
          make.height.mas_equalTo(messageHeight);
      }];
    
    
      
      [self.backgroundView mas_updateConstraints:^(MASConstraintMaker *make) {
          make.height.mas_equalTo(titleHeight + messageHeight + 100);
      }];
      
      
    
      @weakify(self);
      [[self.sureBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
          @strongify(self);
          if(self.sureBlock){
              self.sureBlock(@"");
          }
          [self dismissAnimate:NO];
      }];

      [[self.cancelBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
          @strongify(self);
          [self dismissAnimate:NO];
      }];
            
        [self refreshUI];
      
  
      
      UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismiss)];
      [self addGestureRecognizer:tapGesture];
    
}

 #pragma mark - LZAlertTypeActionSheet
 - (void)initSubActionSheetView{
     
     CGFloat btnHeight = 50;
     NSArray *subTitles = self.dictionary[@"subTitles"];
     
     NSInteger maxCount = (subTitles.count > 8) ? 8 : subTitles.count;
     NSInteger maxHeight = maxCount * btnHeight;
     
     self.backgroundView.frame = CGRectMake(0, K_SCREENHEIGHT, K_SCREENWIDTH, maxHeight + 10 + btnHeight);
     
     UIScrollView *scrollView = [[UIScrollView alloc] init];
     [self.backgroundView addSubview:scrollView];
     scrollView.frame = CGRectMake(0, 0, self.backgroundView.width, maxHeight);
     scrollView.contentSize = CGSizeMake(0, subTitles.count * btnHeight);
     scrollView.showsVerticalScrollIndicator = NO;
     scrollView.bounces = NO;
     if(K_Device_Is_iPhoneX){
        self.backgroundView.height += 43;
     }
     @weakify(self)
     for(int i = 0; i < subTitles.count; i++){
         UIButton * button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
         button.frame = CGRectMake(0, i *  btnHeight, self.backgroundView.width, btnHeight);
         [button setTitle:subTitles[i] forState:UIControlStateNormal];
         [button setTitleColor:COLOR_appColor forState:UIControlStateNormal];
         button.titleLabel.font = PingFangSC(16);
         
         [[button rac_signalForControlEvents:UIControlEventTouchUpInside]subscribeNext:^(id x) {
             @strongify(self)
             if(self.sureBlock){
                 self.sureBlock(@(i));
                 [self dismissAnimate:YES];
             }
         }];
         [scrollView addSubview:button];
         
         if (i != subTitles.count - 1) {
             UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, btnHeight - 0.5, K_SCREENWIDTH, 0.5)];
             lineView.backgroundColor = COLOR_cellLine;
             [button addSubview:lineView];
         }
     }
     
     UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, scrollView.height, K_SCREENWIDTH, 10)];
     lineView.backgroundColor = COLOR_cellLine;
     [self.backgroundView addSubview:lineView];
     
     self.cancelBtn.frame = CGRectMake(0, CGRectGetMaxY(lineView.frame), self.backgroundView.width, btnHeight);
     [self.cancelBtn setTitleColor:COLOR_textColor forState:UIControlStateNormal];
     
     [[self.cancelBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
         @strongify(self)
         [self dismissAnimate:YES];
     }];
     [UIView animateWithDuration:0.2 animations:^{
         self.backgroundView.frame = CGRectMake(0, K_SCREENHEIGHT - self.backgroundView.height, K_SCREENWIDTH,  self.backgroundView.height);
     } completion:^(BOOL finished) {
         
     }];
     
     UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.backgroundView.bounds byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)];
     CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
     //设置大小
     maskLayer.frame = self.backgroundView.bounds;
     //设置图形样子
     maskLayer.path = maskPath.CGPath;
     self.backgroundView.layer.mask = maskLayer;

     UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gestureRecognizerDismiss)];
     [self addGestureRecognizer:tap];
 }




- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self endEditing:YES];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{

    //如果是子视图 self ，设置无法接受 父视图 点击事件。
    if(touch.view == self){
        return YES;
    }
    return NO;

    
}



@end
