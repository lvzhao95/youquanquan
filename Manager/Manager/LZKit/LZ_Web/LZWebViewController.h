//
//  LZWebViewController.h
//  ShareDemo
//
//  Created by 微微 on 2019/3/6.
//  Copyright © 2019 链动科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "LZBaseViewController.h"


//加载web的方法
typedef  NS_ENUM(NSInteger,LZWebLoadingType){
    LZWebLoadingTypeUrl = 0,          //url
    LZWebLoadingTypeHtml,            //html文本
    
};

NS_ASSUME_NONNULL_BEGIN

@interface LZWebViewController : LZBaseViewController

@property (nonatomic,assign) LZWebLoadingType webType;

@property (nonatomic,strong) NSString *webUrl;

@property (nonatomic, strong) WKWebView *  webView;
//网页加载进度视图
@property (nonatomic, strong) UIProgressView * progressView;
@end

NS_ASSUME_NONNULL_END
