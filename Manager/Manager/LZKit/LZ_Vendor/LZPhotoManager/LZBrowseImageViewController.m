//
//  LZBrowseImageViewController.m
//  BrowseImage
//
//  Created by vv-微微 on 2018/7/3.
//  Copyright © 2018年 微微. All rights reserved.
//

#import "LZBrowseImageViewController.h"

#define ScrollView_tag 1000
#define ImageView_tag 2000

@interface LZBrowseImageViewController ()<UIScrollViewDelegate>
//底视图
@property (nonatomic,strong) UIScrollView *scrollView;
//内视图
@property (nonatomic,strong) UIView *contentView;
//当前页数
@property (nonatomic,assign) NSInteger currentPage;
//是否放大
@property (nonatomic,assign) BOOL isBig;

@end

@implementation LZBrowseImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setupUI];
}

- (void)setupUI{
 
    //创建父 scrollView,
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.contentView];
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.right.mas_equalTo(0);
    }];
    
    //创建放图片的View
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.scrollView);
        make.height.mas_equalTo(self.scrollView);
        
    }];
    
    //根据数据源创建显示图片的view
    [self createPhotoscrollView];

    //右按钮
    [self setupNavRightItem];
}


//自定义导航栏的右侧按钮
- (void)setupNavRightItem{
//
//    UIBarButtonItem * rightItem = [LZToolView getBarButtonItemWithTarget:self action:@selector(rightItemClick) isLeftBar:NO
//                                                               normalImg:[UIImage imageNamed:@"more"]
//                                                              hilightImg:[UIImage imageNamed:@"more"]
//                                                                   title:nil];
//
//    self.navigationItem.rightBarButtonItem = rightItem;
}


//创建图片
- (void)createPhotoscrollView{

    UIScrollView * lastScrollView = nil;

    for(int i = 0; i < self.sourceArr.count; i++){
        
        UIScrollView * photoScroView = [[UIScrollView alloc]init];
//        if(i%2 == 0){
//            photoScroView.backgroundColor = [UIColor yellowColor];
//        }else {
//            photoScroView.backgroundColor = [UIColor redColor];
//        }
        [self.contentView addSubview:photoScroView];
        [photoScroView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.left.mas_equalTo(i * K_SCREENWIDTH);
            make.width.mas_equalTo(K_SCREENWIDTH);
            make.bottom.mas_equalTo(0);
        }];
        lastScrollView = photoScroView;
        
        photoScroView.delegate = self;
        photoScroView.maximumZoomScale = 6;
        photoScroView.minimumZoomScale = 1;
        photoScroView.showsHorizontalScrollIndicator = NO;
        photoScroView.showsVerticalScrollIndicator = NO;

        photoScroView.tag = ScrollView_tag + i;


        UITapGestureRecognizer *tapTwo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scaleImag:)];
        [photoScroView addGestureRecognizer:tapTwo];
        tapTwo.numberOfTapsRequired = 2;
        
        
        UITapGestureRecognizer *tapOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endShowImg:)];
        [photoScroView addGestureRecognizer:tapOne];
        tapOne.numberOfTapsRequired = 1;
        [tapOne requireGestureRecognizerToFail:tapTwo];
        
    
        UIImageView *imgView = [[UIImageView alloc] init];
        [photoScroView addSubview:imgView];
        imgView.userInteractionEnabled = YES;
        
        id object = self.sourceArr[i];
        if([object isKindOfClass:[NSString class]]){
        
            [imgView setImageWithURL:[NSURL URLWithString:self.sourceArr[i]] placeholder:nil];
           
        } else if ([object isKindOfClass:[UIImage class]]){
            UIImage *sourceImage = object;
            imgView.image = sourceImage;
        }
        //增加长按手势
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(rightItemClick)];
        [imgView addGestureRecognizer:longPress];
        
        imgView.contentMode = UIViewContentModeScaleAspectFit;
        imgView.tag = ImageView_tag;

        [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(@0);
            make.top.mas_equalTo(@0);
            make.height.mas_equalTo(photoScroView);
            make.width.mas_equalTo(photoScroView);
        }];
    }
    
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(lastScrollView.mas_right);
    }];
 
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(scrollView.tag >= ScrollView_tag)return;
    self.currentPage = (scrollView.contentOffset.x) / K_SCREENWIDTH;
    
    
    UIScrollView *scrollV_next = (UIScrollView *)[scrollView viewWithTag:self.currentPage + 1000+1]; //前一页
    
    if (scrollV_next.zoomScale != 1.0){
        [scrollV_next setZoomScale:1.0 animated:YES];
    }
    
    UIScrollView *scollV_pre = (UIScrollView *)[scrollView viewWithTag:self.currentPage + 1000-1]; //后一页
    if (scollV_pre.zoomScale != 1.0){
        [scollV_pre setZoomScale:1.0 animated:YES];;
    }
    
    [self.scrollView setContentOffset:CGPointMake(K_SCREENWIDTH * self.currentPage, 0) animated:YES];
    
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    if (scrollView != _scrollView) {
        UIImageView *imgView = (UIImageView *)[scrollView viewWithTag:ImageView_tag];
        return imgView;
    }
    
    return nil;
}

#pragma mark - actions
- (void)scaleImag:(UITapGestureRecognizer *)tapTwo
{
    CGFloat newScale = 2.5;
    UIScrollView *currentScrollView = (UIScrollView *)tapTwo.view;
    CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[tapTwo locationInView:tapTwo.view] andScrollView:currentScrollView];
    
    if (self.isBig == NO)  {
        
        [currentScrollView zoomToRect:zoomRect animated:YES];
        
    } else {
        
        [currentScrollView zoomToRect:currentScrollView.frame animated:YES];
    }
    
    self.isBig = !self.isBig;
    
}
- (void)endShowImg:(UITapGestureRecognizer *)tapOne{
    
    [self.navigationController popViewControllerAnimated:NO];
}
//放大and缩小
- (CGRect)zoomRectForScale:(CGFloat)newScale withCenter:(CGPoint)center andScrollView:(UIScrollView *)scrollView{
    
    CGRect zoomRect = CGRectZero;
    
    zoomRect.size.height = scrollView.frame.size.height / newScale;
    zoomRect.size.width = scrollView.frame.size.width  / newScale;
    zoomRect.origin.x = center.x - (zoomRect.size.width  / 2.0);
    zoomRect.origin.y = center.y - (zoomRect.size.height / 2.0);
    
    return zoomRect;
    
}

//保存图片
- (void)rightItemClick{
    UIScrollView *photoScroView  = [self.contentView viewWithTag:ScrollView_tag + self.currentPage];
    UIImageView *imgView =  [photoScroView viewWithTag:ImageView_tag];
    
//    @weakify(self);
//    NSArray * titles = @[@"保存图片",@"取消"];
//    [LZToolView showAlertType:LZAlertViewTypeActionSheet withTitle:@"" message:@"" cancel:@"" sure:@"" objectDict:@{@"subTitles":titles} cancelBlock:^(id object) {} sureBlock:^(id object) {
//        if([object isEqualToString:@"保存图片"]){
//            @strongify(self);
//            [self loadImageFinished:imgView.image];
//        }
//    }];
}

//保存图片
- (void)loadImageFinished:(UIImage *)image{
    if(!image) return;
    //先查询是否有权限
//    [LZTool accessPrivacyPermissionWithType:LZSystemAuthorityTypePhoto completion:^(BOOL response) {
//        if (response) {
//            UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), (__bridge void *)self);
//        }
//    }];
}
//回调
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
//    if (error != NULL){
//        showErrorStatus(@"保存失败", self.view);
//    } else {
//        showSuccessStatus(@"图片已保存到手机相册",self.view);
//    }
    
}


#pragma mark - get and set
- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]init];
        _scrollView.pagingEnabled = YES;
        _scrollView.delegate = self;
        _scrollView.bounces = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.backgroundColor = COLOR_background;
    }
    return _scrollView;
}

//容器View
- (UIView *)contentView{
    if(!_contentView){
        _contentView = [[UIView alloc]init];
    }
    return _contentView;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
