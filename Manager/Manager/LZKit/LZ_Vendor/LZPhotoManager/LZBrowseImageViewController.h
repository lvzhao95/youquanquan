//
//  LZBrowseImageViewController.h
//  BrowseImage
//
//  Created by vv-微微 on 2018/7/3.
//  Copyright © 2018年 微微. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LZBaseViewController.h"

@interface LZBrowseImageViewController : LZBaseViewController


//放置图片的url
@property (nonatomic,strong)NSArray *sourceArr;

@end
