//
//  UIScrollView+EmptyData.h
//  TRC
//
//  Created by JJ on 2018/12/7.
//  Copyright © 2018 Jsongy. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SQEmptyView.h"

NS_ASSUME_NONNULL_BEGIN


@interface UIView (EmptyData)

@property(nonatomic, strong) SQEmptyView *emptyView;

- (void)dismissEmptyView;

/*
 *    数据为空时的提示，图片加文字, offSetY为距离Y的距离
 */
- (void) tableViewDisplayWithImgName:(NSString *)imgName message:(NSString*)message contentoffsetY:(CGFloat)offSetY showEmptyView:(BOOL)isShow;

- (void) tableViewDisplayWithImgName:(NSString *)imgName message:(NSString*)message tipBtn:(NSString *)tipBtntitle contentoffsetY:(CGFloat)offSetY showEmptyView:(BOOL)isShow;



@end


NS_ASSUME_NONNULL_END
