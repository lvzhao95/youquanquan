//
//  SQEmptyView.h
//  FeiPu
//
//  Created by qiu on 16/10/25.
//  Copyright © 2016年 Tang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SQEmptyView : UIView

/** 提示的文本  */
@property(nonatomic, copy) NSString *labelText;

@property(nonatomic, strong) UIButton *tipBtn;

/** 图片   */
@property(nonatomic , strong)UIImageView *iconImageView;

@property (nonatomic, assign) CGFloat offSetY;

@property (nonatomic,copy) void(^btnBlock)(id object);


+(SQEmptyView *)showTitle:(NSString *)title;

+(SQEmptyView *)showTitle:(NSString *)title iconImage:(NSString *)imageName tipBtnTitle:(NSString *)btnTitle;

@end
