//
//  UIView+EmptyData.m
//  TRC
//
//  Created by JJ on 2018/12/7.
//  Copyright © 2018 Jsongy. All rights reserved.
//

#import "UIView+EmptyData.h"
#import <objc/runtime.h>





@implementation UIView (EmptyData)

#pragma mark -方法一
/**
 *   数据为空时的提示，文字
 *
 **/
- (void) tableViewDisplayWithMsg:(NSString *)message showEmptyView:(BOOL)isShow{
    
    [self tableViewDisplayWithImgName:@"" message:message tipBtn:@"" contentoffsetY:(self.centerY - 64 * 0.5) showEmptyView:isShow];
}

/**
 *  数据为空时的文字提示, offSetY距离Y的距离
 */
- (void) tableViewDisplayWithMsg:(NSString *)message contentoffsetY:(CGFloat)offSetY showEmptyView:(BOOL)isShow{
    
    [self tableViewDisplayWithImgName:@"" message:message tipBtn:@"" contentoffsetY:offSetY showEmptyView:isShow];

    
}

/*
 *    数据为空时的提示，图片加文字
 */
- (void) tableViewDisplayWithImgName:(NSString *)imgName message:(NSString*)message showEmptyView:(BOOL)isShow{
    
    
    [self tableViewDisplayWithImgName:imgName message:message tipBtn:@"" contentoffsetY:(self.centerY - 64 * 0.5) showEmptyView:isShow];
    
}
- (void) tableViewDisplayWithImgName:(NSString *)imgName message:(NSString*)message contentoffsetY:(CGFloat)offSetY showEmptyView:(BOOL)isShow{
    [self tableViewDisplayWithImgName:imgName message:message tipBtn:@"" contentoffsetY:offSetY showEmptyView:isShow];
}

/*
 *    数据为空时的提示，图片加文字, offSetY为距离Y的距离
 */
- (void) tableViewDisplayWithImgName:(NSString *)imgName message:(NSString*)message tipBtn:(NSString *)tipBtntitle contentoffsetY:(CGFloat)offSetY showEmptyView:(BOOL)isShow{
    
    if (isShow) {
        if (!self.emptyView) {
            [self layoutIfNeeded];
            self.emptyView = [SQEmptyView showTitle:message iconImage:imgName tipBtnTitle:tipBtntitle];
            self.emptyView.frame = CGRectMake(0, 0, self.width, self.height);
            self.emptyView.offSetY = offSetY;
            self.emptyView.backgroundColor = [UIColor clearColor];
            //label高度加上label和image的间距
            [self addSubview:self.emptyView];
        }
    } else {
        if (self.emptyView) {
            [self.emptyView removeFromSuperview];
            self.emptyView = nil;
        }
    }

}

- (void)dismissEmptyView{
    if (self.emptyView) {
        [self.emptyView removeFromSuperview];
        self.emptyView = nil;
    }
}
#pragma mark -associatedObject
-(void)setEmptyView:(UIView *)emptyView
{
    objc_setAssociatedObject(self, @selector(setEmptyView:), emptyView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIView *)emptyView
{
    return objc_getAssociatedObject(self, @selector(setEmptyView:));
}


@end

