//
//  SQEmptyView.m
//  FeiPu
//
//  Created by qiu on 16/10/25.
//  Copyright © 2016年 Tang. All rights reserved.
//

#import "SQEmptyView.h"
@interface SQEmptyView ()


/** 提示信息 */
@property(nonatomic , strong) UILabel *textLabel;



@end
@implementation SQEmptyView

+(SQEmptyView *)showTitle:(NSString *)title
{
    SQEmptyView *emptyView = [[SQEmptyView alloc]init];
    emptyView.labelText = title;
    return emptyView;
}


+(SQEmptyView *)showTitle:(NSString *)title iconImage:(NSString *)imageName tipBtnTitle:(NSString *)btnTitle
{
    SQEmptyView *emptyView = [self showTitle:title];
    emptyView.iconImageView.image = [UIImage imageNamed:imageName];
    if(btnTitle.length == 0){
        emptyView.tipBtn.hidden = YES;
    } else {
        emptyView.tipBtn.hidden = NO;
        [emptyView.tipBtn setTitle:btnTitle forState:UIControlStateNormal];
    }
    return emptyView;
}


-(void)layoutSubviews {
    [super layoutSubviews];
    

    [self addSubview:self.textLabel];
}

- (void)setOffSetY:(CGFloat)offSetY {
    _offSetY = offSetY;
    
    if (self.iconImageView) {
        CGFloat iconY = offSetY;
        CGFloat iconX = (K_SCREENWIDTH - self.iconImageView.image.size.width ) * 0.5;
        self.iconImageView.frame = CGRectMake(iconX, iconY, self.iconImageView.image.size.width, self.iconImageView.image.size.height);
    }
    
    NSDictionary *attrs = @{NSFontAttributeName:self.textLabel.font};
    CGFloat textLabelHeight = [self.textLabel.text boundingRectWithSize:CGSizeMake(self.frame.size.width, MAXFLOAT)
                                               options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                            attributes:attrs
                                               context:nil].size.height + 2;
    
    self.textLabel.frame = CGRectMake(0 , CGRectGetMaxY(self.iconImageView.frame) + 18, K_SCREENWIDTH, textLabelHeight);
    
    
    self.tipBtn.frame = CGRectMake((K_SCREENWIDTH - 110)/2.0 , CGRectGetMaxY(self.textLabel.frame) + 18, 110, 33);
    [self.tipBtn setGradientBackgroundWithColors:@[UIColorHex(0xFF661B),UIColorHex(0xFFA61B)] locations:@[@(0), @(1.0f)] startPoint:CGPointMake(1, 0.5) endPoint:CGPointMake(0, 0.5)];
    self.tipBtn.cornerRadius = 33/2.0;
}

-(void)setLabelText:(NSString *)labelText
{
    _labelText = labelText;
    self.textLabel.text = labelText;
}

- (void)tipBtnClick:(UIButton *)sender{
    if(self.btnBlock){
        self.btnBlock(@(1));
    }
}

#pragma mark - getting
-(UILabel *)textLabel
{
    if (!_textLabel) {
        _textLabel = [[UILabel alloc]init];
        _textLabel.font = PingFangSC(14);
        _textLabel.textColor = UIColorHex(0xA8A8A8);
        _textLabel.numberOfLines = 0;
        _textLabel.textAlignment = NSTextAlignmentCenter;
        _textLabel.adjustsFontSizeToFitWidth = YES;
    }
    return _textLabel;
}

-(UIImageView *)iconImageView
{
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc]init];
        _iconImageView.contentMode = UIViewContentModeCenter;
        [self addSubview:_iconImageView];
    }
    return _iconImageView;
}



- (UIButton *)tipBtn{
    if(!_tipBtn){
        _tipBtn = [UIButton lz_buttonTitle:@"" titleColor:UIColor.whiteColor fontSize:15];
        [_tipBtn addTarget:self action:@selector(tipBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_tipBtn];
    }
    return _tipBtn;
}
@end
