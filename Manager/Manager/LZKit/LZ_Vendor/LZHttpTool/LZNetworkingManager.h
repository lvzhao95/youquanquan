//
//  LZNetworkingManager.h
//  VVBTC
//
//  Created by 微微 on 2019/7/13.
//  Copyright © 2019  吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


typedef void (^ResponseSuccess) (__nullable id responseObject);
typedef void (^ResponseFailed) (NSError *__nullable error);
typedef void (^ResponseNotWork) (NSError *__nullable error);

@interface LZNetworkingManager : NSObject
/**
 *  设置请求超时时间
 */
+ (void)setRequestTimeout:(NSTimeInterval)timeout;


/**
 *  设置http请求报头
 */
+ (void)setHttpHeaders:(NSDictionary *)httpHeaders;


//每当网络状态发生变化时，就可以发通知，实时监测网络变化，然后做相应的处理
+ (void)startMonitoringNewWork;


/**
 *  是否输出日志
 *
 *  @param autoLog 默认不输出
 */
+ (void) setAutoLog:(BOOL)autoLog;

/**
 *  输出日志
 */
+ (BOOL) getAutoLog;


/**
 *  关闭所有请求
 */
+ (void) cancelAllRequestTasks;

/**
 Convert 开始请求
 @param  type 请求的方式 post get put delete
 @param  url 请求链接
 @param  params 参数
 @param  success 成功回调
 @param  failure 失败回调
 @param  notWork 没有网络回调
 @param  isLoading 请求的loading
 @param  isFailTip 失败的提示
 */

///!!!: 自己处理失败的回调
+ (void)lz_request:(NSString *)type url:(NSString *)url params:(NSDictionary *__nullable)params success:(ResponseSuccess)success failure:(ResponseFailed)failure;

///!!!: 统一处理回调
+ (void)lz_request:(NSString *)type url:(NSString *)url params:(NSDictionary *__nullable)params success:(ResponseSuccess)success failure:(ResponseFailed)failure isFailTip:(BOOL)isFailTip;

///!!!:自己处理失败的回调 自带loading
+ (void)lz_request:(NSString *)type url:(NSString *)url params:(NSDictionary *__nullable)params success:(ResponseSuccess)success failure:(ResponseFailed)failure isLoading:(BOOL)isLoading;

///!!!:统一处理回调 自带loading
+ (void)lz_request:(NSString *)type url:(NSString *)url params:(NSDictionary *__nullable)params success:(ResponseSuccess)success failure:(ResponseFailed)failure isLoading:(BOOL)isLoading isFailTip:(BOOL)isFailTip;

///!!!:需要处理无网络的状态的请求
+ (void)lz_request:(NSString *)type url:(NSString *)url params:(NSDictionary *__nullable)params success:(ResponseSuccess)success failure:(ResponseFailed)failure notwork:(ResponseNotWork)notWork;

///!!!: 很全面
+ (void)lz_request:(NSString *)type url:(NSString *)url params:(NSDictionary *__nullable)params success:(ResponseSuccess)success failure:(ResponseFailed)failure notwork:(ResponseNotWork)notWork isLoading:(BOOL)isLoading isFailTip:( BOOL)isFailTip;


/**
 Convert 开始上传文件
 @param  url 请求链接
 @param  params 参数
 @param  images 文件
 @param  fileName 文件名字
 @param  compressionRatio 压缩系数
 @param  success 成功回调
 @param  failure 失败回调
 @param  notWork 没有网络回调
 */
+ (void)lz_postWithBaseUrl:(NSString *)url
                    params:(NSDictionary *)params
                    images:(NSArray *)images
                  fileName:(NSString* )fileName
          compressionRatio:(CGFloat )compressionRatio
                   success:(ResponseSuccess)success
                   failure:(ResponseFailed)failure
                   notwork:(ResponseNotWork)notWork;



extern void dismiss(UIView *__nullable view);//消失
extern void showLoadingTitle(NSString *__nullable title, UIView *__nullable view); //展示的HUD
extern void showDelayedDismissTitle(NSString *__nullable title, UIView *__nullable view); //停几秒消失
@end

NS_ASSUME_NONNULL_END
