//
//  LZNetworkingManager.m
//  VVBTC
//
//  Created by 微微 on 2019/7/13.
//  Copyright © 2019  吕VV. All rights reserved.
//

#import "LZNetworkingManager.h"
#import "AFNetworking.h"
#import "MBProgressHUD+NHAdd.h"

@interface LZNetworkingManager ()
@end



static NSTimeInterval lz_timeout = 10.0f;
static NSDictionary *lz_httpHeaders = nil;
static NSMutableArray *lz_requestTasks;
static AFHTTPSessionManager *sharedManager = nil;
static BOOL isNetWork = YES;
static BOOL isAutoLog = NO;


@implementation LZNetworkingManager

//不废话. 先搞一个单例
+ (AFHTTPSessionManager *)sharedManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [AFHTTPSessionManager manager];
        sharedManager.securityPolicy.allowInvalidCertificates = YES;
       // 是否校验域名, 默认为YES
       sharedManager.securityPolicy.validatesDomainName = NO;
        
       sharedManager.requestSerializer = [AFHTTPRequestSerializer serializer];
       sharedManager.responseSerializer = [AFJSONResponseSerializer serializer];
       sharedManager.requestSerializer.timeoutInterval = lz_timeout;
       [sharedManager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
       sharedManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", @"text/plain",@"image/jpg",nil];
        //监听网络
        [LZNetworkingManager startMonitoringNewWork];
    });

    if(k_userToken.length > 0){
        [sharedManager.requestSerializer setValue:k_userToken forHTTPHeaderField:@"X-Access-Token"];
    }
    return sharedManager;
}

///!!!:是否输出日志
+ (void) setAutoLog:(BOOL)autoLog{
    
    isAutoLog = autoLog;
}

///!!!:输出日志
+ (BOOL) getAutoLog{
    return isAutoLog;
}

/**
 *  设置请求超时时间
 */
+ (void)setRequestTimeout:(NSTimeInterval)timeout{
    lz_timeout = timeout;
}


/**
 *  设置http请求报头
 */
+ (void)setHttpHeaders:(NSDictionary *)httpHeaders{
    lz_httpHeaders = httpHeaders;
    for (NSString *key in lz_httpHeaders.allKeys) {
        if (lz_httpHeaders[key] != nil) {
            [[self sharedManager].requestSerializer setValue:lz_httpHeaders[key] forHTTPHeaderField:key];
        }
    }
}

//每当网络状态发生变化时，就可以发通知，实时监测网络变化，然后做相应的处理
+ (void)startMonitoringNewWork {
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case -1:
                LZLog(@"未知网络");
                break;
            case 0:
                LZLog(@"网络不可达");
                break;
            case 1:
            {
                LZLog(@"GPRS网络");
            }
                break;
            case 2:
            {
                LZLog(@"wifi网络");
            }
                break;
                
            default:
                LZLog(@"未知网络");
                break;
        }
        
        if (status == AFNetworkReachabilityStatusReachableViaWWAN || status == AFNetworkReachabilityStatusReachableViaWiFi) {
            LZLog(@"有网");
            isNetWork = YES;
        }else{
            LZLog(@"没网");
            isNetWork = NO;
        }
    }];
    
}

/**
 *  全部请求任务
 *
 *  return
 */
+ (NSMutableArray *)allRequestTasks {
    @synchronized(self) {
        if (!lz_requestTasks) {
            lz_requestTasks = [NSMutableArray array];
        }
    }
    return lz_requestTasks;
}


/**
 *  关闭所有请求
 */
+ (void)cancelAllRequestTasks {
    @synchronized(self) {
        [[self allRequestTasks] enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isKindOfClass:[NSURLSessionTask class]]) {
                [obj cancel];
            }
        }];
        
        [[self allRequestTasks] removeAllObjects];
    }
}


/**
 *  自己处理失败的回调 不带loading
 */
+ (void)lz_request:(NSString *)type url:(NSString *)url params:(NSDictionary *)params success:(ResponseSuccess)success failure:(ResponseFailed)failure{
    if([type.lowercaseString isEqualToString:@"get"]){
        
        [self lz_getWithUrl:url params:params success:success failure:failure notwork:nil isLoading:NO isFailTip:NO];
        
    } else if([type.lowercaseString isEqualToString:@"post"]){
        
        [self lz_postWithUrl:url params:params success:success failure:failure notwork:nil isLoading:NO isFailTip:NO];
        
    } else if([type.lowercaseString isEqualToString:@"put"]){
        [self lz_putWithUrl:url params:params success:success failure:failure notwork:nil isLoading:NO isFailTip:NO];
        
    } else if([type.lowercaseString isEqualToString:@"delete"]){
        [self lz_deleteWithUrl:url params:params success:success failure:failure notwork:nil isLoading:NO isFailTip:NO];
        
    }
}

/**
 *  统一处理回调
 */
+ (void)lz_request:(NSString *)type url:(NSString *)url params:(NSDictionary *)params success:(ResponseSuccess)success failure:(ResponseFailed)failure isFailTip:(BOOL)isFailTip{
    if([type.lowercaseString isEqualToString:@"get"]){
        [self lz_getWithUrl:url params:params success:success failure:failure notwork:nil isLoading:NO isFailTip:isFailTip];
        
    } else if([type.lowercaseString isEqualToString:@"post"]){
        
        [self lz_postWithUrl:url params:params success:success failure:failure notwork:nil isLoading:NO isFailTip:isFailTip];
        
    } else if([type.lowercaseString isEqualToString:@"put"]){
        [self lz_putWithUrl:url params:params success:success failure:failure notwork:nil isLoading:NO isFailTip:isFailTip];
        
    } else if([type.lowercaseString isEqualToString:@"delete"]){
        [self lz_deleteWithUrl:url params:params success:success failure:failure notwork:nil isLoading:NO isFailTip:isFailTip];
        
    }
}

/**
 *  统一处理回调 自带loading
 */
+ (void)lz_request:(NSString *)type url:(NSString *)url params:(NSDictionary *)params success:(ResponseSuccess)success failure:(ResponseFailed)failure isLoading:(BOOL)isLoading{
    if([type.lowercaseString isEqualToString:@"get"]){
        [self lz_getWithUrl:url params:params success:success failure:failure notwork:nil isLoading:isLoading isFailTip:NO];
        
    } else if([type.lowercaseString isEqualToString:@"post"]){
        [self lz_postWithUrl:url params:params success:success failure:failure notwork:nil isLoading:isLoading isFailTip:NO];
        
    } else if([type.lowercaseString isEqualToString:@"put"]){
        [self lz_putWithUrl:url params:params success:success failure:failure notwork:nil isLoading:isLoading isFailTip:NO];
        
    } else if([type.lowercaseString isEqualToString:@"delete"]){
        [self lz_deleteWithUrl:url params:params success:success failure:failure notwork:nil isLoading:isLoading isFailTip:NO];
        
    }
}

/**
 *  统一处理回调 自带loading
 */
+ (void)lz_request:(NSString *)type url:(NSString *)url params:(NSDictionary *)params success:(ResponseSuccess)success failure:(ResponseFailed)failure isLoading:(BOOL)isLoading isFailTip:(BOOL)isFailTip{
    
    if([type.lowercaseString isEqualToString:@"get"]){
        [self lz_getWithUrl:url params:params success:success failure:failure notwork:nil isLoading:isLoading isFailTip:isFailTip];

    } else if([type.lowercaseString isEqualToString:@"post"]){
        [self lz_postWithUrl:url params:params success:success failure:failure notwork:nil isLoading:isLoading isFailTip:isFailTip];
        
    } else if([type.lowercaseString isEqualToString:@"put"]){
        [self lz_putWithUrl:url params:params success:success failure:failure notwork:nil isLoading:isLoading isFailTip:isFailTip];
        
    } else if([type.lowercaseString isEqualToString:@"delete"]){
        [self lz_deleteWithUrl:url params:params success:success failure:failure notwork:nil isLoading:isLoading isFailTip:isFailTip];
        
    }
    
    
}

/**
 *  需要处理无网络的状态的请求
 */
+ (void)lz_request:(NSString *)type url:(NSString *)url params:(NSDictionary *)params success:(ResponseSuccess)success failure:(ResponseFailed)failure notwork:(ResponseNotWork)notWork{
    if([type.lowercaseString isEqualToString:@"get"]){
        [self lz_getWithUrl:url params:params success:success failure:failure notwork:notWork isLoading:NO isFailTip:NO];
        
    } else if([type.lowercaseString isEqualToString:@"post"]){
        [self lz_postWithUrl:url params:params success:success failure:failure notwork:notWork isLoading:NO isFailTip:NO];
        
    } else if([type.lowercaseString isEqualToString:@"put"]){
        [self lz_putWithUrl:url params:params success:success failure:failure notwork:notWork isLoading:NO isFailTip:NO];
        
    } else if([type.lowercaseString isEqualToString:@"delete"]){
        [self lz_deleteWithUrl:url params:params success:success failure:failure notwork:notWork isLoading:NO isFailTip:NO];
        
    }
}
/**
 Convert 开始请求
 @param  type 请求的方式 post get put delete
 @param  url 请求链接
 @param  params 参数
 @param  success 成功回调
 @param  failure 失败回调
 @param  notWork 没有网络回调
 @param  isLoading 请求的loading
 @param  isFailTip 失败的提示
 */
+ (void)lz_request:(NSString *)type url:(NSString *)url params:(NSDictionary *)params success:(ResponseSuccess)success failure:(ResponseFailed)failure notwork:(ResponseNotWork)notWork isLoading:(BOOL)isLoading isFailTip:(BOOL)isFailTip{
    
    if([type.lowercaseString isEqualToString:@"get"]){
        [self lz_getWithUrl:url params:params success:success failure:failure notwork:notWork isLoading:isLoading isFailTip:isFailTip];
        
    } else if ([type.lowercaseString isEqualToString:@"post"]){
        
        [self lz_postWithUrl:url params:params success:success failure:failure notwork:notWork isLoading:isLoading isFailTip:isFailTip];
    } else if ([type.lowercaseString isEqualToString:@"put"]){
        
        [self lz_putWithUrl:url params:params success:success failure:failure notwork:notWork isLoading:isLoading isFailTip:isFailTip];
    } else if ([type.lowercaseString isEqualToString:@"delete"]){
        
        [self lz_deleteWithUrl:url params:params success:success failure:failure notwork:notWork isLoading:isLoading isFailTip:isFailTip];
    }
}



/**
 * Delete请求
 */
+ (void)lz_deleteWithUrl:(NSString *)url params:(NSDictionary *)params success:(ResponseSuccess)success failure:(ResponseFailed)failure notwork:(ResponseNotWork)notWork isLoading:(BOOL)isLoading isFailTip:(BOOL)isFailTip {
    
    if ([self getAutoLog]){
        LZLog(@"发送一个Delete请求 url--- %@  params--- %@",url,params);
    }
    
    //没有网络.
    if(notWork){
        if(!isNetWork){
            LZLog(@"网络异常，请检查您的网络");
            notWork([LZNetworkingManager returnNetworkError]);
            return;
        }
    }
    
    if(!isNetWork && isFailTip){
        LZLog(@"网络异常，请检查您的网络");
        if(failure){
            failure([LZNetworkingManager returnNetworkError]);
        };
        //来个没有网络的提示
        showDelayedDismissTitle(@"网络异常，请检查您的网络", nil);
        return;
    }
    //开始请求
    if(isLoading){
        //来个laoding
        showLoadingTitle(@"", nil);
    }

    
    NSURLSessionDataTask *dataTask = [[LZNetworkingManager sharedManager] DELETE:url parameters:params headers:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [[self allRequestTasks] removeObject:task];
        
        [self requestSuccess:responseObject success:success failure:failure notwork:notWork isLoading:isLoading isFailTip:isFailTip];

        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[self allRequestTasks] removeObject:task];

        //dismiss掉loading
        if(isLoading){
            dismiss(nil);
        }
        if ([self getAutoLog]){
            LZLog(@"请求失败 url--- %@  +++ error--- %@",url,error.localizedDescription);
        }
        
        [self tokenInvalid:@{}];
        
        if(isFailTip){
            dismiss(nil);
            // 这里服务器开小差
            if(![error.localizedDescription isEqualToString:@"Request failed: unauthorized (401)"]){
                showDelayedDismissTitle(LZLocalizedString(@"网络连接失败"), nil);
            }
        } else {
            if(failure){
                failure(error);
            }
        }
    }];
    
    [[self allRequestTasks] addObject:dataTask];
    
}


/**
 * Put请求
 */
+ (void)lz_putWithUrl:(NSString *)url params:(NSDictionary *)params success:(ResponseSuccess)success failure:(ResponseFailed)failure notwork:(ResponseNotWork)notWork isLoading:(BOOL)isLoading isFailTip:(BOOL)isFailTip {
    
    if ([self getAutoLog]){
        LZLog(@"发送一个Put请求 url--- %@  params--- %@",url,params);
    }
    
    //没有网络.
    if(notWork){
        if(!isNetWork){
            LZLog(@"网络异常，请检查您的网络");
            notWork([LZNetworkingManager returnNetworkError]);
            return;
        }
    }
    
    if(!isNetWork && isFailTip){
        LZLog(@"网络异常，请检查您的网络");
        if(failure){
            failure([LZNetworkingManager returnNetworkError]);
        };
        //来个没有网络的提示
        showDelayedDismissTitle(LZLocalizedString(@"网络不可用,请检查网络是否连接"), nil);
        return;
    }
    //开始请求
    if(isLoading){
        //来个laoding
        showLoadingTitle(@"", nil);
    }
    
    NSURLSessionDataTask *dataTask = [[LZNetworkingManager sharedManager] PUT:url parameters:params headers:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [[self allRequestTasks] removeObject:task];

        [self requestSuccess:responseObject success:success failure:failure notwork:notWork isLoading:isLoading isFailTip:isFailTip];

        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[self allRequestTasks] removeObject:task];

        //dismiss掉loading
        if(isLoading){
            dismiss(nil);
        }
        if ([self getAutoLog]){
            LZLog(@"请求失败 url--- %@  +++ error--- %@",url,error.localizedDescription);
        }
        
        [self tokenInvalid:@{}];
        
        if(isFailTip){
            dismiss(nil);
            // 这里服务器开小差
            if(![error.localizedDescription isEqualToString:@"Request failed: unauthorized (401)"]){
                showDelayedDismissTitle(LZLocalizedString(@"网络连接失败"), nil);
            }
        } else {
            if(failure){
                failure(error);
            }
        }
    }];
    
    [[self allRequestTasks] addObject:dataTask];

}



/**
 * Post请求
 */
+ (void)lz_postWithUrl:(NSString *)url params:(NSDictionary *)params success:(ResponseSuccess)success failure:(ResponseFailed)failure notwork:(ResponseNotWork)notWork isLoading:(BOOL)isLoading isFailTip:(BOOL)isFailTip {
    
    
    if ([self getAutoLog]){
              
        LZLog(@"params--- %@ url = %@",params, url);
        
    }
    
    //没有网络.
    if(notWork){
        if(!isNetWork){
            LZLog(@"网络异常，请检查您的网络");
            notWork([LZNetworkingManager returnNetworkError]);
            if(isFailTip){
                dismiss(nil);
                //来个没有网络的提示
                showDelayedDismissTitle(LZLocalizedString(@"网络不可用,请检查网络是否连接"), nil);
            }
            return;
        }
    }
    
    
    //开始请求
    if(isLoading){
        //来个laoding
        showLoadingTitle(@"", nil);
    }

    
    NSURLSessionDataTask *dataTask = [[LZNetworkingManager sharedManager] POST:url parameters:params headers:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [[self allRequestTasks] removeObject:task];
        
        [self requestSuccess:responseObject success:success failure:failure notwork:notWork isLoading:isLoading isFailTip:isFailTip];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[self allRequestTasks] removeObject:task];

        
        //dismiss掉loading
        if(isLoading){
            dismiss(nil);
        }
        if ([self getAutoLog]){
            LZLog(@"请求失败 url--- %@  +++ error--- %@",url,error.localizedDescription);
        }
        
        [self tokenInvalid:@{}];
    
        if(isFailTip){
            dismiss(nil);
            if([error.localizedDescription containsString:@"似乎已断开与互联网的连接"]
               ||[error.localizedDescription containsString:@"The request timed out"]
               || [error.localizedDescription containsString:@"请求超时"]
               ||[error.localizedDescription containsString:@"The Internet connection appears to be offline"]){
                
                showDelayedDismissTitle(LZLocalizedString(@"网络不可用,请检查网络是否连接"), nil);
            } else {
                showDelayedDismissTitle(LZLocalizedString(@"网络连接失败"), nil);
            }
        } else {
            if(failure){
                failure(error);
            }
        }
    }];
    
    [[self allRequestTasks] addObject:dataTask];
}


/**
 *  Get请求
 */
+ (void)lz_getWithUrl:(NSString *)url params:(NSDictionary *)params success:(ResponseSuccess)success failure:(ResponseFailed)failure notwork:(ResponseNotWork)notWork isLoading:(BOOL)isLoading isFailTip:(BOOL)isFailTip {
    
    if ([self getAutoLog]){
        LZLog(@"发送一个GET请求 url--- %@  params--- %@",url,params);
    }
    
    //没有网络.
    if(notWork){
        if(!isNetWork){
            LZLog(@"网络异常，请检查您的网络");
            notWork([LZNetworkingManager returnNetworkError]);
            if(isFailTip){
                dismiss(nil);
                //来个没有网络的提示
                showDelayedDismissTitle(LZLocalizedString(@"网络不可用,请检查网络是否连接"), nil);
            }
            return;
        }
    }
    
    //开始请求
    if(isLoading){
        //来个laoding
        showLoadingTitle(@"", nil);
    }
    
    
    NSURLSessionDataTask *dataTask = [[LZNetworkingManager sharedManager] GET:url parameters:params  headers:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [[self allRequestTasks] addObject:task];
        
        [self requestSuccess:responseObject success:success failure:failure notwork:notWork isLoading:isLoading isFailTip:isFailTip];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[self allRequestTasks] addObject:task];

        
        //dismiss掉loading
        if(isLoading){
            dismiss(nil);
        }
        if ([self getAutoLog]){
            LZLog(@"请求失败 url--- %@  +++ error--- %@",url,error.localizedDescription);
        }
        
        if(isFailTip){
            dismiss(nil);
            if([error.localizedDescription containsString:@"似乎已断开与互联网的连接"]
               ||[error.localizedDescription containsString:@"The request timed out"]
               || [error.localizedDescription containsString:@"请求超时"]
               ||[error.localizedDescription containsString:@"The Internet connection appears to be offline"]){
                
                showDelayedDismissTitle(LZLocalizedString(@"网络不可用,请检查网络是否连接"), nil);
            } else {
                showDelayedDismissTitle(LZLocalizedString(@"网络连接失败"), nil);
            }
            
            if(failure){
                failure(error);
            }
        } else {
            if(failure){
                failure(error);
            }
        }
        
        
    }];
    
    [[self allRequestTasks] addObject:dataTask];
}


//统一处理请求成功的数据
+ (void)requestSuccess:(NSDictionary *)responseObject success:(ResponseSuccess)success failure:(ResponseFailed)failure notwork:(ResponseNotWork)notWork isLoading:(BOOL)isLoading isFailTip:(BOOL)isFailTip{
    
    
    if(isLoading){
        dismiss(nil);
    }
    
    
    if ([responseObject isKindOfClass:[NSDictionary class]])
    {
        if ([self getAutoLog]){
            LZLog(@"请求成功 +++ success--- %@",responseObject);
        }
        NSDictionary *dictionary = (NSDictionary *)responseObject;
        if(success){
            success(dictionary);
        }
        if(isFailTip){
            NSString *resultDesc = [NSString stringWithFormat:@"%@",dictionary[kResultMessage]?:@""];
            if(resultDesc.length > 0){
                showDelayedDismissTitle(resultDesc, nil);
            }
        }
        [self tokenInvalid:dictionary];
    }else {
        if ([self getAutoLog]){
            LZLog(@"请求失败 +++ success--- %@",responseObject);
        }
        if (failure) {
            failure(nil);
        }
        showDelayedDismissTitle(LZLocalizedString(@"服务器开小差"), nil);
    }
}


/**
 Convert 开始上传文件
 @param  url 请求链接
 @param  params 参数
 @param  images 文件
 @param  fileName 文件名字
 @param  compressionRatio 进度
 @param  success 成功回调
 @param  failure 失败回调
 @param  notWork 没有网络回调
 */
+ (void)lz_postWithBaseUrl:(NSString *)url
                    params:(NSDictionary *)params
                    images:(NSArray *)images
                  fileName:(NSString* )fileName
          compressionRatio:(CGFloat )compressionRatio
                   success:(ResponseSuccess)success
                   failure:(ResponseFailed)failure
                   notwork:(ResponseNotWork)notWork{
    
    //没有网络.
    if(notWork){
        if(!isNetWork){
            LZLog(@"网络异常，请检查您的网络");
            notWork([LZNetworkingManager returnNetworkError]);
            dismiss(nil);
            //来个没有网络的提示
            showDelayedDismissTitle(LZLocalizedString(@"网络不可用,请检查网络是否连接"), nil);
            return;
        }
    }
    
    NSMutableDictionary *dataDictionary = [NSMutableDictionary dictionaryWithDictionary:params];
    if(k_userToken.length > 0){
        dataDictionary[@"Token"] = k_userToken;
    }
    
    
    params = dataDictionary;
    
    if ([self getAutoLog]){
        LZLog(@"发送一个POST请求 url--- %@ 解密后的参数params--- %@ \n上传到后台的数据params--- %@",url,params,params);
    }
    
    
    // images:图片对象数组
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"multipart/form-data", @"application/json", @"text/html", @"image/jpeg", @"image/png", @"application/octet-stream", @"text/json", nil];
    [manager POST:url parameters:params  headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        for (int i = 0; i < images.count; i++) {
            UIImage *image = images[i];
            NSData *imageData = UIImagePNGRepresentation(image);
            // 在网络开发中，上传文件时，是文件不允许被覆盖，文件重名
            // 要解决此问题，
            // 可以在上传时使用当前的系统事件作为文件名
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            // 设置时间格式
            [formatter setDateFormat:@"yyyyMMddHHmmss"];
            NSString *dateString = [formatter stringFromDate:[NSDate date]];
            NSString *fileName = [NSString  stringWithFormat:@"%@.png", dateString];
            /*
             *该方法的参数
             1. appendPartWithFileData：要上传的照片[二进制流]
             2. name：对应网站上[upload.php中]处理文件的字段（比如upload）
             3. fileName：要保存在服务器上的文件名
             4. mimeType：上传的文件的类型
             */
            [formData appendPartWithFileData:imageData name:@"fs" fileName:fileName mimeType:@"image/png"];
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        CGFloat progress = 100.0* uploadProgress.completedUnitCount/ uploadProgress.totalUnitCount;
        LZLog(@"上传进度 === %f",progress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self requestSuccess:responseObject success:success failure:failure notwork:notWork isLoading:NO isFailTip:NO];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        LZLog(@"上传失败%@",error);
        if (failure) {
            failure(error);
        }
    }];

    
    
    
}


///!!!:辅助工具函数
+ (NSMutableArray* )getFormDataArrByImageArr:(NSArray* )images fileName:(NSString* )name compressionRatio:(CGFloat)compressionRatio{
    NSMutableArray* formDataArray = [NSMutableArray array];
    for (int i = 0 ; i < images.count ; i ++){
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyyMMddHHmmss";
        NSString *str = [formatter stringFromDate:[NSDate date]];
        NSString *fileName = [NSString stringWithFormat:@"%@%u.jpg", str,arc4random_uniform(100000)];
        
        NSMutableDictionary *dataDictionary = [[NSMutableDictionary alloc] init];
        NSData *imageData = [NSData data];
        if ([[images objectAtIndex:i] isKindOfClass:[UIImage class]]){
            UIImage* image = [images objectAtIndex:i];
            imageData = UIImageJPEGRepresentation(image, compressionRatio);
        } else {
            NSData* data = [images objectAtIndex:i];
            imageData = data;
        }
        dataDictionary[@"data"] = imageData;
        dataDictionary[@"name"] = name;
        dataDictionary[@"filename"] = fileName;
        dataDictionary[@"mimeType"] = @"image/jpg";;
        [formDataArray addObject:dataDictionary];
    }
    return formDataArray;
}
//token失效
+ (void)tokenInvalid:(NSDictionary *)dictionary{
    
    if([dictionary isKindOfClass:[NSDictionary class]]){
        if([dictionary[@"Code"] isEqual:@(600)]){
//            [LZToolCache saveUserInfo:[LZUserDetailModel new]];
//            ZCLoginSelectVC *login = [[ZCLoginSelectVC alloc] init];
//            LZBaseNavigationController *loginNav = [[LZBaseNavigationController alloc] initWithRootViewController:login];
//            AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//            delegate.window.rootViewController = loginNav;
        }
    }
}

+ (NSError *)returnNetworkError {
    NSString * networkDescription = LZLocalizedString(@"网络不可用,请检查网络是否连接");
    NSDictionary * userInfo = [NSDictionary dictionaryWithObject:networkDescription forKey:NSLocalizedDescriptionKey];
    NSError *networkError = [NSError errorWithDomain:networkDescription code:404 userInfo:userInfo];
    return networkError;
}


#pragma mark - C 函数
extern void showDelayedDismissTitle(NSString *title, UIView *view)//自动消失的HUD
{
    if( !title ) title = @"";
    if( [title isKindOfClass:[NSNull class] ] ) title = @"" ;
    title =[NSString stringWithFormat:@"%@" , title];
    if (title.length == 0) {
        return;
    }

    if(!view){
        view = [[UIApplication sharedApplication] keyWindow];
    }

    
    
    if (![NSThread isMainThread]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            MBProgressHUD *hud = [MBProgressHUD showTitleToView:view contentStyle:NHHUDContentCustomStyle title:title];
            hud.label.font = PingFangSC(15);
            hud.contentColor = UIColor.whiteColor;
            hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
            hud.bezelView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES];
            });
            
        });
    }else{
        MBProgressHUD *hud = [MBProgressHUD showTitleToView:view contentStyle:NHHUDContentCustomStyle title:title];
        hud.label.font = PingFangSC(15);
        hud.contentColor = UIColor.whiteColor;;
        hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
        hud.bezelView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
        });
    }
    
}

void showLoadingTitle(NSString *title,UIView *view){//带转动的hud he文字
    if( !title ) title = @"";
    if( [title isKindOfClass:[NSNull class] ] ) title = @"" ;
    title =[NSString stringWithFormat:@"%@" , title];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.image = [UIImage imageNamed:@"icon_loading"];
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath: @"transform"];
    animation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    //围绕Z轴旋转，垂直与屏幕
    animation.toValue = [ NSValue valueWithCATransform3D:CATransform3DMakeRotation(M_PI/2.0, 0.0, 0.0, 1.0) ];
    animation.duration = 0.2;
    //旋转效果累计，先转180度，接着再旋转180度，从而实现360旋转
    animation.cumulative = YES;
    animation.repeatCount = MAXFLOAT;
    /*下面可以不需要
    CGRect imageRrect = CGRectMake(0, 0,imageView.imageView.frame.size.width, imageView.imageView.frame.size.height);
    UIGraphicsBeginImageContext(imageRrect.size);
    //在图片边缘添加一个像素的透明区域，去图片锯齿
    [imageView.currentImage drawInRect:CGRectMake(1,1,imageView.imageView.frame.size.width-2,imageView.imageView.frame.size.height-2)];
    [imageView setImage: UIGraphicsGetImageFromCurrentImageContext() forState:UIControlStateNormal];
    UIGraphicsEndImageContext();
    */
    [imageView.layer addAnimation:animation forKey:nil];
    
    if(!view){
        view = [[UIApplication sharedApplication] keyWindow];
    }
    
    if (![NSThread isMainThread]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
                   hud.mode = MBProgressHUDModeCustomView;
                   hud.minSize = CGSizeMake(55,55);//定义弹窗
                   hud.customView = imageView;
                   hud.animationType = MBProgressHUDAnimationFade;
                   hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
                   hud.bezelView.backgroundColor = [UIColor clearColor];

             });
    } else {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
          hud.mode = MBProgressHUDModeCustomView;
          hud.minSize = CGSizeMake(55,55);//定义弹窗
          hud.customView = imageView;
          hud.animationType = MBProgressHUDAnimationFade;
          hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
          hud.bezelView.backgroundColor = [UIColor clearColor];
    }
}

void dismiss(UIView *view){//消失
    if(!view){
        view = [[UIApplication sharedApplication] keyWindow];
    }
    [MBProgressHUD hideHUDForView:view animated:YES];
}

@end
