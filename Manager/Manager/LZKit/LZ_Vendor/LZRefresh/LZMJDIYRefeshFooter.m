//
//  LZMJDIYRefeshFooter.m
//  MTOWallert
//
//  Created by 吕召 on 2018/11/15.
//  Copyright © 2018 吕VV. All rights reserved.
//

#import "LZMJDIYRefeshFooter.h"

@interface LZMJDIYRefeshFooter()

/** 菊花的 */
@property (nonatomic, strong) UIActivityIndicatorView *loadingView;
@end

@implementation LZMJDIYRefeshFooter
#pragma mark - 懒加载子控件
- (UIActivityIndicatorView *)loadingView{
    if (!_loadingView) {
        _loadingView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    return _loadingView;
}



#pragma mark - 实现父类的方法
- (void)prepare
{
    [super prepare];
    self.loadingView.hidden = NO;
    [self.loadingView startAnimating];
    [self addSubview:self.loadingView];

    self.stateLabel.hidden = YES;
//    // 初始化文字
//    [self setTitle:LZLocalizedString(@"MJRefreshAutoFooterIdleText") forState:MJRefreshStateIdle];
//    [self setTitle:LZLocalizedString(@"MJRefreshAutoFooterRefreshingText") forState:MJRefreshStateRefreshing];
//    [self setTitle:LZLocalizedString(@"MJRefreshAutoFooterNoMoreDataText") forState:MJRefreshStateNoMoreData];
}

- (void)placeSubviews
{
   [super placeSubviews];
    self.loadingView.center = CGPointMake(self.mj_w * 0.5, self.mj_h * 0.5);
}

- (void)setState:(MJRefreshState)state
{
    MJRefreshCheckState
    // 根据状态做事情
    if(state == MJRefreshStateNoMoreData){
        self.stateLabel.hidden = NO;
        self.loadingView.hidden = YES;
        [self.loadingView stopAnimating];
        [self setTitle:@"无更多数据" forState:MJRefreshStateNoMoreData];
    } else {
        self.stateLabel.hidden = YES;
        self.loadingView.hidden = NO;
        [self.loadingView startAnimating];
    }
}

@end
