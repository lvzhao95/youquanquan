//
//  UIButton+LZ_Extension.m
//  MTOWallert
//
//  Created by 微微 on 2019/4/20.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import "UIButton+LZ_Extension.h"

@implementation UIButton (LZ_Extension)
+ (instancetype)lz_buttonTitle:(NSString *)title titleColor:(UIColor *)color fontSize:(CGFloat)fontSize {
    UIButton *button = [[self alloc] init];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:color forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:fontSize];
    [button sizeToFit];
    return button;
}
+ (instancetype)lz_buttonTitle:(NSString *)title titleColor:(UIColor *)color fontSize:(CGFloat)fontSize withBackgroundImageName:(NSString *)backgroundImageName {
    UIButton *button = [self lz_buttonTitle:title titleColor:color fontSize:fontSize];
    if (backgroundImageName != nil) {
        [button setBackgroundImage:[UIImage imageNamed:backgroundImageName] forState:UIControlStateNormal];
        NSString *backgroundImageNameHL = [backgroundImageName stringByAppendingString:@"_highlighted"];
        [button setBackgroundImage:[UIImage imageNamed:backgroundImageNameHL] forState:UIControlStateHighlighted];
    }
    return button;
}
+ (instancetype)lz_buttonImageName:(NSString *)imageName backgroundImageName:(NSString *)backgroundImageName {
    
    UIButton *button = [[self alloc] init];

    if (imageName.length > 0) {
        [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }
    
    if (backgroundImageName.length > 0) {
        NSString *imageNameHL = [imageName stringByAppendingString:@"_highlighted"];
        [button setImage:[UIImage imageNamed:imageNameHL] forState:UIControlStateHighlighted];
        [button setBackgroundImage:[UIImage imageNamed:backgroundImageName] forState:UIControlStateNormal];
        NSString *backgroundImageNameHL = [backgroundImageName stringByAppendingString:@"_highlighted"];
        [button setBackgroundImage:[UIImage imageNamed:backgroundImageNameHL] forState:UIControlStateHighlighted];
    }
   
    [button sizeToFit];
    return button;
}
@end
