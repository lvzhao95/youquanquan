//
//  UIButton+LZ_Extension.h
//  MTOWallert
//
//  Created by 微微 on 2019/4/20.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (LZ_Extension)
/**
 实例化Button
 
 @param title 标题文字
 @param color 文字颜色
 @param fontSize 文字大小
 @return UIButton
 */
+ (instancetype)lz_buttonTitle:(NSString *)title titleColor:(UIColor *)color fontSize:(CGFloat)fontSize;
/**
 实例化Button(带背景图)
 
 @param title 标题文字
 @param color 文字颜色
 @param fontSize 文字大小
 @param backgroundImageName 背景图片名称
 @return UIButton
 */
+ (instancetype)lz_buttonTitle:(NSString *)title titleColor:(UIColor *)color fontSize:(CGFloat)fontSize withBackgroundImageName:(NSString *)backgroundImageName;
/**
 实例化Button(图片+背景图)
 
 @param imageName 图片名
 @param backgroundImageName 背景图片名
 @return UIButton
 */
+ (instancetype)lz_buttonImageName:(NSString *)imageName backgroundImageName:(NSString *)backgroundImageName;
@end

NS_ASSUME_NONNULL_END
