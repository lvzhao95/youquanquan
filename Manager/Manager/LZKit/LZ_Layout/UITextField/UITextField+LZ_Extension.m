//
//  UITextField+LZ_Extension.m
//  DJVideo
//
//  Created by 微微 on 2019/2/25.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import "UITextField+LZ_Extension.h"
#include <objc/runtime.h>

@interface UITextField ()

@property (nonatomic, assign) BOOL lz_addNoti;

@property (nonatomic, copy) NSString *lz_lastTextStr;

@property (nonatomic, copy) void(^lz_textHandle) (NSString *textStr);

@end

@implementation UITextField (LZ_Extension)

- (void)setLz_placeholderColor:(UIColor *)lz_placeholderColor {
    
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.placeholder attributes:@{NSForegroundColorAttributeName:lz_placeholderColor}];

}

- (UIColor *)lz_placeholderColor {
    
    return [self valueForKeyPath:@"_placeholderLabel.textColor"];
}

- (void)setLz_addNoti:(BOOL)lz_addNoti {
    
    objc_setAssociatedObject(self, @selector(lz_addNoti), [NSNumber numberWithBool:lz_addNoti], OBJC_ASSOCIATION_ASSIGN);
}

- (BOOL)lz_addNoti {
    
    BOOL obj = [objc_getAssociatedObject(self, _cmd) boolValue];
    return obj;
}

- (void)setLz_maximumLimit:(NSInteger)lz_maximumLimit {
    
    objc_setAssociatedObject(self, @selector(lz_maximumLimit), @(lz_maximumLimit), OBJC_ASSOCIATION_ASSIGN);
    
    [self lz_fixMessyDisplay];
}

- (NSInteger)lz_maximumLimit {
    
    return [objc_getAssociatedObject(self, _cmd) integerValue];
}

- (void)setLz_textHandle:(void (^)(NSString *))lz_textHandle {
    
    objc_setAssociatedObject(self, @selector(lz_textHandle), lz_textHandle, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void (^)(NSString *))lz_textHandle {
    
    return objc_getAssociatedObject(self, @selector(lz_textHandle));
}

- (void)setLz_lastTextStr:(NSString *)lz_lastTextStr {
    
    objc_setAssociatedObject(self, @selector(lz_lastTextStr), lz_lastTextStr, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSString *)lz_lastTextStr {
    
    return [NSString lz_emptyStr:objc_getAssociatedObject(self, _cmd)];
}

/**
 *  监听文字改变
 */
- (void)lz_textDidChange {
    
    [self lz_characterTruncation];
}

- (void)lz_textDidChange:(void (^)(NSString * _Nonnull))handle {
    
    self.lz_textHandle = handle;
    [self lz_fixMessyDisplay];
}

- (void)lz_fixMessyDisplay {
    
    if(self.lz_maximumLimit <= 0) {self.lz_maximumLimit = MAXFLOAT;}
    [self lz_addTextChangeNoti];
}

- (void)lz_addTextChangeNoti {
    
    if(self.lz_addNoti == NO) {
        
        // 当UITextField的文字发生改变时，UITextField自己会发出一个UITextFieldTextDidChangeNotification通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(lz_textDidChange) name:UITextFieldTextDidChangeNotification object:nil];
    }
    self.lz_addNoti = YES;
}

- (NSString *)lz_characterTruncation {
    
    //字符截取
    if(self.lz_maximumLimit > 0) {
        
        UITextRange *selectedRange = [self markedTextRange];
        //获取高亮部分
        UITextPosition *position = [self positionFromPosition:selectedRange.start offset:0];
        //没有高亮选择的字，则对已输入的文字进行字数统计和限制,如果有高亮待选择的字，则暂不对文字进行统计和限制
        if ((position == nil) && (self.text.length > self.lz_maximumLimit)) {
            
            const char *res = [self.text substringToIndex:self.lz_maximumLimit].UTF8String;
            if (res == NULL) {
                self.text = [self.text substringToIndex:self.lz_maximumLimit - 1];
            }else{
                self.text = [self.text substringToIndex:self.lz_maximumLimit];
            }
        }
    }
    if((self.lz_textHandle) && (![self.text isEqualToString:self.lz_lastTextStr])) {
        
        self.lz_textHandle(self.text);
    }
    self.lz_lastTextStr = self.text;
    
    return self.text;
}

- (void)dealloc {
    
    if(self.lz_addNoti == YES) {
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
    }
}


@end
