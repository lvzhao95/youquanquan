//
//  UITextField+LZ_Extension.h
//  DJVideo
//
//  Created by 微微 on 2019/2/25.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextField (LZ_Extension)



/**
 *  占位文字颜色
 */
@property (nonatomic, strong) UIColor *lz_placeholderColor;

/**
 *  文本最大支持多少个字符，设置后会自动根据该属性截取文本字符长度
 */
@property (nonatomic, assign) NSInteger lz_maximumLimit;

/**
 *  文本发生改变时回调
 */
- (void)lz_textDidChange:(void(^)(NSString *textStr))handle;

/**
 *  处理系统输入法导致的乱码
 */
- (void)lz_fixMessyDisplay;



@end

NS_ASSUME_NONNULL_END
