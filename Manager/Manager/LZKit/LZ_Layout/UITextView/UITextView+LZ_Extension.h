//
//  UITextView+LZ_Extension.h
//  MTOWallert
//
//  Created by 微微 on 2019/4/20.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextView (LZ_Extension)
/**
 *  占位文字
 */
@property (nonatomic, copy) NSString *lz_placeholderStr;

/**
 *  占位文字字号
 */
@property (nonatomic, strong) UIFont *lz_placeholderFont;

/**
 *  占位文字颜色
 */
@property (nonatomic, strong) UIColor *lz_placeholderColor;

/**
 *  最大显示字符限制(会自动根据该属性截取文本字符长度)
 */
@property (nonatomic, assign) NSInteger lz_maximumLimit;

/**
 *  右下角字符长度提示(需要设置lz_maximumLimit属性)  默认NO
 */
@property (nonatomic, assign) BOOL lz_characterLengthPrompt;

/**
 *  文本发生改变时回调
 */
- (void)lz_textDidChange:(void(^)(NSString *textStr))handle;

- (void)lz_textDidChange;
/**
 *  处理系统输入法导致的乱码,如果调用了maximumLimit属性，内部会默认处理乱码
 */
- (void)lz_fixMessyDisplay;
@end

NS_ASSUME_NONNULL_END
