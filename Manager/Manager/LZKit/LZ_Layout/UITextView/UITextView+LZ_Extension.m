//
//  UITextView+LZ_Extension.m
//  MTOWallert
//
//  Created by 微微 on 2019/4/20.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import "UITextView+LZ_Extension.h"
#include <objc/runtime.h>

@interface UITextView ()

@property (nonatomic, assign) BOOL lz_addNoti;

@property (nonatomic, copy) NSString *lz_lastTextStr;

@property (nonatomic, copy) void(^lz_textHandle) (NSString *textStr);

@property (nonatomic, weak) UILabel *lz_placeholderLable;

@property (nonatomic, weak) UILabel *lz_charactersLengthLable;

@end

@implementation UITextView (LZ_Extension)

- (void)setLz_addNoti:(BOOL)lz_addNoti {
    
    objc_setAssociatedObject(self, &@selector(lz_addNoti), [NSNumber numberWithBool:lz_addNoti], OBJC_ASSOCIATION_ASSIGN);
}

- (BOOL)lz_addNoti {
    
    BOOL obj = [objc_getAssociatedObject(self, &@selector(lz_addNoti)) boolValue];
    return obj;
}

- (void)setLz_placeholderStr:(NSString *)lz_placeholderStr {
    
    objc_setAssociatedObject(self, &@selector(lz_placeholderStr), lz_placeholderStr, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    [self lz_fixMessyDisplay];
    self.lz_placeholderLable.backgroundColor = [UIColor clearColor];
}

- (NSString *)lz_placeholderStr {
    
    NSString *obj = objc_getAssociatedObject(self, &@selector(lz_placeholderStr));
    return obj;
}

- (void)setLz_placeholderColor:(UIColor *)lz_placeholderColor {
    
    objc_setAssociatedObject(self, &@selector(lz_placeholderColor), lz_placeholderColor, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    [self lz_fixMessyDisplay];
    self.lz_placeholderLable.backgroundColor = [UIColor clearColor];;
}

- (UIColor *)lz_placeholderColor {
    
    UIColor *obj = objc_getAssociatedObject(self, &@selector(lz_placeholderColor));
    return obj;
}

- (void)setLz_placeholderFont:(UIFont *)lz_placeholderFont {
    
    objc_setAssociatedObject(self, &@selector(lz_placeholderFont), lz_placeholderFont, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    self.lz_placeholderLable.backgroundColor = [UIColor clearColor];
}

- (UIFont *)lz_placeholderFont {
    
    UIFont *obj = objc_getAssociatedObject(self, &@selector(lz_placeholderFont));
    return obj;
}

- (void)setLz_maximumLimit:(NSInteger)lz_maximumLimit {
    
    objc_setAssociatedObject(self, &@selector(lz_maximumLimit), [NSNumber numberWithInteger:lz_maximumLimit], OBJC_ASSOCIATION_ASSIGN);
    [self lz_fixMessyDisplay];
}

- (NSInteger)lz_maximumLimit {
    
    id obj = objc_getAssociatedObject(self, &@selector(lz_maximumLimit));
    return [obj integerValue];
}

- (void)setLz_characterLengthPrompt:(BOOL)lz_characterLengthPrompt {
    
    objc_setAssociatedObject(self, &@selector(lz_characterLengthPrompt), [NSNumber numberWithBool:lz_characterLengthPrompt], OBJC_ASSOCIATION_ASSIGN);
    [self lz_fixMessyDisplay];
    
    self.height = (lz_characterLengthPrompt == YES) ? self.height-25 : self.height+25;
    self.lz_charactersLengthLable.text = [NSString stringWithFormat:@"%lu/%ld\t",(unsigned long)self.text.length > (long)self.lz_maximumLimit ? (long)self.lz_maximumLimit : (unsigned long)self.text.length ,(long)self.lz_maximumLimit];
    self.lz_charactersLengthLable.hidden = !lz_characterLengthPrompt;
}

- (BOOL)lz_characterLengthPrompt {
    
    id obj = objc_getAssociatedObject(self, &@selector(lz_characterLengthPrompt));
    return [obj boolValue];
}

- (UILabel *)lz_placeholderLable {
    
    UILabel *obj = objc_getAssociatedObject(self, @selector(lz_placeholderLable));
    if(obj == nil) {
        
        obj = [[UILabel alloc]init];
        obj.left = 5.0f;
        obj.top = 8.0f;
        obj.width = self.width-(obj.left*2);
        obj.numberOfLines = 0;
        obj.userInteractionEnabled = NO;
        [self insertSubview:obj atIndex:0];
        
        objc_setAssociatedObject(self, @selector(lz_placeholderLable), obj, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    
    obj.font = self.lz_placeholderFont ? self.lz_placeholderFont : self.font;
    obj.textColor = self.lz_placeholderColor ? self.lz_placeholderColor : [UIColor lightGrayColor];
    obj.text = self.lz_placeholderStr;
    [obj sizeToFit];
    
    return obj;
}

- (UILabel *)lz_charactersLengthLable {
    
    UILabel *obj = objc_getAssociatedObject(self, @selector(lz_charactersLengthLable));
    if(obj == nil) {
        
        obj = [[UILabel alloc]initWithFrame:CGRectMake(self.left, self.bottom, self.width, 25)];
        obj.backgroundColor = self.backgroundColor;
        obj.textAlignment = NSTextAlignmentRight;
        obj.userInteractionEnabled = YES;
        
        objc_setAssociatedObject(self, @selector(lz_charactersLengthLable), obj, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    
    obj.font = self.lz_placeholderFont ? self.lz_placeholderFont : self.font;
    obj.textColor = self.lz_placeholderColor ? self.lz_placeholderColor : [UIColor lightGrayColor];
    
    return obj;
}

- (void)setLz_textHandle:(void (^)(NSString *))lz_textHandle {
    
    objc_setAssociatedObject(self, &@selector(lz_textHandle), lz_textHandle, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void (^)(NSString *))lz_textHandle {
    
    id handle = objc_getAssociatedObject(self, &@selector(lz_textHandle));
    if (handle) {
        
        return (void(^)(NSString *textStr))handle;
    }
    return nil;
}

- (void)setLz_lastTextStr:(NSString *)lz_lastTextStr {
    
    objc_setAssociatedObject(self, @selector(lz_lastTextStr), lz_lastTextStr, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSString *)lz_lastTextStr {
    
    return [NSString lz_emptyStr:objc_getAssociatedObject(self, _cmd)];
}

- (void)lz_textDidChange:(void (^)(NSString * _Nonnull))handle {
    
    self.lz_textHandle = handle;
    [self lz_fixMessyDisplay];
}

- (void)lz_fixMessyDisplay {
    
    if(self.lz_maximumLimit <= 0) {self.lz_maximumLimit = MAXFLOAT;}
    [self lz_addTextChangeNoti];
}

- (void)lz_textDidChange {
    
    [self lz_characterTruncation];
}

- (void)lz_characterTruncation {
    
    //字符截取
    if(self.lz_maximumLimit > 0) {
        
        UITextRange *selectedRange = [self markedTextRange];
        //获取高亮部分
        UITextPosition *position = [self positionFromPosition:selectedRange.start offset:0];
        //没有高亮选择的字，则对已输入的文字进行字数统计和限制,如果有高亮待选择的字，则暂不对文字进行统计和限制
        if ((position == nil) && (self.text.length > self.lz_maximumLimit)) {
            
            const char *res = [self.text substringToIndex:self.lz_maximumLimit].UTF8String;
            if (res == NULL) {
                self.text = [self.text substringToIndex:self.lz_maximumLimit - 1];
            }else{
                self.text = [self.text substringToIndex:self.lz_maximumLimit];
            }
        }
    }
    
    if((self.lz_textHandle) && (![self.text isEqualToString:self.lz_lastTextStr])) {
        
        self.lz_textHandle(self.text);
    }
    self.lz_lastTextStr = self.text;
    
    self.lz_placeholderLable.hidden = (self.text.length > 0) ? YES : NO;
    self.lz_charactersLengthLable.text = [NSString stringWithFormat:@"%lu/%ld\t",(unsigned long)self.text.length > (long)self.lz_maximumLimit ? (long)self.lz_maximumLimit : (unsigned long)self.text.length ,(long)self.lz_maximumLimit];
}

- (void)lz_addTextChangeNoti {
    
    if(self.lz_addNoti == NO) {
        
        // 当UITextView的文字发生改变时，UITextView自己会发出一个UITextViewTextDidChangeNotification通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(lz_textDidChange) name:UITextViewTextDidChangeNotification object:nil];
    }
    self.lz_addNoti = YES;
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    if(self.lz_characterLengthPrompt == YES) {
        
        self.lz_charactersLengthLable.layer.borderWidth = self.layer.borderWidth;
        self.lz_charactersLengthLable.layer.borderColor = self.layer.borderColor;
        if(self.lz_charactersLengthLable.superview == nil) {
            [self.superview addSubview:self.lz_charactersLengthLable];
        }
    }
}

- (void)dealloc {
    
    if(self.lz_addNoti == YES) {
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextViewTextDidChangeNotification object:nil];
    }
}

@end
