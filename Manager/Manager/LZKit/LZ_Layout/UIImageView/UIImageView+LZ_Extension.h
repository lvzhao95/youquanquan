//
//  UIImageView+LZ_Extension.h
//  MTOWallert
//
//  Created by 微微 on 2019/4/20.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImageView (LZ_Extension)
/** 通过SDWebImage设置imageUrl */
@property (nonatomic, copy) NSString *lz_imageUrl;

/** 通过SDWebImage设置placeholderImage */
@property (nonatomic, strong) UIImage *lz_placeholderImage;

/** 通过SDWebImage设置imageUrl和placeholderImage */
- (void)lz_refreshImageUrl:(NSString *)imageUrl placeholderImage:(UIImage *)placeholderImage;

/** 创建imageView */
+ (UIImageView *)lz_createImageViewWithFrame:(CGRect)frame radius:(CGFloat)radius;

/** 创建本地图片imageView */
+ (UIImageView *)lz_createImageViewWithFrame:(CGRect)frame radius:(CGFloat)radius image:(UIImage *)image;
@end

NS_ASSUME_NONNULL_END
