//
//  UIImageView+LZ_Extension.m
//  MTOWallert
//
//  Created by 微微 on 2019/4/20.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import "UIImageView+LZ_Extension.h"
#include <objc/runtime.h>
#import "YYKit.h"

@implementation UIImageView (lz_Extension)

- (void)setLz_imageUrl:(NSString *)lz_imageUrl {
    
    [self setImageWithURL:[NSURL URLWithString:lz_imageUrl] placeholder:self.lz_placeholderImage];
    
    objc_setAssociatedObject(self, &@selector(lz_imageUrl), lz_imageUrl, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSString *)lz_imageUrl {
    
    NSString *obj = objc_getAssociatedObject(self, &@selector(lz_imageUrl));
    return obj;
}

- (void)setLz_placeholderImage:(UIImage *)lz_placeholderImage {
    
    [self setImageWithURL:[NSURL URLWithString:self.lz_imageUrl] placeholder:self.lz_placeholderImage];

    objc_setAssociatedObject(self, &@selector(lz_placeholderImage), lz_placeholderImage, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIImage *)lz_placeholderImage {
    
    UIImage *obj = objc_getAssociatedObject(self, &@selector(lz_placeholderImage));
    return obj;
}

- (void)lz_refreshImageUrl:(NSString *)imageUrl placeholderImage:(UIImage *)placeholderImage {
    
    self.lz_imageUrl = imageUrl;
    self.lz_placeholderImage = placeholderImage;
    [self setImageWithURL:[NSURL URLWithString:imageUrl] placeholder:placeholderImage];
}

+ (UIImageView *)lz_createImageViewWithFrame:(CGRect)frame radius:(CGFloat)radius {
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:frame];
    imageView.layer.cornerRadius = radius;
    imageView.clipsToBounds = YES;
    
    return imageView;
}

+ (UIImageView *)lz_createImageViewWithFrame:(CGRect)frame radius:(CGFloat)radius image:(nonnull UIImage *)image {
    
    UIImageView *imageView = [self lz_createImageViewWithFrame:frame radius:radius];
    imageView.image = image;
    
    return imageView;
}
@end
