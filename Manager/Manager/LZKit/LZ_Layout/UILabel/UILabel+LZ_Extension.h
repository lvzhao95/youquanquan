//
//  UILabel+LZ_Extension.h
//  MTOWallert
//
//  Created by 微微 on 2019/4/20.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (LZ_Extension)
/** 获取UILable的行高(根据UILable的字号获取的，系统默认字号：17) */
@property (nonatomic, assign, readonly) CGFloat lz_lineHeight;

/** 设置标签左对齐 */
- (void)lz_leftAlignment;

/** 设置标签中心对齐 */
- (void)lz_centerAlignment;

/** 设置标签右对齐 */
- (void)lz_rightAlignment;

/** 创建lable */
+ (UILabel *)lz_createLabWithFrame:(CGRect)frame textColor:(UIColor *)textColor font:(UIFont *)font;
+ (UILabel *)lz_createLabWithTextColor:(UIColor *)textColor font:(UIFont *)font;
+ (instancetype)lz_labelWithText:(NSString *)text fontSize:(UIFont *)font color:(UIColor *)color;

@end

NS_ASSUME_NONNULL_END
