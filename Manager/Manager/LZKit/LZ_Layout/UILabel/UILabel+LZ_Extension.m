//
//  UILabel+LZ_Extension.m
//  MTOWallert
//
//  Created by 微微 on 2019/4/20.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import "UILabel+LZ_Extension.h"

@implementation UILabel (LZ_Extension)
- (CGFloat)lz_lineHeight {return self.font.lineHeight;}

- (void)lz_leftAlignment {self.textAlignment = NSTextAlignmentLeft;}

- (void)lz_centerAlignment {self.textAlignment = NSTextAlignmentCenter;}

- (void)lz_rightAlignment {self.textAlignment = NSTextAlignmentRight;}

+ (UILabel *)lz_createLabWithFrame:(CGRect)frame textColor:(UIColor *)textColor font:(UIFont *)font {
    
    UILabel *lab = [[UILabel alloc]initWithFrame:frame];
    lab.textColor = textColor;
    lab.font = font;
    
    return lab;
}

+ (UILabel *)lz_createLabWithTextColor:(UIColor *)textColor font:(UIFont *)font {
    
    UILabel *lab = [[UILabel alloc]init];
    lab.textColor = textColor;
    lab.font = font;
    return lab;
}

+ (instancetype)lz_labelWithText:(NSString *)text fontSize:(UIFont *)font color:(UIColor *)color {
    UILabel *label = [[self alloc] init];
    label.text = text;
    label.font = font;
    label.textColor = color;
    label.numberOfLines = 0;
    return label;
}
@end
