//
//  LZ_API.h
//  ZeroCloud
//
//  Created by 微微 on 2019/5/13.
//  Copyright © 2019 吕VV. All rights reserved.
//

#ifndef LZ_API_h
#define LZ_API_h

#ifdef DEBUG



#define kBaseHttpRequestUrl @"http://www.youquanquan.cn/boot"
#else

#define kBaseHttpRequestUrl @"http://www.youquanquan.cn/boot"


#endif


#define k_ImageUrl(s) [NSString stringWithFormat:@"%@%@",kBaseHttpImgUrl,s]
#define LZString(...) [NSString stringWithFormat:__VA_ARGS__]
#define kCode    @"ok"
#define kResultStatus @"state"
#define kResultMessage @"msg"
#define kResultData @"data"



#define kPageSize @"50"

#define kResultRecords @"rows"
#define kPrecision 2

#define k_IsNoNetwork [error.localizedDescription containsString:@"似乎已断开与互联网的连接"] ||[error.localizedDescription containsString:@"The request timed out"] || [error.localizedDescription containsString:@"请求超时"]||[error.localizedDescription containsString:@"The Internet connection appears to be offline"]


//基础URL
#define kBaseRequestApi(des)                                LZString(@"%@%@",kBaseHttpRequestUrl,des)//

//项目API
///!!!: 用户相关
#define klogin                                          kBaseRequestApi(@"/admin/login")                            //登录




///!!!: 公共
#define kGetPayChannel                                  kBaseRequestApi(@"/admin/merchant/getPayChannel")                 //商户支付渠道
#define kMemberGetList                                  kBaseRequestApi(@"/admin/member/getList")                         //刷手成员

///!!!:工作台


///!!!:交易流水
#define kGetChargeList                                  kBaseRequestApi(@"/admin/merchant/getChargeList")                 //交易流水
#define kGetTradeList                                   kBaseRequestApi(@"/admin/merchant/getTradeList")                 //付款订单
#define kSummaryTransfer                                kBaseRequestApi(@"/admin/merchant/summaryTransfer")                 //付款订单详情
#define kNotifyCharge                                   kBaseRequestApi(@"/admin/merchant/notifyCharge")                 //回调
#define kNotifyChargeBatch                              kBaseRequestApi(@"/admin/merchant/notifyChargeBatch")                 //一键回调
#define kAuditTransfer                                  kBaseRequestApi(@"/admin/merchant/auditTransfer")                 //审核下发

///!!!:商户管理
#define kEditMerchant                                  kBaseRequestApi(@"/admin/merchant/editMerchant")                 //商户开户
#define kGetMerchantList                               kBaseRequestApi(@"/admin/merchant/getMerchantList")               //商户列表
#define kAuditMerchant                                 kBaseRequestApi(@"/admin/merchant/auditMerchant")                 //审核商户
#define kDisableMerchant                               kBaseRequestApi(@"/admin/merchant/disableMerchant")                 //禁用/启用
#define kDeleteMerchant                                kBaseRequestApi(@"/admin/merchant/deleteMerchant")                 //删除
#define kUpdateMerchantAmount                          kBaseRequestApi(@"/admin/merchant/updateMerchantAmount")                 //更新余额
#define kTransferAmount                                kBaseRequestApi(@"/admin/merchant/transferAmount")                 //操作金额
#define kOpenChannelToMerchant                         kBaseRequestApi(@"/admin/merchant/openChannelToMerchant")                 //开通通道
#define kGetMerchantChannel                            kBaseRequestApi(@"/admin/merchant/getMerchantChannel")                 //授权通到
#define kUpdateMerchantChannelStatus                   kBaseRequestApi(@"/admin/merchant/updateMerchantChannelStatus")                 //更新通道状态
#define kDeleteMerchantChannel                         kBaseRequestApi(@"/admin/merchant/deleteMerchantChannel")                 //删除通道状态
#define kSaveMerchantChannel                         kBaseRequestApi(@"/admin/merchant/saveMerchantChannel")                 //保存费率

///!!!:财务分析
#define kGetSummaryByMerchant                           kBaseRequestApi(@"/admin/summary/getSummaryByMerchant")                 //商户交易统计
#define kGetSummaryByMerchantMemberAgent                kBaseRequestApi(@"/admin/summary/getSummaryByMerchantMemberAgent")                 //付款订单详情
#define kGetSummaryByMerchantMemberAgentMember          kBaseRequestApi(@"/admin/summary/getSummaryByMerchantMemberAgentMember")                 //付款订单详情
#define kGetChannelSummary                              kBaseRequestApi(@"/admin/summary/getChannelSummary")                 //通道分析


///!!!:通道管理
#define kGetParentChannel                               kBaseRequestApi(@"/admin/channel/getParentChannel")                 //
#define kGetPageChannel                                 kBaseRequestApi(@"/admin/channel/getPageChannel")                   //
#define kChannelCreate                                  kBaseRequestApi(@"/admin/channel/create")                           //新增付款
#define kChannelUpdate                                  kBaseRequestApi(@"/admin/channel/update")                           //更新付款
#define kDisableMerchantChannel                         kBaseRequestApi(@"/admin/channel/disableMerchantChannel")                           //一键禁用.启动
#define kSetChannelStatus                               kBaseRequestApi(@"/admin/channel/setChannelStatus")                           //禁用.启动
#define kDeleteChannel                                  kBaseRequestApi(@"/admin/channel/deleteChannel")                           //删除


///!!!:刷手管理
#define kGetMemberPage                                 kBaseRequestApi(@"/admin/member/getMemberPage")                   //
#define kGetMemberCollectionPage                        kBaseRequestApi(@"/admin/member/getMemberCollectionPage")                   //
#define kCreateMember                                   kBaseRequestApi(@"/admin/member/createMember")                   // 创建成员


///!!!:收益
#define kSummaryCharge                                  kBaseRequestApi(@"/admin/merchant/summaryCharge")                 //收益

///!!!:拼多多通道管理
#define kAccountList                                    kBaseRequestApi(@"/admin/othAccount/getOthAccountList")                 //商户管理
#define kAccountTotal                                   kBaseRequestApi(@"/admin/othAccount/getOthAccountTotal")                 //总剩余额度
#define kGetOthGoodsList                                   kBaseRequestApi(@"/admin/othGoods/getOthGoodsList")                 //商品管理
#define kGetOthChargeTotal                                   kBaseRequestApi(@"/admin/othCharge/getOthChargeTotal")                 //订单管理
#define kGetOthChargeList                                   kBaseRequestApi(@"/admin/othCharge/getOthChargeList")                 //订单管理



///!!!:我的
#define kGetUserInfo                                    kBaseRequestApi(@"/admin/user/getInfo")                 //个人信息
#define kGetUserLogout                                  kBaseRequestApi(@"/admin/user/logout")                 //登出

/**
 http://www.youquanquan.cn/boot/admin/othCharge/getOthChargeTotal
 http://www.youquanquan.cn/boot/admin/othAccount/getOthAccountList
 http://www.youquanquan.cn/boot/admin/channel/getPageChannel
 http://www.youquanquan.cn/boot/admin/summary/getSummaryByMerchant
 http://www.youquanquan.cn/boot/admin/merchant/getMerchantList
 http://www.youquanquan.cn/boot/admin/merchant/getMerchantList
 http://www.youquanquan.cn/boot/admin/merchant/getPayChannel
 http://www.youquanquan.cn/boot/admin/member/getList
 http://www.youquanquan.cn/boot/admin/merchant/getMerchantList
 http://www.youquanquan.cn/boot/admin/merchant/summaryCharge
 http://www.youquanquan.cn/boot/admin/merchant/getMerchantList
 http://www.youquanquan.cn/boot/admin/merchant/getPayChannel
 */
#endif /* LZ_API_h */
