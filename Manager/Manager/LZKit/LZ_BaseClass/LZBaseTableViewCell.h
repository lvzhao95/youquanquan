//
//  LZBaseTableViewCell.h
//  ZeroCloud
//
//  Created by 微微 on 2019/5/13.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LZBaseTableViewCell : UITableViewCell
@property (nonatomic,strong) UIView *bottomLineView;

@property (nonatomic,strong) UIImageView *rightImageView;

@property (nonatomic,strong) id modelObject;

@property (nonatomic,strong) NSIndexPath *indexPath;
@end

NS_ASSUME_NONNULL_END
