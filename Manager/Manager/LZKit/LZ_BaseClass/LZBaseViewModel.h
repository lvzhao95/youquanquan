//
//  LZBaseViewModel.h
//  ZeroCloud
//
//  Created by 微微 on 2019/5/13.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LZBaseViewModel : NSObject
///数据源.
@property (nonatomic, strong)NSMutableArray *dataArray;

@property (nonatomic, weak) UIView *target;

//事件回调
@property (nonatomic,strong) RACSubject *actionSubject;

/// 返回的number 如果是-1请求成功但是无数据, 0没有更多数据.  1.是有数据.  -2 请求失败手机无网络, -3请求失败服务器问题.
@property (nonatomic, strong)RACSubject *reloadSubject;

@end

NS_ASSUME_NONNULL_END
