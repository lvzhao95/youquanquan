//
//  LZBaseViewController.m
//  ZeroCloud
//
//  Created by 微微 on 2019/5/13.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import "LZBaseViewController.h"

@interface LZBaseViewController ()

@end

@implementation LZBaseViewController

- (void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    LZLog(@"%@释放了",self.class);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.view.backgroundColor = COLOR_background;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.navigationController.viewControllers.count != 1) {
        [self showCustomNavigationBackButton];
    }
    
    NSLog(@"\n\n🤠当前视图：%@😎\n\n", NSStringFromClass([self class]));

}

//自定义back按钮
- (void)showCustomNavigationBackButton{
    
    UIImage * image       = k_imageName(@"ico_back");
    UIImage * selectedImg = k_imageName(@"ico_back");
    UIButton *backBtn= [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 44)];
    [backBtn addTarget:self action:@selector(onBackButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setImage:image forState:UIControlStateNormal];
    [backBtn setImage:selectedImg forState:UIControlStateSelected];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    backBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [backBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 5)];
}

- (void)setHidesBottomBarWhenPushed:(BOOL)hidesBottomBarWhenPushed{
    [super setHidesBottomBarWhenPushed:YES];
}



- (void)onBackButtonAction:(UIBarButtonItem *)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

//0:leftBarButtonItems,1:rightBarButtonItems
- (void)initBarItem:(UIView*)view withType:(int)type{
    //解决按钮不靠左 靠右的问题.iOS 11系统需要单独处理
    switch (type) {
        case 0:
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:view];
            
            break;
        case 1:
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:view];
            break;
            
        default:
            break;
    }
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    
    return UIStatusBarStyleDefault;// UIStatusBarStyleLightContent : ;
}


@end
