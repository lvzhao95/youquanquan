//
//  LZBaseViewController.h
//  ZeroCloud
//
//  Created by 微微 on 2019/5/13.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LZBaseViewController : UIViewController

- (void)initBarItem:(UIView*)view withType:(int)type;


@end




NS_ASSUME_NONNULL_END
