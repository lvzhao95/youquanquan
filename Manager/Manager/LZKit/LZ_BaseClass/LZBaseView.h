//
//  LZBaseView.h
//  ZeroCloud
//
//  Created by 微微 on 2019/5/13.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LZBaseViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LZBaseView : UIView
- (instancetype)initViewModel:(LZBaseViewModel *)viewModel;

@end

NS_ASSUME_NONNULL_END
