//
//  LZBaseTabBarViewController.m
//  ZeroCloud
//
//  Created by 微微 on 2019/5/13.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import "LZBaseTabBarViewController.h"
#import "LZBaseNavigationController.h"





@interface LZBaseTabBarViewController ()<UITabBarControllerDelegate>

@end

@implementation LZBaseTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBar.translucent = NO;
    
    [[UITabBar appearance] setBarTintColor:COLOR_tabbar];
    //字体大小，颜色（未被选中时）
    [[UITabBarItem appearance]setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:COLOR_tabbarUnSeleText,
                                                      NSForegroundColorAttributeName,PingFangSC(11),NSFontAttributeName,nil]forState:UIControlStateNormal];
    
    
    //字体大小，颜色（选中时）
    [[UITabBarItem appearance]setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:COLOR_tabbarSeleText,
                                                      NSForegroundColorAttributeName,PingFangSC_M(11),NSFontAttributeName,nil]forState:UIControlStateSelected];
    
    if(K_Device_Is_iPhoneX){
        //字体与图片到的间隔
        [[UITabBarItem appearance] setTitlePositionAdjustment:UIOffsetMake(0, 1)];
    } else {
        //字体与图片到的间隔
        [[UITabBarItem appearance] setTitlePositionAdjustment:UIOffsetMake(0, -2)];
    }
    
    self.delegate = self;
    //添加子控制器
    [self addChilds];
    
}
 
//添加子视图
- (void)addChilds{
    
    NSArray *classControllers = [NSArray array];
    classControllers = @[@"PMEarningViewController", @"PMWorkViewController",@"PMAdminViewController"];
    NSArray *normalImages = @[@"pm_tabbar_earning_unselect",@"pm_tabbar_work_unselect", @"pm_tabbar_admin_unselect"];
    NSArray *titles = @[LZLocalizedString(@"收益"),LZLocalizedString(@"工作台"),@"管理员"];
    
    [self creteTabbarControllerAddSubViewsControllers:classControllers addTitlesy:titles addNormalImages:normalImages];
}

# pragma mark - 初始化Tabbar里面的元素
- (void)creteTabbarControllerAddSubViewsControllers:(NSArray *)classControllers addTitlesy:(NSArray *)titles addNormalImages:(NSArray *)normalImages{
    NSMutableArray *conArr = [NSMutableArray array];
    for (int i = 0; i < classControllers.count; i++) {
        Class cts = NSClassFromString(classControllers[i]);
        UIViewController *vc = [[cts alloc] init];
        LZBaseNavigationController *naVC = [[LZBaseNavigationController alloc] initWithRootViewController:vc];
        [conArr addObject:naVC];
        vc.title = titles[i];
        NSString *norImageName = normalImages[i];
        NSString *selectImageName = [norImageName stringByReplacingOccurrencesOfString:@"_un" withString:@"_"];
        UIImage *normalImage = [k_imageName(norImageName) imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        UIImage *selectImage = [k_imageName(selectImageName) imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem = [[UITabBarItem alloc] initWithTitle:titles[i] image:normalImage selectedImage:selectImage];
    }
    
    self.viewControllers = conArr;
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
    return YES;
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    
    
}





@end
