//
//  LZBaseViewModel.m
//  ZeroCloud
//
//  Created by 微微 on 2019/5/13.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import "LZBaseViewModel.h"

@implementation LZBaseViewModel
- (NSMutableArray *)dataArray{
    
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}

//创建信号
- (RACSubject *)reloadSubject{
    if(!_reloadSubject){
        _reloadSubject = [RACSubject subject];
    }
    return _reloadSubject;
}

- (RACSubject *)actionSubject{
    if(!_actionSubject){
        _actionSubject = [RACSubject subject];
    }
    return _actionSubject;
}




@end
