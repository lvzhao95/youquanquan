//
//  LZBaseNavigationController.m
//  ZeroCloud
//
//  Created by 微微 on 2019/5/13.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import "LZBaseNavigationController.h"

@interface LZBaseNavigationController ()<UIGestureRecognizerDelegate,UINavigationControllerDelegate>

@end

@implementation LZBaseNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.interactivePopGestureRecognizer.delegate = self;
    self.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationBar.barTintColor = COLOR_nav;
    [self.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],
                                                 NSFontAttributeName:[UIFont boldSystemFontOfSize:17]}];
    self.navigationBar.translucent = NO;
    self.delegate = self;
    [self.navigationBar setShadowImage:[UIImage new]];
}

//隐藏tabbar hidesBottomBarWhenPushed
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
    if ([self.viewControllers count] > 0) {
        viewController.hidesBottomBarWhenPushed  = YES;
    }
    [super pushViewController:viewController animated:animated];
    
    //    适配iphone X
    //     修改tabBra的frame
    CGRect frame = self.tabBarController.tabBar.frame;
    frame.origin.y = [UIScreen mainScreen].bounds.size.height - frame.size.height;
    self.tabBarController.tabBar.frame = frame;
}

- (UIViewController *)popViewControllerAnimated:(BOOL)animated{
    
    return [super popViewControllerAnimated:animated];
}


#pragma mark - Gesture Recognizer
-(BOOL)gestureRecognizer:(UIPanGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ([touch.view isKindOfClass:[UISlider class]]) {
        return NO;
    }
    if(self.childViewControllers.count == 1)
    {
        return NO;
    } else {
        return YES;
    }
}

#pragma mark - UINavigationControllerDelegate
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
    NSArray * hiddenNavs = @[@"ZCHomeViewController",
                             @"ZCAssetsViewController",
                             @"SWQRCodeViewController",
                             @"ZCMineViewController",
                             @"ZCLoginSelectVC",
                             @"ZCInvitePosterVC",
                             @"ZCProductDetailViewController"];
    NSString * vcString = NSStringFromClass([viewController class]);
    BOOL isHiddden = NO;
    if([hiddenNavs containsObject:vcString]){
        isHiddden = YES;
    }
    [self setNavigationBarHidden:isHiddden animated:YES];
}

- (UIViewController *)childViewControllerForStatusBarStyle{
    return self.topViewController;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
