//
//  LZBaseTableView.m
//  ZeroCloud
//
//  Created by 微微 on 2019/5/13.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import "LZBaseTableView.h"
#import "LZMJDIYRefreshHeader.h"
#import "LZMJDIYRefeshFooter.h"
@implementation LZBaseTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    self = [super initWithFrame:frame style:style];
    if (self) {
        self.estimatedRowHeight = 0;
        self.estimatedSectionFooterHeight = 0;
        self.estimatedSectionHeaderHeight = 0;
        if (@available(iOS 11.0, *)) {
            self.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentAutomatic;
        }
    }
    return self;
}

- (void)addHeadWithRefreshingBlock:(MJRefreshComponentAction)refreshingBlock{
    @weakify(self)
    LZMJDIYRefreshHeader *header = [LZMJDIYRefreshHeader headerWithRefreshingBlock:^{
        @strongify(self)
        if(self.mj_footer.isRefreshing){
            [self.mj_footer endRefreshing];
        }
//        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        if (refreshingBlock) {
            refreshingBlock();
        }
    }];
    self.mj_header = header;
}

- (void)addAutoNormalFooterWithRefreshingBlock:(MJRefreshComponentAction)refreshingBlock{
    @weakify(self)
    LZMJDIYRefeshFooter *footer = [LZMJDIYRefeshFooter footerWithRefreshingBlock:^{
        @strongify(self)
        if(self.mj_header.isRefreshing){
            [self.mj_header endRefreshing];
        }
        if (refreshingBlock) {
            refreshingBlock();
        }
    }];
    
    self.mj_footer = footer;
    
}
@end
