//
//  LZBaseTableViewCell.m
//  ZeroCloud
//
//  Created by 微微 on 2019/5/13.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import "LZBaseTableViewCell.h"

@implementation LZBaseTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.textLabel.hidden = YES;
        self.detailTextLabel.hidden = YES;
        self.imageView.hidden = YES;
        
        [self.contentView addSubview:self.bottomLineView];
        
        [self.contentView addSubview:self.rightImageView];
        [self.bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(15);
            make.width.mas_equalTo(K_SCREENWIDTH-15);
            make.bottom.mas_equalTo(0);
            make.height.mas_equalTo(0.5);
        }];
        UIImage *image = [UIImage imageNamed:@"pm_next"];
        self.rightImageView.image = image;
        [self.rightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo (-15);
            make.size.mas_equalTo(CGSizeMake(image.size.width, image.size.height));
            make.centerY.mas_equalTo(self.mas_centerY);
        }];
        
    }
    return self;
}


- (void)setIndexPath:(NSIndexPath *)indexPath{
    _indexPath = indexPath;
}

- (UIView *)bottomLineView{
    if(!_bottomLineView){
        _bottomLineView = [[UIView alloc]init];
        _bottomLineView.hidden = YES;
        _bottomLineView.backgroundColor = COLOR_cellLine;
    }
    return _bottomLineView;
}

- (UIImageView *)rightImageView{
    if(!_rightImageView){
        _rightImageView = [[UIImageView alloc]init];
        _rightImageView.hidden = YES;
    }
    return _rightImageView;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
