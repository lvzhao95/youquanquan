//
//  LZUserDetailModel.h
//  ZeroCloud
//
//  Created by 微微 on 2019/5/14.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>



NS_ASSUME_NONNULL_BEGIN

@interface LZUserDetailModel : NSObject

/**

 */
@property (nonatomic, copy) NSString *token;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *userId;





@property (nonatomic, copy) NSString *EnCode;
@property (nonatomic, copy) NSString *RecEnCode;
@property (nonatomic, copy) NSString *IDCard;
@property (nonatomic, copy) NSString *Mobile;

@property (nonatomic, copy) NSString *HeadIcon;
@property (nonatomic, assign) NSInteger AuditMark;//认证状态(0-未认证、1-待认证、2-已认证、3-认证失败)
@property (nonatomic, copy) NSString *RealName;
@property (nonatomic, copy) NSString *AreaCode;

@end

NS_ASSUME_NONNULL_END
