//
//  LZUserDetailModel.m
//  ZeroCloud
//
//  Created by 微微 on 2019/5/14.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import "LZUserDetailModel.h"

@implementation LZUserDetailModel


MJCodingImplementation

+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"ID":@"id"};
}

@end
