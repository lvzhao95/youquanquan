//
//  UIColor+LZ_Extension.h
//  MTOWallert
//
//  Created by 微微 on 2019/4/20.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (LZ_Extension)
/**
 实例化(16进制数字)
 
 @param hex 16进制无符号32位整数
 @return 颜色
 */
+ (instancetype)lz_colorWithHex:(uint32_t)hex;
/**
 实例化(RGB)
 
 @param red 红
 @param green 绿
 @param blue 蓝
 @return 颜色
 */
+ (instancetype)lz_colorWithRed:(uint8_t)red green:(uint8_t)green blue:(uint8_t)blue;
/**
 实例化(随机颜色)
 
 @return 颜色
 */
+ (instancetype)lz_randomColor;
/**
 实例化(可调节亮度)
 
 @param color 颜色
 @param delta 亮度
 @return 颜色
 */
+ (UIColor *)lz_colorRGBonvertToHSB:(UIColor *)color withBrighnessDelta:(CGFloat)delta;
/**
 实例化(可调节透明度)
 
 @param color 颜色
 @param delta 透明度
 @return 颜色
 */
+ (UIColor *)lz_colorRGBonvertToHSB:(UIColor *)color withAlphaDelta:(CGFloat)delta;
@end

NS_ASSUME_NONNULL_END
