//
//  NSAttributedString+LZ_Extension.h
//  VVBTC
//
//  Created by 微微 on 2019/7/4.
//  Copyright © 2019  吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSAttributedString (LZ_Extension)
+(NSMutableAttributedString *)getAttributedStringWithString:(NSString *)string lineSpace:(CGFloat)lineSpace alignment:(NSTextAlignment)alignment;

+ (NSMutableAttributedString *)attributedString:(NSString *)title rangeTitle:(NSString *)rangeTitle leftFont:(UIFont *)leftFont rightFont:(UIFont *)rightFont leftColor:(UIColor *)leftColor rightColor:(UIColor *)rightColor;
@end

NS_ASSUME_NONNULL_END
