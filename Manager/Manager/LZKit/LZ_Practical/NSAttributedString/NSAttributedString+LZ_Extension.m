//
//  NSAttributedString+LZ_Extension.m
//  VVBTC
//
//  Created by 微微 on 2019/7/4.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import "NSAttributedString+LZ_Extension.h"

@implementation NSAttributedString (LZ_Extension)
+(NSMutableAttributedString *)getAttributedStringWithString:(NSString *)string lineSpace:(CGFloat)lineSpace alignment:(NSTextAlignment)alignment
{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = lineSpace; // 调整行间距
    NSRange range = NSMakeRange(0, [string length]);
    paragraphStyle.alignment = alignment; //居右
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    return attributedString;
}

+ (NSMutableAttributedString *)attributedString:(NSString *)title rangeTitle:(NSString *)rangeTitle leftFont:(UIFont *)leftFont rightFont:(UIFont *)rightFont leftColor:(UIColor *)leftColor rightColor:(UIColor *)rightColor{
    if(title.length == 0 || !rangeTitle){
        return [NSMutableAttributedString new];
    }
    
    NSMutableAttributedString * str = [[NSMutableAttributedString alloc]initWithString:title];
    
    NSDictionary *dic = @{NSFontAttributeName:rightFont,NSForegroundColorAttributeName:rightColor};
    NSRange range = [title rangeOfString:rangeTitle options:NSBackwardsSearch];
    [str addAttributes:dic range:range];
    
    NSString *surplusString = [title stringByReplacingOccurrencesOfString:rangeTitle withString:@""];
    NSDictionary *surplusDic = @{NSFontAttributeName:leftFont,NSForegroundColorAttributeName:leftColor};
    NSRange surplusRange = [title rangeOfString:surplusString options:NSBackwardsSearch];
    [str addAttributes:surplusDic range:surplusRange];
    
    return str;
    
}
@end
