//
//  NSObject+LZ_Extension.m
//  MTOWallert
//
//  Created by 微微 on 2019/4/20.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import "NSObject+LZ_Extension.h"

@implementation NSObject (LZ_Extension)
//检索字符串是否为空，是则返回:@""
- (NSString *)lz_checkNULLString:(NSString *_Nullable)oriString{
    NSString *string = @"";
    if( !oriString ) return string;
    if( [oriString isKindOfClass:[NSNull class] ] ) return string ;
    string =[NSString stringWithFormat:@"%@" , oriString ];
    
    return string ;
}


//检索字符串是否为空
- (BOOL)lz_isBlankString:(NSString *)string {
    if (string == nil || string == NULL) {
        return YES;
    }
    if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
    return NO;
}
@end
