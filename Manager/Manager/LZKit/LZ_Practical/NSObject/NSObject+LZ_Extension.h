//
//  NSObject+LZ_Extension.h
//  MTOWallert
//
//  Created by 微微 on 2019/4/20.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (LZ_Extension)
//检索字符串是否为空，是则返回:@""
- (NSString *)lz_checkNULLString:(NSString *_Nullable)oriString;

//检索字符串是否为空
- (BOOL)lz_isBlankString:(NSString *)string;
@end

NS_ASSUME_NONNULL_END
