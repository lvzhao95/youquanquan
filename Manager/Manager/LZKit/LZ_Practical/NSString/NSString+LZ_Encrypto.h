//
//  NSString+LZ_Encrypto.h
//  Test
//
//  Created by 微微 on 2019/1/16.
//  Copyright © 2019 铭创科技. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (LZ_Encrypto)
- (NSString *)sha1;
- (NSString *)md5;

@end

NS_ASSUME_NONNULL_END
