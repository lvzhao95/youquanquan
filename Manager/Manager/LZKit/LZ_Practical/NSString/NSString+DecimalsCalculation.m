//
//  NSString+DecimalsCalculation.m
//  YWDecimalsCalculation
//
//  Created by FishYu on 16/11/4.
//  Copyright © 2016年 codeFisher. All rights reserved.
//

#import "NSString+DecimalsCalculation.h"

// CalculationType
typedef NS_ENUM(NSInteger,CalculationType){
    CalculationAdding,
    CalculationSubtracting,
    CalculationMultiplying,
    CalculationDividing,
};

@implementation NSString (DecimalsCalculation)

// Adding
- (NSString *)yw_stringByAdding:(NSString *)stringNumber {
    return [self yw_stringByAdding:stringNumber withRoundingMode:NSRoundPlain scale:kPrecision];
}
- (NSString *)yw_stringByAdding:(NSString *)stringNumber withRoundingMode:(NSRoundingMode)roundingModel {
    return [self yw_stringByAdding:stringNumber withRoundingMode:roundingModel scale:kPrecision];
}
- (NSString *)yw_stringByAdding:(NSString *)stringNumber withRoundingMode:(NSRoundingMode)roundingModel scale:(NSInteger)scale {
    return [self _stringByCalculationType:CalculationAdding by:stringNumber roundingMode:roundingModel scale:scale];
}

// Substracting
- (NSString *)yw_stringBySubtracting:(NSString *)stringNumber {
    return [self  yw_stringBySubtracting:stringNumber withRoundingMode:NSRoundPlain scale:kPrecision];
}
- (NSString *)yw_stringBySubtracting:(NSString *)stringNumber withRoundingMode:(NSRoundingMode)roundingModel {
    return [self  yw_stringBySubtracting:stringNumber withRoundingMode:roundingModel scale:kPrecision];
}
- (NSString *)yw_stringBySubtracting:(NSString *)stringNumber withRoundingMode:(NSRoundingMode)roundingModel scale:(NSInteger)scale {
    return [self _stringByCalculationType:CalculationSubtracting by:stringNumber roundingMode:roundingModel scale:scale];
}

// Multiplying
- (NSString *)yw_stringByMultiplyingBy:(NSString *)stringNumber {
    return [self yw_stringByMultiplyingBy:stringNumber withRoundingMode:NSRoundPlain scale:kPrecision];
}
- (NSString *)yw_stringByMultiplyingBy:(NSString *)stringNumber withRoundingMode:(NSRoundingMode)roundingModel {
    return [self yw_stringByMultiplyingBy:stringNumber withRoundingMode:roundingModel scale:kPrecision];
}
- (NSString *)yw_stringByMultiplyingBy:(NSString *)stringNumber withRoundingMode:(NSRoundingMode)roundingModel scale:(NSInteger)scale {
    return [self _stringByCalculationType:CalculationMultiplying by:stringNumber roundingMode:roundingModel scale:scale];
}

// Dividing
- (NSString *)yw_stringByDividingBy:(NSString *)stringNumber {
    return [self yw_stringByDividingBy:stringNumber withRoundingMode:NSRoundPlain scale:kPrecision];
}
- (NSString *)yw_stringByDividingBy:(NSString *)stringNumber withRoundingMode:(NSRoundingMode)roundingModel {
    return [self yw_stringByDividingBy:stringNumber withRoundingMode:roundingModel scale:kPrecision];
}
- (NSString *)yw_stringByDividingBy:(NSString *)stringNumber withRoundingMode:(NSRoundingMode)roundingModel scale:(NSInteger)scale {
    return [self _stringByCalculationType:CalculationDividing by:stringNumber roundingMode:roundingModel scale:scale];
}


- (NSString *)_stringByCalculationType:(CalculationType)type by:(NSString *)stringNumber roundingMode:(NSRoundingMode)roundingModel scale:(NSUInteger)scale{
    
    NSDecimalNumber *selfNumber = [NSDecimalNumber decimalNumberWithString:self];
    NSDecimalNumber *calcuationNumber = [NSDecimalNumber decimalNumberWithString:stringNumber];
    NSDecimalNumberHandler *handler = [[NSDecimalNumberHandler alloc] initWithRoundingMode:roundingModel scale:scale raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:YES];
    
    NSDecimalNumber *result = nil;
    switch (type) {
        case CalculationAdding:
            result = [selfNumber decimalNumberByAdding:calcuationNumber withBehavior:handler];
            break;
        case CalculationSubtracting:
            result =  [selfNumber decimalNumberBySubtracting:calcuationNumber withBehavior:handler];
            break;
        case CalculationMultiplying:
            result = [selfNumber decimalNumberByMultiplyingBy:calcuationNumber withBehavior:handler];
            break;
        case CalculationDividing:
            result =[selfNumber decimalNumberByDividingBy:calcuationNumber withBehavior:handler];
            break;
    }
    
    // 如果自定义了结果格式化工具使用自定义formatter
    
    NSNumberFormatter *numberFormatter = [self _numberFormatterWithScale:scale];
    NSString *resultNumber = [numberFormatter stringFromNumber:result];
    if([resultNumber.lowercaseString isEqualToString:@"nan"]){
        resultNumber = @"0.00";
    }
    return resultNumber;
    
//    return [self removeFloatAllZeroByString:resultNumber];;

}

- (NSNumberFormatter *)_numberFormatterWithScale:(NSInteger)scale{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.alwaysShowsDecimalSeparator = YES;
    numberFormatter.minimumIntegerDigits = 1;
    numberFormatter.numberStyle = kCFNumberFormatterNoStyle;
    numberFormatter.minimumFractionDigits = scale;
    return numberFormatter;
}

//处理币种数量. 去掉000
- (NSString*)removeFloatAllZeroByString:(NSString *)number{
    
    /*---------第一种方法-----------*/
    NSString * testNumber = number;
    NSString * s = nil;
    NSInteger offset = testNumber.length - 1;
    
    while (offset){
        s = [testNumber substringWithRange:NSMakeRange(offset, 1)];
        if ([s isEqualToString:@"0"] ){
            offset--;
        } else if ([s isEqualToString:@"."]){
            offset--;
            break;
        }
        else {
            break;
        }
    }
    NSString *outNumber = [testNumber substringToIndex:offset+1];
    if([outNumber.lowercaseString isEqualToString:@"nan"]){
        outNumber = @"0";
    }
    return outNumber;
}


//保留4位小数..乘以1
- (NSString *)saveDecimal{
    if(self)
    return [self yw_stringByMultiplyingBy:@"1" withRoundingMode:NSRoundDown scale:4]?:@"0";
    else
    return @"0";

}


//科学计数法. 保留8位小数
- (NSString *)scientific2{
     if(self)
    return [self yw_stringByMultiplyingBy:@"1" withRoundingMode:NSRoundDown scale:2]?:@"0";
    else
    return @"0";

    
}

@end

