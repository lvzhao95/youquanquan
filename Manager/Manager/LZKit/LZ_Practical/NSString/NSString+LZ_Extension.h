//
//  NSString+LZ_Extension.h
//  MTOWallert
//
//  Created by 微微 on 2019/4/20.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (LZ_Extension)

/** 正常号转银行卡号 － 增加4位间的空格 */
+ (NSString *)lz_spacingBankCardNumber:(NSString *)bankCardNumber;

/** 银行卡号转正常号 － 去除4位间的空格 */
+ (NSString *)lz_bankCardNumberRemoveSpacing:(NSString *)bankCardNumber;

/** 银行卡号安全化 格式为****1234保留最后4位 */
+ (NSString *)lz_securityBankCard:(NSString *)bankCardNumber;

/** 格式化HTML代码 */
+ (NSString *)lz_htmlEntityDecode:(NSString *)string;

/** 返回一个计算好的字符串的高度*/
- (CGFloat)lz_textHeightWithFontSize:(UIFont *)font withMaxWidth:(CGFloat)maxWidth;


/** 返回一个计算好的字符串的宽度*/
- (CGFloat)lz_textWidthWithFontSize:(UIFont *)font withMaxHeight:(CGFloat)maxHeight;

/** 返回一个计算好的字符串的高度和高度*/
- (CGRect)lz_textWidthWithFontSize:(UIFont *)font width:(CGFloat)width;


/** 返回一个计算好的字符串的宽度 */
- (CGSize)lz_boundingRectWithSize:(CGSize)size withFont:(UIFont *)font lineSpacing:(CGFloat)lineSpacing;

/** 计算显示文本需要几行 */
- (CGFloat)lz_textShowLinesWithControlWidth:(CGFloat)controlWidth font:(UIFont *)font lineSpacing:(CGFloat)lineSpacing;

/** 计算显示文本到指定行数时需要的高度 */
- (CGFloat)lz_textHeightWithSpecifyRow:(NSInteger)specifyRow font:(UIFont *)font lineSpacing:(CGFloat)lineSpacing;

/** 去除字符串中的空格 */
+ (NSString *)lz_wipeSpaceFromStr:(NSString *)str;

/** 去除字符串两端的空格 */
+ (NSString *)lz_wipeBothEndsSpaceFromStr:(NSString *)str;

/**
 
 *  去除字符串中的特定符号
 
 *  @param str 需要去除指定字符的字符串
 
 *  @param appointSymbol  指定的字符,如#、!
 
 *  @param replacement  要替换的字符(可以为空)
 
 */
+ (NSString *)lz_wipeAppointSymbolFromStr:(NSString *)str AppointSymbol:(NSString *)appointSymbol WithStr:(NSString *)replacement;

/**
 验证非空字符串
 */
+ (NSString *)lz_emptyStr:(NSString *)str;


/**
 验证密码
 */
+ (BOOL)checkPassword:(NSString*)password;

//拼音转化
+ (NSString *)pinyinChinese:(NSString *)chinese;
@end

NS_ASSUME_NONNULL_END
