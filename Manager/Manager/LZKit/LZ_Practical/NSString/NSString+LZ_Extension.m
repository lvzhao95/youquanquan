//
//  NSString+LZ_Extension.m
//  MTOWallert
//
//  Created by 微微 on 2019/4/20.
//  Copyright © 2019 吕VV. All rights reserved.
//

#import "NSString+LZ_Extension.h"

@implementation NSString (LZ_Extension)
/** 正常号转银行卡号 － 增加4位间的空格 */
+ (NSString *)lz_spacingBankCardNumber:(NSString *)bankCardNumber {
    
    NSString *tmpStr = bankCardNumber;
    NSInteger size = (tmpStr.length / 4);
    NSMutableArray *tmpStrArr = [[NSMutableArray alloc] init];
    for (int n = 0;n < size; n++)
    {
        [tmpStrArr addObject:[tmpStr substringWithRange:NSMakeRange(n*4, 4)]];
    }
    
    [tmpStrArr addObject:[tmpStr substringWithRange:NSMakeRange(size*4, (tmpStr.length % 4))]];
    
    tmpStr = [tmpStrArr componentsJoinedByString:@" "];
    
    return tmpStr;
}

/** 银行卡号转正常号 － 去除4位间的空格 */
+ (NSString *)lz_bankCardNumberRemoveSpacing:(NSString *)bankCardNumber {
    
    return [bankCardNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
}

/** 银行卡号安全化 格式为****1234保留最后4位 */
+ (NSString *)lz_securityBankCard:(NSString *)bankCardNumber {
    
    NSString *IDCard =  bankCardNumber;
    if(IDCard.length > 10){
        return [NSString stringWithFormat:@"%@********%@",[IDCard substringToIndex:6], [IDCard substringFromIndex:IDCard.length-4]];
    }else if(IDCard.length > 4){
        return [NSString stringWithFormat:@"********%@", [IDCard substringFromIndex:IDCard.length-4]];
    }
    return IDCard;
}

/** 格式化HTML代码 *///将 &lt 等类似的字符转化为HTML中的“<”等
+ (NSString *)lz_htmlEntityDecode:(NSString *)string {
    
    string = [string stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
    string = [string stringByReplacingOccurrencesOfString:@"&apos;" withString:@"'"];
    string = [string stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    string = [string stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    string = [string stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"]; // Do this last so that, e.g. @"&amp;lt;" goes to @"&lt;" not @"<"
    
    return string;
}
/** 返回一个计算好的字符串的高度*/
- (CGFloat)lz_textHeightWithFontSize:(UIFont *)font withMaxWidth:(CGFloat)maxWidth{
    NSDictionary *attrs = @{NSFontAttributeName:font};
    return ceilf([self boundingRectWithSize:CGSizeMake(maxWidth, MAXFLOAT)
                               options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                            attributes:attrs
                               context:nil].size.height);
}

/** 返回一个计算好的字符串的宽度*/
- (CGFloat)lz_textWidthWithFontSize:(UIFont *)font withMaxHeight:(CGFloat)maxHeight{
    NSDictionary *attrs = @{NSFontAttributeName:font};
    return  ceilf([self boundingRectWithSize:CGSizeMake(MAXFLOAT, maxHeight)
                               options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                            attributes:attrs
                               context:nil].size.width);

}
/** 返回一个计算好的字符串的高度和高度*/
- (CGRect)lz_textWidthWithFontSize:(UIFont *)font width:(CGFloat)width{
    NSDictionary *dic = @{NSFontAttributeName: font};
    CGRect rect = [self boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading  attributes:dic context:nil];
    return rect;
}


/** 返回一个计算好的字符串的高度和宽度 */
- (CGSize)lz_boundingRectWithSize:(CGSize)size withFont:(UIFont *)font lineSpacing:(CGFloat)lineSpacing {
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    [paragraphStyle setLineSpacing:lineSpacing];
    NSDictionary *attribute = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paragraphStyle};
    
    CGSize retSize = [self boundingRectWithSize:size
                                        options:\
                      NSStringDrawingTruncatesLastVisibleLine |
                      NSStringDrawingUsesLineFragmentOrigin |
                      NSStringDrawingUsesFontLeading
                                     attributes:attribute
                                        context:nil].size;
    
    return retSize;
}

/** 计算显示文本需要几行 */
- (CGFloat)lz_textShowLinesWithControlWidth:(CGFloat)controlWidth font:(UIFont *)font lineSpacing:(CGFloat)lineSpacing {
    
    //计算总高度
    CGFloat totalHeight = [self lz_boundingRectWithSize:CGSizeMake(controlWidth, 0) withFont:font lineSpacing:lineSpacing].height;
    
    //计算每行的高度
    CGFloat lineHeight = font.lineHeight+lineSpacing;
    
    return totalHeight/lineHeight;
}

/** 计算显示文本到指定行数时需要的高度 */
- (CGFloat)lz_textHeightWithSpecifyRow:(NSInteger)specifyRow font:(UIFont *)font lineSpacing:(CGFloat)lineSpacing {
    
    return (font.lineHeight+lineSpacing)*specifyRow;
}
/** 去除字符串中的空格 */
+ (NSString *)lz_wipeSpaceFromStr:(NSString *)str {
    
    NSCharacterSet *whitespaces = [NSCharacterSet whitespaceCharacterSet];
    NSPredicate *noEmptyStrings = [NSPredicate predicateWithFormat:@"SELF != ''"];
    NSArray *parts = [str componentsSeparatedByCharactersInSet:whitespaces];//在空格处将字符串分割成一个 NSArray
    NSArray *filteredArray = [parts filteredArrayUsingPredicate:noEmptyStrings];//去除空串
    NSString *jointStr  = @"";
    str = [filteredArray componentsJoinedByString:jointStr];
    
    return str;
}

/** 去除字符串两端的空格 */
+ (NSString *)lz_wipeBothEndsSpaceFromStr:(NSString *)str {
    
    NSString *s = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];//该方法是去掉两端的空格
    
    //NSString *s =  [s stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];//该方法也可以
    
    return s;
}

/** 去除字符串中的特定符号 */
+ (NSString *)lz_wipeAppointSymbolFromStr:(NSString *)str AppointSymbol:(NSString *)appointSymbol WithStr:(NSString *)replacement {
    
    NSString *b = [str stringByReplacingOccurrencesOfString:appointSymbol withString:replacement];//该方法是去掉指定符号
    return b;
}

/** 验证非空字符串*/
+ (NSString *)lz_emptyStr:(NSString *)str {
    
    if(([str isKindOfClass:[NSNull class]]) || ([str isEqual:[NSNull null]]) || (str == nil) || (!str)) {
        
        str = @"";
    }
    return str;
}

+ (BOOL)checkPassword:(NSString*)password
{
    NSString *pattern = @"^(?=.*[a-zA-Z0-9].*)(?=.*[a-zA-Z\\W].*)(?=.*[0-9\\W].*).{6,21}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",pattern];
    BOOL isMatch = [pred evaluateWithObject:password];
    return isMatch;
}

+ (NSString *)pinyinChinese:(NSString *)chinese{
    NSMutableString *str = [chinese mutableCopy];
    CFStringTransform(( CFMutableStringRef)str, NULL, kCFStringTransformMandarinLatin, NO);
    CFStringTransform((CFMutableStringRef)str, NULL, kCFStringTransformStripDiacritics, NO);
    
    return str;
}
@end
