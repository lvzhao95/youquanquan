//
//  NSDate+LZ_Extension.h
//  coin
//
//  Created by 微微 on 2017/12/25.
//  Copyright © 2017年 微微. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (LZ_Extension)
/**
 *  字符串转NSDate
 *
 *  @param timeStr 字符串时间
 *  @param format  转化格式 如yyyy-MM-dd HH:mm:ss,即2015-07-15 15:00:00
 *
 *  @return return value description
 */
+ (NSDate *)dateFromString:(NSString *)timeStr
                    format:(NSString *)format;

/**
 *  NSDate转时间戳
 *
 *  @param date 字符串时间
 *
 *  @return 返回时间戳
 */
+ (NSInteger)cTimestampFromDate:(NSDate *)date;

/**
 *  字符串转时间戳
 *
 *  @param timeStr 字符串时间
 *  @param format  转化格式 如yyyy-MM-dd HH:mm:ss,即2015-07-15 15:00:00
 *
 *  @return 返回时间戳的字符串
 */
+(NSInteger)cTimestampFromString:(NSString *)timeStr
                          format:(NSString *)format;


/**
 *  时间戳转字符串
 *
 *  @param timeStamp 时间戳
 *  @param format    转化格式 如yyyy-MM-dd HH:mm:ss,即2015-07-15 15:00:00
 *
 *  @return 返回字符串格式时间
 */
+ (NSString *)dateStrFromCstampTime:(NSInteger)timeStamp
                     withDateFormat:(NSString *)format;

/**
 *  NSDate转字符串
 *
 *  @param date   NSDate时间
 *  @param format 转化格式 如yyyy-MM-dd HH:mm:ss,即2015-07-15 15:00:00
 *
 *  @return 返回字符串格式时间
 */
+ (NSString *)datestrFromDate:(NSDate *)date
               withDateFormat:(NSString *)format;

/**
 *  NSDate是周几的
 *
 *  @param date   NSDate时间
 *
 *  @return 返回字符串格式周几
 */
+ (NSString *)dateWeekDate:(NSDate *)date;


- (NSInteger) lz_year;
- (NSInteger) lz_month;
- (NSInteger) lz_day;
- (NSInteger) lz_hour;
- (NSInteger) lz_min;


@end

/*
 - (IBAction)strToDate:(id)sender {
 NSString *timeStr = @2015-07-15 15:00:00;
 NSDate *date = [NSDate dateFromString:timeStr format:@yyyy-MM-dd HH:mm:ss];
 NSLog(@字符串转NSDate：%@ -> %@,timeStr,date);
 }
 ?
 1
 2
 3
 4
 5
 6
 - (IBAction)dateToStamp:(id)sender {
 
 NSDate *date = [NSDate date];
 NSInteger stamp = [NSDate cTimestampFromDate:date];
 NSLog(@NSDate转时间戳：%@ ->%ld,date,stamp);
 }
 ?
 1
 2
 3
 4
 5
 - (IBAction)strToStamp:(id)sender {
 NSString *timeStr = @2015-07-15 15:00:00;
 NSInteger stamp = [NSDate cTimestampFromString:timeStr format:@yyyy-MM-dd HH:mm:ss];
 NSLog(@字符串转时间戳：%@ -> %ld,timeStr,stamp);
 }
 ?
 1
 2
 3
 4
 5
 - (IBAction)stampToStr:(id)sender {
 NSInteger stamp = 1436943600;
 NSString *timeStr = [NSDate dateStrFromCstampTime:stamp withDateFormat:@yyyy-MM-dd HH:mm:ss];
 NSLog(@时间戳转字符串：%ld ->%@,stamp,timeStr);
 }
 ?
 1
 2
 3
 4
 5
 - (IBAction)dateToStr:(id)sender {
 NSDate *date = [NSDate date];
 NSString *timeStr = [NSDate datestrFromDate:date withDateFormat:@yyyy-MM-dd HH:mm:ss];
 NSLog(@NSDate转字符串：%@ ->%@,date,timeStr);
 }
*/
