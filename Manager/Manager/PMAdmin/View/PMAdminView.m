//
//  PMAdminView.m
//  Manager
//
//  Created by lvzhao on 2020/7/29.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMAdminView.h"
#import "PMAdminHeadView.h"
#import "PMAdminViewModel.h"
#import "LZBaseTableView.h"
#import "PMAdminCell.h"
#import "PMLoginViewController.h"
#import "PMChannelCategoryView.h"

@interface PMAdminView() <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) PMAdminViewModel *viewModel;

@property (nonatomic,strong) LZBaseTableView *tableView;

@end

@implementation PMAdminView

- (instancetype)initViewModel:(LZBaseViewModel *)viewModel
{
    self = [super initViewModel:viewModel];
    if (self) {
        self.viewModel = (PMAdminViewModel *)viewModel;
        
        [self setupUI];
    }
    return self;
}

#pragma mark 创建UI
- (void)setupUI{
    
    PMAdminHeadView *headView = [[PMAdminHeadView alloc] initViewModel:self.viewModel];
    self.tableView.tableHeaderView = headView;
    [self addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    @weakify(self);
    [self.viewModel.reloadSubject subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        dismiss(nil);
        [self.tableView reloadData];
    }];
    
    //退出登录
    UIButton *logoutBtn = [UIButton lz_buttonTitle:@"退出登录" titleColor:COLOR_textColor fontSize:17];
    logoutBtn.frame = CGRectMake(0, 0, kScreenWidth, 50);
    logoutBtn.backgroundColor = COLOR_cellbackground;
    self.tableView.tableFooterView = logoutBtn;
    [logoutBtn addTarget:self action:@selector(logoutClick:) forControlEvents:UIControlEventTouchUpInside];
    
    showLoadingTitle(@"", nil);
    [self.viewModel loadUserInfo];
    
}


- (void)logoutClick:(UIButton *)sender{
//
//    NSArray *subTitles = @[@"一家人",@"啥都撒的谎",@"一家人",@"啥都撒的谎",@"一家人",@"啥都撒的谎",@"一家人",@"啥都撒的谎",@"一家人",@"啥都撒的谎",@"一家人",@"啥都撒的谎"];
//    [LZToolView showAlertType:LZAlertTypeActionSheet withTitle:@"" message:@"" cancel:@"取消" sure:@"" objectDict:@{@"subTitles":subTitles} cancelBlock:^(id  _Nullable object) {
//
//    } sureBlock:^(id  _Nullable object) {
//
//    }];
    
    YYCache *cache = [YYCache cacheWithName:@"PublicData"];
    [cache removeObjectForKey:@"PublicData"];
    
    @weakify(self);
    [LZToolView showAlertType:LZAlertTypeTipAlter withTitle:@"退出登录" message:@"确定退出登录?" cancel:@"取消" sure:@"确定" objectDict:nil cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
        @strongify(self);
        [self.viewModel logout];
    }];
    
//    NSArray *subTitles = @[@"一家人",@"啥都撒的谎",@"一家人",@"啥都撒的谎",@"一家人",@"啥都撒的谎",@"一家人",@"啥都撒的谎",@"一家人",@"啥都撒的谎",@"一家人",@"啥都撒的谎"];
//    [LZToolView showAlertType:LZAlertTypeReplacePerson withTitle:@"更换刷手" message:@"选择刷手" cancel:@"取消" sure:@"确定" objectDict:@{@"subTitles":subTitles} cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
//
//    }];
//
//    [LZToolView showAlertType:LZAlertTypeAddLimit withTitle:@"增加金额" message:@"增加额度" cancel:@"取消" sure:@"确定" objectDict:nil cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
//
//    }];
    
//    [LZToolView showAlertType:LZAlertTypeAuditStatus withTitle:@"商户审核" message:@"审核状态" cancel:@"取消" sure:@"确定" objectDict:nil cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
//
//      }];


//    NSArray *subTitles = @[@"费率:",@"结算周期:",@"付款下限:",@"付款上限:",@"权重:"];
//    [LZToolView showAlertType:LZAlertTypeModification withTitle:@"修改" message:@"通道: 银联电子代付" cancel:@"取消" sure:@"确定" objectDict:@{@"subTitles":subTitles} cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
//
//    }];
    
    
//    NSDictionary *subTitleDictionary = @{@"支付宝支付(alipay)":@[@"支付宝扫码支付(alipay_qr)",@"支付宝SDK支付(alipay_sdk)",@"支付宝 wap 支付(alipay_wap)",@"外派的哟an的"],
//                                         @"微信支付(wxpay)":@[@"支付宝扫码支付(alipay_qr)",@"支付宝SDK支付(alipay_sdk)",@"支付宝 wap 支付(alipay_wap)"]};
//
//    [PMChannelCategoryView showObjectDict:subTitleDictionary sureBlock:^(id  _Nullable object) {
//
//
//
//    }];
    
//    [LZToolView showAlertType:LZAlertTypeModificaPassword withTitle:@"修改密码" message:@"" cancel:@"取消" sure:@"确定" objectDict:nil cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
//
//    }];

    
//    NSArray *subTitles = @[@"费率:",@"结算周期:",@"付款下限:",@"付款上限:",@"权重:"];
//    [LZToolView showAlertType:LZAlertTypeEditAmout withTitle:@"增加额度" message:@"" cancel:@"取消" sure:@"确定" objectDict:@{@"subTitles":subTitles} cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
//
//    }];
    
//    [LZToolView showAlertType:LZAlertTypeAddLink withTitle:@"添加推广链接" message:@"" cancel:@"取消" sure:@"确定" objectDict:nil cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
//
//    }];
    
    
    
//    NSArray *subTitles = @[@[@"地址1",@"地址2",@"地址3",@"地址4",@"地址5"],
//                           @[@"地址1",@"地址2",@"地址3",@"地址4",@"地址5"]];
//    [LZToolView showAlertType:LZAlertTypeAddress withTitle:@"批量修改收/发货地址" message:@"" cancel:@"取消" sure:@"确定" objectDict:@{@"subTitles":subTitles} cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
//
//    }];
    
    
    
//    [LZToolView showAlertType:LZAlertTypeEmailLogin withTitle:@"短信登录" message:@"" cancel:@"取消" sure:@"确定" objectDict:nil cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
//
//    }];


//    NSArray *subTitles = @[@[@"地址1",@"地址2",@"地址3",@"地址4",@"地址5"],
//                           @[@"地址1",@"地址2",@"地址3",@"地址4",@"地址5"]];
//    [LZToolView showAlertType:LZAlertTypeProductionAccount withTitle:@"批量生产账号" message:@"" cancel:@"取消" sure:@"确定" objectDict:@{@"subTitles":subTitles} cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
//
//    }];
    
    
    
//    [LZToolView showAlertType:LZAlertTypeModificationAccount withTitle:@"账户添加/修改" message:@"" cancel:@"取消" sure:@"确定" objectDict:nil cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
//
//    }];
}



#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0){
        return self.viewModel.dataArray.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PMAdminCell * homeCell = [tableView dequeueReusableCellWithIdentifier:@"PMAdminCell"];
    if(self.viewModel.dataArray.count > indexPath.row){
        homeCell.bottomLineView.hidden = (self.viewModel.dataArray.count - 1)== indexPath.row;
        homeCell.modelObject = self.viewModel.dataArray[indexPath.row];
    }
    return homeCell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 15;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *sectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 15)];
    sectionView.backgroundColor = COLOR_background;
    return sectionView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    
}


#pragma mark -lanjiazai
- (LZBaseTableView *)tableView{
    if(!_tableView){
        _tableView = [[LZBaseTableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.backgroundColor = COLOR_background;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 50;
        [_tableView registerClass:[PMAdminCell class] forCellReuseIdentifier:@"PMAdminCell"];
    }
    return _tableView;
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
