//
//  PMAdminHeadView.m
//  Manager
//
//  Created by lvzhao on 2020/7/29.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMAdminHeadView.h"
#import "PMAdminViewModel.h"
#import "PMAdminSettingViewController.h"

@interface PMAdminHeadView()

@property (nonatomic,strong) PMAdminViewModel *viewModel;

@end

@implementation PMAdminHeadView

- (instancetype)initViewModel:(LZBaseViewModel *)viewModel
{
    self = [super init];
    if (self) {
        self.backgroundColor = COLOR_cellbackground;
        self.frame = CGRectMake(0, 0, kScreenWidth, 84);
        self.viewModel = (PMAdminViewModel *)viewModel;
        
        [self setupUI];
    }
    return self;
}

#pragma mark 创建UI
- (void)setupUI{
    
    ///!!!:头像
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.image = k_imageName(@"pm_mine_head_default");
    [self addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.mas_centerY);
        make.left.mas_equalTo(15);
        make.width.height.mas_equalTo(60);
    }];
    
    ///!!!:账号
    UILabel *accountLab = [UILabel lz_labelWithText:@"" fontSize:PingFangSC(18) color:COLOR_textColor];
    [self addSubview:accountLab];
    [accountLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(18);
        make.left.mas_equalTo(imageView.mas_right).mas_offset(15);
        make.width.mas_equalTo(180);
        make.height.mas_equalTo(25);
    }];
    
    ///!!!:邮箱
    UILabel *emailLab = [UILabel lz_labelWithText:@"" fontSize:PingFangSC(15) color:COLOR_subTextColor];
    [self addSubview:emailLab];
    [emailLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(accountLab.mas_bottom).mas_offset(3);
        make.left.mas_equalTo(accountLab);
        make.width.mas_equalTo(180);
        make.height.mas_equalTo(21);
    }];
    
    
    ///!!!:下一步
    UIButton *nextBtn = [UIButton lz_buttonImageName:@"pm_next" backgroundImageName:@""];
    [self addSubview:nextBtn];
    [nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.mas_centerY);
        make.right.mas_equalTo(0);
        make.width.mas_equalTo(45);
        make.height.mas_equalTo(45);
    }];
    
    
    [[nextBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        
        PMAdminSettingViewController  *settingVC = [[PMAdminSettingViewController alloc] init];
        [LZTool.currentViewController.navigationController pushViewController:settingVC animated:YES];
    }];
    
    @weakify(self);
    [self.viewModel.reloadSubject subscribeNext:^(id  _Nullable x) {
        dismiss(nil);
        LZUserDetailModel *dataModel = [LZToolCache getUserInfo];
        accountLab.text = dataModel.nickname;
        emailLab.text = dataModel.username;
    }];
    
    

}

@end
