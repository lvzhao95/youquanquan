//
//  PMAdminSettingCell.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMAdminSettingCell.h"

@interface PMAdminSettingCell()

@property (nonatomic,strong)UILabel *titleLab; //说明
@property (nonatomic,strong)UILabel *detailLab; //说明

@end

@implementation PMAdminSettingCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupView];
    }
    return self;
}


//设置View
- (void)setupView{
    
    //title
    self.titleLab = [UILabel lz_labelWithText:@"" fontSize:PingFangSC(17) color:COLOR_textColor];
    [self.contentView addSubview:self.titleLab];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.height.mas_equalTo(24);
        make.width.mas_equalTo(100);
    }];
    
    //detail
    self.detailLab = [UILabel lz_labelWithText:@"" fontSize:PingFangSC(17) color:COLOR_textColor];
    [self.detailLab lz_rightAlignment];
    [self.contentView addSubview:self.detailLab];
    [self.detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-40);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.height.mas_equalTo(24);
        make.width.mas_equalTo(150);
    }];

    self.bottomLineView.hidden = NO;
    self.rightImageView.hidden = NO;

}

- (void)setModelObject:(id)modelObject{
  
    NSDictionary *dict = modelObject;
    self.titleLab.text = dict[@"title"];
    self.detailLab.text = dict[@"detail"];

}




@end
