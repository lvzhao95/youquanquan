//
//  PMAdminCell.m
//  Manager
//
//  Created by lvzhao on 2020/7/29.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMAdminCell.h"


@interface PMAdminCell()

@property (nonatomic,strong)UIImageView *imgView; //图片
@property (nonatomic,strong)UILabel *titleLab; //说明
@property (nonatomic,strong)UILabel *detailLab; //详情

@end

@implementation PMAdminCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupView];
    }
    return self;
}


//设置View
- (void)setupView{
    
    //headImageView
    self.imgView = [[UIImageView alloc] init];
    [self.contentView addSubview:self.imgView];
    [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.height.mas_equalTo(28);
        make.width.mas_equalTo(28);
    }];
    
    //title
    self.titleLab = [UILabel lz_labelWithText:@"" fontSize:PingFangSC(17) color:COLOR_textColor];
    [self.contentView addSubview:self.titleLab];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.imgView.mas_right).offset(12);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.height.mas_equalTo(24);
        make.width.mas_equalTo(150);
    }];
    
    //detail
    self.detailLab = [UILabel lz_labelWithText:@"" fontSize:PingFangSC(15) color:UIColorHex(0x999999)];
    [self.detailLab lz_rightAlignment];
    [self.contentView addSubview:self.detailLab];
    [self.detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-35);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.height.mas_equalTo(24);
        make.left.mas_equalTo(self.titleLab.mas_right).mas_offset(40);
    }];
    

    self.bottomLineView.hidden = NO;
    self.rightImageView.hidden = NO;

}

- (void)setModelObject:(id)modelObject{
  
    NSDictionary *dict = modelObject;
    self.imgView.image = k_imageName(dict[@"imageName"]);
    self.titleLab.text = dict[@"title"];
//    self.detailLab.text = dict[@"title"];

}




@end
