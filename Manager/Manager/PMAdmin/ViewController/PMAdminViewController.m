//
//  PMAdminViewController.m
//  Manager
//
//  Created by lvzhao on 2020/7/29.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMAdminViewController.h"
#import "PMAdminView.h"
#import "PMAdminViewModel.h"

@interface PMAdminViewController ()
 
@property (nonatomic,strong) PMAdminViewModel *viewModel;

@property (nonatomic,strong) PMAdminView *adminView;

@end

@implementation PMAdminViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    
    
    
    [LZNetworkingManager lz_request:@"get" url:@"HTTP://www.youquanquan.cn/admin/captcha" params:@{@"captchaKey":@"345678"} success:^(id  _Nullable responseObject) {
    
        
    } failure:^(NSError * _Nullable error) {
        
        
        
    }];
    
    
    
}

- (void)setupUI{
    
    [self.view addSubview:self.adminView];
    [self.adminView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

#pragma mark -懒加载
- (PMAdminView *)adminView{
    if(!_adminView) {
        _adminView = [[PMAdminView alloc] initViewModel:self.viewModel];
    }
    return _adminView;
}

- (PMAdminViewModel *)viewModel{
    if(!_viewModel) {
        _viewModel = [[PMAdminViewModel alloc] init];
    }
    return _viewModel;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
