//
//  PMAdminSettingViewController.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMAdminSettingViewController.h"
#import "PMAdminSettingCell.h"
#import "LZBaseTableView.h"

@interface PMAdminSettingViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) LZBaseTableView *tableView;

@property (nonatomic,strong) NSMutableArray *dataArray;

@end

@implementation PMAdminSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"管理员设置";
    
    [self setupUI];
    // Do any additional setup after loading the view.
}


#pragma mark 创建UI
- (void)setupUI{
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    
    [self loadData];
}

- (void)loadData{
    
    self.dataArray = [[NSMutableArray alloc] init];
    LZUserDetailModel *dataModel = [LZToolCache getUserInfo];
    [self.dataArray addObject:@{@"title":@"昵称",@"detail":dataModel.nickname?:@""}];
    [self.dataArray addObject:@{@"title":@"电子邮箱",@"detail":dataModel.username?:@""}];

    [self.tableView reloadData];

}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
          
    return self.dataArray.count;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PMAdminSettingCell *settingCell = [tableView dequeueReusableCellWithIdentifier:@"PMAdminSettingCell"];
    settingCell.bottomLineView.hidden = (self.dataArray.count - 1)== indexPath.row;
    settingCell.modelObject = self.dataArray[indexPath.row];
    return settingCell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 15;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *sectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 15)];
    sectionView.backgroundColor = COLOR_background;
    return sectionView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    
}


#pragma mark -lanjiazai
- (LZBaseTableView *)tableView{
    if(!_tableView){
        _tableView = [[LZBaseTableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.backgroundColor = COLOR_background;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.scrollEnabled = NO;
        _tableView.rowHeight = 50;
        [_tableView registerClass:[PMAdminSettingCell class] forCellReuseIdentifier:@"PMAdminSettingCell"];
    }
    return _tableView;
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
