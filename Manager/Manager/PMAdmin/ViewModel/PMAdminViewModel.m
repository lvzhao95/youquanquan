//
//  PMAdminViewModel.m
//  Manager
//
//  Created by lvzhao on 2020/7/29.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMAdminViewModel.h"
#import "PMLoginViewController.h"

@implementation PMAdminViewModel


- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [self.dataArray addObject:@{@"title":@"账户密码",@"imageName":@"pm_mine_psd"}];
        [self.dataArray addObject:@{@"title":@"密保手机",@"imageName":@"pm_phone"}];
    }
    return self;
}


/*获取个人信息**/
- (void)loadUserInfo{
    
    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kGetUserInfo params:nil success:^(id  _Nullable responseObject) {
        @strongify(self);
        
        NSDictionary *dataDictionary = responseObject;
        NSDictionary *userDictionary = dataDictionary[@"user"];
        LZUserDetailModel *userModel = [LZToolCache getUserInfo];
        userModel.username = userDictionary[@"username"];
        userModel.nickname = userDictionary[@"nickname"];
        userModel.userId = userDictionary[@"id"];
        [LZToolCache saveUserInfo:userModel];
        
        
        [self.reloadSubject sendNext:@(1)];
    } failure:^(NSError * _Nullable error) {
        [self.reloadSubject sendNext:@(1)];
    }];
}

/*退出登录**/
- (void)logout{
    
    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kGetUserLogout params:nil success:^(id  _Nullable responseObject) {
        @strongify(self);
        [self switchRootView];
    } failure:^(NSError * _Nullable error) {
        @strongify(self);
        [self switchRootView];
    }];
    
}


- (void)switchRootView{
    [LZToolCache saveUserInfo:[LZUserDetailModel new]];
    PMLoginViewController *loginVC = [[PMLoginViewController alloc] init];
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    window.rootViewController = loginVC;
}

@end
