//
//  PMAdminViewModel.h
//  Manager
//
//  Created by lvzhao on 2020/7/29.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZBaseViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PMAdminViewModel : LZBaseViewModel


/*获取个人信息**/
- (void)loadUserInfo;

/*退出登录**/
- (void)logout;


@end

NS_ASSUME_NONNULL_END
