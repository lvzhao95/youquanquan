//
//  LZTableSlideView.h
//  Manager
//
//  Created by lvzhao on 2020/7/31.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LZTableDataModel.h"
#import "LZTableCollectionView.h"
#import "LZSortButton.h"

#define  tableLineColor  [UIColorHex(0x1A8C8F) colorWithAlphaComponent:0.5];

typedef void(^touchCallBlock)(PMTouchTable touchTable, NSInteger index ,LZSortButton * _Nullable sortBtn);

NS_ASSUME_NONNULL_BEGIN


@interface LZTableSlideView : UIView

///是否展示最右边视图
@property (nonatomic, assign) BOOL isShowRight;

///左侧表头视图
@property (nonatomic, strong) UIView *leftHeadView;

///右侧内容视图
@property (nonatomic, strong) UIView *rightHeadView;

///数据源
@property (nonatomic,strong) LZTableDataModel *dataModel;

@property (nonatomic,copy) touchCallBlock callBlock;

///右侧内容视图
@property (nonatomic, strong) UITableView *rightTableView;
@end

NS_ASSUME_NONNULL_END
