//
//  LZTableSlideView.m
//  Manager
//
//  Created by lvzhao on 2020/7/31.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZTableSlideView.h"
#import "LZBaseTableView.h"
#import "LZRightTableCell.h"
#import "LZLeftTableCell.h"
#import "PMLastRightCell.h"

@interface LZTableSlideView () <UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>

///横向滚动视图
@property (nonatomic, strong) UIScrollView *rightScrollView;


///最右侧操作视图
@property (nonatomic, strong) UITableView *lastRightTableView;


///左侧表头视图
@property (nonatomic, strong) UITableView *leftTableView;

///是否展示左侧表头分割线
@property (nonatomic, assign) BOOL isHideLeftTableLine;

@property (nonatomic, strong) LZSortButton *selectSortBtn;

@end


@implementation LZTableSlideView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.isShowRight = YES;
        self.backgroundColor = UIColor.whiteColor;
        self.clipsToBounds = YES;
        [self configUI];
    }
    return self;
}

- (void)configUI{
    [self addSubview:self.rightScrollView];
    [self addSubview:self.leftTableView];
    [self addSubview:self.lastRightTableView];
    [self.rightScrollView addSubview:self.rightTableView];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    self.leftHeadView.frame = CGRectMake(0, 0, self.dataModel.leftWidth, self.dataModel.topHeight);
    self.rightScrollView.frame = CGRectMake(self.dataModel.leftWidth, 0,self.isShowRight ? self.width - self.dataModel.leftWidth - 40 : self.width - self.dataModel.leftWidth , self.height);
    
    self.rightHeadView.frame = CGRectMake(0, 0,self.dataModel.totalItemWidth, self.dataModel.topHeight);
    //设置,右边视图contentSize
    self.rightScrollView.contentSize = CGSizeMake(self.dataModel.totalItemWidth, 0);
    self.leftTableView.frame = CGRectMake(0, 0, self.dataModel.leftWidth, self.height);
    self.rightTableView.frame = CGRectMake(0, 0,self.dataModel.totalItemWidth, self.height);
    self.lastRightTableView.frame = CGRectMake(self.width - 40, 0,40, self.height);

}

#pragma -mark UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if(scrollView == self.rightTableView){
        CGFloat offsetY = self.rightTableView.contentOffset.y;
        CGPoint timeOffsetY = self.leftTableView.contentOffset;
        timeOffsetY.y = offsetY;
        self.leftTableView.contentOffset = timeOffsetY;
        self.lastRightTableView.contentOffset = timeOffsetY;

        if(offsetY == 0){
            self.leftTableView.contentOffset = CGPointZero;
            self.lastRightTableView.contentOffset = CGPointZero;
        }
        
        CGRect rect = [self.rightTableView convertRect:self.rightTableView.bounds toView:[UIApplication sharedApplication].keyWindow];
        self.rightTableView.mj_footer.mj_x = -rect.origin.x;
        self.rightTableView.mj_header.mj_x = -rect.origin.x;
        
    } else if(scrollView == self.leftTableView){
        CGFloat offsetY = self.leftTableView.contentOffset.y;
        CGPoint timeOffsetY = self.rightTableView.contentOffset;
        timeOffsetY.y = offsetY;
        self.rightTableView.contentOffset = timeOffsetY;
        self.lastRightTableView.contentOffset = timeOffsetY;

        if(offsetY == 0){
            self.rightTableView.contentOffset = CGPointZero;
            self.lastRightTableView.contentOffset = CGPointZero;
        }
    } else if(scrollView == self.lastRightTableView){
        
        CGFloat offsetY = self.lastRightTableView.contentOffset.y;
        CGPoint timeOffsetY = self.rightTableView.contentOffset;
        timeOffsetY.y = offsetY;
        self.rightTableView.contentOffset = timeOffsetY;
        self.leftTableView.contentOffset = timeOffsetY;

        if(offsetY == 0){
            self.rightTableView.contentOffset = CGPointZero;
            self.leftTableView.contentOffset = CGPointZero;
        }
    }
}

#pragma -mark UITableViewDelegate,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataModel.dataArray.count;
}

//行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.dataModel.itemHeightArray.count > indexPath.row) {
        NSNumber *height = self.dataModel.itemHeightArray[indexPath.row];
        return  height.integerValue;
    }
    return self.dataModel.rowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (tableView == self.rightTableView) {
        LZRightTableCell *rightCell = [tableView dequeueReusableCellWithIdentifier:@"LZRightTableCell"];
        if(self.dataModel.dataArray.count > indexPath.row){
            rightCell.modelObject = self.dataModel.dataArray[indexPath.row];
        }
        return rightCell;
    } else if(tableView == self.leftTableView){
        LZLeftTableCell *leftCell = [tableView dequeueReusableCellWithIdentifier:@"LZLeftTableCell"];
        if(self.dataModel.leftDataArray.count > indexPath.row){
            leftCell.modelObject = self.dataModel.leftDataArray[indexPath.row];
        }
        return leftCell;
    } else {
        PMLastRightCell *lastRightCell = [tableView dequeueReusableCellWithIdentifier:@"PMLastRightCell"];
        return lastRightCell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    if ([tableView isEqual:self.leftTableView]) {
        return self.leftHeadView;
    } else if(tableView == self.rightTableView){
        return self.rightHeadView;
    } else {
        UIView *sectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, self.dataModel.rowHeight)];
        sectionView.backgroundColor = UIColorHex(0xF8F9FF);
        return sectionView;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    //第一行
    return self.dataModel.topHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == self.lastRightTableView) {
        if(self.callBlock){
            self.callBlock(PMTouchTableRight, indexPath.row,self.selectSortBtn);
        }
    }
}

#pragma mark - SET
- (void)setDataModel:(LZTableDataModel *)dataModel{
    _dataModel = dataModel;
    
    [self setNeedsLayout];
    [self.leftTableView reloadData];
    [self.rightTableView reloadData];
    [self.lastRightTableView reloadData];

    [self reloadTableSetionView];
}

//刷新sectionView
- (void)reloadTableSetionView{
    
    
    if(self.rightHeadView.subviews.count > 0)return;
    
    NSInteger sectionIndex = self.dataModel.topDataModel.columnWidthArray.count;
    
    UIView *lastView = nil;
    for (int i = 0; i < sectionIndex; i++)
    {
        UIView *contentView = [[UIView alloc] init];
        contentView.backgroundColor = UIColorHex(0xF8F9FF);;
        [self.rightHeadView addSubview:contentView];
        NSInteger width = [self.dataModel.topDataModel.columnWidthArray[i] integerValue];
        contentView.frame = CGRectMake((i== 0) ? 0 : lastView.width + lastView.x, 0, width, self.dataModel.rowHeight);
        lastView = contentView;
         
        LZTableRightSubDataModel *subDataModel = self.dataModel.topDataModel.itemModelArray[i];
        
        LZSortButton *sortBtn = [[LZSortButton alloc] init];
        [sortBtn setTitleColor:UIColorHex(0x666666) forState:UIControlStateNormal];
        sortBtn.titleLabel.font = PingFangSC(13);
        sortBtn.tag = i;
//        sortBtn.sortStatue = subDataModel.sortStatus;
        [sortBtn setTitle:subDataModel.titleString forState:UIControlStateNormal];
//        sortBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        sortBtn.frame = CGRectMake(5, 0, contentView.width - 10, self.dataModel.rowHeight);
        [sortBtn SG_imagePositionStyle:SGImagePositionStyleRight spacing:5];
        if(subDataModel.sortStatus >= 0){
//            [sortBtn addTarget:self action:@selector(sortBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        }
        [contentView addSubview:sortBtn];
        
    }
    
    
    //最左边, 最上边的按钮
    LZSortButton *sortBtn = [[LZSortButton alloc] init];
    [sortBtn setTitleColor:UIColorHex(0x666666) forState:UIControlStateNormal];
    sortBtn.titleLabel.font = PingFangSC(13);
    //    sortBtn.sortStatue = self.dataModel.topLeftDataModel.sortStatus;
    [sortBtn setTitle:self.dataModel.topLeftDataModel.titleString forState:UIControlStateNormal];
    sortBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.leftHeadView addSubview:sortBtn];
    sortBtn.frame = CGRectMake(10, 0, self.dataModel.leftWidth - 10, self.dataModel.rowHeight);
    [sortBtn SG_imagePositionStyle:SGImagePositionStyleRight spacing:5];
//    [sortBtn addTarget:self action:@selector(sortBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    sortBtn.tag = 100;
    
    
}

//排序
- (void)sortBtnClick:(LZSortButton *)sortBtn{
    
    if(self.selectSortBtn == sortBtn){
        //排序
        switch (self.selectSortBtn.sortStatue) {
            case 0:
                self.selectSortBtn.sortStatue = 1;
                break;
            case 1:
                self.selectSortBtn.sortStatue = 2;
                break;
            default:
                self.selectSortBtn.sortStatue = 0;
                break;
        }
    } else {
        self.selectSortBtn.sortStatue = 0;
        sortBtn.sortStatue = 1;
            
    }
    self.selectSortBtn = sortBtn;
    
    if(self.callBlock){
        self.callBlock((sortBtn.tag ==100)  ? PMTouchTableTopLeft : PMTouchTableTop, sortBtn.tag,sortBtn);
    }
}

#pragma mark 懒加载
- (UIScrollView *)rightScrollView{
    if (!_rightScrollView) {
        _rightScrollView = [[UIScrollView alloc]initWithFrame:CGRectZero];
        _rightScrollView.delegate = self;
        _rightScrollView.bounces = NO;
        _rightScrollView.showsVerticalScrollIndicator = NO;
        _rightScrollView.showsHorizontalScrollIndicator = NO;
    }
    return _rightScrollView;
}

- (UITableView *)leftTableView{
    if (!_leftTableView) {
        _leftTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _leftTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _leftTableView.estimatedRowHeight = 0;
        _leftTableView.delegate = self;
        _leftTableView.dataSource = self;
        _leftTableView.showsVerticalScrollIndicator = NO;
        _leftTableView.showsHorizontalScrollIndicator = NO;
        [_leftTableView registerClass:[LZLeftTableCell class] forCellReuseIdentifier:@"LZLeftTableCell"];

    }
    return _leftTableView;
}

- (UITableView *)rightTableView{
    if (!_rightTableView) {
        _rightTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _rightTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _rightTableView.estimatedRowHeight = 0;
        _rightTableView.delegate = self;
        _rightTableView.dataSource = self;
        _rightTableView.showsVerticalScrollIndicator = NO;
        _rightTableView.showsHorizontalScrollIndicator = NO;
        [_rightTableView registerClass:[LZRightTableCell class] forCellReuseIdentifier:@"LZRightTableCell"];
    }
    return _rightTableView;
}

- (UITableView *)lastRightTableView{
    if (!_lastRightTableView) {
        _lastRightTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _lastRightTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _lastRightTableView.estimatedRowHeight = 0;
        _lastRightTableView.delegate = self;
        _lastRightTableView.dataSource = self;
        _lastRightTableView.showsVerticalScrollIndicator = NO;
        _lastRightTableView.showsHorizontalScrollIndicator = NO;
        [_lastRightTableView registerClass:[PMLastRightCell class] forCellReuseIdentifier:@"PMLastRightCell"];
    }
    return _lastRightTableView;
}

- (UIView *)leftHeadView{
    if (!_leftHeadView) {
        _leftHeadView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.dataModel.leftWidth, self.dataModel.topHeight)];
        _leftHeadView.backgroundColor = UIColorHex(0xF8F9FF);;
    }
    return _leftHeadView;
}

- (UIView *)rightHeadView{
    if (!_rightHeadView) {
        _rightHeadView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.dataModel.totalItemWidth, self.dataModel.rowHeight)];
    }
    return _rightHeadView;
}



@end
