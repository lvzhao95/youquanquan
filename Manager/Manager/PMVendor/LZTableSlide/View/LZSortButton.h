//
//  LZSortButton.h
//  Manager
//
//  Created by lvzhao on 2020/7/31.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
/// 排序
typedef NS_ENUM(NSInteger, PMTableSort) {
    PMTableSortNo = -1,         //无
    //默认
    PMTableSortNormal = 0,         //pm_sort_ nor
    //升序
    PMTableSortAscend = 1,        // pm_sort_ ascend
    //降序
    PMTableSortDescen = 2,        // pm_sort_ descen
};

@interface LZSortButton : UIButton

@property (nonatomic,assign) PMTableSort sortStatue;

@end

NS_ASSUME_NONNULL_END
