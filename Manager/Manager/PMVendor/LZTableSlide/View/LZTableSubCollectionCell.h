//
//  LZTableSubCollectionCell.h
//  Manager
//
//  Created by lvzhao on 2020/7/31.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LZTableSubCollectionCell : UICollectionViewCell
@property (nonatomic, strong) id modelObject;


@end

NS_ASSUME_NONNULL_END
