//
//  LZLeftTableCell.h
//  Manager
//
//  Created by lvzhao on 2020/7/31.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZBaseTableSliderCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface LZLeftTableCell : LZBaseTableSliderCell
@property (nonatomic, strong) UIView  *rightLine;
@property (nonatomic, strong) UIView  *bottomLine;
@end

NS_ASSUME_NONNULL_END
