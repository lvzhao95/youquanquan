//
//  LZRightTableCell.m
//  Manager
//
//  Created by lvzhao on 2020/7/31.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZRightTableCell.h"
#import "LZTableCollectionView.h"
#import "LZTableDataModel.h"

@interface LZRightTableCell ()

@property (nonatomic,strong) LZTableCollectionView *contenView;


@end
@implementation LZRightTableCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        self.selectionStyle = UITableViewCellSelectionStyleNone;

        self.backgroundColor = UIColor.whiteColor;
        
        [self setupUI];
    }
    return self;
}

- (void)layoutSubviews{
    self.contenView.frame = CGRectMake(0, 0, self.width, self.height);

}

- (void)setupUI{
    self.contenView = [[LZTableCollectionView alloc] initWithFrame:CGRectMake(0, 0, self.width, self.height - 1)];
    [self.contentView addSubview:self.contenView];
}

//赋值
- (void)setModelObject:(id)modelObject{
    LZTableRightDataModel *dataModel = modelObject;
    self.contenView.dataModel = dataModel;
}
@end
