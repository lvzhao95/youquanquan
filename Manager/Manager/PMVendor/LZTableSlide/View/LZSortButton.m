//
//  LZSortButton.m
//  Manager
//
//  Created by lvzhao on 2020/7/31.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZSortButton.h"

@implementation LZSortButton

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}


- (void)setSortStatue:(PMTableSort)sortStatue{
    _sortStatue = sortStatue;
    switch (sortStatue) {
        case PMTableSortNormal:
            
            [self setImage:k_imageName(@"pm_sort_nor") forState:UIControlStateNormal];
            
            break;
            
        case PMTableSortAscend:
            [self setImage:k_imageName(@"pm_sort_ascend") forState:UIControlStateNormal];

            break;
        case PMTableSortDescen:
            [self setImage:k_imageName(@"pm_sort_descen") forState:UIControlStateNormal];

            break;
        default:
            [self setImage:nil forState:UIControlStateNormal];
            break;
    }
}

@end

