//
//  PMLastRightCell.m
//  Manager
//
//  Created by lvzhao on 2020/7/31.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMLastRightCell.h"

@interface PMLastRightCell ()

@property (nonatomic,strong) UIButton *optionBtn;

@end

@implementation PMLastRightCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        self.backgroundColor = UIColor.whiteColor;
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
   
    self.optionBtn = [UIButton lz_buttonImageName:@"pm_option_table" backgroundImageName:@""];
    [self.contentView addSubview:self.optionBtn];
    [self.optionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(0);
        make.width.mas_equalTo(self);
        make.height.mas_equalTo(self);
        make.left.mas_equalTo(0);
    }];
    self.optionBtn.userInteractionEnabled = NO;
    [self.contentView addSubview:self.bottomLine];
    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(0);
        make.width.mas_equalTo(self);
        make.height.mas_equalTo(1);
        make.left.mas_equalTo(0);
    }];
       
}

- (UIView *)bottomLine{
    if (!_bottomLine) {
        _bottomLine = [[UIView alloc]init];
        _bottomLine.backgroundColor = RGBA(140, 143, 255, 0.1);
    }
    return _bottomLine;
}


@end
