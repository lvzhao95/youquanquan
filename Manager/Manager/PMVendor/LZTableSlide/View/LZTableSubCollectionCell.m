//
//  LZTableSubCollectionCell.m
//  Manager
//
//  Created by lvzhao on 2020/7/31.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZTableSubCollectionCell.h"
#import "LZTableDataModel.h"

@interface LZTableSubCollectionCell ()

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UIView *lineView;

@end
@implementation LZTableSubCollectionCell
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.titleLabel];
        [self addSubview:self.lineView];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.titleLabel.frame = CGRectMake(0, 0, self.width, self.height);
    self.lineView.frame = CGRectMake(self.width - 1, 0, 1, self.height);

}

- (void)setModelObject:(id)modelObject{
    LZTableRightSubDataModel *dataModel = modelObject;
    self.titleLabel.text = dataModel.titleString;
    
    
    
}

- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.numberOfLines = 0;
        _titleLabel.font = [UIFont systemFontOfSize:12];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLabel;
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = RGBA(140, 143, 255, 0.1);
    }
    return _lineView;
}
@end
