//
//  LZTableCollectionView.h
//  Manager
//
//  Created by lvzhao on 2020/7/31.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LZTableDataModel.h"
#import "LZSortButton.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^touchCallBlock)(PMTouchTable touchTable, NSInteger index ,LZSortButton * sortBtn);

@interface LZTableCollectionView : UIView
@property (nonatomic, copy) touchCallBlock callBlock;

@property (nonatomic, strong) LZTableRightDataModel *dataModel;

@end

NS_ASSUME_NONNULL_END
