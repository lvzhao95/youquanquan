//
//  LZTableCollectionView.m
//  Manager
//
//  Created by lvzhao on 2020/7/31.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZTableCollectionView.h"
#import "LZTableSubCollectionCell.h"


@interface LZTableCollectionView ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UICollectionViewFlowLayout *layout;
@property (nonatomic, strong) UIView *bottomLine;

@end

@implementation LZTableCollectionView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self addSubview:self.collectionView];
        [self addSubview:self.bottomLine];
    }
    return self;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    self.collectionView.frame = CGRectMake(0, 0, self.width, self.height - 1);
    self.bottomLine.frame = CGRectMake(0, self.collectionView.bottom, self.width, 1);
}

#pragma - mark UICollectionViewDelegate,UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataModel.itemModelArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *baseCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LZTableSubCollectionCell" forIndexPath:indexPath];
    
     if (self.dataModel.classNames) {
        for (NSString *className in self.dataModel.classNames) {
            baseCell = [collectionView dequeueReusableCellWithReuseIdentifier:className forIndexPath:indexPath];
        }
     }
    
    SEL sel = NSSelectorFromString(self.dataModel.setDataMethodName);
    if ([baseCell respondsToSelector:sel]) {
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            [baseCell performSelector:sel withObject:self.dataModel.itemModelArray[indexPath.row]];
    #pragma clang diagnostic pop
    }
    return baseCell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.callBlock) {
        self.callBlock(PMTouchTableTop,indexPath.row,nil);
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger width = [self.dataModel.columnWidthArray[indexPath.row] integerValue];
    return CGSizeMake(width, self.dataModel.itemHeight);
}

#pragma -mark lazy load

- (UICollectionView *)collectionView{
    if (!_collectionView) {
        _collectionView = [[UICollectionView alloc]initWithFrame:self.bounds collectionViewLayout:self.layout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.scrollEnabled = NO;
        [_collectionView registerClass:[LZTableSubCollectionCell class] forCellWithReuseIdentifier:@"LZTableSubCollectionCell"];
    }
    return _collectionView;
}

- (void)setDataModel:(LZTableRightDataModel *)dataModel{
    _dataModel = dataModel;
    if (self.dataModel.classNames) {
        for (NSString *className in self.dataModel.classNames) {
            Class class = NSClassFromString(className);
            [self.collectionView registerClass:class forCellWithReuseIdentifier:className];
        }
    }
    [self.collectionView reloadData];
}

- (UICollectionViewFlowLayout *)layout{
    if (!_layout) {
        _layout = [[UICollectionViewFlowLayout alloc]init];
        _layout.minimumLineSpacing = 0;
        _layout.minimumInteritemSpacing = 0;
    }
    return _layout;
}

- (UIView *)bottomLine{
    if (!_bottomLine) {
        _bottomLine = [[UIView alloc]init];
        _bottomLine.backgroundColor = RGBA(140, 143, 255, 0.1);
    }
    return _bottomLine;
}



@end
