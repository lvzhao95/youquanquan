//
//  LZLeftTableCell.m
//  Manager
//
//  Created by lvzhao on 2020/7/31.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZLeftTableCell.h"
#import "LZTableDataModel.h"

@interface LZLeftTableCell ()

@property (nonatomic,strong) UILabel *titleLabel;



@end

@implementation LZLeftTableCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        self.selectionStyle = UITableViewCellSelectionStyleNone;

        self.backgroundColor = UIColor.whiteColor;
        [self setupUI];
    }
    return self;
}




- (void)setupUI
{
    [self.contentView addSubview:self.rightLine];
    [self.contentView addSubview:self.bottomLine];
    [self.rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(self);
        make.top.mas_equalTo(0);
    }];
    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(0);
        make.width.mas_equalTo(self);
        make.height.mas_equalTo(1);
        make.left.mas_equalTo(0);
    }];
    
    self.titleLabel = [UILabel lz_labelWithText:@"" fontSize:PingFangSC(14) color:COLOR_textColor];
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-5);
        make.top.bottom.mas_equalTo(0);
    }];
}

- (void)setModelObject:(id)modelObject{
    LZTableLeftDataModel *dataModel = modelObject;
    self.titleLabel.text = dataModel.titleString;
}

- (UIView *)rightLine{
    if (!_rightLine) {
        _rightLine = [[UIView alloc]init];
        _rightLine.backgroundColor = RGBA(140, 143, 255, 0.1);
    }
    return _rightLine;
}
- (UIView *)bottomLine{
    if (!_bottomLine) {
        _bottomLine = [[UIView alloc]init];
        _bottomLine.backgroundColor = RGBA(140, 143, 255, 0.1);
    }
    return _bottomLine;
}
@end
