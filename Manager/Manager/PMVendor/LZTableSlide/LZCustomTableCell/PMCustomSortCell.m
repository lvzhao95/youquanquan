//
//  PMCustomSortCell.m
//  Manager
//
//  Created by lvzhao on 2020/7/31.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMCustomSortCell.h"
#import "LZTableDataModel.h"
#import "LZSortButton.h"


@interface PMCustomSortCell ()

@property (nonatomic, strong) LZSortButton *sortBtn;

@end
@implementation PMCustomSortCell
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = UIColorHex(0xF8F9FF);
        [self addSubview:self.sortBtn];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.sortBtn.frame = CGRectMake(10, 0, self.width, self.height);

}

- (void)setModelObject:(id)modelObject{
    LZTableRightSubDataModel *dataModel = modelObject;
    [self.sortBtn setTitle:dataModel.titleString forState:UIControlStateNormal];
    self.sortBtn.frame = CGRectMake(10, 0, self.width - 10, 44);
    [self.sortBtn SG_imagePositionStyle:SGImagePositionStyleRight spacing:5];
    self.sortBtn.userInteractionEnabled = NO;
}

- (LZSortButton *)sortBtn{
    if (!_sortBtn) {
        _sortBtn = [[LZSortButton alloc] init];
        [_sortBtn setTitleColor:UIColorHex(0x666666) forState:UIControlStateNormal];
        _sortBtn.titleLabel.font = PingFangSC(13);
        _sortBtn.sortStatue = PMTableSortNormal;
        _sortBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    }
    return _sortBtn;
}
@end
