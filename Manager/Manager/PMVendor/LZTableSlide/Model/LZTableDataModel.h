//
//  LZTableDataModel.h
//  Manager
//
//  Created by lvzhao on 2020/7/31.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>

/// 点击位置
typedef NS_ENUM(NSInteger, PMTouchTable) {
    // 最上
    PMTouchTableTop = 0,    //
    // 最上左
     PMTouchTableTopLeft = 1,    //
    //最右边
    PMTouchTableRight = 2,
};

@class LZTableRightDataModel,LZTableLeftDataModel,LZTableRightSubDataModel;
NS_ASSUME_NONNULL_BEGIN

@interface LZTableDataModel : NSObject
///第一列宽度
@property (nonatomic, assign) CGFloat  leftWidth;
///第一行的高度
@property (nonatomic, assign) CGFloat  topHeight;

//自定义行高
@property (nonatomic, assign) CGFloat  rowHeight;

///列宽数组 . 自定义列宽, 默认 80
@property (nonatomic, strong) NSArray *itemWidthArray;
@property (nonatomic, assign) CGFloat totalItemWidth;          // 总宽度

///行高数据. 不传的话，以rowHeight 定高
@property (nonatomic,strong)  NSArray *itemHeightArray;

///上方表头的数据
@property (nonatomic, strong) LZTableRightDataModel *topDataModel;

///左侧tableView的数据源
@property (nonatomic, strong) NSArray <LZTableLeftDataModel *>*leftDataArray;   // 左侧方=列表头数据

///自定义左侧cell的类名 , 此cell必须继承LZBaseTableSliderCell
@property (nonatomic, copy) NSString *leftCellClass;

//UICollectionViewCell
@property (nonatomic, copy) NSString *rightCellClass;

@property (nonatomic, strong) NSArray *dataArray;  // 数据源

///最左边的按钮使用
@property (nonatomic, strong) LZTableRightSubDataModel *topLeftDataModel;

@end


///左侧的数据
@interface LZTableLeftDataModel : NSObject
@property (nonatomic, copy) NSString *titleString;

@end




@interface LZTableRightDataModel : NSObject

//继承 UICollectionViewCell
@property (nonatomic, copy) NSMutableArray *classNames; // 注册多个每个单元格小item cell类型
@property (nonatomic, copy) NSString *setDataMethodName;   // cell 设置数据的方法签名

@property (nonatomic, strong) NSArray *columnWidthArray;
@property (nonatomic, assign) CGFloat  itemHeight;          // 单元格高度
@property (nonatomic, strong) NSArray *itemModelArray; //  每个单元格小item 数组

@end

@interface LZTableRightSubDataModel : NSObject
@property (nonatomic, copy) NSString *titleString;  // 标题



@property (nonatomic, strong) UIColor *textColor;  //
@property (nonatomic, strong) UIColor *bColor;  //
@property (nonatomic, strong) UIColor *backgroundColor;  //



@property (nonatomic, assign) CGFloat itemHeight;  // 单元格高度
@property (nonatomic, assign) CGFloat itemWidth;   // 单元格宽度
@property (nonatomic, assign) NSInteger sortStatus;  //排序状态
@property (nonatomic, strong) NSString *param;  // 参数

@end

NS_ASSUME_NONNULL_END


