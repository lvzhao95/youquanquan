//
//  LZTableDataModel.m
//  Manager
//
//  Created by lvzhao on 2020/7/31.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZTableDataModel.h"
@implementation LZTableDataModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.leftCellClass = @"LZBaseTableViewCell";
        
        
        self.leftWidth = 180.0f;
        self.topHeight = 44;
        self.rowHeight = 44;
        self.totalItemWidth = 0.0f;
        NSInteger count = 20;
        
        //宽度数组
        NSInteger itemWidth = 0;
        NSMutableArray *itemWidthArray = [[NSMutableArray alloc]init];
        for (int i = 0; i < count; i++){
            
           //计算高度
           if (i % 3 == 0) {
               itemWidth = 180;
           } else if(i % 2 == 0){
               itemWidth = 180;
           } else {
               itemWidth = 120;
           }
            [itemWidthArray addObject:@(itemWidth)];
            self.totalItemWidth += itemWidth;
        }
    
        
        
        /// top数据
        self.topDataModel = [[LZTableRightDataModel alloc] init];

        NSMutableArray *topSubDataArray = [[NSMutableArray alloc] init];
        for ( int i = 0; i < count; i++) {
            LZTableRightSubDataModel *subDataModel = [[LZTableRightSubDataModel alloc]init];
            subDataModel.titleString = [NSString stringWithFormat:@"列表头 %d",i];
            subDataModel.itemHeight = self.rowHeight;
            [topSubDataArray addObject:subDataModel];
        }
        [self.topDataModel.classNames addObjectsFromArray:@[@"PMCustomSortCell"]];
        self.topDataModel.setDataMethodName = @"setModelObject:";
        self.topDataModel.itemModelArray = topSubDataArray;
        self.topDataModel.columnWidthArray = itemWidthArray;
        self.topDataModel.itemHeight = self.rowHeight;
        

        //行高数组
        NSMutableArray *rowHeightArray = [[NSMutableArray alloc]init];
        //坐标数据模型数组
        NSMutableArray * leftTableDataArray = [[NSMutableArray alloc]init];
        //右侧数据模型数组
        NSMutableArray * rowDataArray = [[NSMutableArray alloc]init];
        
        // 根据房间数构造需要的行数据
        for (int i = 0 ; i < count; i++) {
            // 左侧列表数据
            LZTableLeftDataModel * model = [[LZTableLeftDataModel alloc]init];
            model.titleString = [NSString stringWithFormat:@"我是 第 %d 行",i];
            [leftTableDataArray addObject:model];
            
            //右侧列表数据
            LZTableRightDataModel *rightDataModel = [[LZTableRightDataModel alloc]init];
            
            rightDataModel.itemHeight = self.rowHeight;

            [rowHeightArray addObject:@(self.rowHeight)];
            ///右侧数据源
            NSMutableArray * rightItemArray = [[NSMutableArray alloc]init];
            

            for ( int j = 0; j < count; j++) {
                LZTableRightSubDataModel *itemModel = [[LZTableRightSubDataModel alloc]init];
                itemModel.titleString = [NSString stringWithFormat:@"%d-行 / %d-列",i,j];
                itemModel.itemHeight = self.rowHeight;
                [rightItemArray addObject:itemModel];
            }
            rightDataModel.columnWidthArray = itemWidthArray;
            rightDataModel.itemModelArray = rightItemArray;
            [rowDataArray addObject:rightDataModel];
            
            
            rightDataModel.setDataMethodName = @"setModelObject:";
            [rightDataModel.classNames addObjectsFromArray:@[@"PMCustomPayCell"]];

            
        }
        
       

        
        
        self.leftDataArray = leftTableDataArray;
        self.dataArray = rowDataArray;
        self.itemHeightArray = rowHeightArray;
    }
    return self;
}

@end


@implementation LZTableLeftDataModel
@end
@implementation LZTableRightDataModel

- (NSMutableArray *)classNames{
    if(!_classNames){
        _classNames = [[NSMutableArray alloc] init];
    }
    return _classNames;
}

@end


@implementation LZTableRightSubDataModel
@end
