//
//  LZOptionModel.m
//  Manager
//
//  Created by lvzhao on 2020/8/4.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZOptionModel.h"

@implementation LZOptionModel


+ (NSMutableArray *)createOptionModel:(NSArray *)titles details:(NSArray *)details key:(NSString *)key{

    NSMutableArray *dataArray = [[NSMutableArray alloc] initWithCapacity:titles.count];
    NSInteger index = titles.count;
    for (int i = 0; i < index; i++) {
        LZOptionModel *optionModel = [LZOptionModel new];
        optionModel.title = titles[i];
        optionModel.detail = details[i];
        optionModel.key = key;
        [dataArray addObject:optionModel];
    }
    return dataArray;
}
@end
