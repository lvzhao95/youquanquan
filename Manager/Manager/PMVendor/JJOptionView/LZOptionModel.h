//
//  LZOptionModel.h
//  Manager
//
//  Created by lvzhao on 2020/8/4.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LZOptionModel : NSObject

@property (nonatomic,strong) NSString *title;

@property (nonatomic,strong) NSString *key;
@property (nonatomic,strong) NSString *detail;


+ (NSMutableArray *)createOptionModel:(NSArray *)titles details:(NSArray *)detail key:(NSString *)key;

@end

NS_ASSUME_NONNULL_END
