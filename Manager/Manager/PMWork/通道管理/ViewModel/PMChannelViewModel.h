//
//  PMChannelViewModel.h
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZBaseViewModel.h"
#import "LZTableDataModel.h"
#import "PMChannelAnalysisModel.h"
#import "PMChannelCategoryModel.h"
#import "PMAccessChannelModel.h"
#import "PMChannelCategoryModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface PMChannelViewModel : LZBaseViewModel

@property (nonatomic,assign) NSInteger index;
//表格数据源
@property (nonatomic,strong) LZTableDataModel *dataModel;

//参数
@property (nonatomic,strong) NSMutableDictionary *paramsDictionary;

///新增通道
@property (nonatomic,strong) PMChannelCategoryModel *currentChannelModel;


///应用通用
@property (nonatomic,strong) NSMutableArray *channelArray;



///获取应用通道
- (void)getChannelList;






///获取入金通道详情
- (void)getAccessChannelDetail:(BOOL)isFrist;

///通道分析
- (void)getChannelSummary:(BOOL)isFrist;

///启动/禁用
- (void)disableMerchantChannel:(NSDictionary *)dataDictionary;
- (void)allDisableMerchantChannel:(NSDictionary *)dataDictionary;


///删除
- (void)deleteMerchantChannel:(NSDictionary *)dataDictionary;

@end

NS_ASSUME_NONNULL_END
