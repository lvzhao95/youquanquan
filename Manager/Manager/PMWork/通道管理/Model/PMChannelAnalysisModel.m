//
//  PMChannelAnalysisModel.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMChannelAnalysisModel.h"

@implementation PMChannelAnalysisModel
- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic{
    
    
    self.sum_amount = [NSString stringWithFormat:@"￥ %@",[self.sum_amount yw_stringByDividingBy:@"100"]];
    self.sum_settle_fee = [NSString stringWithFormat:@"￥ %@",[self.sum_settle_fee yw_stringByDividingBy:@"100"]];

    NSString *count_order = [NSString stringWithFormat:@"%@",self.count_order];
    NSString *success_order = [NSString stringWithFormat:@"%@",self.success_order];

    if([count_order isEqualToString:@"0"]){
        self.success_rate  = @"0.00%";
    } else {
        
        self.success_rate = [[NSString stringWithFormat:@"%f",[success_order floatValue] / [count_order floatValue]] yw_stringByMultiplyingBy:@"100"];
        self.success_rate = [NSString stringWithFormat:@"%@%%",self.success_rate];
    }
    
    
    NSString *status = dic[@"status"];
    if([status isEqualToString:@"DISABLE"]){
        self.status = @"禁用";
    } else if([status isEqualToString:@"ENABLE"]){
        self.status = @"启用";
    }

    
   
    return YES;
}
@end
