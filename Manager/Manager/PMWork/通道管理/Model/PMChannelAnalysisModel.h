//
//  PMChannelAnalysisModel.h
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PMChannelAnalysisModel : NSObject


@property (nonatomic , copy) NSString              * scene;
@property (nonatomic , copy) NSString*              fail_order;
@property (nonatomic , copy) NSString*              agent_rate;
@property (nonatomic , copy) NSString              * status;
@property (nonatomic , copy) NSString*              sum_amount;
@property (nonatomic , copy) NSString              * channel_key;
@property (nonatomic , copy) NSString*              sort;
@property (nonatomic , copy) NSString              * vendor;
@property (nonatomic , copy) NSString              * settle_period;
@property (nonatomic , copy) NSString              * channel_name;
@property (nonatomic , copy) NSString*              merchant_rate;
@property (nonatomic , copy) NSString              * type;
@property (nonatomic , copy) NSString              * id;
@property (nonatomic , copy) NSString*              is_transfer;
@property (nonatomic , copy) NSString*              single_min;
@property (nonatomic , copy) NSString*              single_max;
@property (nonatomic , copy) NSString*              count_order;
@property (nonatomic , copy) NSString*              success_order;
@property (nonatomic , copy) NSString*              basic_rate;
@property (nonatomic , copy) NSString*              sum_settle_fee;


@property (nonatomic , copy) NSString*              success_rate;


@end

NS_ASSUME_NONNULL_END
