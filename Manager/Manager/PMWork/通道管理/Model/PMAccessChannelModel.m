//
//  PMAccessChannelModel.m
//  Manager
//
//  Created by lvzhao on 2020/8/6.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMAccessChannelModel.h"

@implementation PMAccessChannelModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"modelid" : @"id"};
}

- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic{
    
    
    self.single_max_str = [NSString stringWithFormat:@"￥ %@",[self.single_max yw_stringByDividingBy:@"100"]];
    self.single_min_str = [NSString stringWithFormat:@"￥ %@",[self.single_min yw_stringByDividingBy:@"100"]];

    
    
    
    NSString *status = dic[@"status"];
    if([status isEqualToString:@"DISABLE"]){
        self.status = @"禁用";
    } else if([status isEqualToString:@"ENABLE"]){
        self.status = @"启用";
    }

    
    return YES;
}


@end
