//
//  PMAgentChannelViewController.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//  代付

#import "PMAgentChannelViewController.h"
#import "PMChannelViewModel.h"
#import "LZTableSlideView.h"
#import "PMChannelCategoryView.h"
#import "PMPublicManager.h"
#import "LZMJDIYRefreshHeader.h"
#import "LZMJDIYRefeshFooter.h"
#import "PMEditChannelViewController.h"

@interface PMAgentChannelViewController ()

@property (nonatomic,strong) PMChannelViewModel *viewModel;

@property (nonatomic,strong) LZTableSlideView *tableView;
@end

@implementation PMAgentChannelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self setupUI];
    
    [self reloadRightItem];
}

//刷新右按钮
- (void)reloadRightItem{
    
    ///!!!:筛选
    self.navigationItem.rightBarButtonItem = nil;
    UIButton *screeningBtn = [UIButton lz_buttonImageName:@"pm_shaixuan" backgroundImageName:@""];
    screeningBtn.frame = CGRectMake(0, 0, 44, 44);
    [LZTool.currentViewController initBarItem:screeningBtn withType:1];
    [screeningBtn addTarget:self action:@selector(screenClick:) forControlEvents:UIControlEventTouchUpInside];
}

- (void) setupUI{
    
    ///!!!:新增通道
    UIButton *addChannelBtn = [UIButton lz_buttonTitle:@"新增通道" titleColor:COLOR_appColor fontSize:15];
    addChannelBtn.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:addChannelBtn];
    [addChannelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(50);
        make.bottom.mas_equalTo(K_Device_Is_iPhoneX ? - 34:0);
    }];
    
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = COLOR_cellLine;
    [addChannelBtn addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(0.5);
        make.top.mas_equalTo(0);
    }];
    
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(0);
        make.bottom.mas_equalTo(addChannelBtn.mas_top);
    }];
    @weakify(self);
    [self.tableView setCallBlock:^(PMTouchTable touchTable, NSInteger index, LZSortButton * _Nullable sortBtn) {
         @strongify(self);
         switch (touchTable) {
             case PMTouchTableRight:
                 [self selectTableIndex:index];
                 break;

             default:
                 break;
         }
     }];
       
    [self addRefresh];
    
    showLoadingTitle(@"", nil);
    [self.viewModel getChannelList];

    [self.viewModel.reloadSubject subscribeNext:^(id  _Nullable x) {
        
        dismiss(nil);
        @strongify(self);
        [self reloadTable:[x intValue]];
        
    }];
}


///添加下拉, 上啦
- (void)addRefresh{
    
    @weakify(self);
    self.tableView.rightTableView.mj_header = [LZMJDIYRefreshHeader headerWithRefreshingBlock:^{
        NSLog(@"下拉刷新");
        @strongify(self);
        [self.viewModel getAccessChannelDetail:YES];
 
    }];
    self.tableView.rightTableView.mj_footer = [LZMJDIYRefeshFooter footerWithRefreshingBlock:^{
        NSLog(@"上拉加载");
        @strongify(self);
        [self.viewModel getAccessChannelDetail:NO];
     
    }];
    
    // 不让mj 自适应宽度
    self.tableView.rightTableView.mj_header.autoresizingMask = UIViewAutoresizingNone;
    self.tableView.rightTableView.mj_header.mj_x = -self.viewModel.dataModel.leftWidth;
    // 设置mj宽度
    self.tableView.rightTableView.mj_header.mj_w = K_SCREENWIDTH;
    self.tableView.rightTableView.mj_footer.autoresizingMask = UIViewAutoresizingNone;
    self.tableView.rightTableView.mj_footer.mj_w = K_SCREENWIDTH ;
    self.tableView.rightTableView.mj_footer.mj_x = -self.viewModel.dataModel.leftWidth;
    
}

///刷线表格
- (void)reloadTable:(NSInteger) type{
    
    if(type > 3)return;
    
    self.tableView.dataModel = self.viewModel.dataModel;
    [self.tableView.rightTableView.mj_header endRefreshing];
    [self.tableView.rightTableView.mj_footer endRefreshing];
    self.tableView.rightTableView.mj_footer.hidden = NO;
    [self.tableView  dismissEmptyView];
    switch (type) {
        case 0:{
            self.tableView.rightTableView.mj_footer.hidden = YES;
        }
            break;
        default:
            break;
    }
    
    if(self.viewModel.dataModel.dataArray.count == 0){
        self.tableView.rightTableView.mj_footer.hidden = YES;
        [self.tableView tableViewDisplayWithImgName:@"no_data" message:@"暂无数据" contentoffsetY:100 showEmptyView:YES];
    }
}


///!!!:筛选
- (void)screenClick:(UIButton *)sender{
    NSArray *subTitles = self.viewModel.channelArray;
    @weakify(self);
    [PMChannelCategoryView showObjectDict:@{@"subTitles":subTitles,
                                            @"currentChannelModel":self.viewModel.currentChannelModel}cancelBlock:^(id  _Nullable object) {
        
        NSInteger optionTag = [object integerValue];
        switch (optionTag) {
            case 10:
            {
                
            }
                break;
            case 11:
            {
                      
            }
                break;
            default:
                break;
        }
  
    
    }  sureBlock:^(id  _Nullable object) {
        @strongify(self);
        self.viewModel.currentChannelModel = object;
        if(self.viewModel.currentChannelModel.isEnanle){
            [self.viewModel.paramsDictionary removeObjectForKey:@"parent_id"];
        } else {
            [self.viewModel.paramsDictionary addEntriesFromDictionary:@{@"parent_id":self.viewModel.currentChannelModel.parent_id?:@""}];
        }
        showLoadingTitle(@"", nil);
        [self.viewModel getAccessChannelDetail:YES];
    }];
}
///选择第几行操作
- (void)selectTableIndex:(NSInteger) index{
    
    PMAccessChannelModel *channelModel = self.viewModel.dataArray[index];
    
    NSMutableArray *subTitles = [[NSMutableArray alloc] init];
    [subTitles addObject:@"编辑"];
    [subTitles addObject:@"删除"];
    
    if([channelModel.status isEqualToString:@"启用"]){
        [subTitles addObject:@"禁用"];
        [subTitles addObject:@"一键禁用"];
    } else {
        [subTitles addObject:@"启用"];
        [subTitles addObject:@"一键启用"];
    }

    @weakify(self);
    [LZToolView showAlertType:LZAlertTypeActionSheet withTitle:@"" message:@"" cancel:@"取消" sure:@"" objectDict:@{@"subTitles":subTitles} cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
    
        NSString *subtitle = subTitles[[object intValue]];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            @strongify(self);
            if([subtitle containsString:@"编辑"]){
                
                RACSubject *subject = [RACSubject subject];
                PMEditChannelViewController *channelVC = [[PMEditChannelViewController alloc] init];
                channelVC.subject = subject;
                channelVC.channelCategoryModel = self.viewModel.currentChannelModel;
                channelVC.channelModel = channelModel;
                [LZTool.currentViewController.navigationController pushViewController:channelVC animated:YES];
                [subject subscribeNext:^(id  _Nullable x) {
                    @strongify(self);
                    [self.viewModel getAccessChannelDetail:YES];
                }];
                
            } else if([subtitle containsString:@"删除"]){
                
                [self deleteMerchantChannel:channelModel];

            } else if([subtitle containsString:@"启用"]
                      ||[subtitle containsString:@"禁用"]){
                
                [self disableMerchantChannel:channelModel title:subtitle];
                
            }
        });
  
        
    }];
}

/**删除//*/
- (void) deleteMerchantChannel:(PMAccessChannelModel *)channelModel {
    @weakify(self);
    [LZToolView showAlertType:LZAlertTypeTipAlter withTitle:@"确定删除" message:[NSString stringWithFormat:@"确定要删除通道%@",channelModel.channel_name] cancel:@"取消" sure:@"确定" objectDict:nil cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
          @strongify(self);
          NSMutableDictionary *dataDictionary = [[NSMutableDictionary alloc] init];
          dataDictionary[@"channel_id"] = channelModel.modelid;
          [self.viewModel deleteMerchantChannel:dataDictionary];
    }];
}

 
 ///启用
- (void) disableMerchantChannel:(PMAccessChannelModel *)channelModel title:(NSString *)title{
    
    NSString *message = @"";
    
    if([title isEqualToString:@"启用"]){
        
        message = [NSString stringWithFormat:@"确定启用商户已授权的%@通道",channelModel.channel_name];

    } else if([title isEqualToString:@"一键启用"]){
        message = [NSString stringWithFormat:@"确定一键启用所有商户已授权的%@通道",channelModel.channel_name];

    } else if([title isEqualToString:@"禁用"]){
        message = [NSString stringWithFormat:@"确定禁用商户已授权的%@通道",channelModel.channel_name];

    }else if([title isEqualToString:@"一键禁用"]){
        message = [NSString stringWithFormat:@"确定一键禁用所有商户已授权的%@通道",channelModel.channel_name];

    }
    
    @weakify(self);
    [LZToolView showAlertType:LZAlertTypeTipAlter withTitle:title message:message cancel:@"取消" sure:@"确定" objectDict:nil cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
        @strongify(self);
        NSMutableDictionary *dataDictionary = [[NSMutableDictionary alloc] init];
        dataDictionary[@"channel_id"] = channelModel.modelid;
        dataDictionary[@"status"] = [title containsString:@"启用"] ? @"ENABLE":@"DISABLE";
        
        if([title containsString:@"一键"]){
            [self.viewModel allDisableMerchantChannel:dataDictionary];
        } else {
            [self.viewModel disableMerchantChannel:dataDictionary];
        }
    }];

}

- (PMChannelViewModel *)viewModel{
    if(!_viewModel){
        _viewModel = [[PMChannelViewModel alloc] init];
        _viewModel.index = 1;
    }
    return _viewModel;
}

- (LZTableSlideView *)tableView{
    if(!_tableView){
        _tableView = [[LZTableSlideView alloc] initWithFrame:CGRectMake(0, 0, K_SCREENWIDTH, K_SCREENHEIGHT - 45 - K_NAVHEIGHT)];
    }
    return _tableView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
