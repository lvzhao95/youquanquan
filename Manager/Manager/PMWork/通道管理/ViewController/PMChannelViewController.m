//
//  PMChannelViewController.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMChannelViewController.h"
#import "PMChannelView.h"
#import "PMChannelViewModel.h"

@interface PMChannelViewController ()
@property (nonatomic,strong) PMChannelViewModel *viewModel;

@property (nonatomic,strong) PMChannelView *channelView;
@end

@implementation PMChannelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"通道管理";
    
    [self setupUI];
    // Do any additional setup after loading the view.
}

- (void)setupUI{
    [self.view addSubview:self.channelView];
    [self.channelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

#pragma mark -懒加载
- (PMChannelView *)channelView{
    if(!_channelView) {
        _channelView = [[PMChannelView alloc] initViewModel:self.viewModel];
    }
    return _channelView;
}

- (PMChannelViewModel *)viewModel{
    if(!_viewModel) {
        _viewModel = [[PMChannelViewModel alloc] init];
    }
    return _viewModel;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
