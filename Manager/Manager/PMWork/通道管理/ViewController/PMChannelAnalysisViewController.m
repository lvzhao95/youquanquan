//
//  PMChannelAnalysisViewController.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMChannelAnalysisViewController.h"
#import "LZBaseTableView.h"
#import "PMChannelAnalysisCell.h"
#import "PMChannelViewModel.h"
#import "LZMJDIYRefreshHeader.h"
#import "LZMJDIYRefeshFooter.h"
#import "PMScreeningViewController.h"


@interface PMChannelAnalysisViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) PMChannelViewModel *viewModel;

@property (nonatomic,strong) LZBaseTableView *tableView;
@end

@implementation PMChannelAnalysisViewController

- (void)viewDidLoad {
    [super viewDidLoad];


}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self setupUI];
}

//刷新右按钮
- (void)reloadRightItem{
    
    ///!!!:筛选
    self.navigationItem.rightBarButtonItem = nil;
    UIButton *screeningBtn = [UIButton lz_buttonImageName:@"pm_shaixuan" backgroundImageName:@""];
    screeningBtn.frame = CGRectMake(0, 0, 44, 44);
    [LZTool.currentViewController initBarItem:screeningBtn withType:1];
    [screeningBtn addTarget:self action:@selector(screenClick:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setupUI{
    
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, K_SCREENWIDTH, 10)];
    self.tableView.tableHeaderView = headView;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    
    showLoadingTitle(@"", nil);
    [self.viewModel getChannelSummary:YES];
    
    @weakify(self);
    [self.viewModel.reloadSubject subscribeNext:^(id  _Nullable x) {
        dismiss(nil);
        @strongify(self);
        [self reloadTable:[x integerValue]];
        
    }];
    
    
    self.tableView.mj_header = [LZMJDIYRefreshHeader headerWithRefreshingBlock:^{
        NSLog(@"下拉刷新");
        @strongify(self);
        [self.viewModel getChannelSummary:YES];
    }];
    self.tableView.mj_footer = [LZMJDIYRefeshFooter footerWithRefreshingBlock:^{
        NSLog(@"上拉加载");
        @strongify(self);
        [self.viewModel getChannelSummary:NO];
    }];
    self.tableView.mj_footer.hidden = YES;

}

///刷线表格
- (void)reloadTable:(NSInteger) type{
    
    if(type > 3)return;
    [self.tableView reloadData];
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    self.tableView.mj_footer.hidden = NO;
    [self.tableView  dismissEmptyView];
    switch (type) {
        case 0:{
            self.tableView.mj_footer.state = MJRefreshStateNoMoreData;
        }
            break;
        default:
            break;
    }
    
    if(self.viewModel.dataArray.count == 0){
        self.tableView.mj_footer.hidden = YES;
        [self.tableView tableViewDisplayWithImgName:@"no_data" message:@"暂无数据" contentoffsetY:100 showEmptyView:YES];
    }
}

///!!!:筛选
- (void)screenClick:(UIButton *)sender{
   RACSubject *subject = [RACSubject subject];
     PMScreeningViewController *screeningVC = [[PMScreeningViewController alloc] init];
     screeningVC.pickerModel = PMDatePickerModelYM;
     screeningVC.reloadSubject = subject;
     [LZTool.currentViewController.navigationController pushViewController:screeningVC animated:YES];
     [subject subscribeNext:^(id  _Nullable x) {
         NSDictionary *dictionary = x;
         showLoadingTitle(@"", nil);
         [self.viewModel.paramsDictionary addEntriesFromDictionary:dictionary];
         [self.viewModel getChannelSummary:YES];
     }];
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewModel.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PMChannelAnalysisCell * channelCell = [tableView dequeueReusableCellWithIdentifier:@"PMChannelAnalysisCell"];
    if(self.viewModel.dataArray.count > indexPath.row){
        channelCell.modelObject = self.viewModel.dataArray[indexPath.row];
    }
    return channelCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    
}


#pragma mark -lanjiazai
- (LZBaseTableView *)tableView{
    if(!_tableView){
        _tableView = [[LZBaseTableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.backgroundColor = COLOR_background;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 245;
        [_tableView registerClass:[PMChannelAnalysisCell class] forCellReuseIdentifier:@"PMChannelAnalysisCell"];
    }
    return _tableView;
}

- (PMChannelViewModel *)viewModel{
    if(!_viewModel){
        _viewModel = [[PMChannelViewModel alloc] init];
    }
    return _viewModel;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
