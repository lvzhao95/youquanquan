//
//  PMChannelAnalysisCell.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMChannelAnalysisCell.h"
#import "PMChannelAnalysisModel.h"


@interface PMChannelAnalysisCell ()

///支付方式
@property (nonatomic,strong) UILabel *titleLab;

///详情lab
@property (nonatomic,strong) NSMutableArray *detailLabs;

//数量
@property (nonatomic,strong) NSMutableArray *countLabs;

//启动
@property (nonatomic,strong) UIButton *startBtn;

@end

@implementation PMChannelAnalysisCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.contentView.backgroundColor = COLOR_background;
        [self setupView];
    }
    return self;
}


//设置View
- (void)setupView{
        
    
    UIView *contView = [[UIView alloc] init];
    contView.backgroundColor = UIColor.whiteColor;
    [self.contentView addSubview:contView];
    [contView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.width.mas_equalTo(K_SCREENWIDTH - 20);
        make.height.mas_equalTo(230);
        make.top.mas_equalTo(0);
    }];
    contView.cornerRadius = 6;
    
    
    
    //title
    self.titleLab = [UILabel lz_labelWithText:@"" fontSize:PingFangSC_M(15) color:COLOR_textColor];
    [contView addSubview:self.titleLab];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(12);
        make.height.mas_equalTo(21);
        make.right.mas_equalTo(-60);
    }];

    
    ///!!!:启动
    [contView addSubview:self.startBtn];
    [self.startBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(30);
        make.centerY.mas_equalTo(self.titleLab.mas_centerY);
        make.height.mas_equalTo(25);
        make.right.mas_equalTo(-15);
    }];

    
    UIView *lineTopView = [[UIView alloc] init];
    lineTopView.backgroundColor = COLOR_cellLine;
    [contView addSubview:lineTopView];
    [lineTopView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(contView);
        make.top.mas_equalTo(45);
        make.height.mas_equalTo(0.5);
        make.left.mas_equalTo(0);
    }];
    
    {
        ///!!!:交易
        self.detailLabs = [[NSMutableArray alloc] init];

        NSArray *titles = @[@"交易金额:",@"￥0.00",
                       @"手续费：",@"￥0.00",
                       @"入金总额：",@"￥0.00"];
        //每个Item宽高
        CGFloat H = 21;
        CGFloat W = (kScreenWidth - 30 - 10 - 40)/2.0;
        //每行列数
        NSInteger rank = 2;
        //每列间距
        CGFloat rankMargin = 10;
        //每行间距
        CGFloat rowMargin = 15;
        //Item索引 ->根据需求改变索引
        NSUInteger index = titles.count;

        CGFloat top = 10;
        for (int i = 0 ; i < index; i++) {

           UILabel *detailLab = [UILabel lz_labelWithText:titles[i] fontSize:PingFangSC(15) color:COLOR_textColor];
           //Item Y轴
           NSUInteger Y = (i / rank) * (H +rowMargin);
           CGFloat X = (i % rank) * (W + rankMargin);
           //Item X轴
           [contView addSubview:detailLab];
           [detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
               make.left.mas_equalTo(X + 15);
               make.top.mas_equalTo(lineTopView.mas_bottom).mas_offset(top + Y);
               make.width.mas_equalTo(W);
               make.height.mas_equalTo(H);
           }];
           if(i % 2 == 0){
               [detailLab lz_leftAlignment];
           } else {
               [detailLab lz_rightAlignment];
               [self.detailLabs addObject:detailLab];

           }
        }

    }
    UIView *linebBottomView = [[UIView alloc] init];
    linebBottomView.backgroundColor = COLOR_cellLine;
    [contView addSubview:linebBottomView];
    [linebBottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(contView);
        make.top.mas_equalTo(lineTopView.mas_bottom).mas_offset(115);
        make.height.mas_equalTo(0.5);
        make.left.mas_equalTo(0);
    }];
    {
        
        ///!!!:交易
        self.countLabs = [[NSMutableArray alloc] init];
           
        NSArray *titles = @[@"交易笔数：0",@"成功笔数：0",
                            @"失败笔数：0",@"成功率：0"];
        //每个Item宽高
        CGFloat H = 19;
        CGFloat W = (kScreenWidth - 30 - 10 - 40)/2.0;
        //每行列数
        NSInteger rank = 2;
        //每列间距
        CGFloat rankMargin = 10;
        //每行间距
        CGFloat rowMargin = 15;
        //Item索引 ->根据需求改变索引
        NSUInteger index = titles.count;

        CGFloat top = 10;
        for (int i = 0 ; i < index; i++) {

            UILabel *detailLab = [UILabel lz_labelWithText:titles[i] fontSize:PingFangSC(13) color:COLOR_textColor];
            //Item Y轴
            NSUInteger Y = (i / rank) * (H +rowMargin);
            CGFloat X = (i % rank) * (W + rankMargin);
            //Item X轴
            [contView addSubview:detailLab];
            [detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(X + 15);
                make.top.mas_equalTo(linebBottomView.mas_bottom).mas_offset(top + Y);
                make.width.mas_equalTo(W);
                make.height.mas_equalTo(H);
            }];
                        
            [self.countLabs addObject:detailLab];

        }
    }

}

- (void)setModelObject:(id)modelObject{
    PMChannelAnalysisModel *analysisModel = modelObject;
    self.titleLab.text = analysisModel.channel_name;
    
    NSArray *details = @[analysisModel.sum_amount,analysisModel.sum_settle_fee,analysisModel.sum_amount];
    for (int i = 0; i < details.count; i++){
        UILabel *detailLab = self.detailLabs[i];
        detailLab.text = details[i];
    }
    
    

    
    NSArray *counts = @[[NSString stringWithFormat:@"交易笔数：%@",analysisModel.count_order],
                        [NSString stringWithFormat:@"成功笔数：%@",analysisModel.success_order],
                        [NSString stringWithFormat:@"失败笔数：%@",analysisModel.fail_order],
                        [NSString stringWithFormat:@"成功率：%@",analysisModel.success_rate]];
    for (int i = 0; i < counts.count; i++){
        UILabel *countLab = self.countLabs[i];
        countLab.text = counts[i];
    }
    
    [self.startBtn setTitle:analysisModel.status forState:UIControlStateNormal];
    
}

- (UIButton *)startBtn{
    if(!_startBtn){
        _startBtn = [UIButton lz_buttonTitle:@"启用" titleColor:UIColorHex(0x0091FF) fontSize:13];
    }
    return _startBtn;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
