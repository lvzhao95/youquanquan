//
//  PMChannelTextField.h
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PMChannelTextField : UIView

@property (nonatomic,strong) UITextField *textField;

@property (nonatomic,strong) UILabel *titleLab;

@property (nonatomic, strong) NSString *placeholder;

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *content;

@property (nonatomic, strong) NSString *key;

@end

NS_ASSUME_NONNULL_END
