//
//  PMChannelView.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMChannelView.h"
#import "PMChannelViewModel.h"
#import "CKSlideMenu.h"
#import "LZBaseViewController.h"
#import "PMScreenDownView.h"

@interface PMChannelView()<CKSlideMenuDelegate>

@property (nonatomic,strong) PMChannelViewModel *viewModel;

@property (nonatomic,strong) NSMutableArray *viewControllers;

@end

@implementation PMChannelView

- (instancetype)initViewModel:(LZBaseViewModel *)viewModel
{
    self = [super initViewModel:viewModel];
    if (self) {
        self.viewModel = (PMChannelViewModel *)viewModel;
        
        [self setupView];
    }
    return self;
}

#pragma mark 创建UI
- (void) setupView{
    
    NSArray *titles = @[@"入金通道设置",@"代付通道设置",@"通道分析"];
    self.viewControllers = [NSMutableArray array];
    
    NSArray *classNames = @[@"PMAccessChannelViewController",@"PMAgentChannelViewController",@"PMChannelAnalysisViewController"];
    for (int i = 0; i < classNames.count; i++) {
        Class cts = NSClassFromString(classNames[i]);
        LZBaseViewController *vc = [[cts alloc] init];
        [self.viewControllers addObject:vc];
    }
    // 创建Menu
    CKSlideMenu *slidMenu = [[CKSlideMenu alloc] initWithFrame:CGRectMake(0, 0, K_SCREENWIDTH, 45) titles:titles controllers:self.viewControllers];
    slidMenu.backgroundColor = COLOR_nav;
    slidMenu.indicatorStyle = SlideMenuIndicatorStyleFollowText;
    slidMenu.isFixed = YES;
    slidMenu.lazyLoad = YES;
    slidMenu.selectedColor = COLOR_appColor;
    slidMenu.unselectedColor = COLOR_subTextColor;
    slidMenu.indicatorHeight = 2;
    slidMenu.indicatorColor =  COLOR_appColor;
    slidMenu.unFont = PingFangSC_M(17);
    slidMenu.font = PingFangSC_M(17);
    slidMenu.delegate = self;
    slidMenu.bodyScrollView.scrollEnabled = NO;
    [self addSubview:slidMenu];
    slidMenu.bodyFrame = CGRectMake(0,45, K_SCREENWIDTH, K_SCREENHEIGHT -  K_NAVHEIGHT - 45);
}


#pragma mark - CKSlideMenuDelegate
- (void)clickBtnActionIndx:(NSInteger)index{
    
    LZBaseViewController *baseVC  = self.viewControllers[index];
    SEL sel = NSSelectorFromString(@"reloadRightItem");
    if ([baseVC respondsToSelector:sel]) {
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
     [baseVC performSelector:sel withObject:nil];
    #pragma clang diagnostic pop
    }
}

@end
