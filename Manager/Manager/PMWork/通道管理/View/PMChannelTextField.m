//
//  PMChannelTextField.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMChannelTextField.h"
@interface PMChannelTextField()




@end
@implementation PMChannelTextField

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setupView];
    }
    return self;
}


- (void)setupView{
    [self addSubview:self.titleLab];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(24);
        make.centerY.mas_equalTo(self.mas_centerY);
    }];
    
    [self addSubview:self.textField];
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(self.titleLab.mas_right).mas_offset(8);
        make.height.mas_equalTo(24);
        make.centerY.mas_equalTo(self.mas_centerY);
    }];
    
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = COLOR_cellLine;
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(self);
        make.height.mas_equalTo(0.5);
        make.bottom.mas_equalTo(0);
    }];
}

- (NSString *)content{
    return self.textField.text;
}

- (void)setTitle:(NSString *)title{
    _title = title;
    
    self.titleLab.text = title;
    if([title containsString:@"*"]){
      
        NSAttributedString *titleAttri = [NSAttributedString attributedString:title rangeTitle:@"*" leftFont:PingFangSC(17) rightFont:PingFangSC(13) leftColor:COLOR_textColor rightColor:UIColorHex(0xE02020)];
        self.titleLab.attributedText = titleAttri;
    }
    
//    CGFloat titleW = [title lz_textWidthWithFontSize:self.titleLab.font withMaxHeight:24];
}


- (void)setPlaceholder:(NSString *)placeholder{
    _placeholder = placeholder;
    if(placeholder.length == 0)return;
    self.textField.placeholder = placeholder;
    self.textField.lz_placeholderColor = COLOR_placeholderColor;
}


- (UILabel *)titleLab{
    if(!_titleLab){
        _titleLab = [UILabel lz_labelWithText:@"" fontSize:PingFangSC(17) color:COLOR_textColor];
    }
    return _titleLab;
}



- (UITextField *)textField{
    if(!_textField){
        _textField = [[UITextField alloc] init];
        _textField.textColor = COLOR_textColor;
        _textField.font = PingFangSC(15);
    }
    return _textField;
}
@end
