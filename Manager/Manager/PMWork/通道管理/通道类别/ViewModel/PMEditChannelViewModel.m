//
//  PMEditChannelViewModel.m
//  Manager
//
//  Created by lvzhao on 2020/8/8.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMEditChannelViewModel.h"

@implementation PMEditChannelViewModel
///新增付款
- (void)createChannel:(NSDictionary *)dataDictionary{
    
    
    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kChannelCreate params:dataDictionary success:^(id  _Nullable responseObject) {
        if([responseObject[kResultStatus] isEqualToString:kCode]){
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                @strongify(self);
                [self.reloadSubject sendNext:@(1)];
                [LZTool.currentViewController.navigationController popViewControllerAnimated:YES];
            });
        }
    } failure:^(NSError * _Nullable error) {} isLoading:YES isFailTip:YES];
    
}


///更新费率
- (void)updateChannel:(NSDictionary *)dataDictionary{

    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kChannelUpdate params:dataDictionary success:^(id  _Nullable responseObject) {
        if([responseObject[kResultStatus] isEqualToString:kCode]){
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                @strongify(self);
                [self.reloadSubject sendNext:@(1)];
                [LZTool.currentViewController.navigationController popViewControllerAnimated:YES];
            });
        }
    } failure:^(NSError * _Nullable error) {} isLoading:YES isFailTip:YES];
    
}

@end
