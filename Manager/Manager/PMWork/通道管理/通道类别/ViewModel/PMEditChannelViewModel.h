//
//  PMEditChannelViewModel.h
//  Manager
//
//  Created by lvzhao on 2020/8/8.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZBaseViewModel.h"
#import "PMChannelCategoryModel.h"
#import "PMAccessChannelModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PMEditChannelViewModel : LZBaseViewModel
///新增通道
@property (nonatomic,strong) PMChannelCategoryModel *channelCategoryModel;
@property (nonatomic,strong) PMAccessChannelModel *channelModel;

//新增
- (void)createChannel:(NSDictionary *)dataDictionary;


///更新
- (void)updateChannel:(NSDictionary *)dataDictionary;


@end

NS_ASSUME_NONNULL_END
