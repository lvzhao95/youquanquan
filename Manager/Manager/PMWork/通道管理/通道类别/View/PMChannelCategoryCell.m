//
//  PMChannelCategoryCell.m
//  Manager
//
//  Created by lvzhao on 2020/8/8.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMChannelCategoryCell.h"
#import "PMChannelCategoryModel.h"

@interface PMChannelCategoryCell ()

///通道
@property (nonatomic,strong) UILabel *titleLab;

//背景
@property (nonatomic,strong) UIView *selectView;

@end

@implementation PMChannelCategoryCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.contentView.backgroundColor = COLOR_cellbackground;
        
        [self setupView];
    }
    return self;
}


- (void)setupView {
    
    self.selectView = [[UIView alloc] init];
    self.selectView.backgroundColor = COLOR_cellbackground;
    [self.contentView addSubview:self.selectView];

    self.titleLab = [UILabel lz_labelWithText:@"" fontSize:PingFangSC(15) color:COLOR_textColor];
    [self.contentView addSubview:self.titleLab];
    
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(25);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(34);
        make.centerY.mas_equalTo(self.contentView.mas_centerY);
    }];
    
    
    [self.selectView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(40);
        make.centerY.mas_equalTo(self.contentView.mas_centerY);
    }];
    [self.selectView clipRectCorner:UIRectCornerTopLeft|UIRectCornerBottomLeft cornerRadius:4];
}

- (void)setModelObject:(id)modelObject{
    PMChannelCategoryModel *channelModel = modelObject;
    self.titleLab.text = [NSString stringWithFormat:@"%@(%@)",channelModel.channel_name,channelModel.channel_key];
}


- (void)setIsSelectView:(BOOL)isSelectView{
    _isSelectView = isSelectView;
    self.selectView.backgroundColor = isSelectView ? COLOR_appColor : UIColor.whiteColor;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
