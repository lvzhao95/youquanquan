//
//  PMChannelCategoryView.h
//  Manager
//
//  Created by lvzhao on 2020/8/1.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface PMChannelCategoryView : LZBaseView


+ (void) showObjectDict:(NSDictionary *__nullable)dictionary cancelBlock:(void(^)(__nullable id object))cancelBlock sureBlock:(void(^)(__nullable id object))sureBlock;


@end

NS_ASSUME_NONNULL_END
