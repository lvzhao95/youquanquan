//
//  PMChannelCategoryCell.h
//  Manager
//
//  Created by lvzhao on 2020/8/8.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface PMChannelCategoryCell : LZBaseTableViewCell

@property (nonatomic,assign) BOOL isSelectView;

@end

NS_ASSUME_NONNULL_END
