//
//  PMChannelCategoryView.m
//  Manager
//
//  Created by lvzhao on 2020/8/1.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMChannelCategoryView.h"
#import "PMChannelCategoryModel.h"
#import "PMChannelCategoryCell.h"



@interface PMChannelCategoryView()<UIGestureRecognizerDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong)UIView *backgroundView; //
@property (nonatomic,copy) void(^sureBlock)(id object);
@property (nonatomic,copy) void(^cancelBlock)(id object);

@property (nonatomic,strong) NSDictionary *dictionary;

@property (nonatomic,strong) NSString *modelid;
@property (nonatomic,strong) UIButton *statusBtn;
@property (nonatomic,strong) UIButton *editBtn;
@property (nonatomic,strong) NSMutableArray *dataArray;


@end

@implementation PMChannelCategoryView


+ (void) showObjectDict:(NSDictionary *__nullable)dictionary cancelBlock:(nonnull void (^)(id _Nullable))cancelBlock sureBlock:(nonnull void (^)(id _Nullable))sureBlock{

    UIWindow * window = [UIApplication sharedApplication].keyWindow;
    for (UIView * aView in window.subviews) {
        if ([aView isKindOfClass:[PMChannelCategoryView class]]){
            return;
        }
    }
    PMChannelCategoryView *alertView = [[PMChannelCategoryView alloc] initWithFrame:CGRectMake(0, 0,K_SCREENWIDTH , K_SCREENHEIGHT) objectDict:dictionary cancelBlock:cancelBlock sureBlock:sureBlock];
    [window addSubview:alertView];
    return;
    
}


///block 回调 AlertView
- (id)initWithFrame:(CGRect)frame objectDict:(NSDictionary *)dictionary cancelBlock:(nonnull void (^)(id _Nullable))cancelBlock  sureBlock:(void(^)(id object))sureBlock{
    
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColorHex(0x000000) colorWithAlphaComponent:0.5];
        self.sureBlock = sureBlock;
        self.cancelBlock = cancelBlock;
        self.dictionary = dictionary;
        
        [self setupView];
    }
    return self;
}

- (void)setupView{
    
    self.backgroundView = [[UIView alloc]init];
    self.backgroundView.backgroundColor = COLOR_nav;
    self.backgroundView.frame = CGRectMake(K_SCREENWIDTH, 0, K_SCREENWIDTH - 75, K_SCREENHEIGHT);
    [self addSubview:self.backgroundView];

    self.dataArray = self.dictionary[@"subTitles"];
    PMChannelCategoryModel *currentChannelModel = self.dictionary[@"currentChannelModel"];
    self.modelid = currentChannelModel.modelid;
    
    ///!!!:title
    UILabel *titleLab = [UILabel lz_labelWithText:@"通道类别" fontSize:PingFangSC(17) color:COLOR_textColor];
    [self.backgroundView addSubview:titleLab];
    [titleLab lz_centerAlignment];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.backgroundView.mas_centerX);
        make.top.mas_equalTo(K_Device_Is_iPhoneX ? 24 + 20 : 20);
        make.width.mas_equalTo(100);
    }];
    
    
    ///!!!:操作按钮
    UIView *optionView = [[UIView alloc] init];
    optionView.backgroundColor = UIColor.whiteColor;
    [self.backgroundView addSubview:optionView];
    [optionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(K_SCREENWIDTH - 75);
        make.bottom.mas_equalTo(K_Device_Is_iPhoneX ? -34 : 0);
        make.height.mas_equalTo(50);
    }];
    CGFloat titleW = (K_SCREENWIDTH - 75)/2.0;

    self.statusBtn = [UIButton lz_buttonTitle:@"禁用" titleColor:COLOR_appColor fontSize:15];
    [self.statusBtn setTitle:@"启用" forState:UIControlStateSelected];
    [optionView addSubview:self.statusBtn];
    [self.statusBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(titleW);
        make.height.mas_equalTo(50);
        make.top.mas_equalTo(0);
    }];
    [self.statusBtn setTitle:[currentChannelModel.status isEqualToString:@"ENABLE"] ? @"禁用":@"启用" forState:UIControlStateNormal];
    self.statusBtn.tag = 10;

    
    self.editBtn = [UIButton lz_buttonTitle:@"修改" titleColor:COLOR_appColor fontSize:15];
    [optionView addSubview:self.editBtn];
    [self.editBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.statusBtn.mas_right).mas_offset(10);
        make.width.mas_equalTo(titleW);
        make.height.mas_equalTo(50);
        make.top.mas_equalTo(0);
    }];
    [self.editBtn addTarget:self action:@selector(optionClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.statusBtn addTarget:self action:@selector(optionClick:) forControlEvents:UIControlEventTouchUpInside];
    self.editBtn.tag = 11;

    
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = COLOR_cellLine;
    [optionView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
    
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    tableView.backgroundColor = COLOR_cellbackground;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.rowHeight = 50;
    [tableView registerClass:[PMChannelCategoryCell class] forCellReuseIdentifier:@"PMChannelCategoryCell"];

    [self.backgroundView addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(titleLab.mas_bottom).mas_offset(15);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(optionView.mas_top);
    }];

    
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundView.frame = CGRectMake(75, 0, K_SCREENWIDTH - 75,  K_SCREENHEIGHT);
    } completion:^(BOOL finished) {
        
    }];
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gestureRecognizerDismiss)];
    tap.delegate = self;
    [self addGestureRecognizer:tap];

}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PMChannelCategoryCell *categoryCell = [tableView dequeueReusableCellWithIdentifier:@"PMChannelCategoryCell"];
    PMChannelCategoryModel * chancelModel = self.dataArray[indexPath.row];
    categoryCell.modelObject = chancelModel;
    categoryCell.isSelectView = [chancelModel.modelid isEqualToString:self.modelid];
    return categoryCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    PMChannelCategoryModel *chancelModel = self.dataArray[indexPath.row];
    if([chancelModel.modelid isEqualToString:self.modelid]){
        chancelModel.isEnanle = !chancelModel.isEnanle;
        self.statusBtn.selected = !self.statusBtn.selected;
    } else {
        chancelModel.isEnanle = NO;
        self.statusBtn.selected = NO;
    }
    self.modelid = chancelModel.modelid;
    [tableView reloadData];
    if(self.sureBlock){
        self.sureBlock(chancelModel);
    }
}

//操作
- (void)optionClick:(UIButton *)option{
    
    if(self.cancelBlock){
        self.cancelBlock(@(option.tag));
    }
    
    [self dismissAnimate:YES];
}



#pragma mark - 取消
- (void)dismissAnimate:(BOOL)animate{
    if(animate){
        [UIView animateWithDuration:0.3 animations:^{
            self.backgroundView.frame = CGRectMake(K_SCREENWIDTH, 0, self.backgroundView.width, self.backgroundView.height);
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    } else {
        [UIView animateWithDuration:0.2 animations:^{
            self.hidden = YES;
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }

}

//手势
- (void)gestureRecognizerDismiss{
    [self dismissAnimate:YES];
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self endEditing:YES];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{

    //如果是子视图 self ，设置无法接受 父视图 点击事件。
    if(touch.view == self){
         return YES;
     }
     return NO;
    
}



@end
