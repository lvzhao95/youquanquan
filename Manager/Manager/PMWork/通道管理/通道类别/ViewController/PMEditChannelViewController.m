//
//  PMEditChannelViewController.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMEditChannelViewController.h"
#import "PMEditChannelViewModel.h"
#import "PMChannelTextField.h"

@interface PMEditChannelViewController ()

@property (nonatomic,strong) PMEditChannelViewModel *viewModel;

@property (nonatomic,strong) NSMutableDictionary *dataDictionary;

@end

@implementation PMEditChannelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"新增付款通道";
    self.view.backgroundColor = COLOR_cellbackground;
    [self setupUI];
    // Do any additional setup after loading the view.
}


- (void) setupUI{
    
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [self.view addSubview:scrollView];
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.right.bottom.mas_equalTo(0);
    }];

    //内容
    UIView *contentView = [[UIView alloc] init];
    [scrollView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.and.right.equalTo(scrollView).with.insets(UIEdgeInsetsZero);
        make.width.equalTo(scrollView);

    }];

    ///!!!: 通道信息
    UIView *channelView = [[UIView alloc] init];
    channelView.backgroundColor = COLOR_appColor;
    [contentView addSubview:channelView];
    [channelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(20);
        make.top.mas_equalTo(20);
    }];
    
    
    UILabel *channelLab= [UILabel lz_labelWithText:@"通道信息" fontSize:PingFangSC(15) color:COLOR_textColor];
    [contentView addSubview:channelLab];
    [channelLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(channelView.mas_right).mas_offset(10);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(20);
        make.top.mas_equalTo(20);
    }];
    
    @weakify(self);
    self.dataDictionary = [[NSMutableDictionary alloc] init];
    UIView *channelLastView = nil;
    {
        NSMutableArray *channelDetails = [[NSMutableArray alloc] init];
        NSArray *titles = @[@"* 通道代码",@"* 支付场景",@"厂家",@"* 权重",@"* 通道名称",@"* 终端分类",@"* 结算周期",@"* 排序"];
        NSArray *placeholders = @[@"请输入通道代码",@"请输入支付场景",@"请输入厂家",@"请输入权重",@"请输入通道名称",@"请输入终端分类",@"请输入结算周期",@"请输入排序"];
        NSArray *keys = @[@"channel_key",@"scene",@"vendor",@"route_weight",@"channel_name",@"type",@"settle_period",@"sort"];
        NSArray *contents  = nil;
        if(self.viewModel.channelModel){
            contents = @[self.viewModel.channelModel.channel_key?:@"",self.viewModel.channelModel.scene?:@"",
                         self.viewModel.channelModel.vendor?:@"",self.viewModel.channelModel.route_weight?:@"",
                         self.viewModel.channelModel.channel_name?:@"",self.viewModel.channelModel.type?:@"",
                         self.viewModel.channelModel.settle_period?:@"",self.viewModel.channelModel.sort?:@""];
        }
       

        /*
       parent_id: a1c53afb-a51f-4af0-9b93-b9c0c6a041a3
        is_channel: true
        basic_rate: 0
        agent_rate: 0
        merchant_rate: 0
        single_min: 0
        single_max: 0
        sort: 102
        channel_key: tongdaotaima1
        channel_name: mingcehng
        scene: changjiang2
        type: fenlei
        vendor: changjia
        route_weight: 1
        settle_period: 2
        auth_signature: qianmmingrenzheng
        channel_seting: tomgdaomeizhi
        is_transfer: false
        status: DISABLE
         */
          for (int i = 0; i < titles.count; i++){
              PMChannelTextField *textField = [[PMChannelTextField alloc] init];
              textField.title = titles[i];
              textField.placeholder = placeholders[i];
              textField.textField.text = self.viewModel.channelModel?contents[i]:@"";
              [self.view addSubview:textField];
              [textField mas_makeConstraints:^(MASConstraintMaker *make) {
                  make.left.mas_equalTo(15);
                  make.right.mas_equalTo(-15);
                  make.height.mas_equalTo(50);
                  if(channelLastView){
                      make.top.mas_equalTo(channelLastView.mas_bottom).mas_offset(10);
                  } else {
                      make.top.mas_equalTo(channelLab.mas_bottom).mas_offset(10);
                  }
              }];
              channelLastView = textField;
              [channelDetails addObject:channelLastView];
              
              
              [[textField.textField rac_textSignal] subscribeNext:^(NSString * _Nullable x) {
                  @strongify(self);
                  self.dataDictionary[keys[i]] = x;
              }];
              
          }
    }
  
    
    ///!!!:签名认证
    UILabel *singLab= [UILabel lz_labelWithText:@"签名认证" fontSize:PingFangSC(15) color:COLOR_textColor];
    [contentView addSubview:singLab];
    [singLab mas_makeConstraints:^(MASConstraintMaker *make) {
          make.top.mas_equalTo(channelLastView.mas_bottom).mas_offset(25);
          make.width.mas_equalTo(100);
          make.height.mas_equalTo(20);
          make.left.mas_equalTo(15);
      }];
    
    
    YYTextView *singTextView = [[YYTextView alloc] init];
    [contentView addSubview:singTextView];
    singTextView.font = PingFangSC(15);
    [singTextView mas_makeConstraints:^(MASConstraintMaker *make) {
          make.top.mas_equalTo(singLab.mas_bottom).mas_offset(12);
          make.width.mas_equalTo(kScreenWidth - 30);
          make.height.mas_equalTo(70);
          make.left.mas_equalTo(15);
      }];
    singTextView.cornerRadius = 5;
    singTextView.bColor = COLOR_cellLine;
    singTextView.bWidth = 0.5;

    ///!!!:通道配置
    UILabel *congifLab= [UILabel lz_labelWithText:@"通道配置" fontSize:PingFangSC(15) color:COLOR_textColor];
    [contentView addSubview:congifLab];
    [congifLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(singTextView.mas_bottom).mas_offset(15);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(15);
    }];
      
      
    YYTextView *congifTextView = [[YYTextView alloc] init];
    [contentView addSubview:congifTextView];
    congifTextView.font = PingFangSC(15);
    [congifTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(congifLab.mas_bottom).mas_offset(12);
        make.width.mas_equalTo(kScreenWidth - 30);
        make.height.mas_equalTo(70);
        make.left.mas_equalTo(15);
    }];
    congifTextView.cornerRadius = 5;
    congifTextView.bColor = COLOR_cellLine;
    congifTextView.bWidth = 0.5;
    
    
    ///!!!:费率设置
    UIView *rateView = [[UIView alloc] init];
    rateView.backgroundColor = COLOR_appColor;
    [contentView addSubview:rateView];
    [rateView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(20);
        make.top.mas_equalTo(congifTextView.mas_bottom).mas_offset(30);
    }];
     
     
     UILabel *rateLab= [UILabel lz_labelWithText:@"费率设置" fontSize:PingFangSC(15) color:COLOR_textColor];
     [contentView addSubview:rateLab];
     [rateLab mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.mas_equalTo(rateView.mas_right).mas_offset(10);
         make.width.mas_equalTo(100);
         make.height.mas_equalTo(20);
         make.centerY.mas_equalTo(rateView.mas_centerY);
     }];
    
    
    NSMutableArray *rateTextFields = [[NSMutableArray alloc] init];
    
    UIView *rateLastView = nil;
    {
        NSArray *titles = @[@"基础费率",@"代理费率",@"商户费率",@"付款下限",@"付款上限"];
        NSArray *placeholders = @[@"(小数，如 0.025 代表 2.5%)",@"(小数，如 0.025 代表 2.5%)",@"(小数，如 0.025 代表 2.5%)",@"(单位：分)",@"(单位：分)"];
        NSArray *keys = @[@"basic_rate",@"agent_rate",@"merchant_rate",@"single_min",@"single_max"];
        
        NSArray *contents  = nil;
        if(self.viewModel.channelModel){
            contents = @[self.viewModel.channelModel.basic_rate?:@"",self.viewModel.channelModel.agent_rate?:@"",
                         self.viewModel.channelModel.merchant_rate?:@"",self.viewModel.channelModel.single_min?:@"",
                         self.viewModel.channelModel.single_max?:@""];
        }
        
        
        for (int i = 0; i < titles.count; i++){
            PMChannelTextField *textField = [[PMChannelTextField alloc] init];
            textField.title = titles[i];
            textField.textField.text =self.viewModel.channelModel?contents[i]:@"";
            textField.placeholder = placeholders[i];
            [self.view addSubview:textField];
            [textField mas_makeConstraints:^(MASConstraintMaker *make) {
                  make.left.mas_equalTo(15);
                  make.right.mas_equalTo(-15);
                  make.height.mas_equalTo(50);
                  if(rateLastView){
                      make.top.mas_equalTo(rateLastView.mas_bottom).mas_offset(10);
                  } else {
                      make.top.mas_equalTo(rateLab.mas_bottom).mas_offset(10);
                  }
            }];
            rateLastView = textField;
            [rateTextFields addObject:rateLastView];
            
            [[textField.textField rac_textSignal] subscribeNext:^(NSString * _Nullable x) {
                @strongify(self);
                self.dataDictionary[keys[i]] = x;
            }];
                         
        }
        
    }

    
    [contentView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(rateLastView.mas_bottom).mas_offset(34);
    }];
    
    
    ///!!!: 完成
    UIButton *finishBtn = [UIButton lz_buttonTitle:@"完成" titleColor:COLOR_appColor fontSize:15];
    finishBtn.frame = CGRectMake(0, 0, 44, 44);
    [self initBarItem:finishBtn withType:1];
    [[finishBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);

        NSArray *placeholders = @[@"请输入通道代码",@"请输入支付场景",@"请输入厂家",@"请输入权重",@"请输入通道名称",@"请输入终端分类",@"请输入结算周期",@"请输入排序"];
        NSArray *keys = @[@"channel_key",@"scene",@"vendor",@"route_weight",@"channel_name",@"type",@"settle_period",@"sort"];
        for (int i = 0; i < keys.count; i++) {
            NSString *key = keys[i];
            if(i != 2){
                if([self.dataDictionary[key] length] == 0){
                    showDelayedDismissTitle(placeholders[i], nil);
                    return;
                }
            }
        }
        
    
        
        self.dataDictionary[@"parent_id"] = self.channelCategoryModel.parent_id?:@"";
        self.dataDictionary[@"auth_signature"] = singTextView.text;
        self.dataDictionary[@"channel_seting"] = congifTextView.text;

        if(self.viewModel.channelModel){
            
            self.dataDictionary[@"channel_id"] = self.viewModel.channelModel.modelid?:@"";
            [self.viewModel updateChannel:self.dataDictionary];

        } else {
            [self.viewModel createChannel:self.dataDictionary];

        }
        
    }];
    
    
    
}


- (PMEditChannelViewModel *)viewModel{
    if(!_viewModel){
        _viewModel = [[PMEditChannelViewModel alloc] init];
        _viewModel.channelCategoryModel = self.channelCategoryModel;
        _viewModel.channelModel = self.channelModel;
        _viewModel.reloadSubject = self.subject;
    }
    return _viewModel;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
