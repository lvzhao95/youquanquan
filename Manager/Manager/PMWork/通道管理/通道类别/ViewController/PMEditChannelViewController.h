//
//  PMEditChannelViewController.h
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZBaseViewController.h"
#import "PMChannelCategoryModel.h"
#import "PMAccessChannelModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PMEditChannelViewController : LZBaseViewController
@property (nonatomic,strong) RACSubject *subject;
@property (nonatomic,strong) PMChannelCategoryModel *channelCategoryModel;
@property (nonatomic,strong) PMAccessChannelModel *channelModel;

@end

NS_ASSUME_NONNULL_END
