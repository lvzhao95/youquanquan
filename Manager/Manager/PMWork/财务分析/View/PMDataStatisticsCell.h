//
//  PMDataStatisticsCell.h
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface PMDataStatisticsCell : LZBaseTableViewCell
@property (nonatomic,strong)UILabel *titleLab; //说明
@property (nonatomic,strong)UILabel *detailLab; //说明


//更新箭头
- (void)updateItem;
@end

NS_ASSUME_NONNULL_END
