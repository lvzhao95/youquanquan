//
//  PMStatisticeDetailCell.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMStatisticeDetailCell.h"
#import "PMStatisticeDetailModel.h"

@implementation PMStatisticeDetailCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self reloadView];
    }
    return self;
}


//设置View
- (void)reloadView{

    self.titleLab.font = PingFangSC(13);
    self.detailLab.font = PingFangSC(13);

    [self.titleLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(150);
    }];
    
    //detail
    [self.detailLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-80);
    }];
    
    [self.rightImageView mas_updateConstraints:^(MASConstraintMaker *make) {
         make.right.mas_equalTo(-55);
     }];
    
    self.bottomLineView.hidden = YES;
    self.rightImageView.hidden = YES;

}

- (void)setModelObject:(id)modelObject{
    PMStatisticeDetailModel *detailModel = modelObject;
//    otherStaticModel.
    self.titleLab.text = detailModel.member_name ? :@"其他";
    self.detailLab.text = detailModel.sum_amount;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
