//
//  PMOtherStaticCell.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMOtherStaticCell.h"
#import "PMOtherStaticModel.h"

@interface PMOtherStaticCell()
@property(nonatomic,strong) PMOtherStaticModel *otherStaticModel;

@end
@implementation PMOtherStaticCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self reloadView];
    }
    return self;
}


//设置View
- (void)reloadView{

    self.titleLab.font = PingFangSC(15);
      self.detailLab.font = PingFangSC(15);
    
    //detail
    [self.detailLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-80);
    }];
    
    [self.rightImageView mas_updateConstraints:^(MASConstraintMaker *make) {
         make.right.mas_equalTo(-55);
     }];
    
    self.bottomLineView.hidden = YES;
}

- (void)setModelObject:(id)modelObject{
    PMOtherStaticModel *otherStaticModel = modelObject;
    self.otherStaticModel = otherStaticModel;
    self.bottomLineView.hidden = !self.otherStaticModel.isConvert;
    
    self.otherStaticModel = otherStaticModel;
    self.titleLab.text = otherStaticModel.agent_name;
    self.detailLab.text = otherStaticModel.sum_amount;

    [self refreshArrow];
}

- (void)updateItem {
    // 刷新 title 前面的箭头方向
    [UIView animateWithDuration:0.25 animations:^{
        [self refreshArrow];
    }];
}
- (void)refreshArrow {
    
    if (self.otherStaticModel.isConvert) {
        self.rightImageView.transform = CGAffineTransformMakeRotation(M_PI_2);
    } else {
        self.rightImageView.transform = CGAffineTransformMakeRotation(0);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
