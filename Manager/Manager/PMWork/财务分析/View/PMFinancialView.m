//
//  PMFinancialView.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMFinancialView.h"
#import "PMFinancialViewModel.h"
#import "CKSlideMenu.h"
#import "LZBaseViewController.h"


@interface PMFinancialView()<CKSlideMenuDelegate>

@property (nonatomic,strong) PMFinancialViewModel *viewModel;

@property (nonatomic,strong) NSMutableArray *viewControllers;

@end

@implementation PMFinancialView

- (instancetype)initViewModel:(LZBaseViewModel *)viewModel
{
    self = [super initViewModel:viewModel];
    if (self) {
        self.viewModel = (PMFinancialViewModel *)viewModel;
        
        [self setupView];
    }
    return self;
}

#pragma mark 创建UI
- (void) setupView{
    
    NSArray *titles = @[@"商户交易统计",@"刷手数据统计"];
    self.viewControllers = [NSMutableArray array];
    
    NSArray *classNames = @[@"PMTradeStatisticsViewController",@"PMDataStatisticsViewController"];
    for (int i = 0; i < classNames.count; i++) {
        Class cts = NSClassFromString(classNames[i]);
        LZBaseViewController *vc = [[cts alloc] init];
        [self.viewControllers addObject:vc];
    }
    // 创建Menu
    CKSlideMenu *slidMenu = [[CKSlideMenu alloc] initWithFrame:CGRectMake(0, 0, K_SCREENWIDTH, 45) titles:titles controllers:self.viewControllers];
    slidMenu.backgroundColor = COLOR_nav;
    slidMenu.indicatorStyle = SlideMenuIndicatorStyleFollowText;
    slidMenu.isFixed = YES;
    slidMenu.lazyLoad = YES;
    slidMenu.selectedColor = COLOR_appColor;
    slidMenu.unselectedColor = COLOR_subTextColor;
    slidMenu.indicatorHeight = 2;
    slidMenu.indicatorColor =  COLOR_appColor;
    slidMenu.unFont = PingFangSC_M(17);
    slidMenu.font = PingFangSC_M(17);
    slidMenu.delegate = self;
    [self addSubview:slidMenu];
    slidMenu.bodyScrollView.scrollEnabled = NO;
    slidMenu.bodyFrame = CGRectMake(0,45, K_SCREENWIDTH, K_SCREENHEIGHT -  K_NAVHEIGHT - 45);
}


#pragma mark - CKSlideMenuDelegate
- (void)clickBtnActionIndx:(NSInteger)index{
    

    LZBaseViewController *baseVC  = self.viewControllers[index];
    SEL sel = NSSelectorFromString(@"reloadRightItem");
    if ([baseVC respondsToSelector:sel]) {
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
          [baseVC performSelector:sel withObject:nil];
    #pragma clang diagnostic pop
    }
    
}


@end
