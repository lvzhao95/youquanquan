//
//  PMOrderStatisticsView.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMOrderStatisticsView.h"
#import "PMFinancialViewModel.h"

@interface PMOrderStatisticsView()

@property (nonatomic,strong) PMFinancialViewModel *viewModel;

@end

@implementation PMOrderStatisticsView
- (instancetype)initViewModel:(LZBaseViewModel *)viewModel
{
    self = [super initViewModel:viewModel];
    if (self) {
        
        self.viewModel = (PMFinancialViewModel *)viewModel;
        [self setupView];
        
    }
    return self;
}

#pragma mark 创建UI
- (void) setupView{
    
    ///三分钟前的1分钟内
    
    UILabel *timeLab = [UILabel lz_labelWithText:@"三分钟前的1分钟内" fontSize:PingFangSC(15) color:COLOR_textColor];
    [self addSubview:timeLab];
    [timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(300);
        make.height.mas_equalTo(21);
        make.top.mas_equalTo(15);
    }];
    
    
    NSMutableArray *detailLabs = [[NSMutableArray alloc] init];
    
    NSArray *titles = @[@"下单:0笔  0.00元",@"成功:0笔  0.00元",@"下单成功率:0.00%",@"金额成功率:0.00%"];
    //每个Item宽高
    CGFloat H = 21;
    CGFloat W = (kScreenWidth - 30 - 10)/2.0;
    //每行列数
    NSInteger rank = 2;
    //每列间距
    CGFloat rankMargin = 10;
    //每行间距
    CGFloat rowMargin = 15;
    //Item索引 ->根据需求改变索引
    NSUInteger index = titles.count;
    
    CGFloat top = 50;
    for (int i = 0 ; i< index; i++) {
        
        UILabel *detailLab = [UILabel lz_labelWithText:titles[i] fontSize:PingFangSC(15) color:COLOR_textColor];
        //Item Y轴
        NSUInteger Y = (i / rank) * (H +rowMargin);
        CGFloat X = (i % rank) * (W + rankMargin);
        //Item X轴
        [self addSubview:detailLab];
        [detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(X + 15);
            make.top.mas_equalTo(top + Y);
            make.width.mas_equalTo(W);
            make.height.mas_equalTo(H);
        }];
        [detailLabs addObject:detailLab];
    }
}

@end
