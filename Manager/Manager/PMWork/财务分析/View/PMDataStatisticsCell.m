//
//  PMDataStatisticsCell.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMDataStatisticsCell.h"
#import "PMStatisticsModel.h"

@interface PMDataStatisticsCell()
@property (nonatomic,strong) PMStatisticsModel *statisticsModel;
@end

@implementation PMDataStatisticsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupView];
    }
    return self;
}


//设置View
- (void)setupView{
    
    //title
    self.titleLab = [UILabel lz_labelWithText:@"" fontSize:PingFangSC(15) color:COLOR_textColor];
    [self.contentView addSubview:self.titleLab];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(25);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.height.mas_equalTo(24);
        make.width.mas_equalTo(100);
    }];
    
    //detail
    self.detailLab = [UILabel lz_labelWithText:@"" fontSize:PingFangSC_M(15) color:COLOR_textColor];
    [self.detailLab lz_rightAlignment];
    [self.contentView addSubview:self.detailLab];
    [self.detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-46);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.height.mas_equalTo(24);
        make.width.mas_equalTo(150);
    }];
    
    [self.rightImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-25);
    }];
    

    self.bottomLineView.hidden = NO;
    self.rightImageView.hidden = NO;

}

- (void)setModelObject:(id)modelObject{
    PMStatisticsModel *statisticsModel = modelObject;
    self.statisticsModel = statisticsModel;
    self.bottomLineView.hidden = NO;
    
    self.titleLab.text = [statisticsModel.company_name length] > 0 ? statisticsModel.company_name:@"其他";
    self.detailLab.text = statisticsModel.sum_amount;

    [self refreshArrow];
}


- (void)updateItem {
    // 刷新 title 前面的箭头方向
    [UIView animateWithDuration:0.25 animations:^{
        [self refreshArrow];
    }];
}
- (void)refreshArrow {
    
    if (!self.statisticsModel.isConvert) {
        self.rightImageView.transform = CGAffineTransformMakeRotation(M_PI_2);
    } else {
        self.rightImageView.transform = CGAffineTransformMakeRotation(0);
    }
}



@end
