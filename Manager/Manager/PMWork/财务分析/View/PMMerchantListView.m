//
//  PMMerchantListView.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMMerchantListView.h"
#import "PMFinancialViewModel.h"
#import "LZTableSlideView.h"
#import "LZMJDIYRefreshHeader.h"
#import "LZMJDIYRefeshFooter.h"


@interface PMMerchantListView()

@property (nonatomic,strong) PMFinancialViewModel *viewModel;

@property (nonatomic,strong) LZTableSlideView *tableView;

@end

@implementation PMMerchantListView
- (instancetype)initViewModel:(LZBaseViewModel *)viewModel
{
    self = [super initViewModel:viewModel];
    if (self) {
        
        self.viewModel = (PMFinancialViewModel *)viewModel;
        [self setupView];
        
    }
    return self;
}

#pragma mark 创建UI
- (void) setupView{
    
    [self addSubview:self.tableView];
    self.tableView.isShowRight = NO;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];

    showLoadingTitle(@"", nil);
    [self.viewModel getSummaryByMerchant:YES];
    
    @weakify(self);
    [self.viewModel.reloadSubject subscribeNext:^(id  _Nullable x) {
        dismiss(nil);
        @strongify(self);
        [self reloadTable:[x intValue]];
    }];
    
    ///!!!:添加上拉加载,
    [self addRefresh];
    
}


///刷线表格
- (void)reloadTable:(NSInteger) type{
    if(type > 3)return;
    self.tableView.dataModel = self.viewModel.dataModel;
    [self.tableView.rightTableView.mj_header endRefreshing];
    [self.tableView  dismissEmptyView];
    
    if(self.viewModel.dataModel.dataArray.count == 0){
        [self.tableView tableViewDisplayWithImgName:@"no_data" message:@"暂无数据" contentoffsetY:100 showEmptyView:YES];
    }
}


///添加下拉, 上啦
- (void)addRefresh{
    
    @weakify(self);
    self.tableView.rightTableView.mj_header = [LZMJDIYRefreshHeader headerWithRefreshingBlock:^{
        NSLog(@"下拉刷新");
        @strongify(self);
        [self.viewModel getSummaryByMerchant:YES];
 
    }];

    
    // 不让mj 自适应宽度
    self.tableView.rightTableView.mj_header.autoresizingMask = UIViewAutoresizingNone;
    self.tableView.rightTableView.mj_header.mj_x = -self.viewModel.dataModel.leftWidth;
    // 设置mj宽度
    self.tableView.rightTableView.mj_header.mj_w = K_SCREENWIDTH;
    self.tableView.rightTableView.mj_footer.autoresizingMask = UIViewAutoresizingNone;
    self.tableView.rightTableView.mj_footer.mj_w = K_SCREENWIDTH ;
    self.tableView.rightTableView.mj_footer.mj_x = -self.viewModel.dataModel.leftWidth;
    
}


- (LZTableSlideView *)tableView{
    if(!_tableView){
        _tableView = [[LZTableSlideView alloc] initWithFrame:CGRectMake(0, 0, K_SCREENWIDTH, 500)];
    }
    return _tableView;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
