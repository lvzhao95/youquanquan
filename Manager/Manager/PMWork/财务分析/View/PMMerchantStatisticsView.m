//
//  PMMerchantStatisticsView.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMMerchantStatisticsView.h"
#import "PMFinancialViewModel.h"

@interface PMMerchantStatisticsView()

@property (nonatomic,strong) PMFinancialViewModel *viewModel;

@end

@implementation PMMerchantStatisticsView
- (instancetype)initViewModel:(LZBaseViewModel *)viewModel
{
    self = [super initViewModel:viewModel];
    if (self) {
        
        self.viewModel = (PMFinancialViewModel *)viewModel;
        [self setupView];
        
    }
    return self;
}

#pragma mark 创建UI
- (void) setupView{
    
    
    NSMutableArray *detailLabs = [[NSMutableArray alloc] init];
    
    NSArray *titles = @[@"商户:0个",@"提交订单: 0条",@"已付订单: 0条",@"未付订单:0条",@"成功率:0.00%",@"提交金额:￥0.00元",@"实付金额:￥0.00元",@"入金手续费:￥0.00元"];
    //每个Item宽高
    CGFloat H = 21;
    CGFloat W = (kScreenWidth - 30 - 10)/2.0;
    //每行列数
    NSInteger rank = 2;
    //每列间距
    CGFloat rankMargin = 10;
    //每行间距
    CGFloat rowMargin = 15;
    //Item索引 ->根据需求改变索引
    NSUInteger index = titles.count;
    
    CGFloat top = 10;
    for (int i = 0 ; i < index; i++) {
        
        UILabel *detailLab = [UILabel lz_labelWithText:titles[i] fontSize:PingFangSC(15) color:COLOR_textColor];
        //Item Y轴
        NSUInteger Y = (i / rank) * (H +rowMargin);
        CGFloat X = (i % rank) * (W + rankMargin);
        //Item X轴
        [self addSubview:detailLab];
        [detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(X + 15);
            make.top.mas_equalTo(top + Y);
            make.width.mas_equalTo(W);
            make.height.mas_equalTo(H);
        }];
        [detailLabs addObject:detailLab];
    }
    
    
    @weakify(self);
    [self.viewModel.reloadSubject subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        dismiss(nil);
        
        PMTradeStatisticsModel *statisticsModel = self.viewModel.dataArray.firstObject;
        if(!statisticsModel)return;
        NSArray *titles = @[[NSString stringWithFormat:@"商户:%lu个",(unsigned long)self.viewModel.dataArray.count],
                            [NSString stringWithFormat:@"提交订单: %@条",statisticsModel.count_order],
                            [NSString stringWithFormat:@"已付订单: %@条",statisticsModel.success_order],
                            [NSString stringWithFormat:@"未付订单: %@条",statisticsModel.fail_order],
                            [NSString stringWithFormat:@"成功率:%@",statisticsModel.success_rate],
                            [NSString stringWithFormat:@"提交金额:%@元",statisticsModel.order_amount],
                            [NSString stringWithFormat:@"实付金额:%@元",statisticsModel.sum_amount],
                            [NSString stringWithFormat:@"入金手续费:%@元",statisticsModel.sum_settle_fee]];
        for (int i = 0; i < titles.count; i++){
            UILabel *detailLab = detailLabs[i];
            detailLab.text = titles[i];
        }
        
        
    }];
    
}

@end
