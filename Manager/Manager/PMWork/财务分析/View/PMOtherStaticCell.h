//
//  PMOtherStaticCell.h
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMDataStatisticsCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface PMOtherStaticCell : PMDataStatisticsCell


//更新箭头
- (void)updateItem;
@end

NS_ASSUME_NONNULL_END
