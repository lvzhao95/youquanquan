//
//  PMFinancialViewModel.h
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZBaseViewModel.h"
#import "PMStatisticsModel.h"
#import "LZTableDataModel.h"
#import "PMTradeStatisticsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PMFinancialViewModel : LZBaseViewModel
@property (nonatomic,assign) NSInteger index;
//表格数据源
@property (nonatomic,strong) LZTableDataModel *dataModel;
//
@property (nonatomic,strong) NSMutableDictionary *paramsDictionary;
@property (nonatomic,strong) NSMutableDictionary *dataDictionary;

///商户交易统计
- (void)getSummaryByMerchant:(BOOL)isFrist;


///获取数据统计
- (void)getDataStatistics:(BOOL)isFrist;

//获取子数数据
- (void)getSubStatistics:(PMStatisticsModel *)statisticsModel;

//详情
- (void)getDataStatisticsDetail:(PMOtherStaticModel *)subStaticModel;

@end

NS_ASSUME_NONNULL_END
