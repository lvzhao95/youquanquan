//
//  PMFinancialViewModel.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMFinancialViewModel.h"
@interface  PMFinancialViewModel()

@property (nonatomic,assign) NSInteger pageIndex;

@end

@implementation PMFinancialViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
    

    }
    return self;
}


- (void)setIndex:(NSInteger)index{
    _index = index;
    switch (index) {
        case 0:
            [self createMerchantTableData];
            break;
        case 1:
            break;
        default:
            break;
    }
    
    
}


///商户交易统计
- (void)getSummaryByMerchant:(BOOL)isFrist{
    
    if(isFrist){
        self.pageIndex = 1;
        self.dataModel.leftDataArray = @[];
        self.dataModel.dataArray = @[];
        [self.dataArray removeAllObjects];
    }
    
    
    NSString *startTime = [NSString stringWithFormat:@"%@ 00:00",[NSDate datestrFromDate:[NSDate date] withDateFormat:@"yyyy-MM-dd"]];
    NSString *endTime = [NSString stringWithFormat:@"%@ 23:59",[NSDate datestrFromDate:[NSDate date] withDateFormat:@"yyyy-MM-dd"]];

    
   NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{@"page":@(self.pageIndex),
                                                                                    @"size":kPageSize,
                                                                                    @"startTime":startTime,
                                                                                    @"endTime":endTime}];
    [params addEntriesFromDictionary:self.paramsDictionary];
    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kGetSummaryByMerchant params:params success:^(id  _Nullable responseObject) {
        @strongify(self);
        NSArray *dataArray = responseObject[@"sums"];
      
        NSMutableArray * leftTableDataArray = [[NSMutableArray alloc]init];
        NSMutableArray * rowDataArray = [[NSMutableArray alloc]init];
    
        for (int i = 0; i < dataArray.count; i++){
            PMTradeStatisticsModel *merchantModel = [PMTradeStatisticsModel modelWithJSON:dataArray[i]];
            [self.dataArray addObject:merchantModel];
            // 左侧列表数据
            LZTableLeftDataModel * model = [[LZTableLeftDataModel alloc]init];
            model.titleString = merchantModel.merchant_no;
            [leftTableDataArray addObject:model];

            //右侧列表数据
            LZTableRightDataModel *rightDataModel = [[LZTableRightDataModel alloc]init];
            rightDataModel.itemHeight = self.dataModel.rowHeight;

            ///右侧数据源
            NSMutableArray * rightItemArray = [[NSMutableArray alloc]init];

            NSArray *detail = @[merchantModel.company_name?:@"",
                                merchantModel.count_order?:@"",
                                merchantModel.success_order?:@"",
                                merchantModel.fail_order?:@"",
                                merchantModel.success_rate?:@"",
                                merchantModel.order_amount?:@"",
                                merchantModel.sum_amount?:@"",
                                merchantModel.sum_settle_fee?:@""];

                for ( int j = 0; j < detail.count; j++) {
                    LZTableRightSubDataModel *itemModel = [[LZTableRightSubDataModel alloc]init];
                    itemModel.titleString = detail[j];
                    itemModel.itemHeight = self.dataModel.rowHeight;
                    [rightItemArray addObject:itemModel];
                }
                rightDataModel.columnWidthArray = self.dataModel.topDataModel.columnWidthArray;
                rightDataModel.itemModelArray = rightItemArray;
                [rowDataArray addObject:rightDataModel];

                rightDataModel.setDataMethodName = @"setModelObject:";
                [rightDataModel.classNames addObjectsFromArray:@[@"PMCustomPayCell"]];
            }
    
        NSMutableArray *mutableLeftDataArray = [[NSMutableArray alloc] initWithArray:self.dataModel.leftDataArray];
        NSMutableArray *mutableRowDataArray = [[NSMutableArray alloc] initWithArray:self.dataModel.dataArray];

        [mutableLeftDataArray addObjectsFromArray:leftTableDataArray];
        [mutableRowDataArray addObjectsFromArray:rowDataArray];

        self.dataModel.leftDataArray = mutableLeftDataArray;
        self.dataModel.dataArray = mutableRowDataArray;
        ///回调
        if(dataArray.count < [kPageSize intValue]){
            [self.reloadSubject sendNext:@(0)];

        } else {
            self.pageIndex ++;
            [self.reloadSubject sendNext:@(1)];
        }
    

        } failure:^(NSError * _Nullable error) {
            @strongify(self);
            [self.reloadSubject sendNext:@(1)];
        }];
    
    
}

//创建表格
- (void)createMerchantTableData{
    
    [self dataModel];
    /// top数据
    self.dataModel.topLeftDataModel = [[LZTableRightSubDataModel alloc] init];
    self.dataModel.topLeftDataModel.titleString = @"商户编号";
    self.dataModel.topLeftDataModel.sortStatus = -1;
    
    NSArray *titles = @[@"商户名称",@"提交订单",@"已付订单",@"未付订单",@"成功率",@"提交金额",@"实付金额",@"入金手续费"];
    NSArray *itemWidthArray = @[@(80),@(80),@(80),@(80),@(80),@(80),@(80),@(80)];


    NSMutableArray *topSubDataArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < titles.count; i++) {
        LZTableRightSubDataModel *subDataModel = [[LZTableRightSubDataModel alloc]init];
        subDataModel.titleString = titles[i];
        subDataModel.itemHeight = self.dataModel.rowHeight;
        subDataModel.sortStatus = -1;
        [topSubDataArray addObject:subDataModel];
    }
    self.dataModel.leftWidth = 100;
    [self.dataModel.topDataModel.classNames addObjectsFromArray:@[@"PMCustomSortCell"]];
    self.dataModel.topDataModel.setDataMethodName = @"setModelObject:";
    self.dataModel.topDataModel.itemModelArray = topSubDataArray;
    self.dataModel.topDataModel.columnWidthArray = itemWidthArray;
    self.dataModel.totalItemWidth = [[itemWidthArray valueForKeyPath:@"@sum.floatValue"] integerValue];

}

///获取数据统计
- (void)getDataStatistics:(BOOL)isFrist{
 
    NSString *startTime = [NSString stringWithFormat:@"%@",[NSDate datestrFromDate:[NSDate date] withDateFormat:@"yyyy-MM-dd"]];
    NSString *endTime = [NSString stringWithFormat:@"%@",[NSDate datestrFromDate:[NSDate date] withDateFormat:@"yyyy-MM-dd"]];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{@"startTime":startTime,
                                                                                    @"endTime":endTime}];
    [params addEntriesFromDictionary:self.paramsDictionary];
    
    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kGetSummaryByMerchant params:params success:^(id  _Nullable responseObject) {
        @strongify(self);
        NSArray *dataArray = responseObject[@"sums"];
        [self.dataArray removeAllObjects];
        for (int i = 0; i < dataArray.count; i++){
            PMStatisticsModel *merchantModel = [PMStatisticsModel modelWithJSON:dataArray[i]];
            [self.dataArray addObject:merchantModel];
        }
        [self.reloadSubject sendNext:@(1)];
    } failure:^(NSError * _Nullable error) {}];
}

//获取子数数据
- (void)getSubStatistics:(PMStatisticsModel *)statisticsModel{
    
    if(statisticsModel.isConvert){
        [self.reloadSubject sendNext:@(2)];
        return;
    }
    
    if(statisticsModel.dataArray.count > 0){
        [self.reloadSubject sendNext:@(2)];
        return;
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{@"merchant_id":statisticsModel.merchant_id?:@""}];
    [params addEntriesFromDictionary:self.paramsDictionary];
    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kGetSummaryByMerchantMemberAgent params:params success:^(id  _Nullable responseObject) {
       @strongify(self);
       NSArray *dataArray = responseObject[@"sums"];
        for (int i = 0; i < dataArray.count; i++){
           PMOtherStaticModel *staticModel = [PMOtherStaticModel modelWithJSON:dataArray[i]];
            staticModel.merchant_id = statisticsModel.merchant_id;
            [statisticsModel.dataArray addObject:staticModel];

       }
        [statisticsModel.dataArray insertObject:statisticsModel atIndex:0];
        
       [self.reloadSubject sendNext:@(2)];
   } failure:^(NSError * _Nullable error) {} isLoading:YES];
}

//详情
- (void)getDataStatisticsDetail:(PMOtherStaticModel *)subStaticModel{
    
    if(!subStaticModel.isConvert){
        [self.reloadSubject sendNext:@(3)];
        return;
    }
       
    if(subStaticModel.dataArray.count > 0){
        [self.reloadSubject sendNext:@(3)];
        return;
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{@"member_agent_id":subStaticModel.agent_id?:@"",
                                                                                    @"merchant_id":subStaticModel.merchant_id?:@""}];
    [params addEntriesFromDictionary:self.paramsDictionary];
    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kGetSummaryByMerchantMemberAgentMember params:params success:^(id  _Nullable responseObject) {
        @strongify(self);
        NSArray *dataArray = responseObject[@"sums"];
        for (int i = 0; i < dataArray.count; i++){
            PMStatisticeDetailModel *staticModel = [PMStatisticeDetailModel modelWithJSON:dataArray[i]];
            [subStaticModel.dataArray addObject:staticModel];
        }
        [self.reloadSubject sendNext:@(3)];
    } failure:^(NSError * _Nullable error) {} isLoading:YES];
}


- (LZTableDataModel *)dataModel{
    if(!_dataModel){
        _dataModel = [[LZTableDataModel alloc] init];
        _dataModel.leftDataArray = @[];
        _dataModel.dataArray = @[];
    }
    return _dataModel;
}

- (NSMutableDictionary *)paramsDictionary{
    if(!_paramsDictionary){
        _paramsDictionary = [[NSMutableDictionary alloc] init];
    }
    return _paramsDictionary;
}

- (NSMutableDictionary *)dataDictionary{
    if(!_dataDictionary){
        _dataDictionary = [[NSMutableDictionary alloc] init];
    }
    return _dataDictionary;
}


@end
