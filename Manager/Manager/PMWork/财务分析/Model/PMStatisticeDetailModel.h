//
//  PMStatisticeDetailModel.h
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PMStatisticeDetailModel : NSObject
@property (nonatomic,strong) NSString *member_name;
@property (nonatomic,strong) NSString *sum_amount;
@end

NS_ASSUME_NONNULL_END
