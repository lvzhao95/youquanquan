//
//  PMStatisticsModel.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMStatisticsModel.h"

@implementation PMStatisticsModel
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.isConvert = YES;

    }
    return self;
}


- (NSMutableArray *)dataArray{
    if(!_dataArray){
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}
@end
