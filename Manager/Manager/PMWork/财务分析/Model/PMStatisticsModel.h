//
//  PMStatisticsModel.h
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PMOtherStaticModel.h"
#import "PMTradeStatisticsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PMStatisticsModel : PMTradeStatisticsModel


@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *amout;


//是否折合
@property (nonatomic,assign) BOOL isConvert;

@property (nonatomic,strong) NSMutableArray *dataArray;

@end

NS_ASSUME_NONNULL_END
