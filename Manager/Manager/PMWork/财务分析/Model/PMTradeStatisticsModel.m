//
//  PMTradeStatisticsModel.m
//  Manager
//
//  Created by lvzhao on 2020/8/5.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMTradeStatisticsModel.h"

@implementation PMTradeStatisticsModel
- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic{
    
    self.count_order = [NSString stringWithFormat:@"%@",dic[@"count_order"]];
    self.success_order = [NSString stringWithFormat:@"%@",dic[@"success_order"]];
    
    self.success_rate = [[NSString stringWithFormat:@"%f",[self.success_order floatValue] / [self.count_order floatValue]] yw_stringByMultiplyingBy:@"100"];
    self.fail_order = [NSString stringWithFormat:@"%@",dic[@"fail_order"]];
    self.success_rate = [NSString stringWithFormat:@"%@%%",self.success_rate];

    self.order_amount = [NSString stringWithFormat:@"%@",dic[@"order_amount"]];
    
    self.sum_amount = [NSString stringWithFormat:@"￥ %@",[self.sum_amount yw_stringByDividingBy:@"100"]];
    self.order_amount = [NSString stringWithFormat:@"￥ %@",[self.order_amount yw_stringByDividingBy:@"100"]];
    self.sum_settle_fee = [NSString stringWithFormat:@"￥ %@",[self.sum_settle_fee yw_stringByDividingBy:@"100"]];


    return YES;
}
@end
