//
//  PMOtherStaticModel.h
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PMStatisticeDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PMOtherStaticModel : NSObject

@property (nonatomic,strong) NSString *sum_amount;
@property (nonatomic,strong) NSString *agent_name;
@property (nonatomic,strong) NSString *agent_id;
@property (nonatomic,strong) NSString *merchant_id;


//是否折合
@property (nonatomic,assign) BOOL isConvert;

@property (nonatomic,strong) NSMutableArray *dataArray;

@end

NS_ASSUME_NONNULL_END
