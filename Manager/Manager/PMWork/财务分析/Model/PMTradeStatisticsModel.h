//
//  PMTradeStatisticsModel.h
//  Manager
//
//  Created by lvzhao on 2020/8/5.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PMTradeStatisticsModel : NSObject


@property (nonatomic,strong) NSString *merchant_id;
@property (nonatomic,strong) NSString *merchant_no;

@property (nonatomic,strong) NSString *company_name;
@property (nonatomic,strong) NSString *count_order;
@property (nonatomic,strong) NSString *success_order;
@property (nonatomic,strong) NSString *fail_order;
@property (nonatomic,strong) NSString *success_rate;
@property (nonatomic,strong) NSString *order_amount;
@property (nonatomic,strong) NSString *sum_amount;
@property (nonatomic,strong) NSString *sum_settle_fee;


@end

NS_ASSUME_NONNULL_END
