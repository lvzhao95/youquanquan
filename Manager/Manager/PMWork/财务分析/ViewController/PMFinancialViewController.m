//
//  PMFinancialViewController.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMFinancialViewController.h"
#import "PMFinancialViewModel.h"
#import "PMFinancialView.h"

@interface PMFinancialViewController ()

@property (nonatomic,strong) PMFinancialViewModel *viewModel;

@property (nonatomic,strong) PMFinancialView *financialView;

@end

@implementation PMFinancialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"财务分析";
    
    [self setupUI];
}
- (void)setupUI{
    [self.view addSubview:self.financialView];
    [self.financialView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

#pragma mark -懒加载
- (PMFinancialView *)financialView{
    if(!_financialView) {
        _financialView = [[PMFinancialView alloc] initViewModel:self.viewModel];
    }
    return _financialView;
}

- (PMFinancialViewModel *)viewModel{
    if(!_viewModel) {
        _viewModel = [[PMFinancialViewModel alloc] init];
    }
    return _viewModel;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
