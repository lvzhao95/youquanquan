//
//  PMTradeStatisticsViewController.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMTradeStatisticsViewController.h"
#import "PMOrderStatisticsView.h"
#import "PMMerchantStatisticsView.h"
#import "PMFinancialViewModel.h"
#import "PMMerchantListView.h"
#import "PMScreeningViewController.h"


@interface PMTradeStatisticsViewController ()

@property (nonatomic,strong) PMFinancialViewModel *viewModel;

@end

@implementation PMTradeStatisticsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self setupView];
    
    [self reloadRightItem];
}
    
//刷新右按钮
- (void)reloadRightItem{
    
    ///!!!:筛选
    self.navigationItem.rightBarButtonItem = nil;
    UIButton *screeningBtn = [UIButton lz_buttonImageName:@"pm_shaixuan" backgroundImageName:@""];
    screeningBtn.frame = CGRectMake(0, 0, 44, 44);
    [LZTool.currentViewController initBarItem:screeningBtn withType:1];
    [screeningBtn addTarget:self action:@selector(screenClick:) forControlEvents:UIControlEventTouchUpInside];
}


- (void)setupView{
    
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:scrollView];
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.right.bottom.mas_equalTo(0);
    }];

    //内容
    UIView *contentView = [[UIView alloc] init];
    contentView.backgroundColor = COLOR_cellbackground;
    [scrollView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.and.right.equalTo(scrollView).with.insets(UIEdgeInsetsZero);
        make.width.equalTo(scrollView);
    }];

       
    ///!!!: 商户
    PMMerchantStatisticsView *merchantsView = [[PMMerchantStatisticsView alloc] initViewModel:self.viewModel];
    [contentView addSubview:merchantsView];
    [merchantsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(160);
    }];

    
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = COLOR_cellLine;
    [contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(merchantsView.mas_bottom);
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(kScreenWidth - 30);
        make.height.mas_equalTo(1.0);
    }];
    
    ///!!!:订单
    PMOrderStatisticsView *orderView = [[PMOrderStatisticsView alloc] initViewModel:self.viewModel];
    [contentView addSubview:orderView];
    [orderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lineView.mas_bottom);
       make.left.mas_equalTo(0);
       make.width.mas_equalTo(kScreenWidth);
       make.height.mas_equalTo(130);
    }];
    
    
    ///!!!;商户列表
    PMMerchantListView *listView = [[PMMerchantListView alloc] initViewModel:self.viewModel];
    [contentView addSubview:listView];
    [listView mas_makeConstraints:^(MASConstraintMaker *make) {
       make.top.mas_equalTo(orderView.mas_bottom);
       make.left.mas_equalTo(0);
       make.width.mas_equalTo(kScreenWidth);
       make.height.mas_equalTo(500);
    }];
    
    
       
    [contentView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(listView.mas_bottom).mas_offset(34);
    }];
       
}

//筛选
- (void)screenClick:(UIButton *)sender{
    RACSubject *subject = [RACSubject subject];
    PMScreeningViewController *screeningVC = [[PMScreeningViewController alloc] init];
    screeningVC.pickerModel = PMDatePickerModelYM;
    screeningVC.reloadSubject = subject;
    [LZTool.currentViewController.navigationController pushViewController:screeningVC animated:YES];
    [subject subscribeNext:^(id  _Nullable x) {
        NSDictionary *dict = x;
        NSLog(@"%@",x);
        showLoadingTitle(@"", nil);
        [self.viewModel.paramsDictionary addEntriesFromDictionary:dict];
        [self.viewModel getSummaryByMerchant:YES];
    }];
    
}


- (PMFinancialViewModel *)viewModel{
    if(!_viewModel){
        _viewModel = [[PMFinancialViewModel alloc] init];
        _viewModel.index = 0;
    }
    return _viewModel;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
