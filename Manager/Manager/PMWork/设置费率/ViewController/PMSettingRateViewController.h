//
//  PMSettingRateViewController.h
//  Manager
//
//  Created by lvzhao on 2020/8/1.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZBaseViewController.h"
#import "PMMerchantManagerModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface PMSettingRateViewController : LZBaseViewController
@property (nonatomic,strong) PMMerchantManagerModel *merchantModel;
@property (nonatomic,strong) RACSubject *subject;
@end

NS_ASSUME_NONNULL_END
