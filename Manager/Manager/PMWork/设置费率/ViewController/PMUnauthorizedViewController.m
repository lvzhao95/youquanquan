//
//  PMUnauthorizedViewController.m
//  Manager
//
//  Created by lvzhao on 2020/8/1.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMUnauthorizedViewController.h"
#import "PMSettingRateViewModel.h"
#import "LZTableSlideView.h"
#import "LZMJDIYRefreshHeader.h"
#import "LZMJDIYRefeshFooter.h"
@interface PMUnauthorizedViewController ()

@property (nonatomic,strong) PMSettingRateViewModel *viewModel;

@property (nonatomic,strong) LZTableSlideView *tableView;


@end

@implementation PMUnauthorizedViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.viewModel.merchantModel = self.merchantModel;
    self.viewModel.actionSubject = self.subject;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self setupUI];
}


//刷新右按钮
- (void)reloadRightItem{
    
    [self.viewModel getUnauthChannel:YES];
}


- (void) setupUI{
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];

    ///上拉加载. 下拉刷新
    [self addRefresh];
        
    @weakify(self);
    [self.tableView setCallBlock:^(PMTouchTable touchTable, NSInteger index, LZSortButton *sortBtn) {
         @strongify(self);
        switch (touchTable) {
            case PMTouchTableRight:
                [self selectTableIndex:index];
                
                break;

            default:
                break;
        }
        
    }];
    
    [self.viewModel.reloadSubject subscribeNext:^(id  _Nullable x) {
        dismiss(nil);
        @strongify(self);
        [self reloadTable:[x intValue]];
    }];
    
    //获取详情
    showLoadingTitle(@"", nil);
    [self.viewModel getUnauthChannel:YES];

}


///刷线表格
- (void)reloadTable:(NSInteger) type{
    
    
    if(type > 3)return;
    
    self.tableView.dataModel = self.viewModel.dataModel;
    [self.tableView.rightTableView.mj_header endRefreshing];
    [self.tableView.rightTableView.mj_footer endRefreshing];
    self.tableView.rightTableView.mj_footer.hidden = NO;
    [self.tableView  dismissEmptyView];
    switch (type) {
        case 0:{
            self.tableView.rightTableView.mj_footer.hidden = YES;
        }
            break;
        default:
            break;
    }
    
    if(self.viewModel.dataModel.dataArray.count == 0){
        self.tableView.rightTableView.mj_footer.hidden = YES;
        [self.tableView tableViewDisplayWithImgName:@"no_data" message:@"暂无数据" contentoffsetY:100 showEmptyView:YES];
    }
}

///添加下拉, 上啦
- (void)addRefresh{
    
    @weakify(self);
    self.tableView.rightTableView.mj_header = [LZMJDIYRefreshHeader headerWithRefreshingBlock:^{
        NSLog(@"下拉刷新");
        @strongify(self);
        [self.viewModel getUnauthChannel:YES];
 
    }];
    self.tableView.rightTableView.mj_footer = [LZMJDIYRefeshFooter footerWithRefreshingBlock:^{
        NSLog(@"上拉加载");
        @strongify(self);
        [self.viewModel getUnauthChannel:NO];
     
    }];
    
    // 不让mj 自适应宽度
    self.tableView.rightTableView.mj_header.autoresizingMask = UIViewAutoresizingNone;
    self.tableView.rightTableView.mj_header.mj_x = -self.viewModel.dataModel.leftWidth;
    // 设置mj宽度
    self.tableView.rightTableView.mj_header.mj_w = K_SCREENWIDTH;
    self.tableView.rightTableView.mj_footer.autoresizingMask = UIViewAutoresizingNone;
    self.tableView.rightTableView.mj_footer.mj_w = K_SCREENWIDTH ;
    self.tableView.rightTableView.mj_footer.mj_x = -self.viewModel.dataModel.leftWidth;
    
}


///选择第几行操作
- (void)selectTableIndex:(NSInteger) index{

    PMAuthorizedModel *authorizedModel = self.viewModel.dataArray[index];
    @weakify(self);
    [LZToolView showAlertType:LZAlertTypeTipAlter withTitle:@"是否授权开通" message:@"确认是否授权开通" cancel:@"取消" sure:@"确定" objectDict:nil cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
        @strongify(self);
        NSMutableDictionary *dataDictionary = [[NSMutableDictionary alloc] init];
        dataDictionary[@"merchantId"] = self.merchantModel.modelid;
        dataDictionary[@"channelId"] = authorizedModel.modelid;
        [self.viewModel getOpenChannel:dataDictionary];
    }];
}


#pragma mark 懒加载
- (PMSettingRateViewModel *)viewModel{
    if(!_viewModel){
        _viewModel = [[PMSettingRateViewModel alloc] init];
        _viewModel.index = 0;
    }
    return _viewModel;
}

- (LZTableSlideView *)tableView{
    if(!_tableView){
        _tableView = [[LZTableSlideView alloc] initWithFrame:CGRectMake(0, 0, K_SCREENWIDTH, K_SCREENHEIGHT - 45 - K_NAVHEIGHT)];
    }
    return _tableView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
