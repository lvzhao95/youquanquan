//
//  PMSettingRateViewController.m
//  Manager
//
//  Created by lvzhao on 2020/8/1.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMSettingRateViewController.h"
#import "PMSettingRateView.h"
#import "PMSettingRateViewModel.h"

@interface PMSettingRateViewController ()

@property (nonatomic,strong) PMSettingRateView *rateView;

@property (nonatomic,strong) PMSettingRateViewModel *viewModel;

@end

@implementation PMSettingRateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"设置费率";
    
    [self setupUI];
    // Do any additional setup after loading the view.
}

- (void)setupUI{
    [self.view addSubview:self.rateView];
    [self.rateView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    
}




#pragma mark -懒加载
- (PMSettingRateView *)rateView{
    if(!_rateView) {
        _rateView = [[PMSettingRateView alloc] initViewModel:self.viewModel];
    }
    return _rateView;
}

- (PMSettingRateViewModel *)viewModel{
    if(!_viewModel) {
        _viewModel = [[PMSettingRateViewModel alloc] init];
        _viewModel.actionSubject = self.subject;
        _viewModel.merchantModel = self.merchantModel;
    }
    return _viewModel;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
