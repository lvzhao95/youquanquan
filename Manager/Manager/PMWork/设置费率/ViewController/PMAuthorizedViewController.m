//
//  PMAuthorizedViewController.m
//  Manager
//
//  Created by lvzhao on 2020/8/1.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMAuthorizedViewController.h"
#import "PMSettingRateViewModel.h"
#import "LZTableSlideView.h"
#import "LZMJDIYRefreshHeader.h"
#import "LZMJDIYRefeshFooter.h"
@interface PMAuthorizedViewController ()

@property (nonatomic,strong) PMSettingRateViewModel *viewModel;

@property (nonatomic,strong) LZTableSlideView *tableView;


@end

@implementation PMAuthorizedViewController
- (instancetype)init
{
    self = [super init];
    if (self) {

    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewModel.actionSubject = self.subject;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self setupUI];
}

//刷新右按钮
- (void)reloadRightItem{
    
     [self.viewModel getAuthChannel:YES];
}


- (void) setupUI{
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];

  ///上拉加载. 下拉刷新
    [self addRefresh];
        
    @weakify(self);
    [self.tableView setCallBlock:^(PMTouchTable touchTable, NSInteger index, LZSortButton *sortBtn) {
         @strongify(self);
        switch (touchTable) {
            case PMTouchTableRight:
                [self selectTableIndex:index];
                break;
            default:
                break;
        }
        
    }];
    [self.viewModel.reloadSubject subscribeNext:^(id  _Nullable x) {
        dismiss(nil);
        @strongify(self);
        [self reloadTable:[x intValue]];
    }];
    
    //获取详情
    showLoadingTitle(@"", nil);
    [self.viewModel getAuthChannel:YES];

}


///刷线表格
- (void)reloadTable:(NSInteger) type{
    
    
    if(type > 3)return;
    
    self.tableView.dataModel = self.viewModel.dataModel;
    [self.tableView.rightTableView.mj_header endRefreshing];
    [self.tableView.rightTableView.mj_footer endRefreshing];
    self.tableView.rightTableView.mj_footer.hidden = NO;
    [self.tableView  dismissEmptyView];
    switch (type) {
        case 0:{
            self.tableView.rightTableView.mj_footer.hidden = YES;
        }
            break;
        default:
            break;
    }
    
    if(self.viewModel.dataModel.dataArray.count == 0){
        self.tableView.rightTableView.mj_footer.hidden = YES;
        [self.tableView tableViewDisplayWithImgName:@"no_data" message:@"暂无数据" contentoffsetY:100 showEmptyView:YES];
    }
}

///添加下拉, 上啦
- (void)addRefresh{
    
    @weakify(self);
    self.tableView.rightTableView.mj_header = [LZMJDIYRefreshHeader headerWithRefreshingBlock:^{
        NSLog(@"下拉刷新");
        @strongify(self);
        [self.viewModel getAuthChannel:YES];
 
    }];
    self.tableView.rightTableView.mj_footer = [LZMJDIYRefeshFooter footerWithRefreshingBlock:^{
        NSLog(@"上拉加载");
        @strongify(self);
        [self.viewModel getAuthChannel:NO];
     
    }];
    
    // 不让mj 自适应宽度
    self.tableView.rightTableView.mj_header.autoresizingMask = UIViewAutoresizingNone;
    self.tableView.rightTableView.mj_header.mj_x = -self.viewModel.dataModel.leftWidth;
    // 设置mj宽度
    self.tableView.rightTableView.mj_header.mj_w = K_SCREENWIDTH;
    self.tableView.rightTableView.mj_footer.autoresizingMask = UIViewAutoresizingNone;
    self.tableView.rightTableView.mj_footer.mj_w = K_SCREENWIDTH ;
    self.tableView.rightTableView.mj_footer.mj_x = -self.viewModel.dataModel.leftWidth;
    
}


///选择第几行操作
- (void)selectTableIndex:(NSInteger) index{

    PMAuthorizedModel *authorizedModel = self.viewModel.dataArray[index];
    
    NSMutableArray *subTitles = [[NSMutableArray alloc] init];
    [subTitles addObject:@"修改"];
    [subTitles addObject:[authorizedModel.status isEqualToString:@"启用"] ? @"关闭" : @"启用"];
    [subTitles addObject:@"删除"];
    @weakify(self);
    [LZToolView showAlertType:LZAlertTypeActionSheet withTitle:@"" message:@"" cancel:@"取消" sure:@"" objectDict:@{@"subTitles":subTitles} cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
        NSInteger index = [object integerValue];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            @strongify(self);
            
            NSString *subTitle = subTitles[index];
            if([subTitle containsString:@"修改"]){

                [self modificationChannel:authorizedModel];
            } else if ([subTitle containsString:@"关闭"]
                       ||[subTitle containsString:@"启用"]){
        
                [self.viewModel updateMerchantChannelStatus:@{@"mc_id":authorizedModel.modelid?:@"",
                                                            @"status": [subTitle containsString:@"关闭"] ? @"DISABLE":@"ENABLE"}];


            } else if ([subTitle containsString:@"删除"]){

                [self.viewModel deleteMerchantChannel:@{@"mc_id":authorizedModel.modelid?:@""}];
            }
        });
        
        
       
    }];
    
}

- (void)modificationChannel:(PMAuthorizedModel *)authorizedModel{
    
    NSArray *subTitles = @[@"费率:",@"结算周期:",@"付款下限:",@"付款上限:",@"权重:"];
    NSArray *keys = @[@"settle_rate",@"settle_period",@"single_min:",@"single_max",@"custom_weight"];
    NSArray *contents = @[authorizedModel.settle_rate?:@"",
                          authorizedModel.settle_period?:@"",
                          authorizedModel.single_min?:@"",
                          authorizedModel.single_max?:@"",
                          authorizedModel.custom_weight?:@""];
    @weakify(self);
    NSString *message = [NSString stringWithFormat:@"通道: %@",authorizedModel.channel_name];
    [LZToolView showAlertType:LZAlertTypeModification withTitle:@"修改" message:message cancel:@"取消" sure:@"确定" objectDict:@{@"subTitles":subTitles,
                                                                                                                                        @"keys":keys,
                                                                                                                                        @"contents":contents
    } cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
        @strongify(self);
        NSMutableDictionary *dataDictionary = [[NSMutableDictionary alloc] initWithDictionary:object];
        dataDictionary[@"mc_id"] = authorizedModel.modelid?:@"";
        [self.viewModel saveMerchantChannel:dataDictionary];
    }];

}


- (void)setMerchantModel:(PMMerchantManagerModel *)merchantModel{
    _merchantModel = merchantModel;
    self.viewModel.merchantModel = merchantModel;
}

- (PMSettingRateViewModel *)viewModel{
    if(!_viewModel){
        _viewModel = [[PMSettingRateViewModel alloc] init];
        _viewModel.index = 1;
    }
    return _viewModel;
}

- (LZTableSlideView *)tableView{
    if(!_tableView){
        _tableView = [[LZTableSlideView alloc] initWithFrame:CGRectMake(0, 0, K_SCREENWIDTH, K_SCREENHEIGHT - 45 - K_NAVHEIGHT)];
    }
    return _tableView;
}@end
