//
//  PMAuthorizedModel.m
//  Manager
//
//  Created by lvzhao on 2020/8/7.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMAuthorizedModel.h"

@implementation PMAuthorizedModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"modelid" : @"id"
    };
}

- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic{
    
    if([self.is_transfer isEqualToString:@"1"]){
        self.is_transfer = @"是";
    } else {
        self.is_transfer = @"否";
    }
    
    self.single_max = [self.single_max yw_stringByDividingBy:@"100"];
    self.single_min = [self.single_min yw_stringByDividingBy:@"100"];

    if([self.status isEqualToString:@"DISABLE"]){
        self.status = @"关闭";
    } else if ([self.status isEqualToString:@"ENABLE"]){
        self.status = @"启用";
    }
    
    
    return YES;
}

@end
