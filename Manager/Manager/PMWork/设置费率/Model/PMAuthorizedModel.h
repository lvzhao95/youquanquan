//
//  PMAuthorizedModel.h
//  Manager
//
//  Created by lvzhao on 2020/8/7.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PMAuthorizedModel : NSObject
@property (nonatomic , copy) NSString *              is_channel;
@property (nonatomic , copy) NSString *              settle_fee_min;
@property (nonatomic , copy) NSString *              agent_rate;
@property (nonatomic , copy) NSString              * status;
@property (nonatomic , copy) NSString *              channel_amount;
@property (nonatomic , copy) NSString              * channel_key;
@property (nonatomic , copy) NSString *              sort;
@property (nonatomic , copy) NSString              * parent_id;
@property (nonatomic , copy) NSString              * vendor;
@property (nonatomic , copy) NSString              * settle_period;
@property (nonatomic , copy) NSString              * create_date;
@property (nonatomic , copy) NSString              * channel_name;
@property (nonatomic , copy) NSString              * route_mode;
@property (nonatomic , copy) NSString *              merchant_rate;
@property (nonatomic , copy) NSString              * type;
@property (nonatomic , copy) NSString *              channel_profit;
@property (nonatomic , copy) NSString              * modelid;
@property (nonatomic , copy) NSString *              is_transfer;
@property (nonatomic , copy) NSString *              single_max;
@property (nonatomic , copy) NSString *              single_min;
@property (nonatomic , copy) NSString              * support_biz;
@property (nonatomic , copy) NSString *              route_weight;
@property (nonatomic , copy) NSString *              basic_rate;
@property (nonatomic , copy) NSString              * scene;
@property (nonatomic , copy) NSString              * custom_weight;
@property (nonatomic , copy) NSString              * settle_rate;



@end

NS_ASSUME_NONNULL_END
