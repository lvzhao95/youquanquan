//
//  PMSettingRateView.m
//  Manager
//
//  Created by lvzhao on 2020/8/1.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMSettingRateView.h"
#import "PMSettingRateViewModel.h"
#import "CKSlideMenu.h"
#import "PMSettingRateViewController.h"

@interface PMSettingRateView()<CKSlideMenuDelegate>

@property (nonatomic,strong) PMSettingRateViewModel *viewModel;
@property (nonatomic,strong) NSMutableArray *viewControllers;


@end

@implementation PMSettingRateView

- (instancetype)initViewModel:(LZBaseViewModel *)viewModel
{
    self = [super initViewModel:viewModel];
    if (self) {
        self.viewModel = (PMSettingRateViewModel *)viewModel;
        
        [self setupView];
    }
    return self;
}

#pragma mark 创建UI
- (void) setupView{
    
    NSArray *titles = @[@"未授权通道",@"已授权通道"];
    self.viewControllers = [NSMutableArray array];
   NSArray *classNames = @[@"PMUnauthorizedViewController",@"PMAuthorizedViewController"];
    
    for (int i = 0; i < classNames.count; i++) {
        Class cts = NSClassFromString(classNames[i]);
        PMSettingRateViewController *vc = [[cts alloc] init];
        vc.merchantModel = self.viewModel.merchantModel;
        vc.subject = self.viewModel.actionSubject;
        [self.viewControllers  addObject:vc];
    }
    // 创建Menu
    CKSlideMenu *slidMenu = [[CKSlideMenu alloc] initWithFrame:CGRectMake(0, 0, K_SCREENWIDTH, 45) titles:titles controllers:self.viewControllers ];
    slidMenu.backgroundColor = COLOR_nav;
    slidMenu.indicatorStyle = SlideMenuIndicatorStyleFollowText;
    slidMenu.isFixed = YES;
    slidMenu.lazyLoad = NO;
    slidMenu.selectedColor = COLOR_appColor;
    slidMenu.unselectedColor = COLOR_subTextColor;
    slidMenu.indicatorHeight = 2;
    slidMenu.indicatorColor =  COLOR_appColor;
    slidMenu.unFont = PingFangSC_M(17);
    slidMenu.font = PingFangSC_M(17);
    slidMenu.delegate = self;
    [self addSubview:slidMenu];
    slidMenu.bodyScrollView.scrollEnabled = NO;
    slidMenu.bodyFrame = CGRectMake(0,45, K_SCREENWIDTH, K_SCREENHEIGHT -  K_NAVHEIGHT - 45);
}


#pragma mark - CKSlideMenuDelegate
- (void)clickBtnActionIndx:(NSInteger)index{
    
    
    LZBaseViewController *baseVC  = self.viewControllers[index];
    SEL sel = NSSelectorFromString(@"reloadRightItem");
    if ([baseVC respondsToSelector:sel]) {
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
          [baseVC performSelector:sel withObject:nil];
    #pragma clang diagnostic pop
    }
    
    
    
}



@end
