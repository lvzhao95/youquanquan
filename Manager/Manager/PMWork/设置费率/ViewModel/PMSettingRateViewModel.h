//
//  PMSettingRateViewModel.h
//  Manager
//
//  Created by lvzhao on 2020/8/1.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZBaseViewModel.h"
#import "LZTableDataModel.h"
#import "PMMerchantManagerModel.h"
#import "PMAuthorizedModel.h"



NS_ASSUME_NONNULL_BEGIN

@interface PMSettingRateViewModel : LZBaseViewModel

@property (nonatomic,assign) NSInteger index;

//表格数据源
@property (nonatomic,strong) LZTableDataModel *dataModel;
//
@property (nonatomic,strong) NSMutableDictionary *paramsDictionary;
//商户
@property (nonatomic,strong) PMMerchantManagerModel *merchantModel;

///获取未授权
- (void)getUnauthChannel:(BOOL)isFrist;

///授权开通
- (void)getOpenChannel:(NSDictionary *)dataDictionary;

///获取授权通道
- (void)getAuthChannel:(BOOL)isFrist;

///更新状态费率
- (void)updateMerchantChannelStatus:(NSDictionary *)dictionary;

///删除费率
- (void)deleteMerchantChannel:(NSDictionary *)dictionary;

///保存费率
- (void)saveMerchantChannel:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END
