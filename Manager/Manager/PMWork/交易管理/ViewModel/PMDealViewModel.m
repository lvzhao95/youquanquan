//
//  PMDealViewModel.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMDealViewModel.h"
@interface  PMDealViewModel()

@property (nonatomic,assign) NSInteger pageIndex;

@end

@implementation PMDealViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}


- (void)setIndex:(NSInteger)index{
    _index = index;
    switch (index) {
        case 0:
            [self createDealTableData];
            break;
        case 1:
            [self createTradeTableData];
            break;
        default:
            break;
    }
    
    
}


///获取交易管理
- (void)getDealDetail:(BOOL)isFrist{
    
    if(self.index == 0){
        [self dealDetail:isFrist];
    } else {
        /** 付款订单*/
        [self summaryTransferDetail];
        [self transferDetail:isFrist];
    }
    
}

//创建表格
- (void)createTradeTableData{
    
    [self dataModel];
    /// top数据
    self.dataModel.topLeftDataModel = [[LZTableRightSubDataModel alloc] init];
    self.dataModel.topLeftDataModel.titleString = @"平台订单号";
    self.dataModel.topLeftDataModel.param = @"order_id";
    self.dataModel.topLeftDataModel.sortStatus = 0;
    
    NSArray *titles = @[@"付款渠道",@"付款金额(元)",@"结算费",@"付款详情",@"付款状态",@"归集状态",@"审核结果",@"创建时间",@"结算时间",@"所属商户"];
    NSArray *itemWidthArray = @[@(100),@(110),@(80),@(180),@(80),@(80),@(120),@(140),@(140),@(130)];
    NSArray *params = @[@"channel_transfer",@"amount",@"settle_fee",@"",@"transfer_status",@"ext_collection",@"",@"created",@"time_settle",@""];


    NSMutableArray *topSubDataArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < titles.count; i++) {
        LZTableRightSubDataModel *subDataModel = [[LZTableRightSubDataModel alloc]init];
        subDataModel.titleString = titles[i];
        subDataModel.itemHeight = self.dataModel.rowHeight;
        subDataModel.param = params[i];
        if([titles[i] isEqualToString:@"付款详情"]||
           [titles[i] isEqualToString:@"审核结果"]||
           [titles[i] isEqualToString:@"所属商户"]){
            subDataModel.sortStatus = -1;
        }
        
        [topSubDataArray addObject:subDataModel];
    }
    self.dataModel.leftWidth = 100;
    [self.dataModel.topDataModel.classNames addObjectsFromArray:@[@"PMCustomSortCell"]];
    self.dataModel.topDataModel.setDataMethodName = @"setModelObject:";
    self.dataModel.topDataModel.itemModelArray = topSubDataArray;
    self.dataModel.topDataModel.columnWidthArray = itemWidthArray;
    self.dataModel.totalItemWidth = [[itemWidthArray valueForKeyPath:@"@sum.floatValue"] integerValue];

}


//创建表格
- (void)createDealTableData{
    
    [self dataModel];
    
    /// top数据
    self.dataModel.topLeftDataModel = [[LZTableRightSubDataModel alloc] init];
    self.dataModel.topLeftDataModel.titleString = @"平台订单号";
    self.dataModel.topLeftDataModel.param = @"order_id";
    self.dataModel.topLeftDataModel.sortStatus = 0;

    
    NSArray *titles = @[@"商户订单号",@"交易流水号",@"支付类型",@"交易金额",@"显示金额",@"结算金额",@"交易状态",@"回调状态",@"创建时间",@"所属商户",@"收款刷手"];
    NSArray *itemWidthArray = @[@(100),@(100),@(150),@(80),@(80),@(80),@(80),@(100),@(130),@(130),@(130)];
    NSArray *params = @[@"order_no",@"transcation_no",@"channel_name",@"amount",@"amount_show",@"amount_settle",@"paid",@"notify_status",@"created",@"",@""];

    NSMutableArray *topSubDataArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < titles.count; i++) {
        LZTableRightSubDataModel *subDataModel = [[LZTableRightSubDataModel alloc]init];
        subDataModel.titleString = titles[i];
        subDataModel.itemHeight = self.dataModel.rowHeight;
        subDataModel.param = params[i];
        if([titles[i] isEqualToString:@"所属商户"]||
           [titles[i] isEqualToString:@"收款刷手"]){
            subDataModel.sortStatus = -1;
        }
        
        [topSubDataArray addObject:subDataModel];
    }
    self.dataModel.leftWidth = 100;
    [self.dataModel.topDataModel.classNames addObjectsFromArray:@[@"PMCustomSortCell"]];
    self.dataModel.topDataModel.setDataMethodName = @"setModelObject:";
    self.dataModel.topDataModel.itemModelArray = topSubDataArray;
    self.dataModel.topDataModel.columnWidthArray = itemWidthArray;
    self.dataModel.totalItemWidth = [[itemWidthArray valueForKeyPath:@"@sum.floatValue"] integerValue];

}
/** 交易流水*/
- (void)dealDetail:(BOOL)isFrist{

    if(isFrist){
        self.pageIndex = 1;
        self.dataModel.leftDataArray = @[];
        self.dataModel.dataArray = @[];
        [self.dataArray removeAllObjects];
    }

    NSString *startTime = [NSString stringWithFormat:@"%@ 00:00",[NSDate datestrFromDate:[NSDate date] withDateFormat:@"yyyy-MM-dd"]];
    NSString *endTime = [NSString stringWithFormat:@"%@ 23:59",[NSDate datestrFromDate:[NSDate date] withDateFormat:@"yyyy-MM-dd"]];
//    @"sortColumn":@"",
//    @"sortOrder":@""
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{@"page":@(self.pageIndex),
                                                                                    @"size":kPageSize,
                                                                                    @"startTime":startTime,
                                                                                    @"endTime":endTime}];
    [params addEntriesFromDictionary:self.paramsDictionary];
    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kGetChargeList params:params success:^(id  _Nullable responseObject) {
        @strongify(self);
        NSArray *dataArray = responseObject[@"list"];
        NSMutableArray * leftTableDataArray = [[NSMutableArray alloc]init];
        NSMutableArray * rowDataArray = [[NSMutableArray alloc]init];
        
        for (int i = 0; i < dataArray.count; i++){
            PMDealModel *dealModel = [PMDealModel modelWithJSON:dataArray[i]];
            [self.dataArray addObject:dealModel];
            // 左侧列表数据
            LZTableLeftDataModel * model = [[LZTableLeftDataModel alloc]init];
            model.titleString = dealModel.order_id;
            [leftTableDataArray addObject:model];
                
            //右侧列表数据
            LZTableRightDataModel *rightDataModel = [[LZTableRightDataModel alloc]init];
            rightDataModel.itemHeight = self.dataModel.rowHeight;

            ///右侧数据源
            NSMutableArray * rightItemArray = [[NSMutableArray alloc]init];
            
            NSArray *detail = @[dealModel.order_no?:@"",
                                dealModel.transcation_no?:@"",
                                dealModel.channel_name?:@"",
                                dealModel.amount?:@"",
                                dealModel.amount_show?:@"",
                                dealModel.amount_settle?:@"",
                                dealModel.paid?:@"",
                                dealModel.notify_status?:@"",
                                dealModel.created ? :@"",
                                dealModel.company_name?:@"",
                                [NSString stringWithFormat:@"%@%@",dealModel.nickname?:@"",dealModel.username?:@""]];
            for ( int j = 0; j < detail.count; j++) {
                LZTableRightSubDataModel *itemModel = [[LZTableRightSubDataModel alloc]init];
                itemModel.titleString = detail[j];
                //特殊处理
                if([itemModel.titleString isEqualToString:@"已支付"]
                   ||[itemModel.titleString isEqualToString:@"成功"]){
                        itemModel.textColor = UIColorHex(0x0091FF);
                    
                } else if ([itemModel.titleString isEqualToString:@"未支付/取消"]
                           ||[itemModel.titleString isEqualToString:@"失败"]){
                    itemModel.textColor = UIColorHex(0xFA6400);
                }
                itemModel.itemHeight = self.dataModel.rowHeight;
                [rightItemArray addObject:itemModel];
            }
            rightDataModel.columnWidthArray = self.dataModel.topDataModel.columnWidthArray;
            rightDataModel.itemModelArray = rightItemArray;
            [rowDataArray addObject:rightDataModel];
            
            rightDataModel.setDataMethodName = @"setModelObject:";
            [rightDataModel.classNames addObjectsFromArray:@[@"PMCustomPayCell"]];
        }
        
        NSMutableArray *mutableLeftDataArray = [[NSMutableArray alloc] initWithArray:self.dataModel.leftDataArray];
        NSMutableArray *mutableRowDataArray = [[NSMutableArray alloc] initWithArray:self.dataModel.dataArray];

        [mutableLeftDataArray addObjectsFromArray:leftTableDataArray];
        [mutableRowDataArray addObjectsFromArray:rowDataArray];

        self.dataModel.leftDataArray = mutableLeftDataArray;
        self.dataModel.dataArray = mutableRowDataArray;
        ///回调
        if(dataArray.count < [kPageSize intValue]){
            [self.reloadSubject sendNext:@(0)];

        } else {
            self.pageIndex ++;
            [self.reloadSubject sendNext:@(1)];
        }
    } failure:^(NSError * _Nullable error) {
            @strongify(self);
          [self.reloadSubject sendNext:@(1)];
    }];
}


/** 付款订单*/
- (void)summaryTransferDetail{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{@"page":@(1),
                                                                                    @"size":@"20"}];
    
    [params addEntriesFromDictionary:self.paramsDictionary];

    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kSummaryTransfer params:params success:^(id  _Nullable responseObject) {
        @strongify(self);
        if([responseObject[kResultStatus] isEqualToString:kCode]){
            NSString *data = [NSString stringWithFormat:@"%@",responseObject[kResultData]];
            self.dataDictionary = [data mj_JSONObject];
        }
        [self.reloadSubject sendNext:@(4)];
    } failure:^(NSError * _Nullable error) {
        @strongify(self);
        [self.reloadSubject sendNext:@(4)];
    }];
}


/** 付款订单List*/
- (void)transferDetail:(BOOL)isFrist{
    
    if(isFrist){
         self.pageIndex = 1;
        self.dataModel.leftDataArray = @[];
        self.dataModel.dataArray = @[];
        [self.dataArray removeAllObjects];

     }
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{@"page":@(self.pageIndex),
                                                                                    @"size":kPageSize}];
    [params addEntriesFromDictionary:self.paramsDictionary];

    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kGetTradeList params:params success:^(id  _Nullable responseObject) {
    @strongify(self);
        NSArray *dataArray = responseObject[@"list"];

        NSMutableArray * leftTableDataArray = [[NSMutableArray alloc]init];
        NSMutableArray * rowDataArray = [[NSMutableArray alloc]init];
        
        for (int i = 0; i < dataArray.count; i++){
            PMDealModel *dealModel = [PMDealModel modelWithJSON:dataArray[i]];
            [self.dataArray addObject:dealModel];
            // 左侧列表数据
            LZTableLeftDataModel * model = [[LZTableLeftDataModel alloc]init];
            model.titleString = dealModel.order_id;
            [leftTableDataArray addObject:model];
            
            //右侧列表数据
            LZTableRightDataModel *rightDataModel = [[LZTableRightDataModel alloc]init];
            rightDataModel.itemHeight = self.dataModel.rowHeight;

            ///右侧数据源
            NSMutableArray * rightItemArray = [[NSMutableArray alloc]init];
                        
            NSArray *detail = @[dealModel.channel_transfer?:@"",
                                dealModel.amount?:@"",
                                dealModel.profit?:@"",
                                [NSString stringWithFormat:@"%@,%@%@,%@",dealModel.ext_user_name,dealModel.ext_open_bank,dealModel.ext_sub_bank,dealModel.ext_card_number],
                                dealModel.transfer_status?:@"",
                                dealModel.ext_collection?:@"",
                                dealModel.audit_ret ? :@"",
                                dealModel.created?:@"",
                                dealModel.time_settle?:@"",
                                dealModel.company_name?:@""];

                for ( int j = 0; j < detail.count; j++) {
                    LZTableRightSubDataModel *itemModel = [[LZTableRightSubDataModel alloc]init];
                    itemModel.titleString = detail[j];
                    //特殊处理
                    if([itemModel.titleString isEqualToString:@"付款成功"]){
                        itemModel.textColor = UIColorHex(0xFF8040);
                    } else if([itemModel.titleString  isEqualToString:@"付款成功"]){
                        itemModel.textColor = UIColor.greenColor;
                    } else if([itemModel.titleString  isEqualToString:@"付款失败"]
                              ||[itemModel.titleString  isEqualToString:@"失败回退中"]
                              ||[itemModel.titleString isEqualToString:@"未指定"]){
                        itemModel.textColor = UIColor.redColor;
                    } else if ([itemModel.titleString  isEqualToString:@"回退完成"]
                               ||[itemModel.titleString isEqualToString:@"已指定"]){
                        itemModel.textColor = UIColorHex(0x0091FF);
                    }

            
                    itemModel.itemHeight = self.dataModel.rowHeight;
                    [rightItemArray addObject:itemModel];
                }
                rightDataModel.columnWidthArray = self.dataModel.topDataModel.columnWidthArray;
                rightDataModel.itemModelArray = rightItemArray;
                [rowDataArray addObject:rightDataModel];
                
                rightDataModel.setDataMethodName = @"setModelObject:";
                [rightDataModel.classNames addObjectsFromArray:@[@"PMCustomPayCell"]];
            }
        
        NSMutableArray *mutableLeftDataArray = [[NSMutableArray alloc] initWithArray:self.dataModel.leftDataArray];
        NSMutableArray *mutableRowDataArray = [[NSMutableArray alloc] initWithArray:self.dataModel.dataArray];

        [mutableLeftDataArray addObjectsFromArray:leftTableDataArray];
        [mutableRowDataArray addObjectsFromArray:rowDataArray];

        self.dataModel.leftDataArray = mutableLeftDataArray;
        self.dataModel.dataArray = mutableRowDataArray;
        ///回调
        if(dataArray.count < [kPageSize intValue]){
            [self.reloadSubject sendNext:@(0)];

        } else {
            self.pageIndex ++;
            [self.reloadSubject sendNext:@(1)];
        }

    } failure:^(NSError * _Nullable error) {
        @strongify(self);
        [self.reloadSubject sendNext:@(1)];
    }];
    
}
///回调
- (void)dealCallBack:(PMDealModel *)dealModel{
    
    NSDictionary *params = @{@"order_id":dealModel.modelid?:@""};
    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kNotifyCharge params:params success:^(id  _Nullable responseObject) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            @strongify(self);
            [self getDealDetail:YES];
        });
    } failure:^(NSError * _Nullable error) {} isLoading:YES];
    
}

///一键回调
- (void)dealALLCallBack{
    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kNotifyChargeBatch params:@{} success:^(id  _Nullable responseObject) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            @strongify(self);
            [self getDealDetail:YES];
        });
    } failure:^(NSError * _Nullable error) {} isLoading:YES isFailTip:YES];
    
}

///审核下发
- (void)checkAuditTransfer:(NSDictionary *)params{
    
    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kAuditTransfer params:params success:^(id  _Nullable responseObject) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
           @strongify(self);
           [self getDealDetail:YES];
        });
    } failure:^(NSError * _Nullable error) {} isLoading:YES];
       
}

- (LZTableDataModel *)dataModel{
    if(!_dataModel){
        _dataModel = [[LZTableDataModel alloc] init];
        _dataModel.leftDataArray = @[];
        _dataModel.dataArray = @[];
    }
    return _dataModel;
}

- (NSMutableDictionary *)paramsDictionary{
    if(!_paramsDictionary){
        _paramsDictionary = [[NSMutableDictionary alloc] init];
    }
    return _paramsDictionary;
}

- (NSMutableDictionary *)dataDictionary{
    if(!_dataDictionary){
        _dataDictionary = [[NSMutableDictionary alloc] init];
    }
    return _dataDictionary;
}

@end
