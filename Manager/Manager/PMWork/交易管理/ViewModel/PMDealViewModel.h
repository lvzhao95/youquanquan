//
//  PMDealViewModel.h
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZBaseViewModel.h"
#import "LZTableDataModel.h"
#import "PMDealModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PMDealViewModel : LZBaseViewModel

@property (nonatomic,assign) NSInteger index;

//表格数据源
@property (nonatomic,strong) LZTableDataModel *dataModel;

//
@property (nonatomic,strong) NSMutableDictionary *paramsDictionary;

@property (nonatomic,strong) NSMutableDictionary *dataDictionary;


///获取交易管理
- (void)getDealDetail:(BOOL)isFrist;


///回调
- (void)dealCallBack:(PMDealModel *)dealModel;


///一键回调
- (void)dealALLCallBack;

///审核下发
- (void)checkAuditTransfer:(NSDictionary *)params;
@end

NS_ASSUME_NONNULL_END
