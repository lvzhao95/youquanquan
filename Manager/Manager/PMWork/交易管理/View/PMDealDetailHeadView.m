//
//  PMDealDetailHeadView.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMDealDetailHeadView.h"
#import "PMDealViewModel.h"


@interface PMDealDetailHeadView()

@property (nonatomic,strong) PMDealViewModel *viewModel;

@end

@implementation PMDealDetailHeadView

- (instancetype)initViewModel:(LZBaseViewModel *)viewModel
{
    self = [super initViewModel:viewModel];
    if (self) {
        self.viewModel = (PMDealViewModel *)viewModel;
        [self setupView];
        
    }
    return self;
}

#pragma mark 创建UI
- (void) setupView{
    
    
    NSMutableArray *detailLabs = [[NSMutableArray alloc] init];
    
    NSArray *titles = @[@"成功金额：￥0.00",@"成功笔数：0",@"待审核金额：￥0.00",@"待审核笔数：0",@"待下发金额：￥0.00"];
    //每个Item宽高
    CGFloat H = 21;
    CGFloat W = (kScreenWidth - 30 - 10)/2.0;
    //每行列数
    NSInteger rank = 2;
    //每列间距
    CGFloat rankMargin = 10;
    //每行间距
    CGFloat rowMargin = 15;
    //Item索引 ->根据需求改变索引
    NSUInteger index = titles.count;
    
    CGFloat top = 10;
    for (int i = 0 ; i < index; i++) {
        
        UILabel *detailLab = [UILabel lz_labelWithText:titles[i] fontSize:PingFangSC(15) color:COLOR_textColor];
        detailLab.adjustsFontSizeToFitWidth = YES;
        //Item Y轴
        NSUInteger Y = (i / rank) * (H +rowMargin);
        //Item X轴
        CGFloat X = (i % rank) * (W + rankMargin);
        
        
        if (i % 2 == 0) {
            [detailLab lz_leftAlignment];
        } else {
            
            [detailLab lz_rightAlignment];
        }
        
        [self addSubview:detailLab];
        [detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(X + 15);
            make.top.mas_equalTo(top + Y);
            make.width.mas_equalTo(W);
            make.height.mas_equalTo(H);
        }];
        [detailLabs addObject:detailLab];
    }
    
    
    [self.viewModel.reloadSubject subscribeNext:^(id  _Nullable x) {
        dismiss(nil);
        NSString *pending_amount = [[NSString stringWithFormat:@"%@",self.viewModel.dataDictionary[@"pending_amount"]] yw_stringByDividingBy:@"100"];
        NSString *success_amount = [[NSString stringWithFormat:@"%@",self.viewModel.dataDictionary[@"success_amount"]] yw_stringByDividingBy:@"100"];
        NSString *amount_useable = [[NSString stringWithFormat:@"%@",self.viewModel.dataDictionary[@"amount_useable"]] yw_stringByDividingBy:@"100"];

        NSArray *details = @[[NSString stringWithFormat:@"成功金额:￥%@",success_amount],
                             [NSString stringWithFormat:@"成功笔数:%@",self.viewModel.dataDictionary[@"success_cnt"]?:@"0"],
                             [NSString stringWithFormat:@"待审核金额:￥%@",pending_amount],
                             [NSString stringWithFormat:@"待审核笔数:%@",self.viewModel.dataDictionary[@"pending_cnt"]?:@"0"],
                             [NSString stringWithFormat:@"待下发金额:￥%@",amount_useable]];
        
        for (int i = 0; i < details.count; i++){
            UILabel *detailLab = detailLabs[i];
            detailLab.text = details[i];
        }
        
        
    }];
}

@end
