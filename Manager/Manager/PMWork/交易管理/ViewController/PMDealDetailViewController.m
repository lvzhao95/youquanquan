//
//  PMDealDetailViewController.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMDealDetailViewController.h"
#import "PMDealViewModel.h"
#import "PMDealDetailHeadView.h"
#import "LZTableSlideView.h"
#import "LZTableDataModel.h"
#import "LZSortButton.h"
#import "LZMJDIYRefreshHeader.h"
#import "LZMJDIYRefeshFooter.h"
#import "PMScreenDownView.h"
#import "LZOptionModel.h"

@interface PMDealDetailViewController ()

@property (nonatomic,strong)  PMDealViewModel *viewModel;

@property (nonatomic,strong)  LZTableSlideView *tableView;

@end

@implementation PMDealDetailViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColor.whiteColor;

    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.viewModel.index = 0;
    
    [self setupUI];
  
    [self reloadRightItem];
}

//刷新右按钮
- (void)reloadRightItem{
    
    ///!!!:筛选
    self.navigationItem.rightBarButtonItem = nil;
    UIButton *screeningBtn = [UIButton lz_buttonImageName:@"pm_shaixuan" backgroundImageName:@""];
    screeningBtn.frame = CGRectMake(0, 0, 44, 44);
    [LZTool.currentViewController initBarItem:screeningBtn withType:1];
    [screeningBtn addTarget:self action:@selector(screenClick:) forControlEvents:UIControlEventTouchUpInside];
}
///创建UI
- (void)setupUI{
    
    ///!!!:回调
    UIButton *callbackBtn = [UIButton lz_buttonTitle:@"一键回调" titleColor:COLOR_appColor fontSize:15];
    [callbackBtn setImage:k_imageName(@"pm_callback") forState:UIControlStateNormal];
    [self.view addSubview:callbackBtn];
    [callbackBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(50);
        make.bottom.mas_equalTo(K_Device_Is_iPhoneX ? - 34:0);
    }];
    [callbackBtn SG_imagePositionStyle:SGImagePositionStyleDefault spacing:5];
    [callbackBtn addTarget:self action:@selector(callBackClick) forControlEvents:UIControlEventTouchUpInside];
    
    ///!!!:表格
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo((self.viewModel.index == 0)? 0:130);
        make.width.mas_equalTo(kScreenWidth);
        make.bottom.mas_equalTo(callbackBtn.mas_top);
    }];
    
    ///上拉加载. 下拉刷新
    [self addRefresh];
        
    @weakify(self);
    [self.tableView setCallBlock:^(PMTouchTable touchTable, NSInteger index, LZSortButton *sortBtn) {
         @strongify(self);
        switch (touchTable) {
            case PMTouchTableTop:
            case PMTouchTableTopLeft:
                [self selectTableSortOrder:index sortBtn:sortBtn];
                break;
            case PMTouchTableRight:
                [self selectTableIndex:index];
                
                break;

            default:
                break;
        }
        
    }];
    
    [self.viewModel.reloadSubject subscribeNext:^(id  _Nullable x) {
        dismiss(nil);
        @strongify(self);
        [self reloadTable:[x intValue]];
    }];
    

    //获取详情
    showLoadingTitle(@"", nil);
    [self.viewModel getDealDetail:YES];

}

//筛选
- (void)screenClick:(UIButton *)sender{
    
    NSArray *merchantArray = [PMPublicManager sharedInstance].merchantArray;
    NSArray *channelArray = [PMPublicManager sharedInstance].channelArray;
    NSArray *personArray = [PMPublicManager sharedInstance].personArray;
        
    NSMutableArray *notifyStatus = [LZOptionModel createOptionModel:@[@"成功",@"失败"] details:@[@"OK",@"FAIL"] key:@"notify_status"];
    NSMutableArray *payStatus = [LZOptionModel createOptionModel:@[@"未支付/取消",@"已支付",@"已支付(未结算)",@"已支付(已结算)"] details:@[@"paid.false",@"paid.true",@"settled.false",@"settled.true"] key:@"status"];
    
    
    NSArray *subSelectTitles = @[];
    NSArray *subTitles = @[];
    NSArray *placeholders = @[];

    subSelectTitles = @[merchantArray,channelArray,@[],personArray,notifyStatus,payStatus];
    subTitles = @[@"商户：",@"支付类型：",@"单号：",@"刷手：",@"回调：",@"支付：",@"时间："];
    placeholders = @[@"请选择商户",@"请选择支付类型",@"商户订单/平台订单/流水号",@"选择刷手",@"回调状态",@"支付状态",@"请选择时间"];
    
    @weakify(self);
    [PMScreenDownView showScreenAlertType:LZAlertTypeTradeScreen cancel:@"重置" sure:@"确定" objectDict:@{@"subTitles":subTitles,@"subSelectTitles":subSelectTitles,@"placeholders":placeholders} cancelBlock:^(id  _Nullable object) {
        @strongify(self);
        [self.viewModel.paramsDictionary removeAllObjects];
    } sureBlock:^(id  _Nullable object) {
        @strongify(self);
        NSMutableDictionary *dataDictionary = [[NSMutableDictionary alloc] initWithDictionary:object];
        
        [self.viewModel.paramsDictionary addEntriesFromDictionary:dataDictionary];
        showLoadingTitle(@"", nil);
        [self.viewModel getDealDetail:YES];
        
    }];

}


///刷线表格
- (void)reloadTable:(NSInteger) type{
    
    
    if(type > 3)return;
    
    self.tableView.dataModel = self.viewModel.dataModel;
    [self.tableView.rightTableView.mj_header endRefreshing];
    [self.tableView.rightTableView.mj_footer endRefreshing];
    self.tableView.rightTableView.mj_footer.hidden = NO;
    [self.tableView  dismissEmptyView];
    switch (type) {
        case 0:{
            self.tableView.rightTableView.mj_footer.hidden = YES;
        }
            break;
        default:
            break;
    }
    
    if(self.viewModel.dataModel.dataArray.count == 0){
        self.tableView.rightTableView.mj_footer.hidden = YES;
        [self.tableView tableViewDisplayWithImgName:@"no_data" message:@"暂无数据" contentoffsetY:100 showEmptyView:YES];
    }
}

///添加下拉, 上啦
- (void)addRefresh{
    
    @weakify(self);
    self.tableView.rightTableView.mj_header = [LZMJDIYRefreshHeader headerWithRefreshingBlock:^{
        NSLog(@"下拉刷新");
        @strongify(self);
        [self.viewModel getDealDetail:YES];
 
    }];
    self.tableView.rightTableView.mj_footer = [LZMJDIYRefeshFooter footerWithRefreshingBlock:^{
        NSLog(@"上拉加载");
        @strongify(self);
        [self.viewModel getDealDetail:NO];
     
    }];
    
    // 不让mj 自适应宽度
    self.tableView.rightTableView.mj_header.autoresizingMask = UIViewAutoresizingNone;
    self.tableView.rightTableView.mj_header.mj_x = -self.viewModel.dataModel.leftWidth;
    // 设置mj宽度
    self.tableView.rightTableView.mj_header.mj_w = K_SCREENWIDTH;
    self.tableView.rightTableView.mj_footer.autoresizingMask = UIViewAutoresizingNone;
    self.tableView.rightTableView.mj_footer.mj_w = K_SCREENWIDTH ;
    self.tableView.rightTableView.mj_footer.mj_x = -self.viewModel.dataModel.leftWidth;
    
}

///一键回调
- (void)callBackClick{

    @weakify(self);
    [LZToolView showAlertType:LZAlertTypeTipAlter withTitle:@"一键回调" message:@"确定一键回调(72小时内)所有回调失败的订单？" cancel:@"取消" sure:@"确定" objectDict:nil cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
        @strongify(self);
        [self.viewModel dealALLCallBack];
    }];
    
}

///选择第几行操作
- (void)selectTableIndex:(NSInteger) index{
    
    PMDealModel *dealModel = self.viewModel.dataArray[index];
    
    NSMutableArray *subTitles = [[NSMutableArray alloc] init];
    if([dealModel.notify_status isEqualToString:@""] && [dealModel.paid isEqualToString:@"已支付"]){
        [subTitles addObject:@"回调"];
    }
//    [subTitles addObject:@"补单"];
//    [subTitles addObject:@"更换刷手"];
    if(subTitles.count == 0)return;
    @weakify(self);
    [LZToolView showAlertType:LZAlertTypeActionSheet withTitle:@"" message:@"" cancel:@"取消" sure:@"" objectDict:@{@"subTitles":subTitles} cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
        @strongify(self);
        [self.viewModel dealCallBack:dealModel];

    }];
}



///选择排序
- (void)selectTableSortOrder:(NSInteger)index sortBtn:(LZSortButton *)sortBtn{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    LZTableRightSubDataModel *subDataModel = nil;
    if(sortBtn.tag == 100){
        subDataModel  = self.viewModel.dataModel.topLeftDataModel;
    } else {
        subDataModel  = self.viewModel.dataModel.topDataModel.itemModelArray[index];
    }
  
    switch (sortBtn.sortStatue) {
        case 1:
            params[@"sortColumn"] = subDataModel.param?:@"";
            params[@"sortOrder"] = @"asc";

            break;
        case 2:
            params[@"sortColumn"] = subDataModel.param?:@"";
            params[@"sortOrder"] = @"desc";
            break;
        default:
            params[@"sortColumn"] = @"";
            params[@"sortOrder"] = @"";
            break;
    }
    
    [self.viewModel.paramsDictionary addEntriesFromDictionary:params];
    showLoadingTitle(@"", nil);
    [self.viewModel getDealDetail:YES];

}

- (PMDealViewModel *)viewModel{
    if(!_viewModel){
        _viewModel = [[PMDealViewModel alloc] init];
    }
    return _viewModel;;
}

- (LZTableSlideView *)tableView{
    if(!_tableView){
        _tableView = [[LZTableSlideView alloc] initWithFrame:CGRectMake(0, (self.viewModel.index == 0)? 0:130, kScreenWidth, 400)];
    }
    return _tableView;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
