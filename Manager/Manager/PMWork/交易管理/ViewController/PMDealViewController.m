//
//  PMDealViewController.m
//  Manager
//
//  Created by lvzhao on 2020/7/30.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMDealViewController.h"
#import "PMDealView.h"
#import "PMDealViewModel.h"
#import "PMScreenDownView.h"

@interface PMDealViewController ()
@property (nonatomic,strong) PMDealViewModel *viewModel;

@property (nonatomic,strong) PMDealView *dealView;
@end

@implementation PMDealViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"交易管理";
    
    [self setupUI];
    // Do any additional setup after loading the view.
}

- (void)setupUI{
    [self.view addSubview:self.dealView];
    [self.dealView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    
 
    
}

#pragma mark -懒加载
- (PMDealView *)dealView{
    if(!_dealView) {
        _dealView = [[PMDealView alloc] initViewModel:self.viewModel];
    }
    return _dealView;
}

- (PMDealViewModel *)viewModel{
    if(!_viewModel) {
        _viewModel = [[PMDealViewModel alloc] init];
    }
    return _viewModel;
}

@end
