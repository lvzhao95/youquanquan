//
//  PMDealModel.h
//  Manager
//
//  Created by lvzhao on 2020/8/3.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PMDealModel : NSObject

@property (nonatomic,strong) NSString *modelid;     //order_no
@property (nonatomic,strong) NSString *profit;     //profit


@property (nonatomic,strong) NSString *order_no;     //order_no
@property (nonatomic,strong) NSString *transcation_no;  //transcation_no
@property (nonatomic,strong) NSString *order_id;  //order_id
@property (nonatomic,strong) NSString *channel_name;    //channel_name


@property (nonatomic,strong) NSString *amount;          //交易金额
@property (nonatomic,strong) NSString *settle_fee;       //          settle_fee
@property (nonatomic,strong) NSString *amount_show;       //显示金额          amount_show
@property (nonatomic,strong) NSString *amount_settle;       //结算金额          amount_settle

@property (nonatomic,strong) NSString *created;       //时间
@property (nonatomic,strong) NSString *company_name;       //所属商户
@property (nonatomic,strong) NSString *nickname;       //收款刷手
@property (nonatomic,strong) NSString *username;       //收款刷手

@property (nonatomic,strong) NSString *paid;       //是否付款
@property (nonatomic,strong) NSString *notify_status;       //收款刷手


///付款订单

@property (nonatomic,strong) NSString *channel_transfer;       //付款渠道
@property (nonatomic,strong) NSString *ext_open_bank;
@property (nonatomic,strong) NSString *ext_user_name;
@property (nonatomic,strong) NSString *ext_card_number;
@property (nonatomic,strong) NSString *ext_sub_bank;
@property (nonatomic,strong) NSString *transfer_status;
@property (nonatomic,strong) NSString *ext_collection;
@property (nonatomic,strong) NSString *audit_ret;
@property (nonatomic,strong) NSString *time_settle;


@end

NS_ASSUME_NONNULL_END
