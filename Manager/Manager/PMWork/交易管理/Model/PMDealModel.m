//
//  PMDealModel.m
//  Manager
//
//  Created by lvzhao on 2020/8/3.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMDealModel.h"

@implementation PMDealModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"modelid" : @"id",
             @"orderNo" : @"order_no",
             @"transcationNo" : @"transcation_no",
             @"orderId" : @"order_id",
             @"channelName" : @"channel_name",
             @"amountShow" : @"amount_show",
             @"amountSettle" : @"amount_settle",
             @"companyName" : @"company_name",
             @"notifyStatus": @"notify_status"};
}

- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic{
    
    
    
    NSString *amount = [NSString stringWithFormat:@"%@",dic[@"amount"]?:@"0"];
    NSString *amount_settle = [NSString stringWithFormat:@"%@",dic[@"amount_settle"]?:@"0"];
    NSString *profit = [NSString stringWithFormat:@"%@",dic[@"profit"]?:@"0"];

    self.amount_settle = [NSString stringWithFormat:@"￥ %@",[[amount yw_stringBySubtracting:amount_settle] yw_stringByDividingBy:@"100"]];
    self.amount_show = [NSString stringWithFormat:@"￥ %@",[self.amount_show yw_stringByDividingBy:@"100"]];
    self.amount = [NSString stringWithFormat:@"￥ %@",[self.amount yw_stringByDividingBy:@"100"]];
    self.profit = [NSString stringWithFormat:@"￥ %@",[profit yw_stringByDividingBy:@"100"]];

    
    BOOL isPaid = [dic[@"paid"] boolValue];
    self.paid = isPaid ? @"已支付":@"未支付/取消";
    NSString *notify_status = dic[@"notify_status"];
    if([notify_status.lowercaseString isEqualToString:@"ok"]){
        self.notify_status = @"成功";
    } else {
        self.notify_status = @"";
    }
    
    
    NSString *transfer_status = dic[@"transfer_status"];
    if([transfer_status isEqualToString:@"pending"]){
        self.transfer_status = @"处理中";
    }
    else if([transfer_status isEqualToString:@"paid"]){
        self.transfer_status = @"付款成功";
        
    }
    else if([transfer_status isEqualToString:@"fallback"]){
        self.transfer_status = @"失败回退中";
    }
    else if([transfer_status isEqualToString:@"fallback_complete"]){
        self.transfer_status = @"回退完成";
    }  else if([transfer_status isEqualToString:@"failed"]){
           
        self.transfer_status = @"付款失败";
    }
    
        
    NSString *ext_collection = dic[@"ext_collection"];
    if([ext_collection isEqualToString:@"true"]){
        self.ext_collection = @"已指定";
    } else {
        self.ext_collection = @"未指定";
    }
    
    return YES;
}

@end
