//
//  PMPersonManagerViewController.m
//  Manager
//
//  Created by lvzhao on 2020/8/1.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPersonManagerViewController.h"
#import "PMPersonManagerView.h"
#import "PMPersonManagerViewModel.h"

@interface PMPersonManagerViewController ()

@property (nonatomic,strong) PMPersonManagerView *managerView;

@property (nonatomic,strong) PMPersonManagerViewModel *viewModel;

@end

@implementation PMPersonManagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"刷手管理";
    [self setupUI];
    // Do any additional setup after loading the view.
}

- (void)setupUI{
    [self.view addSubview:self.managerView];
    [self.managerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}


#pragma mark -懒加载
- (PMPersonManagerView *)managerView{
    if(!_managerView) {
        _managerView = [[PMPersonManagerView alloc] initViewModel:self.viewModel];
    }
    return _managerView;
}

- (PMPersonManagerViewModel *)viewModel{
    if(!_viewModel) {
        _viewModel = [[PMPersonManagerViewModel alloc] init];
        _viewModel.modelid = self.modelid;
    }
    return _viewModel;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
