//
//  PMPersonListViewController.m
//  Manager
//
//  Created by lvzhao on 2020/8/1.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPersonListViewController.h"
#import "LZTableSlideView.h"
#import "LZSortButton.h"
#import "LZMJDIYRefreshHeader.h"
#import "LZMJDIYRefeshFooter.h"
#import "PMScreenDownView.h"
#import "PMPayeeInfoViewController.h"

@interface PMPersonListViewController ()


@property (nonatomic,strong) LZTableSlideView *tableView;
@end

@implementation PMPersonListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewModel.index = 0;

    self.view.backgroundColor = UIColor.whiteColor;
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self setupUI];
    
    [self reloadRightItem];
}

//刷新右按钮
- (void)reloadRightItem{
    
    ///!!!:筛选
    self.navigationItem.rightBarButtonItem = nil;
    UIButton *screeningBtn = [UIButton lz_buttonImageName:@"pm_shaixuan" backgroundImageName:@""];
    screeningBtn.frame = CGRectMake(0, 0, 44, 44);
    [LZTool.currentViewController initBarItem:screeningBtn withType:1];
    [screeningBtn addTarget:self action:@selector(screenClick:) forControlEvents:UIControlEventTouchUpInside];
}


- (void)setupUI{
    
    ///!!!:操作按钮
    UIView *optionView = [[UIView alloc] init];
    optionView.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:optionView];
    [optionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(K_Device_Is_iPhoneX ? -34 : 0);
        make.height.mas_equalTo(50);
    }];
    
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = COLOR_cellLine;
    [optionView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
    
    CGFloat titleW = (K_SCREENWIDTH - 30 - 20)/2.0;
    NSArray *titles = @[@"申请开户",@"清空额度"];
    UIView *lastView = nil;
    for (int i = 0; i < titles.count; i++){
        UIButton *optionBtn = [UIButton lz_buttonTitle:titles[i] titleColor:COLOR_appColor fontSize:15];
        [optionView addSubview:optionBtn];
        [optionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            if(lastView){
                make.left.mas_equalTo(lastView.mas_right).mas_offset(10);
            } else {
                make.left.mas_equalTo(15);
            }
            make.width.mas_equalTo(titleW);
            make.height.mas_equalTo(50);
            make.top.mas_equalTo(0);
        }];
        optionBtn.tag = 10+i;
        [optionBtn addTarget:self action:@selector(optionClick:) forControlEvents:UIControlEventTouchUpInside];
        lastView = optionBtn;

    }
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(kScreenWidth);
        make.bottom.mas_equalTo(optionView.mas_top);
    }];
    
    [self addRefresh];
  
    showLoadingTitle(@"", nil);
    [self.viewModel getPersonList:YES];

    @weakify(self);
    [self.viewModel.reloadSubject subscribeNext:^(id  _Nullable x) {
        
        dismiss(nil);
        @strongify(self);
        [self reloadTable:[x intValue]];
        
    }];
}


///添加下拉, 上啦
- (void)addRefresh{
    
    @weakify(self);
    self.tableView.rightTableView.mj_header = [LZMJDIYRefreshHeader headerWithRefreshingBlock:^{
        NSLog(@"下拉刷新");
        @strongify(self);
        [self.viewModel getPersonList:YES];
 
    }];
    self.tableView.rightTableView.mj_footer = [LZMJDIYRefeshFooter footerWithRefreshingBlock:^{
        NSLog(@"上拉加载");
        @strongify(self);
        [self.viewModel getPersonList:NO];
     
    }];
    
    // 不让mj 自适应宽度
    self.tableView.rightTableView.mj_header.autoresizingMask = UIViewAutoresizingNone;
    self.tableView.rightTableView.mj_header.mj_x = -self.viewModel.dataModel.leftWidth;
    // 设置mj宽度
    self.tableView.rightTableView.mj_header.mj_w = K_SCREENWIDTH;
    self.tableView.rightTableView.mj_footer.autoresizingMask = UIViewAutoresizingNone;
    self.tableView.rightTableView.mj_footer.mj_w = K_SCREENWIDTH ;
    self.tableView.rightTableView.mj_footer.mj_x = -self.viewModel.dataModel.leftWidth;
    
}

///刷线表格
- (void)reloadTable:(NSInteger) type{
    
    if(type > 3)return;
    
    self.tableView.dataModel = self.viewModel.dataModel;
    [self.tableView.rightTableView.mj_header endRefreshing];
    [self.tableView.rightTableView.mj_footer endRefreshing];
    self.tableView.rightTableView.mj_footer.hidden = NO;
    [self.tableView  dismissEmptyView];
    switch (type) {
        case 0:{
            self.tableView.rightTableView.mj_footer.hidden = YES;
        }
            break;
        default:
            break;
    }
    
    if(self.viewModel.dataModel.dataArray.count == 0){
        self.tableView.rightTableView.mj_footer.hidden = YES;
        [self.tableView tableViewDisplayWithImgName:@"no_data" message:@"暂无数据" contentoffsetY:100 showEmptyView:YES];
    }
}

///排序
- (void)screenClick:(UIButton *)btn{
   

       NSArray *subSelectTitles = @[];
       NSArray *subTitles = @[];
       NSArray *placeholders = @[];
    
       subSelectTitles = @[@[],@[],@[]];
       subTitles = @[@"收款人名称：",@"收款人电话：",@"时间："];
       placeholders = @[@"收款人名称",@"收款人电话",@"请选择时间"];

       @weakify(self);
       [PMScreenDownView showScreenAlertType:LZAlertTypeTradeScreen cancel:@"重置" sure:@"确定" objectDict:@{@"subTitles":subTitles,@"subSelectTitles":subSelectTitles,@"placeholders":placeholders} cancelBlock:^(id  _Nullable object) {
           @strongify(self);
           [self.viewModel.paramsDictionary removeAllObjects];
       } sureBlock:^(id  _Nullable object) {
           @strongify(self);
           NSMutableDictionary *dataDictionary = [[NSMutableDictionary alloc] initWithDictionary:object];

           [self.viewModel.paramsDictionary addEntriesFromDictionary:dataDictionary];
           showLoadingTitle(@"", nil);
           [self.viewModel getPersonList:YES];

       }];
}

///操作
- (void)optionClick:(UIButton *)sender{
    
    @weakify(self);
    switch (sender.tag) {
        case 10:
        {
            RACSubject *subject = [RACSubject subject];
            PMPayeeInfoViewController *editInfoVC = [[PMPayeeInfoViewController alloc] init];
            editInfoVC.subject = subject;
            [LZTool.currentViewController.navigationController pushViewController:editInfoVC animated:YES];
        
            [subject subscribeNext:^(id  _Nullable x) {
                @strongify(self);
                
                
            }];
        }
            break;
            
        default:
            break;
    }
    
    
    
}


- (LZTableSlideView *)tableView{
    if(!_tableView){
        _tableView = [[LZTableSlideView alloc] initWithFrame:CGRectMake(0, 0, K_SCREENWIDTH, K_SCREENHEIGHT - 45 - K_NAVHEIGHT)];
    }
    return _tableView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
