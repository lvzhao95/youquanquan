//
//  PMPersonManagerViewController.h
//  Manager
//
//  Created by lvzhao on 2020/8/1.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PMPersonManagerViewController : LZBaseViewController

@property (nonatomic,strong) NSString *modelid;

@end

NS_ASSUME_NONNULL_END
