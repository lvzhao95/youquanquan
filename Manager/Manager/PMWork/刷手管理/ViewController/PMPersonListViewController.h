//
//  PMPersonListViewController.h
//  Manager
//
//  Created by lvzhao on 2020/8/1.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZBaseViewController.h"
#import "PMPersonManagerViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PMPersonListViewController : LZBaseViewController

@property (nonatomic,strong) PMPersonManagerViewModel *viewModel;

@end

NS_ASSUME_NONNULL_END
