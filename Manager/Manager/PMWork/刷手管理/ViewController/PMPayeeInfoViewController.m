//
//  PMPayeeInfoViewController.m
//  Manager
//
//  Created by lvzhao on 2020/8/2.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPayeeInfoViewController.h"
#import "PMPersonManagerViewModel.h"
#import "PMChannelTextField.h"


@interface PMPayeeInfoViewController ()

@property (nonatomic,strong) PMPersonManagerViewModel *viewModel;

@property (nonatomic,strong) NSMutableDictionary *dataDictionary;

@end

@implementation PMPayeeInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"收款人资料";
    self.view.backgroundColor = UIColor.whiteColor;
    [self setupUI];
}

- (void) setupUI{
    
    //请求商户列表
    [self.viewModel getMerchantList];
    self.dataDictionary = [[NSMutableDictionary alloc] init];
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [self.view addSubview:scrollView];
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.right.bottom.mas_equalTo(0);
    }];

    //内容
    UIView *contentView = [[UIView alloc] init];
    [scrollView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.and.right.equalTo(scrollView).with.insets(UIEdgeInsetsZero);
        make.width.equalTo(scrollView);

    }];

    NSMutableArray *merchantViews = [[NSMutableArray alloc] init];
    UIView *lastMerchantView = nil;
    @weakify(self);
    PMChannelTextField *merchantTypeView = nil;
    PMChannelTextField *membersTypeView = nil;
    {
        NSArray *titles = @[@"商户类型",@"* 收款人名称",@"* 联系电话",@"* 收费费率",@"* 登录密码",@"* 会员类型"];
        NSArray *placeholders = @[@"",@"请输入收款人名称",@"请输入收款人联系电话",@"请输入收费费率",@"请输入登录密码",@""];
        NSArray *keys= @[@"",@"nickname",@"mobile",@"settle_rate",@"loginpwd",@""];

          
          for (int i = 0; i < titles.count; i++){
              PMChannelTextField *textField = [[PMChannelTextField alloc] init];
              textField.title = titles[i];
              textField.placeholder = placeholders[i];
              [contentView addSubview:textField];
              [textField mas_makeConstraints:^(MASConstraintMaker *make) {
                  make.left.mas_equalTo(15);
                  make.right.mas_equalTo(-15);
                  make.height.mas_equalTo(50);
                  if(lastMerchantView){
                      make.top.mas_equalTo(lastMerchantView.mas_bottom).mas_offset(10);
                  } else {
                      make.top.mas_equalTo(5);
                  }
              }];
              lastMerchantView = textField;
              [merchantViews addObject:lastMerchantView];
              NSString *title = titles[i];
              if([title containsString:@"商户类型"]){
                  merchantTypeView = textField;
              }
              if([title containsString:@"会员类型"]){
                  membersTypeView = textField;
              }
              [[textField.textField rac_textSignal] subscribeNext:^(NSString * _Nullable x) {
                  @strongify(self);
                  self.dataDictionary[keys[i]] = x;
              }];
                                    
              
          }
    }
    
    ///!!!:商户类型
    merchantTypeView.textField.userInteractionEnabled = NO;
    UIButton *merchantTypeBtn = [UIButton lz_buttonTitle:@"请选择绑定商户" titleColor:COLOR_placeholderColor fontSize:17];
    [merchantTypeBtn setTitleColor:COLOR_textColor forState:UIControlStateSelected];

    [merchantTypeBtn setImage:k_imageName(@"pm_next") forState:UIControlStateNormal];
    [merchantTypeView addSubview:merchantTypeBtn];
    [merchantTypeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.top.mas_equalTo(merchantTypeView.textField);
    }];
    merchantTypeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [merchantTypeBtn SG_imagePositionStyle:SGImagePositionStyleRight spacing:5];
    merchantTypeBtn.tag = 100;
    [merchantTypeBtn addTarget:self action:@selector(selectClick:) forControlEvents:UIControlEventTouchUpInside];

    
    ///!!!:开户银行
    self.dataDictionary[@"type"] = @"MEMBER";

    membersTypeView.textField.userInteractionEnabled = NO;
    UIButton *membersTypeBtn = [UIButton lz_buttonTitle:@"代理" titleColor:COLOR_textColor fontSize:17];
    [membersTypeBtn setImage:k_imageName(@"pm_next") forState:UIControlStateNormal];
    [membersTypeView addSubview:membersTypeBtn];
    [membersTypeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
       make.left.right.bottom.top.mas_equalTo(membersTypeView.textField);
    }];
    membersTypeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [membersTypeBtn SG_imagePositionStyle:SGImagePositionStyleRight spacing:5];
    membersTypeBtn.tag = 101;
    [membersTypeBtn addTarget:self action:@selector(selectClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [contentView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(lastMerchantView.mas_bottom).mas_offset(34);
    }];
  
    
    ///!!!: 完成
    UIButton *sureBtn = [UIButton lz_buttonTitle:@"确定" titleColor:COLOR_appColor fontSize:15];
    sureBtn.frame = CGRectMake(0, 0, 44, 44);
    [self initBarItem:sureBtn withType:1];
    [[sureBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);
        NSArray *placeholders = @[@"",@"请输入收款人名称",@"请输入收款人联系电话",@"请输入商户联系电话",@"请输入登录密码",@""];
        NSArray *keys= @[@"",@"nickname",@"mobile",@"settle_rate",@"loginpwd",@""];
        for (int i = 0; i < keys.count; i++) {
            NSString *key = keys[i];
            if(i != 0 && i != 5){
                if([self.dataDictionary[key] length] == 0){
                    showDelayedDismissTitle(placeholders[i], nil);
                    return;
                }
            }
        }
        
        
        [self.viewModel createMember:self.dataDictionary];
        
    }];
    
    
}


- (void)selectClick:(UIButton *)sender{
    
    

    @weakify(self);
    switch (sender.tag) {
        case 100:
        {
            sender.selected = NO;
            [sender setTitle:@"请选择绑定商户" forState:UIControlStateNormal];
            [sender SG_imagePositionStyle:SGImagePositionStyleRight spacing:5];

            NSArray *subTitles = self.viewModel.dataArray;
            [LZToolView showAlertType:LZAlertTypeActionSheet withTitle:@"" message:@"" cancel:@"取消" sure:@"" objectDict:@{@"subTitles":subTitles} cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
                @strongify(self);
                sender.selected = YES;
                NSInteger index = [object intValue];
                [sender setTitle:subTitles[index] forState:UIControlStateNormal];
                [sender SG_imagePositionStyle:SGImagePositionStyleRight spacing:5];
                self.dataDictionary[@"bind_merchant_id"] = self.viewModel.merchantIds[index];
                
                
            }];
        }
            break;
        case 101:
        {
            NSArray *subTitles = @[@"代理",@"会员"];
            [LZToolView showAlertType:LZAlertTypeActionSheet withTitle:@"" message:@"" cancel:@"取消" sure:@"" objectDict:@{@"subTitles":subTitles} cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
                @strongify(self);
                NSInteger index = [object intValue];
                self.dataDictionary[@"type"] = (index == 0)?@"MEMBER":@"AGENT";
            }];
        }
            break;
            
        default:
            break;
    }
    
    
}

- (PMPersonManagerViewModel *)viewModel{
    if(!_viewModel){
        _viewModel = [[PMPersonManagerViewModel alloc] init];
        _viewModel.reloadSubject = self.subject;
    }
    return _viewModel;;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
