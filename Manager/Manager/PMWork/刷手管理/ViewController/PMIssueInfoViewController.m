//
//  PMIssueInfoViewController.m
//  Manager
//
//  Created by lvzhao on 2020/8/2.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMIssueInfoViewController.h"
#import "PMPersonManagerViewModel.h"
#import "PMChannelTextField.h"


@interface PMIssueInfoViewController ()

@property (nonatomic,strong) PMPersonManagerViewModel *viewModel;

@end

@implementation PMIssueInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"归集下发";
    self.view.backgroundColor = UIColor.whiteColor;
    [self setupUI];
}

- (void) setupUI{
    
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [self.view addSubview:scrollView];
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.right.bottom.mas_equalTo(0);
    }];

    //内容
    UIView *contentView = [[UIView alloc] init];
    [scrollView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.and.right.equalTo(scrollView).with.insets(UIEdgeInsetsZero);
        make.width.equalTo(scrollView);

    }];
    
    ///!!!:金额
    PMChannelTextField *moneyTextField = [[PMChannelTextField alloc] init];
    moneyTextField.title = @"归集金额";
    moneyTextField.placeholder = @"请选择绑定商户";
    [contentView addSubview:moneyTextField];
    [moneyTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(50);
        make.top.mas_equalTo(5);
    }];
    

    
    UILabel *amountNumLab = [UILabel lz_labelWithText:@"可归集金额：￥0.13" fontSize:PingFangSC(13) color:COLOR_placeholderColor];
    [contentView addSubview:amountNumLab];
    [amountNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(moneyTextField.mas_bottom).mas_offset(3);
        make.width.mas_equalTo(150);
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(15);
    }];
    
    
    UIButton *allBtn = [UIButton lz_buttonTitle:@"全部归集" titleColor:UIColorHex(0xD5A775) fontSize:13];
    [contentView addSubview:allBtn];
    allBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [allBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(amountNumLab.mas_centerY);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(-15);
    }];
    
    
    NSMutableArray *textFieldViews = [[NSMutableArray alloc] init];
    UIView *lastTextFieldView = nil;
    
    PMChannelTextField *depositBankView = nil;
    {
        NSArray *titles = @[@"开户银行",@"支行名称",@"持卡人姓名",@"银行卡号"];
        NSArray *placeholders = @[@"",@"请输入银行卡所属支行名称",@"请输入银行卡开户人姓名",@"请输入银行卡号码"];

          
        for (int i = 0; i < titles.count; i++){
            PMChannelTextField *textField = [[PMChannelTextField alloc] init];
            textField.title = titles[i];
            textField.placeholder = placeholders[i];
            [contentView addSubview:textField];
            [textField mas_makeConstraints:^(MASConstraintMaker *make) {
                  make.left.mas_equalTo(15);
                  make.right.mas_equalTo(-15);
                  make.height.mas_equalTo(50);
                  if(lastTextFieldView){
                      make.top.mas_equalTo(lastTextFieldView.mas_bottom).mas_offset(10);
                  } else {
                      make.top.mas_equalTo(85);
                  }
            }];
            lastTextFieldView = textField;
            [textFieldViews addObject:lastTextFieldView];
            NSString *title = titles[i];
           
            if([title containsString:@"开户银行"]){
                depositBankView = textField;
            }
        }
    }
    
    ///!!!:开户银行
    depositBankView.textField.userInteractionEnabled = NO;
    UIButton *depositBankBtn = [UIButton lz_buttonTitle:@"" titleColor:COLOR_placeholderColor fontSize:17];
    [depositBankBtn setImage:k_imageName(@"pm_next") forState:UIControlStateNormal];
    [depositBankView addSubview:depositBankBtn];
    [depositBankBtn mas_makeConstraints:^(MASConstraintMaker *make) {
       make.left.right.bottom.top.mas_equalTo(depositBankView.textField);
    }];
    depositBankBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [depositBankBtn SG_imagePositionStyle:SGImagePositionStyleRight spacing:5];
    depositBankBtn.tag = 100;
    [depositBankBtn addTarget:self action:@selector(selectClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [contentView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(lastTextFieldView.mas_bottom).mas_offset(34);
    }];
  
    
    
    
    
    ///!!!: 完成
    UIButton *sureBtn = [UIButton lz_buttonTitle:@"确定" titleColor:COLOR_appColor fontSize:15];
    sureBtn.frame = CGRectMake(0, 0, 44, 44);
    [self initBarItem:sureBtn withType:1];
    @weakify(self);
    [[sureBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);
        
        
    }];
    
    
}


- (void)selectClick:(UIButton *)sender{
    
    switch (sender.tag) {
        case 100:
        {
            NSArray *subTitles = @[@"普通商户",@"VIP商户",@"VVIP商户",@"SVIP商户"];
            [LZToolView showAlertType:LZAlertTypeActionSheet withTitle:@"" message:@"" cancel:@"取消" sure:@"" objectDict:@{@"subTitles":subTitles} cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
                NSInteger index = [object intValue];
                [sender setTitle:subTitles[index] forState:UIControlStateNormal];
                [sender SG_imagePositionStyle:SGImagePositionStyleRight spacing:5];

            }];
        }
            break;
        case 101:
        {
            
        }
            break;
            
        default:
            break;
    }
    
    
}

- (PMPersonManagerViewModel *)viewModel{
    if(!_viewModel){
        _viewModel = [[PMPersonManagerViewModel alloc] init];
    }
    return _viewModel;;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
