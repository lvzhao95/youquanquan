//
//  PMPersonListModel.m
//  Manager
//
//  Created by lvzhao on 2020/8/6.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPersonListModel.h"

@implementation PMPersonListModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"modelid" : @"id"};
}

- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic{
        
    NSString *sum_divided = [NSString stringWithFormat:@"%@",dic[@"sum_divided"]?:@"0"];
    self.sum_divided = [NSString stringWithFormat:@"￥ %@",[sum_divided yw_stringByDividingBy:@"100"]];

    NSString *sum_amount = [NSString stringWithFormat:@"%@",dic[@"sum_amount"]?:@"0"];
    self.sum_amount = [NSString stringWithFormat:@"￥ %@",[sum_amount yw_stringByDividingBy:@"100"]];
    
    NSString *deposit = [NSString stringWithFormat:@"%@",dic[@"deposit"]?:@"0"];
    self.deposit = [NSString stringWithFormat:@"￥ %@",[deposit yw_stringByDividingBy:@"100"]];
    
    NSString *receipts = [NSString stringWithFormat:@"%@",dic[@"receipts"]?:@"0"];
    self.receipts = [NSString stringWithFormat:@"￥ %@",[receipts yw_stringByDividingBy:@"100"]];

    NSString *settle_rate = [[NSString stringWithFormat:@"%@",dic[@"settle_rate"]?:@"0"] yw_stringByMultiplyingBy:@"100"];
    self.settle_rate = [settle_rate stringByAppendingString:@"%"];
    
    
    
    
    NSString *status = dic[@"status"];
    if([status isEqualToString:@"USED"]){
        self.status = @"正常";
    } else if([status isEqualToString:@"CLOSE"]){
        self.status = @"禁用";
    } else if([status isEqualToString:@"LOCKED"]){
        self.status = @"锁定";
    } else if([status isEqualToString:@"NOTACTIVE"]){
        self.status = @"未激活";
    }
    
    
     NSString *type = dic[@"type"];
    if([type isEqualToString:@"MEMBER"]){
       self.type = @"会员";
    } else if([type isEqualToString:@"AGENT"]){
       self.type = @"代理";
    }
    
    NSString *is_online = [NSString stringWithFormat:@"%@",dic[@"is_online"]?:@"0"];
    if([is_online isEqualToString:@"1"]){
        self.is_online = @"在线";
    } else {
        self.is_online = @"下线";
    }
    
    NSString *is_service = [NSString stringWithFormat:@"%@",dic[@"is_service"]?:@"0"];
      if([is_service isEqualToString:@"1"]){
          self.is_service = @"打开";
      } else {
          self.is_service = @"关闭";
      }
    return YES;
}
@end
