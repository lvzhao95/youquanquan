//
//  PMIssueModel.h
//  Manager
//
//  Created by lvzhao on 2020/8/6.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PMIssueModel : NSObject
@property (nonatomic,strong) NSString *real_money;

@property (nonatomic,strong) NSString *apply_money;
@property (nonatomic,strong) NSString *nickname;
@property (nonatomic,strong) NSString *status;
@property (nonatomic,strong) NSString *sum_amount;
@property (nonatomic,strong) NSString *account_name;
@property (nonatomic,strong) NSString *settle_rate;
@property (nonatomic,strong) NSString *open_bank;
@property (nonatomic,strong) NSString *realname;
@property (nonatomic,strong) NSString *create_date;

@end

NS_ASSUME_NONNULL_END
