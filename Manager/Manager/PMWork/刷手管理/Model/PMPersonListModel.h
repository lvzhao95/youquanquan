//
//  PMPersonListModel.h
//  Manager
//
//  Created by lvzhao on 2020/8/6.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PMPersonListModel : NSObject

@property (nonatomic , copy) NSString              * status;
@property (nonatomic , copy) NSString*              settle_rate;
@property (nonatomic , copy) NSString*              amount;
@property (nonatomic , copy) NSString*              sum_real_amount;
@property (nonatomic , copy) NSString*              sum_amount;
@property (nonatomic , copy) NSString              * nickname;
@property (nonatomic , copy) NSString*             is_online;
@property (nonatomic , copy) NSString              * agreement;
@property (nonatomic , copy) NSString*              score;
@property (nonatomic , copy) NSString              * create_date;
@property (nonatomic , copy) NSString              * signature;
@property (nonatomic , copy) NSString              * sys_user_id;
@property (nonatomic , copy) NSString              * realname;
@property (nonatomic , copy) NSString              * type;
@property (nonatomic , copy) NSString*              receipts;
@property (nonatomic , copy) NSString*              deposit;
@property (nonatomic , copy) NSString              * modelid;
@property (nonatomic , copy) NSString              * promo_code;
@property (nonatomic , copy) NSString              * member_level_id;
@property (nonatomic , copy) NSString*              sum_amount_show;
@property (nonatomic , copy) NSString              * mobile;
@property (nonatomic , copy) NSString*              sum_divided;
@property (nonatomic , copy) NSString*              member_no;
@property (nonatomic , copy) NSString*              merchant_no;
@property (nonatomic , copy) NSString*              is_service;


@end

NS_ASSUME_NONNULL_END
