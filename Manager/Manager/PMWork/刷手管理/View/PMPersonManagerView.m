//
//  PMPersonManagerView.m
//  Manager
//
//  Created by lvzhao on 2020/8/1.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPersonManagerView.h"
#import "CKSlideMenu.h"
#import "LZBaseViewController.h"
#import "PMPersonManagerViewModel.h"
#import "PMPersonListViewController.h"
#import "PMIssueViewController.h"
#import "PMScreenDownView.h"


@interface PMPersonManagerView()<CKSlideMenuDelegate>

@property (nonatomic,strong) PMPersonManagerViewModel *viewModel;

@property (nonatomic,strong) NSMutableArray *viewControllers;

@end

@implementation PMPersonManagerView

- (instancetype)initViewModel:(LZBaseViewModel *)viewModel
{
    self = [super initViewModel:viewModel];
    if (self) {
        self.viewModel = (PMPersonManagerViewModel *)viewModel;
        
        [self setupView];
    }
    return self;
}

#pragma mark 创建UI
- (void) setupView{
    
    NSArray *titles = @[@"刷手列表",@"归集下发"];
    self.viewControllers = [NSMutableArray array];
    
    PMPersonListViewController *personListVC = [[PMPersonListViewController alloc] init];
    personListVC.viewModel = self.viewModel;
    [self.viewControllers addObject:personListVC];
    

    PMIssueViewController *issueVC = [[PMIssueViewController alloc] init];
    [self.viewControllers addObject:issueVC];

    // 创建Menu
    CKSlideMenu *slidMenu = [[CKSlideMenu alloc] initWithFrame:CGRectMake(0, 0, K_SCREENWIDTH, 45) titles:titles controllers:self.viewControllers];
    slidMenu.backgroundColor = COLOR_nav;
    slidMenu.indicatorStyle = SlideMenuIndicatorStyleFollowText;
    slidMenu.isFixed = YES;
    slidMenu.lazyLoad = YES;
    slidMenu.selectedColor = COLOR_appColor;
    slidMenu.unselectedColor = COLOR_subTextColor;
    slidMenu.indicatorHeight = 2;
    slidMenu.indicatorColor =  COLOR_appColor;
    slidMenu.unFont = PingFangSC_M(17);
    slidMenu.font = PingFangSC_M(17);
    slidMenu.delegate = self;
    [self addSubview:slidMenu];
    slidMenu.bodyScrollView.scrollEnabled = NO;
    slidMenu.bodyFrame = CGRectMake(0,45, K_SCREENWIDTH, K_SCREENHEIGHT -  K_NAVHEIGHT - 45);
}


#pragma mark - CKSlideMenuDelegate

- (void)clickBtnActionIndx:(NSInteger)index{
    
    [PMScreenDownView dismiss];
    
    LZBaseViewController *baseVC  = self.viewControllers[index];
    SEL sel = NSSelectorFromString(@"reloadRightItem");
    if ([baseVC respondsToSelector:sel]) {
        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            [baseVC performSelector:sel withObject:nil];
        #pragma clang diagnostic pop
    }
    
}


@end
