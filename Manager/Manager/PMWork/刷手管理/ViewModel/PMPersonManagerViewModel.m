//
//  PMPersonManagerViewModel.m
//  Manager
//
//  Created by lvzhao on 2020/8/1.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPersonManagerViewModel.h"

@interface  PMPersonManagerViewModel()

@property (nonatomic,assign) NSInteger pageIndex;

@end

@implementation PMPersonManagerViewModel

- (void)setIndex:(NSInteger)index{
    _index = index;
    switch (index) {
        case 0:
            [self createPersonTableData];
            break;
        case 1:
            [self createIssureTableData];
            break;
        default:
            break;
    }
}

//创建表格
- (void)createPersonTableData{
    
    [self dataModel];
    /// top数据
    self.dataModel.topLeftDataModel = [[LZTableRightSubDataModel alloc] init];
    self.dataModel.topLeftDataModel.titleString = @"收款人名称";
    self.dataModel.topLeftDataModel.param = @"nickname";
    self.dataModel.topLeftDataModel.sortStatus = 0;
    
    NSArray *titles = @[@"收款人电话",@"刷手编号",@"刷手分润",@"团队收款",@"保证金",@"剩余收款额度",@"收款费率",@"状态",@"类型",@"绑定商户",@"在线状态",@"服务状态"];
    NSArray *itemWidthArray = @[@(130),@(110),@(80),@(80),@(80),@(150),@(80),@(80),@(80),@(80),@(80),@(80)];
    NSArray *params = @[@"",@"",@"",@"",@"",@"",@"",@"status",@"type",@"",@"",@""];


    NSMutableArray *topSubDataArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < titles.count; i++) {
        LZTableRightSubDataModel *subDataModel = [[LZTableRightSubDataModel alloc]init];
        subDataModel.titleString = titles[i];
        subDataModel.itemHeight = self.dataModel.rowHeight;
        subDataModel.param = params[i];
        if([titles[i] isEqualToString:@"状态"]||
           [titles[i] isEqualToString:@"类型"]){
            subDataModel.sortStatus = 0;
        } else {
            subDataModel.sortStatus = -1;
        }
        [topSubDataArray addObject:subDataModel];
    }
    self.dataModel.leftWidth = 100;
    [self.dataModel.topDataModel.classNames addObjectsFromArray:@[@"PMCustomSortCell"]];
    self.dataModel.topDataModel.setDataMethodName = @"setModelObject:";
    self.dataModel.topDataModel.itemModelArray = topSubDataArray;
    self.dataModel.topDataModel.columnWidthArray = itemWidthArray;
    self.dataModel.totalItemWidth = [[itemWidthArray valueForKeyPath:@"@sum.floatValue"] integerValue];

}


//创建表格
- (void)createIssureTableData{
    
    [self dataModel];
    
    /// top数据
    self.dataModel.topLeftDataModel = [[LZTableRightSubDataModel alloc] init];
    self.dataModel.topLeftDataModel.titleString = @"下发金额(元)";
    self.dataModel.topLeftDataModel.param = @"order_id";
    self.dataModel.topLeftDataModel.sortStatus = 0;

    
    NSArray *titles = @[@"实际下发(元)",@"刷手",@"申请状态",@"账号信息",@"付款凭证",@"申请日期"];
    NSArray *itemWidthArray = @[@(130),@(80),@(100),@(100),@(80),@(140)];
    NSArray *params = @[@"order_no",@"transcation_no",@"channel_name",@"amount",@"amount_show",@"amount_settle",@"paid",@"notify_status",@"created",@"",@""];

    NSMutableArray *topSubDataArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < titles.count; i++) {
        LZTableRightSubDataModel *subDataModel = [[LZTableRightSubDataModel alloc]init];
        subDataModel.titleString = titles[i];
        subDataModel.itemHeight = self.dataModel.rowHeight;
        subDataModel.param = params[i];
        if([titles[i] isEqualToString:@"账号信息"]||
           [titles[i] isEqualToString:@"付款凭证"]){
            subDataModel.sortStatus = -1;
        }
        
        [topSubDataArray addObject:subDataModel];
    }
    self.dataModel.leftWidth = 130;
    [self.dataModel.topDataModel.classNames addObjectsFromArray:@[@"PMCustomSortCell"]];
    self.dataModel.topDataModel.setDataMethodName = @"setModelObject:";
    self.dataModel.topDataModel.itemModelArray = topSubDataArray;
    self.dataModel.topDataModel.columnWidthArray = itemWidthArray;
    self.dataModel.totalItemWidth = [[itemWidthArray valueForKeyPath:@"@sum.floatValue"] integerValue];

}
/** 刷手列表*/
- (void)getPersonList:(BOOL)isFrist{

    if(isFrist){
        self.pageIndex = 1;
        self.dataModel.leftDataArray = @[];
        self.dataModel.dataArray = @[];
        [self.dataArray removeAllObjects];
    }

    NSString *startTime = [NSString stringWithFormat:@"%@ 00:00",[NSDate datestrFromDate:[NSDate date] withDateFormat:@"yyyy-MM-dd"]];
    NSString *endTime = [NSString stringWithFormat:@"%@ 23:59",[NSDate datestrFromDate:[NSDate date] withDateFormat:@"yyyy-MM-dd"]];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{@"page":@(self.pageIndex),
                                                                                    @"size":kPageSize,
                                                                                    @"startTime":startTime,
                                                                                    @"endTime":endTime}];
    [params addEntriesFromDictionary:self.paramsDictionary];
    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kGetMemberPage params:params success:^(id  _Nullable responseObject) {
        @strongify(self);
        NSDictionary *pageDict = responseObject[@"page"];
        NSArray *dataArray = pageDict[@"list"];
        NSMutableArray * leftTableDataArray = [[NSMutableArray alloc]init];
        NSMutableArray * rowDataArray = [[NSMutableArray alloc]init];
        
        for (int i = 0; i < dataArray.count; i++){
            PMPersonListModel *dealModel = [PMPersonListModel modelWithJSON:dataArray[i]];
            [self.dataArray addObject:dealModel];
            // 左侧列表数据
            LZTableLeftDataModel * model = [[LZTableLeftDataModel alloc]init];
            model.titleString = dealModel.nickname;
            [leftTableDataArray addObject:model];

            //右侧列表数据
            LZTableRightDataModel *rightDataModel = [[LZTableRightDataModel alloc]init];
            rightDataModel.itemHeight = self.dataModel.rowHeight;

            ///右侧数据源
            NSMutableArray * rightItemArray = [[NSMutableArray alloc]init];

            NSArray *detail = @[dealModel.mobile?:@"",
                                dealModel.member_no?:@"",
                                dealModel.sum_divided?:@"",
                                dealModel.sum_amount?:@"",
                                dealModel.deposit?:@"",
                                dealModel.receipts?:@"",
                                dealModel.settle_rate?:@"",
                                dealModel.status?:@"",
                                dealModel.type ? :@"",
                                dealModel.merchant_no?:@"",
                                dealModel.is_online?:@"",
                                dealModel.is_service?:@"",];
            for ( int j = 0; j < detail.count; j++) {
                LZTableRightSubDataModel *itemModel = [[LZTableRightSubDataModel alloc]init];
                itemModel.titleString = detail[j];
                //特殊处理
                if([itemModel.titleString isEqualToString:@"正常"]
                   ||[itemModel.titleString isEqualToString:@"代理"]
                   ||[itemModel.titleString isEqualToString:@"下线"]
                   ||[itemModel.titleString isEqualToString:@"关闭"]){
                    
                    itemModel.textColor = UIColor.redColor;
                    
                } else if([itemModel.titleString isEqualToString:@"禁用"]
                          ||[itemModel.titleString isEqualToString:@"锁定"]
                          ||[itemModel.titleString isEqualToString:@"未激活"]
                          ||[itemModel.titleString isEqualToString:@"会员"]
                          ||[itemModel.titleString isEqualToString:@"在线"]
                          ||[itemModel.titleString isEqualToString:@"打开"]){
                    
                    itemModel.textColor = UIColorHex(0x0091FF);

                }
                itemModel.itemHeight = self.dataModel.rowHeight;
                [rightItemArray addObject:itemModel];
            }
            rightDataModel.columnWidthArray = self.dataModel.topDataModel.columnWidthArray;
            rightDataModel.itemModelArray = rightItemArray;
            [rowDataArray addObject:rightDataModel];

            rightDataModel.setDataMethodName = @"setModelObject:";
            [rightDataModel.classNames addObjectsFromArray:@[@"PMCustomPayCell"]];
        }

        NSMutableArray *mutableLeftDataArray = [[NSMutableArray alloc] initWithArray:self.dataModel.leftDataArray];
        NSMutableArray *mutableRowDataArray = [[NSMutableArray alloc] initWithArray:self.dataModel.dataArray];

        [mutableLeftDataArray addObjectsFromArray:leftTableDataArray];
        [mutableRowDataArray addObjectsFromArray:rowDataArray];

        self.dataModel.leftDataArray = mutableLeftDataArray;
        self.dataModel.dataArray = mutableRowDataArray;
        ///回调
        if(dataArray.count < [kPageSize intValue]){
            [self.reloadSubject sendNext:@(0)];

        } else {
            self.pageIndex ++;
            [self.reloadSubject sendNext:@(1)];
        }
    } failure:^(NSError * _Nullable error) {
            @strongify(self);
          [self.reloadSubject sendNext:@(1)];
    }];
}


///归集下发
- (void)getMemberCollectionPage:(BOOL)isFrist{
    
    if(isFrist){
        self.pageIndex = 1;
        self.dataModel.leftDataArray = @[];
        self.dataModel.dataArray = @[];
        [self.dataArray removeAllObjects];
    }

    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{@"page":@(self.pageIndex),
                                                                                    @"size":kPageSize}];
    [params addEntriesFromDictionary:self.paramsDictionary];
    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kGetMemberCollectionPage params:params success:^(id  _Nullable responseObject) {
        @strongify(self);
        NSDictionary *pageDict = responseObject[@"page"];
        NSArray *dataArray = pageDict[@"list"];
        NSMutableArray * leftTableDataArray = [[NSMutableArray alloc]init];
        NSMutableArray * rowDataArray = [[NSMutableArray alloc]init];
        
        for (int i = 0; i < dataArray.count; i++){
            PMIssueModel *dealModel = [PMIssueModel modelWithJSON:dataArray[i]];
            [self.dataArray addObject:dealModel];
            // 左侧列表数据
            LZTableLeftDataModel * model = [[LZTableLeftDataModel alloc]init];
            model.titleString = dealModel.nickname;
            [leftTableDataArray addObject:model];

            //右侧列表数据
            LZTableRightDataModel *rightDataModel = [[LZTableRightDataModel alloc]init];
            rightDataModel.itemHeight = self.dataModel.rowHeight;

            ///右侧数据源
            NSMutableArray * rightItemArray = [[NSMutableArray alloc]init];

            NSArray *detail = @[dealModel.apply_money?:@"",
                                dealModel.real_money?:@"",
                                dealModel.nickname?:@"",
                                dealModel.open_bank?:@"",
                                dealModel.open_bank?:@"",
                                dealModel.create_date?:@""];
            for ( int j = 0; j < detail.count; j++) {
                LZTableRightSubDataModel *itemModel = [[LZTableRightSubDataModel alloc]init];
                itemModel.titleString = detail[j];
                //特殊处理
                if([itemModel.titleString isEqualToString:@"APPLY"]
                   ||[itemModel.titleString isEqualToString:@"成功"]){
                        itemModel.textColor = UIColorHex(0x0091FF);

                } else if ([itemModel.titleString isEqualToString:@"ACCECPT"]
                           ||[itemModel.titleString isEqualToString:@"失败"]){
                    itemModel.textColor = UIColorHex(0xFA6400);
                }
                itemModel.itemHeight = self.dataModel.rowHeight;
                [rightItemArray addObject:itemModel];
            }
            rightDataModel.columnWidthArray = self.dataModel.topDataModel.columnWidthArray;
            rightDataModel.itemModelArray = rightItemArray;
            [rowDataArray addObject:rightDataModel];

            rightDataModel.setDataMethodName = @"setModelObject:";
            [rightDataModel.classNames addObjectsFromArray:@[@"PMCustomPayCell"]];
        }

        NSMutableArray *mutableLeftDataArray = [[NSMutableArray alloc] initWithArray:self.dataModel.leftDataArray];
        NSMutableArray *mutableRowDataArray = [[NSMutableArray alloc] initWithArray:self.dataModel.dataArray];

        [mutableLeftDataArray addObjectsFromArray:leftTableDataArray];
        [mutableRowDataArray addObjectsFromArray:rowDataArray];

        self.dataModel.leftDataArray = mutableLeftDataArray;
        self.dataModel.dataArray = mutableRowDataArray;
        ///回调
        if(dataArray.count < [kPageSize intValue]){
            [self.reloadSubject sendNext:@(0)];

        } else {
            self.pageIndex ++;
            [self.reloadSubject sendNext:@(1)];
        }
    } failure:^(NSError * _Nullable error) {
            @strongify(self);
          [self.reloadSubject sendNext:@(1)];
    }];
    
    
    
}

///请求商户列表
- (void)getMerchantList{
    
    NSDictionary *params = @{@"size":@"999",
                             @"page":@"1",
                             @"status":@"NORMAL"};
    
    self.merchantIds = [[NSMutableArray alloc] init];
    [LZNetworkingManager lz_request:@"post" url:kGetMerchantList params:params success:^(id  _Nullable responseObject) {
            NSArray *dataArray = responseObject[@"list"];
        for (int i = 0; i < dataArray.count; i++){
            NSDictionary *dictionary = dataArray[i];
            [self.dataArray addObject:dictionary[@"company_name"]];
            [self.merchantIds  addObject:dictionary[@"id"]];
        }
    } failure:^(NSError * _Nullable error) {
        
    }];
    
}


///开户
- (void)createMember:(NSDictionary *)dictionary{
    
    [LZNetworkingManager lz_request:@"post" url:kCreateMember params:dictionary success:^(id  _Nullable responseObject) {
        if([responseObject[kResultStatus] isEqualToString:kCode]){
            showDelayedDismissTitle(@"成功", nil);
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.reloadSubject sendNext:@(1)];
                [LZTool.currentViewController.navigationController popViewControllerAnimated:YES];
            });
        }
    } failure:^(NSError * _Nullable error) {} isLoading:YES isFailTip:YES];
    
}


- (LZTableDataModel *)dataModel{
    if(!_dataModel){
        _dataModel = [[LZTableDataModel alloc] init];
        _dataModel.leftDataArray = @[];
        _dataModel.dataArray = @[];
    }
    return _dataModel;
}

- (NSMutableDictionary *)paramsDictionary{
    if(!_paramsDictionary){
        _paramsDictionary = [[NSMutableDictionary alloc] init];
    }
    return _paramsDictionary;
}

@end
