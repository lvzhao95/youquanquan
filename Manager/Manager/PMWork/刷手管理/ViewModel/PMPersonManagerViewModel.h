//
//  PMPersonManagerViewModel.h
//  Manager
//
//  Created by lvzhao on 2020/8/1.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZBaseViewModel.h"
#import "LZTableDataModel.h"
#import "PMPersonListModel.h"
#import "PMIssueModel.h"
#import "LZOptionModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PMPersonManagerViewModel : LZBaseViewModel

@property (nonatomic,assign) NSInteger index;

//表格数据源
@property (nonatomic,strong) LZTableDataModel *dataModel;

//
@property (nonatomic,strong) NSMutableDictionary *paramsDictionary;

//
@property (nonatomic,strong) NSString *modelid;

///商家IDs
@property (nonatomic,strong) NSMutableArray *merchantIds;

///请求商户列表
- (void)getMerchantList;

///开户
- (void)createMember:(NSDictionary *)dictionary;

///刷手列表
- (void)getPersonList:(BOOL)isFrist;

///归集下发
- (void)getMemberCollectionPage:(BOOL)isFrist;
@end

NS_ASSUME_NONNULL_END
