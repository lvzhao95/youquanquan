//
//  PMPDDParamViewController.m
//  Manager
//
//  Created by lvzhao on 2020/8/1.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPDDParamViewController.h"
#import "PMPDDParamView.h"
#import "PMPDDParamViewModel.h"


@interface PMPDDParamViewController ()

@property (nonatomic,strong) PMPDDParamView *paramView;

@property (nonatomic,strong) PMPDDParamViewModel *viewModel;

@end

@implementation PMPDDParamViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupUI];
}

- (void)setupUI{
    [self.view addSubview:self.paramView];
    [self.paramView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    
    UIButton *saveBtn = [UIButton lz_buttonTitle:@"确认" titleColor:COLOR_appColor fontSize:15];
    saveBtn.frame = CGRectMake(0, 0, 44, 44);
    [self initBarItem:saveBtn withType:1];
    [saveBtn addTarget:self action:@selector(saveClick:) forControlEvents:UIControlEventTouchUpInside];
    
}


//保存
- (void)saveClick:(UIButton *)sender{
    
    

    
}

#pragma mark -懒加载
- (PMPDDParamView *)paramView{
    if(!_paramView){
        _paramView = [[PMPDDParamView alloc] initViewModel:self.viewModel];
    }
    return _paramView;
}

- (PMPDDParamViewModel *)viewModel{
    if(!_viewModel) {
        _viewModel = [[PMPDDParamViewModel alloc] init];
    }
    return _viewModel;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
