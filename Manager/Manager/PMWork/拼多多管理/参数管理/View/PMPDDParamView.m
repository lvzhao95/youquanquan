//
//  PMPDDParamView.m
//  Manager
//
//  Created by lvzhao on 2020/8/2.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPDDParamView.h"
#import "PMPDDParamViewModel.h"
#import "PMPDDParamCourierView.h"
#import "PMPDDParamPlatformView.h"
#import "PMPDDParamLogisticsView.h"
#import "PMPDDParamAccountView.h"

@interface PMPDDParamView ()

@property (nonatomic,strong) PMPDDParamViewModel *viewModel;

@end

@implementation PMPDDParamView


- (instancetype)initViewModel:(LZBaseViewModel *)viewModel
{
    self = [super init];
    if (self) {
        
        self.backgroundColor = COLOR_cellbackground;
        self.viewModel = (PMPDDParamViewModel *)viewModel;
        [self setupView];
    }
    return self;
}

- (void) setupView{
    
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [self addSubview:scrollView];
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.right.bottom.mas_equalTo(0);
    }];

    //内容
    UIView *contentView = [[UIView alloc] init];
    contentView.backgroundColor = COLOR_cellbackground;
    [scrollView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.and.right.equalTo(scrollView).with.insets(UIEdgeInsetsZero);
        make.width.equalTo(scrollView);

    }];
    
    
    ///!!!:快递
    PMPDDParamCourierView *courierView = [[PMPDDParamCourierView alloc] initViewModel:self.viewModel];
    [contentView addSubview:courierView];
    [courierView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(0);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(165);
    }];
    
    
    ///!!!:平台
    PMPDDParamPlatformView *platformView = [[PMPDDParamPlatformView alloc] initViewModel:self.viewModel];
    [contentView addSubview:platformView];
    [platformView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(courierView.mas_bottom);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(240);
    }];
    
    
    
    ///!!!:物流
    PMPDDParamLogisticsView *logisticsView = [[PMPDDParamLogisticsView alloc] initViewModel:self.viewModel];
    [contentView addSubview:logisticsView];
    [logisticsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(platformView.mas_bottom);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(550);
    }];
    
    
    ///!!!:账号
      PMPDDParamAccountView *accountView = [[PMPDDParamAccountView alloc] initViewModel:self.viewModel];
      [contentView addSubview:accountView];
      [accountView mas_makeConstraints:^(MASConstraintMaker *make) {
          make.left.mas_equalTo(0);
          make.top.mas_equalTo(logisticsView.mas_bottom);
          make.width.mas_equalTo(kScreenWidth);
          make.height.mas_equalTo(290);
      }];
    
    
    [contentView mas_updateConstraints:^(MASConstraintMaker *make) {
          make.bottom.mas_equalTo(accountView.mas_bottom).mas_offset(34);
      }];
   
    
    
    
}
  
    

@end
