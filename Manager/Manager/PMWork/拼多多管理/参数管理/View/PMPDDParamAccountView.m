//
//  PMPDDParamAccountView.m
//  Manager
//
//  Created by lvzhao on 2020/8/2.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPDDParamAccountView.h"

#import "PMPDDParamViewModel.h"
#import "PMChannelTextField.h"

@interface PMPDDParamAccountView()

@property (nonatomic,strong) PMPDDParamViewModel *viewModel;

@end

@implementation PMPDDParamAccountView

- (instancetype)initViewModel:(LZBaseViewModel *)viewModel
{
    self = [super initViewModel:viewModel];
    if (self) {
        
        self.viewModel = (PMPDDParamViewModel *)viewModel;
        self.backgroundColor = UIColor.whiteColor;
        [self setupView];
    }
    return self;
}

- (void)setupView{
    
    ///!!!:title
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = COLOR_appColor;
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(20);
        make.top.mas_equalTo(0);
    }];
       
       
    UILabel *titleLab= [UILabel lz_labelWithText:@"账号配置" fontSize:PingFangSC(15) color:COLOR_textColor];
    [self addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(lineView.mas_right).mas_offset(10);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(20);
        make.top.mas_equalTo(0);
    }];
    
    NSMutableArray *textFields = [[NSMutableArray alloc] init];
      
      {
          NSArray *titles = @[@"购买次数限制",@"初始购买额度",@"销售次数限制",@"初始销售额度"];
          NSArray *units = @[@"次",@"分",@"次",@"分"];

           for (int i = 0; i < titles.count; i++){
               PMChannelTextField *textField = [[PMChannelTextField alloc] init];
               textField.title = titles[i];
               textField.textField.font = PingFangSC(17);
               textField.placeholder = @"限制次数";
               [self addSubview:textField];
               [textField mas_makeConstraints:^(MASConstraintMaker *make) {
                   make.left.mas_equalTo(15);
                   make.right.mas_equalTo(-15);
                   make.height.mas_equalTo(50);
                   make.top.mas_equalTo(35 + i * (60));
               }];

               [textField.titleLab mas_updateConstraints:^(MASConstraintMaker *make) {
                   make.width.mas_equalTo(110);
               }];

               [textField.textField mas_updateConstraints:^(MASConstraintMaker *make) {
                   make.right.mas_equalTo(-50);
               }];
               
               UILabel *rightLab = [UILabel lz_labelWithText:units[i] fontSize:PingFangSC(17) color:COLOR_textColor];
               [rightLab lz_rightAlignment];
               [textField addSubview:rightLab];
               [rightLab mas_makeConstraints:^(MASConstraintMaker *make) {
                   make.right.mas_equalTo(0);
                   make.centerY.mas_equalTo(textField.mas_centerY);
                   make.width.mas_equalTo(35);
                   make.height.mas_equalTo(24);
               }];
               [textFields addObject:textField];
           }
      }
      
}

@end
