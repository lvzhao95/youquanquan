//
//  PMPDDParamCourierView.m
//  Manager
//
//  Created by lvzhao on 2020/8/2.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPDDParamCourierView.h"
#import "PMPDDParamViewModel.h"
#import "PMChannelTextField.h"

@interface PMPDDParamCourierView()

@property (nonatomic,strong) PMPDDParamViewModel *viewModel;

@end

@implementation PMPDDParamCourierView

- (instancetype)initViewModel:(LZBaseViewModel *)viewModel
{
    self = [super initViewModel:viewModel];
    if (self) {
        
        self.viewModel = (PMPDDParamViewModel *)viewModel;
        self.backgroundColor = UIColor.whiteColor;
        [self setupView];
    }
    return self;
}

- (void)setupView{
    
    ///!!!:title
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = COLOR_appColor;
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(20);
        make.top.mas_equalTo(20);
    }];
       
       
    UILabel *titleLab= [UILabel lz_labelWithText:@"空包网快递选择" fontSize:PingFangSC(15) color:COLOR_textColor];
    [self addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(lineView.mas_right).mas_offset(10);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(20);
        make.top.mas_equalTo(20);
    }];
    
    
    
    NSArray *titles = @[@"空包网账号",@"空包网apikey"];
    NSArray *placeholders = @[@"空包网账号",@"请已保存过就可不输"];
    NSMutableArray *textFields = [[NSMutableArray alloc] init];
     for (int i = 0; i < titles.count; i++){
         PMChannelTextField *textField = [[PMChannelTextField alloc] init];
         textField.title = titles[i];
         textField.placeholder = placeholders[i];
         [self addSubview:textField];
         [textField mas_makeConstraints:^(MASConstraintMaker *make) {
             make.left.mas_equalTo(15);
             make.right.mas_equalTo(-15);
             make.height.mas_equalTo(50);
             make.top.mas_equalTo(50 + i * 50);

         }];
         
         [textField.titleLab mas_updateConstraints:^(MASConstraintMaker *make) {
             make.width.mas_equalTo(110);
         }];
         
         [textFields addObject:textField];
     }
    
}
@end
