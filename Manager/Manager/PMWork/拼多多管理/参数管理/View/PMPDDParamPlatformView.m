//
//  PMPDDParamPlatformView.m
//  Manager
//
//  Created by lvzhao on 2020/8/2.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPDDParamPlatformView.h"
#import "PMPDDParamViewModel.h"
#import "PMChannelTextField.h"

@interface PMPDDParamPlatformView()

@property (nonatomic,strong) PMPDDParamViewModel *viewModel;

@end

@implementation PMPDDParamPlatformView

- (instancetype)initViewModel:(LZBaseViewModel *)viewModel
{
    self = [super initViewModel:viewModel];
    if (self) {
        
        self.viewModel = (PMPDDParamViewModel *)viewModel;
        self.backgroundColor = UIColor.whiteColor;
        [self setupView];
    }
    return self;
}

- (void)setupView{
    
    ///!!!:title
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = COLOR_appColor;
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(20);
        make.top.mas_equalTo(20);
    }];
       
       
    UILabel *titleLab= [UILabel lz_labelWithText:@"接码平台账号设置" fontSize:PingFangSC(15) color:COLOR_textColor];
    [self addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(lineView.mas_right).mas_offset(10);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(20);
        make.top.mas_equalTo(20);
    }];
    
    
    
    NSArray *titles = @[@"接码平台API账号",@"接码平台密码",@"拼多多项目ID"];
    NSArray *placeholders = @[@"API账号",@"已保存过就可不输",@"拼多多项目ID"];
    NSMutableArray *textFields = [[NSMutableArray alloc] init];
    for (int i = 0; i < titles.count; i++){
        PMChannelTextField *textField = [[PMChannelTextField alloc] init];
        textField.title = titles[i];
        textField.placeholder = placeholders[i];
        [self addSubview:textField];
        [textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(15);
            make.right.mas_equalTo(-15);
            make.height.mas_equalTo(50);
            make.top.mas_equalTo(50 + i * 50);
        }];

        [textField.titleLab mas_updateConstraints:^(MASConstraintMaker *make) {
            if(i == 0){
                make.width.mas_equalTo(130);
            } else {
                make.width.mas_equalTo(105);
            }
        }];

        [textFields addObject:textField];
    }
    
    UILabel *tipLab = [UILabel lz_labelWithText:@"可填写多个，以英文半角逗号','分隔，如：28,27,22" fontSize:PingFangSC(13) color:COLOR_placeholderColor];
    [self addSubview:tipLab];
    [tipLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(21);
        make.top.mas_equalTo(205);
    }];
    
}

@end
