//
//  PMPDDParamLogisticsView.m
//  Manager
//
//  Created by lvzhao on 2020/8/2.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPDDParamLogisticsView.h"
#import "PMPDDParamViewModel.h"
#import "PMChannelTextField.h"

@interface PMPDDParamLogisticsView()

@property (nonatomic,strong) PMPDDParamViewModel *viewModel;

@end

@implementation PMPDDParamLogisticsView

- (instancetype)initViewModel:(LZBaseViewModel *)viewModel
{
    self = [super initViewModel:viewModel];
    if (self) {
        
        self.viewModel = (PMPDDParamViewModel *)viewModel;
        self.backgroundColor = UIColor.whiteColor;
        [self setupView];
    }
    return self;
}

- (void)setupView{
    
    ///!!!:title
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = COLOR_appColor;
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(20);
        make.top.mas_equalTo(20);
    }];
       
       
    UILabel *titleLab= [UILabel lz_labelWithText:@"物流配置" fontSize:PingFangSC(15) color:COLOR_textColor];
    [self addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(lineView.mas_right).mas_offset(10);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(20);
        make.top.mas_equalTo(20);
    }];
    
    NSMutableArray *detailLabs = [[NSMutableArray alloc] init];
    
    {
        NSArray *titles = @[@"自动发货：",@"自动评论：",@"自动收货：",@"基于物流：",@"自动绑商品："];
        //每个Item宽高
        CGFloat H = 40;
        CGFloat W = (kScreenWidth - 30 - 10)/2.0;
        //每行列数
        NSInteger rank = 2;
        //每列间距
        CGFloat rankMargin = 10;
        //每行间距
        CGFloat rowMargin = 10;
        //Item索引 ->根据需求改变索引
        NSUInteger index = titles.count;
           
        CGFloat top = 50;
        for (int i = 0 ; i < index; i++) {
               
            UIView *contentView = [[UIView alloc] init];
            //Item Y轴
            NSUInteger Y = (i / rank) * (H +rowMargin);
            //Item X轴
            CGFloat X = (i % rank) * (W + rankMargin);
               
            [self addSubview:contentView];
            [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(X + 15);
                make.top.mas_equalTo(top + Y);
                make.width.mas_equalTo(W);
                make.height.mas_equalTo(H);
            }];
            
            UILabel *titleLab = [UILabel lz_labelWithText:titles[i] fontSize:PingFangSC(17) color:COLOR_textColor];
            [contentView addSubview:titleLab];
            [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(0);
                make.centerY.mas_equalTo(contentView.mas_centerY);
                make.width.mas_equalTo(105);
                make.height.mas_equalTo(H);
            }];
            
            UISwitch *swith = [[UISwitch alloc] init];
            [contentView addSubview:swith];
            swith.onTintColor = COLOR_appColor;
            swith.thumbTintColor = COLOR_appColor;
            swith.tintColor = UIColor.whiteColor;
            [swith mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(titleLab.mas_right);
                make.centerY.mas_equalTo(titleLab.mas_centerY);
                make.width.mas_equalTo(50);
                make.height.mas_equalTo(H);
            }];
            
            [detailLabs addObject:swith];
        }
    }
    
    NSMutableArray *textFields = [[NSMutableArray alloc] init];
    
    {
        NSArray *titles = @[@"发货时间",@"收货时间",@"评论时间",@"绑商品时间"];
        NSArray *bottoms = @[@"距离订单创建后多少分钟发货",@"距离订单创建后多少分钟收货",@"距离订单创建后多少分钟评论",@"距离订单创建后多少分钟绑商品"];

         for (int i = 0; i < titles.count; i++){
             PMChannelTextField *textField = [[PMChannelTextField alloc] init];
             textField.title = titles[i];
             textField.textField.font = PingFangSC(17);
             textField.placeholder = @"分钟数";
             [self addSubview:textField];
             [textField mas_makeConstraints:^(MASConstraintMaker *make) {
                 make.left.mas_equalTo(15);
                 make.right.mas_equalTo(-15);
                 make.height.mas_equalTo(50);
                 make.top.mas_equalTo(200 + i * (60 + 20));
             }];

             [textField.titleLab mas_updateConstraints:^(MASConstraintMaker *make) {
                 make.width.mas_equalTo(85);
             }];

             [textField.textField mas_updateConstraints:^(MASConstraintMaker *make) {
                 make.right.mas_equalTo(-50);
             }];
             
             UILabel *rightLab = [UILabel lz_labelWithText:@"分钟" fontSize:PingFangSC(17) color:COLOR_textColor];
             [textField addSubview:rightLab];
             [rightLab mas_makeConstraints:^(MASConstraintMaker *make) {
                 make.right.mas_equalTo(0);
                 make.centerY.mas_equalTo(textField.mas_centerY);
                 make.width.mas_equalTo(35);
                 make.height.mas_equalTo(24);
             }];
             
             
             UILabel *bottomLab = [UILabel lz_labelWithText:bottoms[i] fontSize:PingFangSC(13) color:COLOR_subTextColor];
             [self addSubview:bottomLab];
             [bottomLab mas_makeConstraints:^(MASConstraintMaker *make) {
                 make.left.mas_equalTo(15);
                 make.top.mas_equalTo(textField.mas_bottom).mas_offset(2);
                 make.width.mas_equalTo(200);
                 make.height.mas_equalTo(20);
             }];
             
             [textFields addObject:textField];
         }
    }
    
}


@end
