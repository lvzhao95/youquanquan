//
//  PMPDDGoodsViewController.m
//  Manager
//
//  Created by lvzhao on 2020/8/1.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPDDGoodsViewController.h"
#import "PMPDDManagerViewModel.h"
#import "LZTableSlideView.h"
#import "LZSortButton.h"



@interface PMPDDGoodsViewController ()

@property (nonatomic,strong) PMPDDManagerViewModel *viewModel;

@property (nonatomic,strong) LZTableSlideView *tableView;
@end

@implementation PMPDDGoodsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.view.backgroundColor = UIColor.whiteColor;
    [self setupUI];
}


- (void)setupUI{
    

    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(kScreenWidth);
        make.bottom.mas_equalTo(0);
    }];
    
  
    @weakify(self);
    showLoadingTitle(@"", nil);
    [self.viewModel getGoodsList:YES];
    [self.viewModel.reloadSubject subscribeNext:^(id  _Nullable x) {
        dismiss(nil);
        @strongify(self);

        [self reloadTable:[x intValue]];
    }];
    
}

///刷线表格
- (void)reloadTable:(NSInteger) type{
    
    
    if(type > 3)return;
    
    self.tableView.dataModel = self.viewModel.dataModel;
    [self.tableView.rightTableView.mj_header endRefreshing];
    [self.tableView.rightTableView.mj_footer endRefreshing];
    self.tableView.rightTableView.mj_footer.hidden = NO;
    [self.tableView  dismissEmptyView];
    switch (type) {
        case 0:{
            self.tableView.rightTableView.mj_footer.hidden = YES;
        }
            break;
        default:
            break;
    }
    
    if(self.viewModel.dataModel.dataArray.count == 0){
        self.tableView.rightTableView.mj_footer.hidden = YES;
        [self.tableView tableViewDisplayWithImgName:@"no_data" message:@"暂无数据" contentoffsetY:100 showEmptyView:YES];
    }
}


- (PMPDDManagerViewModel *)viewModel{
    if(!_viewModel){
        _viewModel = [[PMPDDManagerViewModel alloc] init];
        _viewModel.index = 1;
    }
    return _viewModel;
}

- (LZTableSlideView *)tableView{
    if(!_tableView){
        _tableView = [[LZTableSlideView alloc] initWithFrame:CGRectMake(0, 0, K_SCREENWIDTH, K_SCREENHEIGHT - 45 - K_NAVHEIGHT)];
    }
    return _tableView;
}


@end
