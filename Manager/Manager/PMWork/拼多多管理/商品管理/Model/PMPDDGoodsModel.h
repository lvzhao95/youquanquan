//
//  PMPDDGoodsModel.h
//  Manager
//
//  Created by lvzhao on 2020/8/6.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PMPDDGoodsModel : NSObject
@property (nonatomic , copy) NSString              * create_date;
@property (nonatomic , copy) NSString              * goods_title;
@property (nonatomic , copy) NSString*              amount;
@property (nonatomic , copy) NSString*              profit;
@property (nonatomic , copy) NSString              * oth_type;
@property (nonatomic , copy) NSString              * sku_id;
@property (nonatomic , copy) NSString*              is_del;
@property (nonatomic , copy) NSString*              group_amount;
@property (nonatomic , copy) NSString *              can_use;
@property (nonatomic , copy) NSString              * group_id;
@property (nonatomic , copy) NSString              * sell_id;
@property (nonatomic , copy) NSString              * goods_id;
@property (nonatomic , copy) NSString*              total_use_order;
@property (nonatomic , copy) NSString              * goods_type;
@property (nonatomic , copy) NSString              * creatd_member_id;
@end

NS_ASSUME_NONNULL_END
