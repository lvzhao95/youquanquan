//
//  PMPDDEmbeddedView.m
//  Manager
//
//  Created by lvzhao on 2020/8/2.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPDDEmbeddedView.h"
#import "PMPDDEmbeddedViewModel.h"
#import "PMChannelTextField.h"


@interface PMPDDEmbeddedView()

@property (nonatomic,strong) PMPDDEmbeddedViewModel *viewModel;

@end

@implementation PMPDDEmbeddedView

- (instancetype)initViewModel:(LZBaseViewModel *)viewModel
{
    self = [super initViewModel:viewModel];
    if (self) {
        self.viewModel = (PMPDDEmbeddedViewModel *)viewModel;
        self.backgroundColor = UIColor.whiteColor;
        [self setupView];
    }
    return self;
}

#pragma mark 创建UI
- (void)setupView{
    
    ///!!!:表头
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.showsVerticalScrollIndicator = NO;
    [self addSubview:scrollView];
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.right.bottom.mas_equalTo(0);
    }];

    //内容
    UIView *contentView = [[UIView alloc] init];
    [scrollView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.and.right.equalTo(scrollView).with.insets(UIEdgeInsetsZero);
        make.width.equalTo(scrollView);

    }];

    NSArray *titles = @[@"账号：pdd74070518382",@"名称：胖子家居生活专营店"];
    for (int i = 0; i < titles.count; i++){
        UILabel *titleLab = [UILabel lz_labelWithText:titles[i] fontSize:PingFangSC(15) color:COLOR_textColor];
        [contentView addSubview:titleLab];
        [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(15);
            make.width.mas_equalTo(K_SCREENHEIGHT - 30);
            make.height.mas_equalTo(40);
            make.top.mas_equalTo(i * 40);
        }];
    }
        
    
    
    ///!!!:支付通道
    PMChannelTextField *payChannelView = [[PMChannelTextField alloc] init];
    payChannelView.title = @"支付通道";
    payChannelView.placeholder = @"";
    [contentView addSubview:payChannelView];
    [payChannelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(K_SCREENWIDTH - 30);
        make.height.mas_equalTo(50);
        make.top.mas_equalTo(100);
    }];
    payChannelView.textField.userInteractionEnabled = NO;
    UIButton *payChannelBtn = [UIButton lz_buttonTitle:@"请选择支付通道" titleColor:COLOR_textColor fontSize:17];
    [payChannelBtn setImage:k_imageName(@"pm_next") forState:UIControlStateNormal];
    [payChannelView addSubview:payChannelBtn];
    [payChannelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.top.mas_equalTo(payChannelView.textField);
    }];
    payChannelBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    payChannelBtn.tag = 100;
    [payChannelBtn addTarget:self action:@selector(selectClick:) forControlEvents:UIControlEventTouchUpInside];
    [payChannelBtn SG_imagePositionStyle:SGImagePositionStyleRight spacing:5];

     
     
     ///!!!:下单金额
     PMChannelTextField *amoutView = [[PMChannelTextField alloc] init];
     amoutView.title = @"下单金额";
     amoutView.placeholder = @"请输入下单金额";
     [contentView addSubview:amoutView];
     [amoutView mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.mas_equalTo(15);
         make.width.mas_equalTo(K_SCREENWIDTH - 30);
         make.height.mas_equalTo(50);
         make.top.mas_equalTo(payChannelView.mas_bottom);
     }];

    
    ///!!!:获取下单二维码
    UIButton *saveBtn = [UIButton lz_buttonTitle:@"获取下单二维码" titleColor:UIColor.whiteColor fontSize:18];
    saveBtn.backgroundColor = COLOR_appColor;
    saveBtn.cornerRadius = 6;
    [contentView addSubview:saveBtn];
    [saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(K_SCREENWIDTH - 30);
        make.top.mas_equalTo(amoutView.mas_bottom).mas_offset(25);
        make.height.mas_equalTo(40);
    }];
   
    
    ///!!!:提示
    UILabel *messageLab = [UILabel lz_labelWithText:@"请按通道性质打开浏览器\\微信\\支付宝扫码下单支付(请误重复支付)" fontSize:PingFangSC(13) color:UIColorHex(0x6666666)];
    [messageLab lz_centerAlignment];
    [contentView addSubview:messageLab];
    [messageLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.top.mas_equalTo(saveBtn.mas_bottom).mas_offset(20);
        make.height.mas_equalTo(40);
    }];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    [contentView addSubview:imageView];
    imageView.backgroundColor = UIColor.lz_randomColor;
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(contentView.mas_centerX);
        make.width.mas_equalTo(212);
        make.top.mas_equalTo(messageLab.mas_bottom).mas_offset(20);
        make.height.mas_equalTo(212);
    }];

    
    [contentView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(imageView.mas_bottom).mas_offset(34);
    }];
       
       
       
    
    
    
    
    
}

- (void)selectClick:(UIButton *)sender{
    
    switch (sender.tag) {
        case 100:
        {
            NSArray *subTitles = @[@"普通商户",@"VIP商户",@"VVIP商户",@"SVIP商户"];
            [LZToolView showAlertType:LZAlertTypeActionSheet withTitle:@"" message:@"" cancel:@"取消" sure:@"" objectDict:@{@"subTitles":subTitles} cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
                NSInteger index = [object intValue];
                [sender setTitle:subTitles[index] forState:UIControlStateNormal];
                [sender SG_imagePositionStyle:SGImagePositionStyleRight spacing:5];

            }];
        }
            break;
        case 101:
        {
            
        }
            break;
            
        default:
            break;
    }
    
    
}


@end
