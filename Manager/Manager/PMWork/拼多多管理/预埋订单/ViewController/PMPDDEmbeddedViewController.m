//
//  PMPDDEmbeddedViewController.m
//  Manager
//
//  Created by lvzhao on 2020/8/2.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPDDEmbeddedViewController.h"
#import "PMPDDEmbeddedView.h"
#import "PMPDDEmbeddedViewModel.h"

@interface PMPDDEmbeddedViewController ()

@property (nonatomic,strong) PMPDDEmbeddedView *embeddedView;

@property (nonatomic,strong) PMPDDEmbeddedViewModel *viewModel;

@end

@implementation PMPDDEmbeddedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"预埋订单";
    [self setupUI];
    // Do any additional setup after loading the view.
}

- (void)setupUI{
    [self.view addSubview:self.embeddedView];
    [self.embeddedView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

#pragma mark -懒加载
- (PMPDDEmbeddedView *)embeddedView{
    if(!_embeddedView){
        _embeddedView = [[PMPDDEmbeddedView alloc] initViewModel:self.viewModel];
    }
    return _embeddedView;
}

- (PMPDDEmbeddedViewModel *)viewModel{
    if(!_viewModel) {
        _viewModel = [[PMPDDEmbeddedViewModel alloc] init];
    }
    return _viewModel;
}

@end
