//
//  PMPDDManagerViewController.m
//  Manager
//
//  Created by lvzhao on 2020/8/1.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPDDManagerViewController.h"
#import "PMPDDManagerView.h"
#import "PMPDDManagerViewModel.h"

@interface PMPDDManagerViewController ()

@property (nonatomic,strong) PMPDDManagerView *managerView;

@property (nonatomic,strong) PMPDDManagerViewModel *viewModel;

@end

@implementation PMPDDManagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"拼多多通道管理";
    [self setupUI];
    // Do any additional setup after loading the view.
}

- (void)setupUI{
    [self.view addSubview:self.managerView];
    [self.managerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    
    ///!!!:筛选
    UIButton *screeningBtn = [UIButton lz_buttonImageName:@"pm_shaixuan" backgroundImageName:@""];
    screeningBtn.frame = CGRectMake(0, 0, 44, 44);
    [self initBarItem:screeningBtn withType:1];
    @weakify(self);
    [[screeningBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);
//        RACSubject *subject = [RACSubject subject];
//        PMScreeningViewController *screeningVC = [[PMScreeningViewController alloc] init];
//        screeningVC.pickerModel = PMDatePickerModelYMDHM;
//        screeningVC.reloadSubject = subject;
//        [LZTool.currentViewController.navigationController pushViewController:screeningVC animated:YES];
//        [subject subscribeNext:^(id  _Nullable x) {
//            NSDictionary *dict = x;
//            NSLog(@"%@",x);
//        }];

    }];
}


//确认
- (void)sureClick:(UIButton *)sender{
    
    

    
}

#pragma mark -懒加载
- (PMPDDManagerView *)managerView{
    if(!_managerView) {
        _managerView = [[PMPDDManagerView alloc] initViewModel:self.viewModel];
    }
    return _managerView;
}

- (PMPDDManagerViewModel *)viewModel{
    if(!_viewModel) {
        _viewModel = [[PMPDDManagerViewModel alloc] init];
    }
    return _viewModel;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
