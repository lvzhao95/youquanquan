//
//  PMPDDMerchantModel.h
//  Manager
//
//  Created by lvzhao on 2020/8/6.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PMPDDMerchantModel : NSObject

@property (nonatomic , copy) NSString * account_no;
@property (nonatomic , copy) NSString * post_code;
@property (nonatomic , copy) NSString * region;
@property (nonatomic , copy) NSString * cookie1;
@property (nonatomic , copy) NSString * account_name;
@property (nonatomic , copy) NSString * tenant_id;
@property (nonatomic , copy) NSString * home;
@property (nonatomic , copy) NSString * agency;
@property (nonatomic , copy) NSString * login_status;
@property (nonatomic , copy) NSString *  is_del;
@property (nonatomic , copy) NSString *  rppay;
@property (nonatomic , copy) NSString * cookie;
@property (nonatomic , copy) NSString * alipay;
@property (nonatomic , copy) NSString * account_uid;
@property (nonatomic , copy) NSString * addr_id;
@property (nonatomic , copy) NSString * create_date;
@property (nonatomic , copy) NSString * mall_id;
@property (nonatomic , copy) NSString * access_token;
@property (nonatomic , copy) NSString * account_type;
@property (nonatomic , copy) NSString * use_num;
@property (nonatomic , copy) NSString * city;
@property (nonatomic , copy) NSString * oth_type;
@property (nonatomic , copy) NSString * post_name;
@property (nonatomic , copy) NSString * remarks;
@property (nonatomic , copy) NSString * account_bal;
@property (nonatomic , copy) NSString * province;
@property (nonatomic , copy) NSString * can_use;
@property (nonatomic , copy) NSString * limit_amount;
@property (nonatomic , copy) NSString * wxpay;
@property (nonatomic , copy) NSString * succ_num;
@property (nonatomic , copy) NSString * phone;
@property (nonatomic , copy) NSString * creatd_member_id;
@property (nonatomic , copy) NSString * update_date;
@property (nonatomic , copy) NSString * account_pwd;


@end

NS_ASSUME_NONNULL_END
