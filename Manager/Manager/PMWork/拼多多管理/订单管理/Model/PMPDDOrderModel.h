//
//  PMPDDOrderModel.h
//  Manager
//
//  Created by lvzhao on 2020/8/6.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PMPDDOrderModel : NSObject

@property (nonatomic,strong) NSString *name;

@property (nonatomic , copy) NSString* bind_goods_id;
@property (nonatomic , copy) NSString* send_date;
@property (nonatomic , copy) NSString* order_sn;
@property (nonatomic , copy) NSString* buy_id;
@property (nonatomic , copy) NSString* create_date;
@property (nonatomic , copy) NSString* goods_title;
@property (nonatomic , copy) NSString* charge_type;
@property (nonatomic , copy) NSString* order_id;
@property (nonatomic , copy) NSString* buy_num;
@property (nonatomic , copy) NSString* is_group;

@property (nonatomic , copy) NSString* goods_type;
@property (nonatomic , copy) NSString* oth_type;
@property (nonatomic , copy) NSString* goods_id;
@property (nonatomic , copy) NSString* sell_name;
@property (nonatomic , copy) NSString* sell_no;
@property (nonatomic , copy) NSString* order_no;
@property (nonatomic , copy) NSString* limit_amount;
@property (nonatomic , copy) NSString* pay_channel;
@property (nonatomic , copy) NSString* succ_num;
@property (nonatomic , copy) NSString* channel_code;
@property (nonatomic , copy) NSString* buy_no;
@property (nonatomic , copy) NSString* pay_info;
@property (nonatomic , copy) NSString* sell_id;
@property (nonatomic , copy) NSString* creatd_member_id;
@property (nonatomic , copy) NSString* goods_key_id;
@property (nonatomic , copy) NSString* amount;
@end

NS_ASSUME_NONNULL_END
