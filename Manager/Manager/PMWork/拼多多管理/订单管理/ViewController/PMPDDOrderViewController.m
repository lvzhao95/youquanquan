//
//  PMPDDOrderViewController.m
//  Manager
//
//  Created by lvzhao on 2020/8/2.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPDDOrderViewController.h"
#import "PMPDDManagerViewModel.h"
#import "PMPDDOrderHeadView.h"
#import "LZTableSlideView.h"
#import "LZTableDataModel.h"
#import "LZSortButton.h"

@interface PMPDDOrderViewController ()

@property (nonatomic,strong)  PMPDDManagerViewModel *viewModel;

@property (nonatomic,strong)  LZTableSlideView *tableView;

@end

@implementation PMPDDOrderViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColor.whiteColor;
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self setupUI];
}

- (void)setupUI{
    
    PMPDDOrderHeadView *headView = [[PMPDDOrderHeadView alloc] initViewModel:self.viewModel];
    [self.view addSubview:headView];
    [headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(0);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(120);
    }];

     ///!!!:操作按钮
     UIView *optionView = [[UIView alloc] init];
     optionView.backgroundColor = UIColor.whiteColor;
     [self.view addSubview:optionView];
     [optionView mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.right.mas_equalTo(0);
         make.bottom.mas_equalTo(K_Device_Is_iPhoneX ? -34 : 0);
         make.height.mas_equalTo(50);
     }];
     
     UIView *lineView = [[UIView alloc] init];
     lineView.backgroundColor = COLOR_cellLine;
     [optionView addSubview:lineView];
     [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.right.mas_equalTo(0);
         make.top.mas_equalTo(0);
         make.height.mas_equalTo(0.5);
     }];
 
     CGFloat titleW = (K_SCREENWIDTH - 30 - 20)/4.0;
     NSArray *titles = @[@"一键清团",@"一键发货",@"一键收货",@"更多"];
     UIView *lastView = nil;
     for (int i = 0; i < titles.count; i++){
         UIButton *optionBtn = [UIButton lz_buttonTitle:titles[i] titleColor:COLOR_appColor fontSize:15];
         optionBtn.backgroundColor = UIColor.lz_randomColor;
         [optionView addSubview:optionBtn];
         [optionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
             if(lastView){
                 make.left.mas_equalTo(lastView.mas_right).mas_offset(5);
             } else {
                 make.left.mas_equalTo(15);
             }
             make.width.mas_equalTo(titleW);
             make.height.mas_equalTo(50);
             make.top.mas_equalTo(0);
         }];
         lastView = optionBtn;
     }
    
     [self.view addSubview:self.tableView];
     [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.mas_equalTo(0);
         make.top.mas_equalTo(headView.mas_bottom);
         make.width.mas_equalTo(kScreenWidth);
         make.bottom.mas_equalTo(optionView.mas_top);
     }];
    
    
    
    @weakify(self);
    showLoadingTitle(@"", nil);
    [self.viewModel getOrderList:YES];
    [self.viewModel getOrderTotal];
    [self.viewModel.reloadSubject subscribeNext:^(id  _Nullable x) {
        dismiss(nil);
        @strongify(self);

        [self reloadTable:[x intValue]];
    }];
    
}

///刷线表格
- (void)reloadTable:(NSInteger) type{
    
    
    if(type > 3)return;
    
    self.tableView.dataModel = self.viewModel.dataModel;
    [self.tableView.rightTableView.mj_header endRefreshing];
    [self.tableView.rightTableView.mj_footer endRefreshing];
    self.tableView.rightTableView.mj_footer.hidden = NO;
    [self.tableView  dismissEmptyView];
    switch (type) {
        case 0:{
            self.tableView.rightTableView.mj_footer.hidden = YES;
        }
            break;
        default:
            break;
    }
    
    if(self.viewModel.dataModel.dataArray.count == 0){
        self.tableView.rightTableView.mj_footer.hidden = YES;
        [self.tableView tableViewDisplayWithImgName:@"no_data" message:@"暂无数据" contentoffsetY:100 showEmptyView:YES];
    }
}


- (PMPDDManagerViewModel *)viewModel{
    if(!_viewModel){
        _viewModel = [[PMPDDManagerViewModel alloc] init];
        _viewModel.index = 4;
    }
    return _viewModel;;
}

- (LZTableSlideView *)tableView{
    if(!_tableView){
        _tableView = [[LZTableSlideView alloc] initWithFrame:CGRectMake(0, 120, kScreenWidth, 400)];
    }
    return _tableView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
