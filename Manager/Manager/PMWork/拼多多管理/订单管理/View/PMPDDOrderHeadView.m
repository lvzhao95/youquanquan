//
//  PMPDDOrderHeadView.m
//  Manager
//
//  Created by lvzhao on 2020/8/2.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPDDOrderHeadView.h"
#import "PMPDDManagerViewModel.h"

@interface PMPDDOrderHeadView()

@property (nonatomic,strong) PMPDDManagerViewModel *viewModel;

@end

@implementation PMPDDOrderHeadView

- (instancetype)initViewModel:(LZBaseViewModel *)viewModel
{
    self = [super initViewModel:viewModel];
    if (self) {
        self.viewModel = (PMPDDManagerViewModel *)viewModel;
        [self setupView];
        
    }
    return self;
}

#pragma mark 创建UI
- (void) setupView{
    
    
    NSMutableArray *detailLabs = [[NSMutableArray alloc] init];
    
    NSArray *titles = @[@"总笔数：3",@"支付笔数：1",@"成功率：33.33%",@"支付金额：￥10.00",@"发货笔数：1",@"收货笔数：1"];
    //每个Item宽高
    CGFloat H = 40;
    CGFloat W = (kScreenWidth - 30 - 10)/2.0;
    //每行列数
    NSInteger rank = 2;
    //每列间距
    CGFloat rankMargin = 10;
    //每行间距
    CGFloat rowMargin = 0;
    //Item索引 ->根据需求改变索引
    NSUInteger index = titles.count;
    
    CGFloat top = 0;
    for (int i = 0 ; i < index; i++) {
        
        UILabel *detailLab = [UILabel lz_labelWithText:titles[i] fontSize:PingFangSC(15) color:COLOR_textColor];
        //Item Y轴
        NSUInteger Y = (i / rank) * (H +rowMargin);
        //Item X轴
        CGFloat X = (i % rank) * (W + rankMargin);
        
        [self addSubview:detailLab];
        [detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(X + 15);
            make.top.mas_equalTo(top + Y);
            make.width.mas_equalTo(W);
            make.height.mas_equalTo(H);
        }];
        [detailLabs addObject:detailLab];
    }
}

@end
