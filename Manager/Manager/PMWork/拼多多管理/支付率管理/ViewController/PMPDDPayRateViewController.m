//
//  PMPDDPayRateViewController.m
//  Manager
//
//  Created by lvzhao on 2020/8/2.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPDDPayRateViewController.h"
#import "PMPDDManagerViewModel.h"
#import "PMPDDPayRateHeadView.h"
#import "LZTableSlideView.h"
#import "LZTableDataModel.h"
#import "LZSortButton.h"

@interface PMPDDPayRateViewController ()

@property (nonatomic,strong)  PMPDDManagerViewModel *viewModel;

@property (nonatomic,strong)  LZTableSlideView *tableView;

@end

@implementation PMPDDPayRateViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColor.whiteColor;
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self setupUI];
}

- (void)setupUI{
    
    PMPDDPayRateHeadView *headView = [[PMPDDPayRateHeadView alloc] initViewModel:self.viewModel];
    [self.view addSubview:headView];
    [headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(0);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(120);
    }];

     ///!!!:操作按钮
     UIView *optionView = [[UIView alloc] init];
     optionView.backgroundColor = UIColor.whiteColor;
     [self.view addSubview:optionView];
     [optionView mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.right.mas_equalTo(0);
         make.bottom.mas_equalTo(K_Device_Is_iPhoneX ? -34 : 0);
         make.height.mas_equalTo(50);
     }];
     
     UIView *lineView = [[UIView alloc] init];
     lineView.backgroundColor = COLOR_cellLine;
     [optionView addSubview:lineView];
     [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.right.mas_equalTo(0);
         make.top.mas_equalTo(0);
         make.height.mas_equalTo(0.5);
     }];
 
     CGFloat titleW = (K_SCREENWIDTH - 30 - 20)/4.0;
     NSArray *titles = @[@"一键清团",@"一键发货",@"一键收货",@"更多"];
     UIView *lastView = nil;
     for (int i = 0; i < titles.count; i++){
         UIButton *optionBtn = [UIButton lz_buttonTitle:titles[i] titleColor:COLOR_appColor fontSize:15];
         optionBtn.backgroundColor = UIColor.lz_randomColor;
         [optionView addSubview:optionBtn];
         [optionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
             if(lastView){
                 make.left.mas_equalTo(lastView.mas_right).mas_offset(5);
             } else {
                 make.left.mas_equalTo(15);
             }
             make.width.mas_equalTo(titleW);
             make.height.mas_equalTo(50);
             make.top.mas_equalTo(0);
         }];
         lastView = optionBtn;
     }
    
     [self.view addSubview:self.tableView];
     [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.mas_equalTo(0);
         make.top.mas_equalTo(headView.mas_bottom);
         make.width.mas_equalTo(kScreenWidth);
         make.bottom.mas_equalTo(optionView.mas_top);
     }];
    
    LZTableDataModel *dataModel = [[LZTableDataModel alloc] init];
    self.tableView.dataModel = dataModel;
    
    //最左边, 最上边的按钮
    LZSortButton *sortBtn = [[LZSortButton alloc] init];
    [sortBtn setTitleColor:UIColorHex(0x666666) forState:UIControlStateNormal];
    sortBtn.titleLabel.font = PingFangSC(13);
    sortBtn.sortStatue = PMTableSortNormal;
    [sortBtn setTitle:@"代理人" forState:UIControlStateNormal];
    sortBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.tableView.leftHeadView addSubview:sortBtn];
    [sortBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-5);
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
    [sortBtn SG_imagePositionStyle:SGImagePositionStyleRight spacing:5];
//
//    [self.tableView setCallBlock:^(PMTouchTable touchTable, NSInteger index) {
//
//
//        [LZToolView showAlertType:LZAlertTypeTipAlter withTitle:@"提示" message:[NSString stringWithFormat:@"我点击了第%ld个",(long)index] cancel:@"确信" sure:@"取消" objectDict:nil cancelBlock:^(id  _Nullable object) {
//
//
//        } sureBlock:^(id  _Nullable object) {
//
//        }];
//
//    }];
    
}

- (PMPDDManagerViewModel *)viewModel{
    if(!_viewModel){
        _viewModel = [[PMPDDManagerViewModel alloc] init];
    }
    return _viewModel;;
}

- (LZTableSlideView *)tableView{
    if(!_tableView){
        _tableView = [[LZTableSlideView alloc] initWithFrame:CGRectMake(0, 120, kScreenWidth, 400)];
    }
    return _tableView;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
