//
//  PMPDDCheckModel.h
//  Manager
//
//  Created by lvzhao on 2020/8/6.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PMPDDCheckModel : NSObject
@property (nonatomic , copy) NSString*              use_num;
@property (nonatomic , copy) NSString              * create_date;
@property (nonatomic , copy) NSString*              succ_num;
@property (nonatomic , copy) NSString              * account_type;
@property (nonatomic , copy) NSString              * oth_type;
@property (nonatomic , copy) NSString              * account_no;
@property (nonatomic , copy) NSString*               rppay;
@property (nonatomic , copy) NSString*               can_use;
@property (nonatomic , copy) NSString*               is_del;
@property (nonatomic , copy) NSString*               wxpay;
@property (nonatomic , copy) NSString              * tenant_id;
@property (nonatomic , copy) NSString*              limit_amount;
@property (nonatomic , copy) NSString*              alipay;
@property (nonatomic , copy) NSString              * verify_info;
@property (nonatomic , copy) NSString              * login_status;
@property (nonatomic , copy) NSString              * agency;




 @property (nonatomic , copy) NSString* bind_goods_id;

 @property (nonatomic , copy) NSString* is_group;
 @property (nonatomic , copy) NSString* charge_status;
 @property (nonatomic , copy) NSString* charge_id;
 @property (nonatomic , copy) NSString* is_group_succ;
 @property (nonatomic , copy) NSString* vendor_code;
 @property (nonatomic , copy) NSString* merchant_id;


@end

NS_ASSUME_NONNULL_END
