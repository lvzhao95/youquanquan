//
//  PMPDDWithdrawalViewController.m
//  Manager
//
//  Created by lvzhao on 2020/8/2.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPDDWithdrawalViewController.h"
#import "PMPDDWithdrawalView.h"
#import "PMPDDWithdrawalViewModel.h"

@interface PMPDDWithdrawalViewController ()

@property (nonatomic,strong) PMPDDWithdrawalView *withdrawalView;

@property (nonatomic,strong) PMPDDWithdrawalViewModel *viewModel;

@end

@implementation PMPDDWithdrawalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"提现记录";
    [self setupUI];
    // Do any additional setup after loading the view.
}

- (void)setupUI{
    [self.view addSubview:self.withdrawalView];
    [self.withdrawalView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}


//确认
- (void)sureClick:(UIButton *)sender{
    
    

    
}

#pragma mark -懒加载
- (PMPDDWithdrawalView *)withdrawalView{
    if(!_withdrawalView){
        _withdrawalView = [[PMPDDWithdrawalView alloc] initViewModel:self.viewModel];
    }
    return _withdrawalView;
}

- (PMPDDWithdrawalViewModel *)viewModel{
    if(!_viewModel) {
        _viewModel = [[PMPDDWithdrawalViewModel alloc] init];
    }
    return _viewModel;
}
@end
