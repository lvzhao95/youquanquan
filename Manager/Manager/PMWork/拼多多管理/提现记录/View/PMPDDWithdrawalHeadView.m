//
//  PMPDDWithdrawalHeadView.m
//  Manager
//
//  Created by lvzhao on 2020/8/2.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPDDWithdrawalHeadView.h"
#import "PMPDDWithdrawalViewModel.h"

@interface PMPDDWithdrawalHeadView()

@property (nonatomic,strong) PMPDDWithdrawalViewModel *viewModel;

@end

@implementation PMPDDWithdrawalHeadView

- (instancetype)initViewModel:(LZBaseViewModel *)viewModel
{
    self = [super initViewModel:viewModel];
    if (self) {
        self.viewModel = (PMPDDWithdrawalViewModel *)viewModel;
        self.backgroundColor = UIColor.whiteColor;
        [self setupView];
    }
    return self;
}

#pragma mark 创建UI
- (void)setupView{
    
    
    NSArray *titles = @[@"账号：pdd74070518382",@"名称：胖子家居生活专营店",@"持有人：持有人"];
    
    for (int i = 0; i < titles.count; i++){
        
        UILabel *titleLab = [UILabel lz_labelWithText:titles[i] fontSize:PingFangSC(15) color:COLOR_textColor];
        [self addSubview:titleLab];
        [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(15);
            make.width.mas_equalTo(K_SCREENHEIGHT - 30);
            make.height.mas_equalTo(40);
            make.top.mas_equalTo(i * 40);
        }];
    }
    
    
    
    
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
