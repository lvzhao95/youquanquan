//
//  PMPDDWithdrawalView.m
//  Manager
//
//  Created by lvzhao on 2020/8/2.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPDDWithdrawalView.h"
#import "PMPDDWithdrawalViewModel.h"
#import "PMPDDWithdrawalHeadView.h"
#import "LZTableSlideView.h"
#import "LZSortButton.h"


@interface PMPDDWithdrawalView()

@property (nonatomic,strong) PMPDDWithdrawalViewModel *viewModel;

@property (nonatomic,strong) LZTableSlideView *tableView;
@end

@implementation PMPDDWithdrawalView

- (instancetype)initViewModel:(LZBaseViewModel *)viewModel
{
    self = [super initViewModel:viewModel];
    if (self) {
        self.viewModel = (PMPDDWithdrawalViewModel *)viewModel;
        
        [self setupView];
    }
    return self;
}

#pragma mark 创建UI
- (void)setupView{
    
    PMPDDWithdrawalHeadView *headView = [[PMPDDWithdrawalHeadView alloc] initViewModel:self.viewModel];
    [self addSubview:headView];
    [headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(0);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(120);
    }];

    
    [self addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(130);
        make.width.mas_equalTo(kScreenWidth);
        make.bottom.mas_equalTo(0);
    }];
    
    LZTableDataModel *dataModel = [[LZTableDataModel alloc] init];
    self.tableView.dataModel = dataModel;
    
    //最左边, 最上边的按钮
    LZSortButton *sortBtn = [[LZSortButton alloc] init];
    [sortBtn setTitleColor:UIColorHex(0x666666) forState:UIControlStateNormal];
    sortBtn.titleLabel.font = PingFangSC(13);
    sortBtn.sortStatue = PMTableSortNormal;
    [sortBtn setTitle:@"时间" forState:UIControlStateNormal];
    sortBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.tableView.leftHeadView addSubview:sortBtn];
    [sortBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-5);
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
    [sortBtn SG_imagePositionStyle:SGImagePositionStyleRight spacing:5];

//    [self.tableView setCallBlock:^(PMTouchTable touchTable, NSInteger index) {
//
//
//        [LZToolView showAlertType:LZAlertTypeTipAlter withTitle:@"提示" message:[NSString stringWithFormat:@"我点击了第%ld个",(long)index] cancel:@"确信" sure:@"取消" objectDict:nil cancelBlock:^(id  _Nullable object) {
//
//
//        } sureBlock:^(id  _Nullable object) {
//
//        }];
//
//    }];
    
}

- (LZTableSlideView *)tableView{
    if(!_tableView){
        _tableView = [[LZTableSlideView alloc] initWithFrame:CGRectMake(0, 130, kScreenWidth, 400)];
    }
    return _tableView;
}


@end
