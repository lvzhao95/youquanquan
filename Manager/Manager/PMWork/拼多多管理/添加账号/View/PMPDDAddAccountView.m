//
//  PMPDDAddAccountView.m
//  Manager
//
//  Created by lvzhao on 2020/8/2.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPDDAddAccountView.h"
#import "PMPDDAddAccountViewModel.h"
#import "LZBaseViewController.h"


@interface PMPDDAddAccountView()

@property (nonatomic,strong) PMPDDAddAccountViewModel *viewModel;

@end

@implementation PMPDDAddAccountView

- (instancetype)initViewModel:(LZBaseViewModel *)viewModel
{
    self = [super initViewModel:viewModel];
    if (self) {
        self.viewModel = (PMPDDAddAccountViewModel *)viewModel;
        self.backgroundColor = UIColor.whiteColor;

        [self setupView];
    }
    return self;
}

#pragma mark 创建UI
- (void)setupView{
    
    ///!!!:格式
    UILabel *titleLab = [UILabel lz_labelWithText:@"请按\"账号----UID----cookie\"格式填写账号信息，例：" fontSize:PingFangSC(15) color:COLOR_textColor];
    [self addSubview:titleLab];
    CGFloat titleH = [titleLab.text lz_textHeightWithFontSize:titleLab.font withMaxWidth:K_SCREENWIDTH - 30];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(10);
        make.width.mas_equalTo(K_SCREENWIDTH - 30);
        make.height.mas_equalTo(titleH);
    }];
    
    UILabel *detailLab = [UILabel lz_labelWithText:@"16265477361----7336128833637----YBXKFKSSKCBGFAYHTGIQNDWVDIEMTQR6VXVZKXCDESGYAURC5ISA1106532" fontSize:PingFangSC(13) color:COLOR_subTextColor];
    [self addSubview:detailLab];
    CGFloat detailLabH = [detailLab.text lz_textHeightWithFontSize:detailLab.font withMaxWidth:K_SCREENWIDTH - 30];
    [detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(titleLab.mas_bottom).mas_offset(5);
        make.width.mas_equalTo(K_SCREENWIDTH - 30);
        make.height.mas_equalTo(detailLabH);
    }];
    
    
    YYTextView *accountTextView = [[YYTextView alloc] init];
    accountTextView.placeholderText = @"请输入账号信息";
    accountTextView.placeholderFont = PingFangSC(15);
    accountTextView.font = PingFangSC(15);
    accountTextView.placeholderTextColor = COLOR_subTextColor;
    accountTextView.cornerRadius = 6;
    accountTextView.bWidth = 0.5;
    accountTextView.bColor = COLOR_subTextColor;
    [self addSubview:accountTextView];
    [accountTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(detailLab.mas_bottom).mas_offset(20);
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(K_SCREENWIDTH - 30);
        make.height.mas_equalTo(230);
    }];

    
    ///!!!:确认
    UIButton *sureBtn = [UIButton lz_buttonTitle:@"确定" titleColor:COLOR_appColor fontSize:15];
    sureBtn.frame = CGRectMake(0, 0, 44, 44);
    [(LZBaseViewController *) LZTool.currentViewController initBarItem:sureBtn withType:1];
    [[sureBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        
    }];
}

@end
