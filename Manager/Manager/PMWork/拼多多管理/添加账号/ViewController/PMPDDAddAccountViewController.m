//
//  PMPDDAddAccountViewController.m
//  Manager
//
//  Created by lvzhao on 2020/8/2.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPDDAddAccountViewController.h"
#import "PMPDDAddAccountView.h"
#import "PMPDDAddAccountViewModel.h"

@interface PMPDDAddAccountViewController ()

@property (nonatomic,strong) PMPDDAddAccountView *addAccountView;

@property (nonatomic,strong) PMPDDAddAccountViewModel *viewModel;

@end

@implementation PMPDDAddAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"账号批量添加";
    [self setupUI];
    // Do any additional setup after loading the view.
}

- (void)setupUI{
    [self.view addSubview:self.addAccountView];
    [self.addAccountView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    ///!!!:确认
    UIButton *sureBtn = [UIButton lz_buttonTitle:@"确定" titleColor:COLOR_appColor fontSize:15];
    sureBtn.frame = CGRectMake(0, 0, 44, 44);
    [self initBarItem:sureBtn withType:1];
    [sureBtn addTarget:self action:@selector(sureClick:) forControlEvents:UIControlEventTouchUpInside];
}


//确认
- (void)sureClick:(UIButton *)sender{
    
    

    
}


#pragma mark -懒加载
- (PMPDDAddAccountView *)addAccountView{
    if(!_addAccountView){
        _addAccountView = [[PMPDDAddAccountView alloc] initViewModel:self.viewModel];
    }
    return _addAccountView;
}

- (PMPDDAddAccountViewModel *)viewModel{
    if(!_viewModel) {
        _viewModel = [[PMPDDAddAccountViewModel alloc] init];
    }
    return _viewModel;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
