//
//  PMPDDManagerView.m
//  Manager
//
//  Created by lvzhao on 2020/8/2.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPDDManagerView.h"
#import "CKSlideMenu.h"
#import "LZBaseViewController.h"
#import "PMPDDManagerViewModel.h"


@interface PMPDDManagerView()<CKSlideMenuDelegate>

@property (nonatomic,strong) PMPDDManagerViewModel *viewModel;

@end

@implementation PMPDDManagerView

- (instancetype)initViewModel:(LZBaseViewModel *)viewModel
{
    self = [super initViewModel:viewModel];
    if (self) {
        self.viewModel = (PMPDDManagerViewModel *)viewModel;
        
        [self setupView];
    }
    return self;
}

#pragma mark 创建UI
- (void) setupView{
    
    NSArray *titles = @[@"拼多多商户管理",@"拼多多商品管理",@"拼多多买手管理",@"拼多多查手管理",@"拼多多订单管理",@"资金预埋单管理",@"支付率订单管理",@"拼多多工单管理",@"拼多多参数管理"];
    NSMutableArray *viewControllers = [NSMutableArray array];
    
    NSArray *classNames = @[@"PMPDDMerchantViewController",@"PMPDDGoodsViewController",@"PMPDDPersonViewController",@"PMPDDCheckViewController",@"PMPDDOrderViewController",@"PMPDDEmbeddedOderViewController",@"PMPDDPayRateViewController",@"PMPDDRepairOrderViewController",@"PMPDDParamViewController"];
    for (int i = 0; i < classNames.count; i++) {
        Class cts = NSClassFromString(classNames[i]);
        LZBaseViewController *vc = [[cts alloc] init];
        [viewControllers addObject:vc];
    }
    // 创建Menu
    CKSlideMenu *slidMenu = [[CKSlideMenu alloc] initWithFrame:CGRectMake(0, 0, K_SCREENWIDTH, 45) titles:titles controllers:viewControllers];
    slidMenu.backgroundColor = COLOR_nav;
    slidMenu.indicatorStyle = SlideMenuIndicatorStyleFollowText;
    slidMenu.isFixed = NO;
    slidMenu.lazyLoad = YES;
    slidMenu.selectedColor = COLOR_appColor;
    slidMenu.unselectedColor = COLOR_subTextColor;
    slidMenu.indicatorHeight = 2;
    slidMenu.indicatorColor =  COLOR_appColor;
    slidMenu.unFont = PingFangSC_M(17);
    slidMenu.font = PingFangSC_M(17);
    slidMenu.delegate = self;
    slidMenu.bodyScrollView.scrollEnabled = NO;
    [self addSubview:slidMenu];
    slidMenu.bodyFrame = CGRectMake(0,45, K_SCREENWIDTH, K_SCREENHEIGHT -  K_NAVHEIGHT - 45);
}


#pragma mark - CKSlideMenuDelegate
- (void)clickBtnActionIndx:(NSInteger)index{
    
    
    
    
    
}

@end
