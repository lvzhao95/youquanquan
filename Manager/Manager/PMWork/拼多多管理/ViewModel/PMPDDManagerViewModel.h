//
//  PMPDDManagerViewModel.h
//  Manager
//
//  Created by lvzhao on 2020/8/2.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZBaseViewModel.h"
#import "LZTableDataModel.h"
#import "PMPDDMerchantModel.h"
#import "PMPDDGoodsModel.h"
#import "PMPDDPersonModel.h"
#import "PMPDDCheckModel.h"
#import "PMPDDOrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PMPDDManagerViewModel : LZBaseViewModel
@property (nonatomic,assign) NSInteger index;

//表格数据源
@property (nonatomic,strong) LZTableDataModel *dataModel;

//
@property (nonatomic,strong) NSMutableDictionary *paramsDictionary;

@property (nonatomic,strong) NSMutableDictionary *dataDictionary;

///获取总额度
- (void)getOthAccountTotal;

///获取商户管理列表
- (void)getOthAccountList:(BOOL)isFrist;

///获取商品列表
- (void)getGoodsList:(BOOL)isFrist;

///获取买手列表
- (void)getPersonList:(BOOL)isFrist;

///获取查手列表
- (void)getCheckList:(BOOL)isFrist;

///获取订单列表
- (void)getOrderList:(BOOL)isFrist;
///获取订单总数
- (void)getOrderTotal;
@end

NS_ASSUME_NONNULL_END
