//
//  PMPDDManagerViewModel.m
//  Manager
//
//  Created by lvzhao on 2020/8/2.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPDDManagerViewModel.h"


@interface  PMPDDManagerViewModel()

@property (nonatomic,assign) NSInteger pageIndex;

@end

@implementation PMPDDManagerViewModel

- (void)setIndex:(NSInteger)index{
    _index = index;
    switch (index) {
        case 0:
            [self createMerchantTable];
            break;
        case 1:
            [self createGoodsTable];
            break;
        case 2:
            [self createPersonTable];
            break;
        case 3:
            [self createCheckTable];
            break;
        case 4:
            [self createOrderTable];
        default:
            break;
    }
    
    
}

#pragma mark 商户管理
///!!!:创建表格
- (void)createMerchantTable{
    
    [self dataModel];
   /// top数据
   self.dataModel.topLeftDataModel = [[LZTableRightSubDataModel alloc] init];
   self.dataModel.topLeftDataModel.titleString = @"代理人";
   self.dataModel.topLeftDataModel.param = @"order_id";
   self.dataModel.topLeftDataModel.sortStatus = 0;
       
   NSArray *titles = @[@"持有人",@"名称",@"账号",@"余额",@"状态",@"可用",@"地址",@"已售金额",@"预埋金额",@"剩余额度",@"次数",@"通道开通",@"备注信息",@"代理地址",@"直播间",@"店铺ID",@"UID",@"上线时间"];
   NSArray *itemWidthArray = @[@(100),@(110),@(80),@(150),@(80),@(80),@(120),@(130),@(130),@(130),@(80),@(150),@(80),@(80),@(120),@(130),@(130),@(130)];
   NSArray *params = @[@"channel_transfer",@"amount",@"settle_fee",@"",@"transfer_status",@"ext_collection",@"",@"created",@"time_settle",@""];


   NSMutableArray *topSubDataArray = [[NSMutableArray alloc] init];
   for (int i = 0; i < titles.count; i++) {
       LZTableRightSubDataModel *subDataModel = [[LZTableRightSubDataModel alloc]init];
       subDataModel.titleString = titles[i];
       subDataModel.itemHeight = self.dataModel.rowHeight;
//           subDataModel.param = params[i];
       if([titles[i] isEqualToString:@"付款详情"]||
          [titles[i] isEqualToString:@"审核结果"]||
          [titles[i] isEqualToString:@"所属商户"]){
           subDataModel.sortStatus = -1;
       }
       
       [topSubDataArray addObject:subDataModel];
   }
   self.dataModel.leftWidth = 160;
   [self.dataModel.topDataModel.classNames addObjectsFromArray:@[@"PMCustomSortCell"]];
   self.dataModel.topDataModel.setDataMethodName = @"setModelObject:";
   self.dataModel.topDataModel.itemModelArray = topSubDataArray;
   self.dataModel.topDataModel.columnWidthArray = itemWidthArray;
   self.dataModel.totalItemWidth = [[itemWidthArray valueForKeyPath:@"@sum.floatValue"] integerValue];

    
}

- (void)getOthAccountList:(BOOL)isFrist{

    if(isFrist){
        self.pageIndex = 1;
        self.dataModel.leftDataArray = @[];
        self.dataModel.dataArray = @[];
        [self.dataArray removeAllObjects];
    }

    NSString *startTime = [NSString stringWithFormat:@"%@",[NSDate datestrFromDate:[NSDate date] withDateFormat:@"yyyy-MM-dd"]];
    NSString *endTime = [NSString stringWithFormat:@"%@",[NSDate datestrFromDate:[NSDate date] withDateFormat:@"yyyy-MM-dd"]];
//    @"sortColumn":@"",
//    @"sortOrder":@""
    /**
     page: 1
     size: 10
     minBuyNum: 0
     account_type: sell
     oth_type: pdd
     startTime: 2020-08-06
     endTime: 2020-08-06
     */
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{@"page":@(self.pageIndex),
                                                                                    @"size":kPageSize,
                                                                                    @"minBuyNum":@"0",
                                                                                    @"account_type":@"sell",
                                                                                    @"oth_type":@"pdd",
                                                                                    @"startTime":startTime,
                                                                                    @"endTime":endTime}];
    [params addEntriesFromDictionary:self.paramsDictionary];
    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kAccountList params:params success:^(id  _Nullable responseObject) {
        @strongify(self);
        NSArray *dataArray = responseObject[@"list"];
        NSMutableArray * leftTableDataArray = [[NSMutableArray alloc]init];
        NSMutableArray * rowDataArray = [[NSMutableArray alloc]init];
        
        for (int i = 0; i < dataArray.count; i++){
            PMPDDMerchantModel *dealModel = [PMPDDMerchantModel modelWithJSON:dataArray[i]];
            [self.dataArray addObject:dealModel];
            // 左侧列表数据
            LZTableLeftDataModel * model = [[LZTableLeftDataModel alloc]init];
            model.titleString = @"未知";
            [leftTableDataArray addObject:model];

            //右侧列表数据
            LZTableRightDataModel *rightDataModel = [[LZTableRightDataModel alloc]init];
            rightDataModel.itemHeight = self.dataModel.rowHeight;

            ///右侧数据源
            NSMutableArray * rightItemArray = [[NSMutableArray alloc]init];

            NSArray *detail = @[dealModel.post_name?:@"",
                                dealModel.account_name?:@"",
                                dealModel.account_no?:@"",
                                dealModel.account_bal?:@"",
                                dealModel.login_status?:@"",
                                dealModel.login_status?:@"",
                                dealModel.city?:@"",
                                dealModel.succ_num?:@"",
                                dealModel.succ_num ? :@"",
                                dealModel.limit_amount?:@"",
                                dealModel.use_num?:@"",
                                @"通道开关",
                                dealModel.remarks?:@"",
                                dealModel.agency?:@"",
                                dealModel.is_del?:@"",
                                dealModel.mall_id?:@"",
                                dealModel.account_uid?:@"",
                                dealModel.create_date?:@""];
            for ( int j = 0; j < detail.count; j++) {
                LZTableRightSubDataModel *itemModel = [[LZTableRightSubDataModel alloc]init];
                itemModel.titleString = detail[j];
                //特殊处理
                if([itemModel.titleString isEqualToString:@"已支付"]
                   ||[itemModel.titleString isEqualToString:@"成功"]){
                        itemModel.textColor = UIColorHex(0x0091FF);

                } else if ([itemModel.titleString isEqualToString:@"未支付/取消"]
                           ||[itemModel.titleString isEqualToString:@"失败"]){
                    itemModel.textColor = UIColorHex(0xFA6400);
                }
                itemModel.itemHeight = self.dataModel.rowHeight;
                [rightItemArray addObject:itemModel];
            }
            rightDataModel.columnWidthArray = self.dataModel.topDataModel.columnWidthArray;
            rightDataModel.itemModelArray = rightItemArray;
            [rowDataArray addObject:rightDataModel];

            rightDataModel.setDataMethodName = @"setModelObject:";
            [rightDataModel.classNames addObjectsFromArray:@[@"PMCustomPayCell"]];
        }

        NSMutableArray *mutableLeftDataArray = [[NSMutableArray alloc] initWithArray:self.dataModel.leftDataArray];
        NSMutableArray *mutableRowDataArray = [[NSMutableArray alloc] initWithArray:self.dataModel.dataArray];

        [mutableLeftDataArray addObjectsFromArray:leftTableDataArray];
        [mutableRowDataArray addObjectsFromArray:rowDataArray];

        self.dataModel.leftDataArray = mutableLeftDataArray;
        self.dataModel.dataArray = mutableRowDataArray;
        ///回调
        if(dataArray.count < [kPageSize intValue]){
            [self.reloadSubject sendNext:@(0)];

        } else {
            self.pageIndex ++;
            [self.reloadSubject sendNext:@(1)];
        }
    } failure:^(NSError * _Nullable error) {
            @strongify(self);
          [self.reloadSubject sendNext:@(1)];
    }];
}


///获取总额度
- (void)getOthAccountTotal{

    NSString *startTime = [NSString stringWithFormat:@"%@",[NSDate datestrFromDate:[NSDate date] withDateFormat:@"yyyy-MM-dd"]];
    NSString *endTime = [NSString stringWithFormat:@"%@",[NSDate datestrFromDate:[NSDate date] withDateFormat:@"yyyy-MM-dd"]];
    NSDictionary *params = @{@"account_type":@"sell",
                             @"oth_type":@"pdd",
                             @"startTime":startTime,
                             @"endTime":endTime};
    
    [LZNetworkingManager lz_request:@"post" url:kAccountTotal params:params success:^(id  _Nullable responseObject) {
        if([responseObject[kResultStatus] isEqualToString:kCode]){
            self.dataDictionary = [[NSMutableDictionary alloc] initWithDictionary:responseObject[@"sum"]];
        }
        [self.reloadSubject sendNext:@(4)];
    } failure:^(NSError * _Nullable error) {}];
    
}

#pragma mark 商品管理
///!!!:创建表格
- (void)createGoodsTable{
    
    [self dataModel];
   /// top数据
   self.dataModel.topLeftDataModel = [[LZTableRightSubDataModel alloc] init];
   self.dataModel.topLeftDataModel.titleString = @"代理人";
   self.dataModel.topLeftDataModel.param = @"order_id";
   self.dataModel.topLeftDataModel.sortStatus = 0;
       
   NSArray *titles = @[@"归属商户",@"商品标题",@"是否可用",@"价格",@"团购价",@"商品ID",@"SKU_ID",@"分组ID"];
   NSArray *itemWidthArray = @[@(100),@(110),@(80),@(150),@(80),@(80),@(120),@(130)];
   NSArray *params = @[@"channel_transfer",@"amount",@"settle_fee",@"",@"transfer_status",@"ext_collection",@"",@"created",@"time_settle",@""];


   NSMutableArray *topSubDataArray = [[NSMutableArray alloc] init];
   for (int i = 0; i < titles.count; i++) {
       LZTableRightSubDataModel *subDataModel = [[LZTableRightSubDataModel alloc]init];
       subDataModel.titleString = titles[i];
       subDataModel.itemHeight = self.dataModel.rowHeight;
//           subDataModel.param = params[i];
       if([titles[i] isEqualToString:@"付款详情"]||
          [titles[i] isEqualToString:@"审核结果"]||
          [titles[i] isEqualToString:@"所属商户"]){
           subDataModel.sortStatus = -1;
       }
       
       [topSubDataArray addObject:subDataModel];
   }
   self.dataModel.leftWidth = 160;
   [self.dataModel.topDataModel.classNames addObjectsFromArray:@[@"PMCustomSortCell"]];
   self.dataModel.topDataModel.setDataMethodName = @"setModelObject:";
   self.dataModel.topDataModel.itemModelArray = topSubDataArray;
   self.dataModel.topDataModel.columnWidthArray = itemWidthArray;
   self.dataModel.totalItemWidth = [[itemWidthArray valueForKeyPath:@"@sum.floatValue"] integerValue];

    
}

- (void)getGoodsList:(BOOL)isFrist{

    if(isFrist){
        self.pageIndex = 1;
        self.dataModel.leftDataArray = @[];
        self.dataModel.dataArray = @[];
        [self.dataArray removeAllObjects];
    }
    /**
     page: 1
     size: 10
     minBuyNum: 0
     account_type: sell
     oth_type: pdd
     startTime: 2020-08-06
     endTime: 2020-08-06
     */
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{@"page":@(self.pageIndex),
                                                                                    @"size":kPageSize,
                                                                                    @"oth_type":@"pdd"}];
    [params addEntriesFromDictionary:self.paramsDictionary];
    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kGetOthGoodsList params:params success:^(id  _Nullable responseObject) {
        @strongify(self);
        NSArray *dataArray = responseObject[@"list"];
        NSMutableArray * leftTableDataArray = [[NSMutableArray alloc]init];
        NSMutableArray * rowDataArray = [[NSMutableArray alloc]init];
        
        for (int i = 0; i < dataArray.count; i++){
            PMPDDGoodsModel *dealModel = [PMPDDGoodsModel modelWithJSON:dataArray[i]];
            [self.dataArray addObject:dealModel];
            // 左侧列表数据
            LZTableLeftDataModel * model = [[LZTableLeftDataModel alloc]init];
            model.titleString = @"未知";
            [leftTableDataArray addObject:model];

            //右侧列表数据
            LZTableRightDataModel *rightDataModel = [[LZTableRightDataModel alloc]init];
            rightDataModel.itemHeight = self.dataModel.rowHeight;

            ///右侧数据源
            NSMutableArray * rightItemArray = [[NSMutableArray alloc]init];

            NSArray *detail = @[@"未知",
                                dealModel.goods_title?:@"",
                                dealModel.can_use?:@"",
                                dealModel.amount?:@"",
                                dealModel.group_amount?:@"",
                                dealModel.goods_id?:@"",
                                dealModel.sku_id?:@"",
                                dealModel.group_id ? :@""];
            for ( int j = 0; j < detail.count; j++) {
                LZTableRightSubDataModel *itemModel = [[LZTableRightSubDataModel alloc]init];
                itemModel.titleString = detail[j];
                //特殊处理
                if([itemModel.titleString isEqualToString:@"已支付"]
                   ||[itemModel.titleString isEqualToString:@"成功"]){
                        itemModel.textColor = UIColorHex(0x0091FF);

                } else if ([itemModel.titleString isEqualToString:@"未支付/取消"]
                           ||[itemModel.titleString isEqualToString:@"失败"]){
                    itemModel.textColor = UIColorHex(0xFA6400);
                }
                itemModel.itemHeight = self.dataModel.rowHeight;
                [rightItemArray addObject:itemModel];
            }
            rightDataModel.columnWidthArray = self.dataModel.topDataModel.columnWidthArray;
            rightDataModel.itemModelArray = rightItemArray;
            [rowDataArray addObject:rightDataModel];

            rightDataModel.setDataMethodName = @"setModelObject:";
            [rightDataModel.classNames addObjectsFromArray:@[@"PMCustomPayCell"]];
        }

        NSMutableArray *mutableLeftDataArray = [[NSMutableArray alloc] initWithArray:self.dataModel.leftDataArray];
        NSMutableArray *mutableRowDataArray = [[NSMutableArray alloc] initWithArray:self.dataModel.dataArray];

        [mutableLeftDataArray addObjectsFromArray:leftTableDataArray];
        [mutableRowDataArray addObjectsFromArray:rowDataArray];

        self.dataModel.leftDataArray = mutableLeftDataArray;
        self.dataModel.dataArray = mutableRowDataArray;
        ///回调
        if(dataArray.count < [kPageSize intValue]){
            [self.reloadSubject sendNext:@(0)];

        } else {
            self.pageIndex ++;
            [self.reloadSubject sendNext:@(1)];
        }
    } failure:^(NSError * _Nullable error) {
            @strongify(self);
          [self.reloadSubject sendNext:@(1)];
    }];
}

#pragma mark 买手管理
///!!!:创建表格
- (void)createPersonTable{
    
    [self dataModel];
   /// top数据
   self.dataModel.topLeftDataModel = [[LZTableRightSubDataModel alloc] init];
   self.dataModel.topLeftDataModel.titleString = @"名称";
   self.dataModel.topLeftDataModel.param = @"order_id";
   self.dataModel.topLeftDataModel.sortStatus = 0;
       
   NSArray *titles = @[@"账号",@"状态",@"可用",@"地址",@"剩余额度",@"使用次数",@"成功次数",@"免拼卡数",@"备注信息",@"代理地址",@"UID"];
   NSArray *itemWidthArray = @[@(100),@(110),@(80),@(150),@(80),@(80),@(120),@(130),@(130),@(130),@(130)];
   NSArray *params = @[@"channel_transfer",@"amount",@"settle_fee",@"",@"transfer_status",@"ext_collection",@"",@"created",@"time_settle",@""];


   NSMutableArray *topSubDataArray = [[NSMutableArray alloc] init];
   for (int i = 0; i < titles.count; i++) {
       LZTableRightSubDataModel *subDataModel = [[LZTableRightSubDataModel alloc]init];
       subDataModel.titleString = titles[i];
       subDataModel.itemHeight = self.dataModel.rowHeight;
//           subDataModel.param = params[i];
       if([titles[i] isEqualToString:@"付款详情"]||
          [titles[i] isEqualToString:@"审核结果"]||
          [titles[i] isEqualToString:@"所属商户"]){
           subDataModel.sortStatus = -1;
       }
       
       [topSubDataArray addObject:subDataModel];
   }
   self.dataModel.leftWidth = 160;
   [self.dataModel.topDataModel.classNames addObjectsFromArray:@[@"PMCustomSortCell"]];
   self.dataModel.topDataModel.setDataMethodName = @"setModelObject:";
   self.dataModel.topDataModel.itemModelArray = topSubDataArray;
   self.dataModel.topDataModel.columnWidthArray = itemWidthArray;
   self.dataModel.totalItemWidth = [[itemWidthArray valueForKeyPath:@"@sum.floatValue"] integerValue];

    
}

- (void)getPersonList:(BOOL)isFrist{

    if(isFrist){
        self.pageIndex = 1;
        self.dataModel.leftDataArray = @[];
        self.dataModel.dataArray = @[];
        [self.dataArray removeAllObjects];
    }
    /**
     page: 1
     size: 10
     minBuyNum: 0
     account_type: sell
     oth_type: pdd
     startTime: 2020-08-06
     endTime: 2020-08-06
     */
    NSString *startTime = [NSString stringWithFormat:@"%@",[NSDate datestrFromDate:[NSDate date] withDateFormat:@"yyyy-MM-dd"]];
    NSString *endTime = [NSString stringWithFormat:@"%@",[NSDate datestrFromDate:[NSDate date] withDateFormat:@"yyyy-MM-dd"]];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{@"page":@(self.pageIndex),
                                                                                    @"size":kPageSize,
                                                                                    @"minBuyNum":@"0",
                                                                                    @"startTime":startTime?:@"",
                                                                                    @"endTime":endTime?:@"",
                                                                                    @"account_type":@"buy",
                                                                                    @"oth_type":@"pdd"}];
    [params addEntriesFromDictionary:self.paramsDictionary];
    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kAccountList params:params success:^(id  _Nullable responseObject) {
        @strongify(self);
        NSArray *dataArray = responseObject[@"list"];
        NSMutableArray * leftTableDataArray = [[NSMutableArray alloc]init];
        NSMutableArray * rowDataArray = [[NSMutableArray alloc]init];
        
        for (int i = 0; i < dataArray.count; i++){
            PMPDDPersonModel *dealModel = [PMPDDPersonModel modelWithJSON:dataArray[i]];
            [self.dataArray addObject:dealModel];
            // 左侧列表数据
            LZTableLeftDataModel * model = [[LZTableLeftDataModel alloc]init];
            model.titleString = dealModel.account_name;
            [leftTableDataArray addObject:model];

            //右侧列表数据
            LZTableRightDataModel *rightDataModel = [[LZTableRightDataModel alloc]init];
            rightDataModel.itemHeight = self.dataModel.rowHeight;

            ///右侧数据源
            NSMutableArray * rightItemArray = [[NSMutableArray alloc]init];

            NSArray *detail = @[dealModel.account_no?:@"",
                                dealModel.login_status?:@"",
                                dealModel.can_use?:@"",
                                dealModel.home?:@"",
                                dealModel.limit_amount?:@"",
                                dealModel.use_num?:@"",
                                dealModel.succ_num?:@"",
                                dealModel.oth_type ? :@"",
                                dealModel.oth_type ? :@"",
                                dealModel.agency?:@"",
                                dealModel.account_uid];
            for ( int j = 0; j < detail.count; j++) {
                LZTableRightSubDataModel *itemModel = [[LZTableRightSubDataModel alloc]init];
                itemModel.titleString = detail[j];
                //特殊处理
                if([itemModel.titleString isEqualToString:@"已支付"]
                   ||[itemModel.titleString isEqualToString:@"成功"]){
                        itemModel.textColor = UIColorHex(0x0091FF);

                } else if ([itemModel.titleString isEqualToString:@"未支付/取消"]
                           ||[itemModel.titleString isEqualToString:@"失败"]){
                    itemModel.textColor = UIColorHex(0xFA6400);
                }
                itemModel.itemHeight = self.dataModel.rowHeight;
                [rightItemArray addObject:itemModel];
            }
            rightDataModel.columnWidthArray = self.dataModel.topDataModel.columnWidthArray;
            rightDataModel.itemModelArray = rightItemArray;
            [rowDataArray addObject:rightDataModel];

            rightDataModel.setDataMethodName = @"setModelObject:";
            [rightDataModel.classNames addObjectsFromArray:@[@"PMCustomPayCell"]];
        }

        NSMutableArray *mutableLeftDataArray = [[NSMutableArray alloc] initWithArray:self.dataModel.leftDataArray];
        NSMutableArray *mutableRowDataArray = [[NSMutableArray alloc] initWithArray:self.dataModel.dataArray];

        [mutableLeftDataArray addObjectsFromArray:leftTableDataArray];
        [mutableRowDataArray addObjectsFromArray:rowDataArray];

        self.dataModel.leftDataArray = mutableLeftDataArray;
        self.dataModel.dataArray = mutableRowDataArray;
        ///回调
        if(dataArray.count < [kPageSize intValue]){
            [self.reloadSubject sendNext:@(0)];

        } else {
            self.pageIndex ++;
            [self.reloadSubject sendNext:@(1)];
        }
    } failure:^(NSError * _Nullable error) {
            @strongify(self);
          [self.reloadSubject sendNext:@(1)];
    }];
}


#pragma mark 查手管理
///!!!:创建表格
- (void)createCheckTable{
    
    [self dataModel];
   /// top数据
   self.dataModel.topLeftDataModel = [[LZTableRightSubDataModel alloc] init];
   self.dataModel.topLeftDataModel.titleString = @"名称";
   self.dataModel.topLeftDataModel.param = @"order_id";
   self.dataModel.topLeftDataModel.sortStatus = 0;
       
   NSArray *titles = @[@"账号",@"状态",@"可用",@"地址",@"次数",@"备注信息",@"代理地址",@"UID"];
   NSArray *itemWidthArray = @[@(100),@(110),@(80),@(150),@(80),@(80),@(120),@(130)];
   NSArray *params = @[@"channel_transfer",@"amount",@"settle_fee",@"",@"transfer_status",@"ext_collection",@"",@"created",@"time_settle",@""];


   NSMutableArray *topSubDataArray = [[NSMutableArray alloc] init];
   for (int i = 0; i < titles.count; i++) {
       LZTableRightSubDataModel *subDataModel = [[LZTableRightSubDataModel alloc]init];
       subDataModel.titleString = titles[i];
       subDataModel.itemHeight = self.dataModel.rowHeight;
//           subDataModel.param = params[i];
       if([titles[i] isEqualToString:@"付款详情"]||
          [titles[i] isEqualToString:@"审核结果"]||
          [titles[i] isEqualToString:@"所属商户"]){
           subDataModel.sortStatus = -1;
       }
       
       [topSubDataArray addObject:subDataModel];
   }
   self.dataModel.leftWidth = 160;
   [self.dataModel.topDataModel.classNames addObjectsFromArray:@[@"PMCustomSortCell"]];
   self.dataModel.topDataModel.setDataMethodName = @"setModelObject:";
   self.dataModel.topDataModel.itemModelArray = topSubDataArray;
   self.dataModel.topDataModel.columnWidthArray = itemWidthArray;
   self.dataModel.totalItemWidth = [[itemWidthArray valueForKeyPath:@"@sum.floatValue"] integerValue];

    
}

- (void)getCheckList:(BOOL)isFrist{

    if(isFrist){
        self.pageIndex = 1;
        self.dataModel.leftDataArray = @[];
        self.dataModel.dataArray = @[];
        [self.dataArray removeAllObjects];
    }
    /**
     page: 1
     size: 10
     minBuyNum: 0
     account_type: sell
     oth_type: pdd
     startTime: 2020-08-06
     endTime: 2020-08-06
     */
    NSString *startTime = [NSString stringWithFormat:@"%@",[NSDate datestrFromDate:[NSDate date] withDateFormat:@"yyyy-MM-dd"]];
    NSString *endTime = [NSString stringWithFormat:@"%@",[NSDate datestrFromDate:[NSDate date] withDateFormat:@"yyyy-MM-dd"]];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{@"page":@(self.pageIndex),
                                                                                    @"size":kPageSize,
                                                                                    @"minBuyNum":@"0",
                                                                                    @"startTime":startTime?:@"",
                                                                                    @"endTime":endTime?:@"",
                                                                                    @"account_type":@"qry",
                                                                                    @"oth_type":@"pdd"}];
    [params addEntriesFromDictionary:self.paramsDictionary];
    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kAccountList params:params success:^(id  _Nullable responseObject) {
        @strongify(self);
        NSArray *dataArray = responseObject[@"list"];
        NSMutableArray * leftTableDataArray = [[NSMutableArray alloc]init];
        NSMutableArray * rowDataArray = [[NSMutableArray alloc]init];
        
        for (int i = 0; i < dataArray.count; i++){
            PMPDDCheckModel *dealModel = [PMPDDCheckModel modelWithJSON:dataArray[i]];
            [self.dataArray addObject:dealModel];
            // 左侧列表数据
            LZTableLeftDataModel * model = [[LZTableLeftDataModel alloc]init];
            model.titleString = @"";
            [leftTableDataArray addObject:model];

            //右侧列表数据
            LZTableRightDataModel *rightDataModel = [[LZTableRightDataModel alloc]init];
            rightDataModel.itemHeight = self.dataModel.rowHeight;

            ///右侧数据源
            NSMutableArray * rightItemArray = [[NSMutableArray alloc]init];

            NSArray *detail = @[dealModel.account_no?:@"",
                                dealModel.login_status?:@"",
                                dealModel.can_use?:@"",
                                dealModel.account_type?:@"",
                                dealModel.use_num?:@"",
                                dealModel.succ_num?:@"",
                                dealModel.agency?:@"",
                                dealModel.oth_type];
            for ( int j = 0; j < detail.count; j++) {
                LZTableRightSubDataModel *itemModel = [[LZTableRightSubDataModel alloc]init];
                itemModel.titleString = detail[j];
                //特殊处理
                if([itemModel.titleString isEqualToString:@"已支付"]
                   ||[itemModel.titleString isEqualToString:@"成功"]){
                        itemModel.textColor = UIColorHex(0x0091FF);

                } else if ([itemModel.titleString isEqualToString:@"未支付/取消"]
                           ||[itemModel.titleString isEqualToString:@"失败"]){
                    itemModel.textColor = UIColorHex(0xFA6400);
                }
                itemModel.itemHeight = self.dataModel.rowHeight;
                [rightItemArray addObject:itemModel];
            }
            rightDataModel.columnWidthArray = self.dataModel.topDataModel.columnWidthArray;
            rightDataModel.itemModelArray = rightItemArray;
            [rowDataArray addObject:rightDataModel];

            rightDataModel.setDataMethodName = @"setModelObject:";
            [rightDataModel.classNames addObjectsFromArray:@[@"PMCustomPayCell"]];
        }

        NSMutableArray *mutableLeftDataArray = [[NSMutableArray alloc] initWithArray:self.dataModel.leftDataArray];
        NSMutableArray *mutableRowDataArray = [[NSMutableArray alloc] initWithArray:self.dataModel.dataArray];

        [mutableLeftDataArray addObjectsFromArray:leftTableDataArray];
        [mutableRowDataArray addObjectsFromArray:rowDataArray];

        self.dataModel.leftDataArray = mutableLeftDataArray;
        self.dataModel.dataArray = mutableRowDataArray;
        ///回调
        if(dataArray.count < [kPageSize intValue]){
            [self.reloadSubject sendNext:@(0)];

        } else {
            self.pageIndex ++;
            [self.reloadSubject sendNext:@(1)];
        }
    } failure:^(NSError * _Nullable error) {
            @strongify(self);
          [self.reloadSubject sendNext:@(1)];
    }];
}

#pragma mark 订单管理
///!!!:创建表格
- (void)createOrderTable{
    
    [self dataModel];
   /// top数据
   self.dataModel.topLeftDataModel = [[LZTableRightSubDataModel alloc] init];
   self.dataModel.topLeftDataModel.titleString = @"代理人";
   self.dataModel.topLeftDataModel.param = @"order_id";
   self.dataModel.topLeftDataModel.sortStatus = 0;
       
   NSArray *titles = @[@"订单ID",@"数量",@"价格",@"买手账号",@"商户账号",@"商户名称",@"订单状态",@"快递单号",@"发货时间",@"创建时间",@"拼多多订单号",@"支付后处理",@"是否绑定商品",@"绑商品ID",@"团购",@"团购号",@"商品名称",@"备注"];
   NSArray *itemWidthArray = @[@(100),@(110),@(80),@(150),@(80),@(80),@(120),@(130),@(130),@(130),@(80),@(150),@(80),@(80),@(120),@(130),@(130),@(130)];
   NSArray *params = @[@"channel_transfer",@"amount",@"settle_fee",@"",@"transfer_status",@"ext_collection",@"",@"created",@"time_settle",@""];


   NSMutableArray *topSubDataArray = [[NSMutableArray alloc] init];
   for (int i = 0; i < titles.count; i++) {
       LZTableRightSubDataModel *subDataModel = [[LZTableRightSubDataModel alloc]init];
       subDataModel.titleString = titles[i];
       subDataModel.itemHeight = self.dataModel.rowHeight;
//           subDataModel.param = params[i];
       if([titles[i] isEqualToString:@"付款详情"]||
          [titles[i] isEqualToString:@"审核结果"]||
          [titles[i] isEqualToString:@"所属商户"]){
           subDataModel.sortStatus = -1;
       }
       
       [topSubDataArray addObject:subDataModel];
   }
   self.dataModel.leftWidth = 160;
   [self.dataModel.topDataModel.classNames addObjectsFromArray:@[@"PMCustomSortCell"]];
   self.dataModel.topDataModel.setDataMethodName = @"setModelObject:";
   self.dataModel.topDataModel.itemModelArray = topSubDataArray;
   self.dataModel.topDataModel.columnWidthArray = itemWidthArray;
   self.dataModel.totalItemWidth = [[itemWidthArray valueForKeyPath:@"@sum.floatValue"] integerValue];

    
}

- (void)getOrderList:(BOOL)isFrist{

    if(isFrist){
        self.pageIndex = 1;
        self.dataModel.leftDataArray = @[];
        self.dataModel.dataArray = @[];
        [self.dataArray removeAllObjects];
    }

    NSString *startTime = [NSString stringWithFormat:@"%@",[NSDate datestrFromDate:[NSDate date] withDateFormat:@"yyyy-MM-dd"]];
    NSString *endTime = [NSString stringWithFormat:@"%@",[NSDate datestrFromDate:[NSDate date] withDateFormat:@"yyyy-MM-dd"]];
//    @"sortColumn":@"",
//    @"sortOrder":@""
    /**
     page: 1
     size: 10
     oth_type: pdd
     charge_type: 1
     startTime: 2020-07-01 00:00
     endTime: 2020-08-06 23:59
     */
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{@"page":@(self.pageIndex),
                                                                                    @"size":kPageSize,
                                                                                    @"charge_type":@"1",
                                                                                    @"oth_type":@"pdd",
                                                                                    @"startTime":@"2020-07-01 00:00",
                                                                                    @"endTime":@"2020-08-06 23:59"}];
    [params addEntriesFromDictionary:self.paramsDictionary];
    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kGetOthChargeList params:params success:^(id  _Nullable responseObject) {
        @strongify(self);
        NSArray *dataArray = responseObject[@"list"];
        NSMutableArray * leftTableDataArray = [[NSMutableArray alloc]init];
        NSMutableArray * rowDataArray = [[NSMutableArray alloc]init];
        
        for (int i = 0; i < dataArray.count; i++){
            NSDictionary *dataDictionary = dataArray[i];
            PMPDDOrderModel *dealModel = [PMPDDOrderModel modelWithJSON:dataDictionary];
            [self.dataArray addObject:dealModel];
            // 左侧列表数据
            LZTableLeftDataModel * model = [[LZTableLeftDataModel alloc]init];
            model.titleString = @"未知";
            [leftTableDataArray addObject:model];

            //右侧列表数据
            LZTableRightDataModel *rightDataModel = [[LZTableRightDataModel alloc]init];
            rightDataModel.itemHeight = self.dataModel.rowHeight;

            ///右侧数据源
            NSMutableArray * rightItemArray = [[NSMutableArray alloc]init];

            NSArray *detail = @[dealModel.order_id?:@"",
                                dealModel.buy_num?:@"",
                                dealModel.amount?:@"",
                                dealModel.buy_no?:@"",
                                dealModel.sell_no?:@"",
                                dealModel.sell_name?:@"",
                                dealModel.channel_code?:@"",
                                dealModel.channel_code?:@"",
                                dealModel.send_date ? :@"",
                                dealModel.create_date?:@"",
                                dealModel.order_sn?:@"",
                                @"支付后处理",
                                dealModel.bind_goods_id?:@"",
                                dealModel.is_group?:@"",
                                dealModel.goods_id?:@"",
                                dealModel.goods_title?:@"",
                                dealModel.oth_type?:@""];
            for ( int j = 0; j < detail.count; j++) {
                LZTableRightSubDataModel *itemModel = [[LZTableRightSubDataModel alloc]init];
                itemModel.titleString = detail[j];
                //特殊处理
                if([itemModel.titleString isEqualToString:@"已支付"]
                   ||[itemModel.titleString isEqualToString:@"成功"]){
                        itemModel.textColor = UIColorHex(0x0091FF);

                } else if ([itemModel.titleString isEqualToString:@"未支付/取消"]
                           ||[itemModel.titleString isEqualToString:@"失败"]){
                    itemModel.textColor = UIColorHex(0xFA6400);
                }
                itemModel.itemHeight = self.dataModel.rowHeight;
                [rightItemArray addObject:itemModel];
            }
            rightDataModel.columnWidthArray = self.dataModel.topDataModel.columnWidthArray;
            rightDataModel.itemModelArray = rightItemArray;
            [rowDataArray addObject:rightDataModel];

            rightDataModel.setDataMethodName = @"setModelObject:";
            [rightDataModel.classNames addObjectsFromArray:@[@"PMCustomPayCell"]];
        }

        NSMutableArray *mutableLeftDataArray = [[NSMutableArray alloc] initWithArray:self.dataModel.leftDataArray];
        NSMutableArray *mutableRowDataArray = [[NSMutableArray alloc] initWithArray:self.dataModel.dataArray];

        [mutableLeftDataArray addObjectsFromArray:leftTableDataArray];
        [mutableRowDataArray addObjectsFromArray:rowDataArray];

        self.dataModel.leftDataArray = mutableLeftDataArray;
        self.dataModel.dataArray = mutableRowDataArray;
        ///回调
        if(dataArray.count < [kPageSize intValue]){
            [self.reloadSubject sendNext:@(0)];

        } else {
            self.pageIndex ++;
            [self.reloadSubject sendNext:@(1)];
        }
    } failure:^(NSError * _Nullable error) {
            @strongify(self);
          [self.reloadSubject sendNext:@(1)];
    }];
}


///获取总额度
- (void)getOrderTotal{

    NSString *startTime = [NSString stringWithFormat:@"%@",[NSDate datestrFromDate:[NSDate date] withDateFormat:@"yyyy-MM-dd"]];
    NSString *endTime = [NSString stringWithFormat:@"%@",[NSDate datestrFromDate:[NSDate date] withDateFormat:@"yyyy-MM-dd"]];
    NSDictionary *params = @{@"charge_type":@"1",
                             @"oth_type":@"pdd",
                             @"startTime":@"2020-07-01 00:00",
                             @"endTime":@"2020-08-06 23:59"};
    
    [LZNetworkingManager lz_request:@"post" url:kGetOthChargeTotal params:params success:^(id  _Nullable responseObject) {
        if([responseObject[kResultStatus] isEqualToString:kCode]){
            self.dataDictionary = [[NSMutableDictionary alloc] initWithDictionary:responseObject[@"sum"]];
        }
        [self.reloadSubject sendNext:@(4)];
    } failure:^(NSError * _Nullable error) {}];
    
}


- (LZTableDataModel *)dataModel{
    if(!_dataModel){
        _dataModel = [[LZTableDataModel alloc] init];
        _dataModel.leftDataArray = @[];
        _dataModel.dataArray = @[];
    }
    return _dataModel;
}

- (NSMutableDictionary *)paramsDictionary{
    if(!_paramsDictionary){
        _paramsDictionary = [[NSMutableDictionary alloc] init];
    }
    return _paramsDictionary;
}

- (NSMutableDictionary *)dataDictionary{
    if(!_dataDictionary){
        _dataDictionary = [[NSMutableDictionary alloc] init];
    }
    return _dataDictionary;
}

@end
