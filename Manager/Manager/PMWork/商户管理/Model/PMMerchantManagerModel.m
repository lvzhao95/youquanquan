//
//  PMMerchantManagerModel.m
//  Manager
//
//  Created by lvzhao on 2020/8/1.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMMerchantManagerModel.h"

@implementation PMMerchantManagerModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"modelid" : @"id"};
}

- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic{
    
    
    self.amount_useable = [NSString stringWithFormat:@"￥ %@",[self.amount_useable yw_stringByDividingBy:@"100"]];
    self.amount_freeze = [NSString stringWithFormat:@"￥ %@",[self.amount_freeze yw_stringByDividingBy:@"100"]];

    
    NSString *type = dic[@"type"];
    if([type isEqualToString:@"MERCHANT"]){
        self.type = @"普通商户";
    } else if([type isEqualToString:@"AGENT"]){
        self.type = @"代理商";
    }

    
    NSString *status = dic[@"status"];
    if([status isEqualToString:@"WAIT"]){
        self.status = @"待审核";
    } else if ([status isEqualToString:@"NORMAL"]){
        self.status = @"审核通过";
    } else if ([status isEqualToString:@"AUDIT"]){
        self.status = @"审核中";
    } else if ([status isEqualToString:@"REGJECT"]){
        self.status = @"审核未通过";
    } else if ([status isEqualToString:@"CLOSE"]){
        self.status = @"已禁用";
    }

    return YES;
}

@end
