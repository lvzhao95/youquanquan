//
//  PMMerchantManagerModel.h
//  Manager
//
//  Created by lvzhao on 2020/8/1.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PMMerchantManagerModel : NSObject


@property (nonatomic,strong) NSString *modelid;

@property (nonatomic,strong) NSString *merchant_no;
@property (nonatomic,strong) NSString *type;
@property (nonatomic,strong) NSString *company_name;
@property (nonatomic,strong) NSString *amount_useable;
@property (nonatomic,strong) NSString *amount_freeze;
@property (nonatomic,strong) NSString *contact_name;
@property (nonatomic,strong) NSString *contact_telephone;
@property (nonatomic,strong) NSString *status;

@property (nonatomic,strong) NSString *create_date;
@property (nonatomic,strong) NSString *audit_date;
@property (nonatomic,strong) NSString *open_bank;
@property (nonatomic,strong) NSString *open_bank_detail;
@property (nonatomic,strong) NSString *open_bank_user;
@property (nonatomic,strong) NSString *account_no;





@end

NS_ASSUME_NONNULL_END
