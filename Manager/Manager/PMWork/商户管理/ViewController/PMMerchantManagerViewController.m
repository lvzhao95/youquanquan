//
//  PMMerchantManagerViewController.m
//  Manager
//
//  Created by lvzhao on 2020/8/1.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMMerchantManagerViewController.h"
#import "PMMerchantManagerView.h"
#import "PMMerchantManagerViewModel.h"
#import "PMScreenDownView.h"

@interface PMMerchantManagerViewController ()
 
@property (nonatomic,strong) PMMerchantManagerViewModel *viewModel;

@property (nonatomic,strong) PMMerchantManagerView *managerView;

@end

@implementation PMMerchantManagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    
    self.title = @"商户列表";
    
}

- (void)setupUI{
    
    [self.view addSubview:self.managerView];
    [self.managerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];

}


#pragma mark -懒加载
- (PMMerchantManagerView *)managerView{
    if(!_managerView) {
        _managerView = [[PMMerchantManagerView alloc] initViewModel:self.viewModel];
    }
    return _managerView;
}

- (PMMerchantManagerViewModel *)viewModel{
    if(!_viewModel) {
        _viewModel = [[PMMerchantManagerViewModel alloc] init];
    }
    return _viewModel;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
