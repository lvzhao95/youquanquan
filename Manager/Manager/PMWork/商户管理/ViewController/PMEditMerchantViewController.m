//
//  PMEditMerchantViewController.m
//  Manager
//
//  Created by lvzhao on 2020/8/1.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMEditMerchantViewController.h"
#import "PMMerchantManagerViewModel.h"
#import "PMChannelTextField.h"


@interface PMEditMerchantViewController ()

@property (nonatomic,strong) PMMerchantManagerViewModel *viewModel;
@property (nonatomic,strong) NSMutableDictionary *dataDcitionry;

@end

@implementation PMEditMerchantViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"商户资料";
    self.view.backgroundColor = UIColor.whiteColor;
    [self setupUI];
}

- (void) setupUI{
    
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [self.view addSubview:scrollView];
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.right.bottom.mas_equalTo(0);
    }];

    //内容
    UIView *contentView = [[UIView alloc] init];
    [scrollView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.and.right.equalTo(scrollView).with.insets(UIEdgeInsetsZero);
        make.width.equalTo(scrollView);

    }];

    NSMutableArray *merchantViews = [[NSMutableArray alloc] init];
    UIView *lastMerchantView = nil;
    
    PMChannelTextField *merchantTypeView = nil;
    PMChannelTextField *depositBankView = nil;
    
    NSArray *titles = @[@"* 商户类型",@"* 企业名称",@"* 联系人",@"* 联系电话",@"* 登录密码",@"开户银行",@"支行名称",@"持卡人姓名",@"银行卡号"];
    NSArray *placeholders = @[@"",@"请输入企业名称",@"请输入商户联系人姓名",@"请输入商户联系电话",@"请设置商户登录密码",@"",@"请输入银行卡所属支行名称",@"请输入银行卡开户人姓名",@"请输入银行卡号码"];
    NSArray *keys = @[@"",@"company_name",@"contact_name",@"contact_telephone",@"loginpwd",@"",@"open_bank_detail",@"open_bank_user",@"account_no"];
    
    
    self.dataDcitionry = [[NSMutableDictionary alloc] init];
    self.dataDcitionry[@"single"] = @(1);
    NSArray *contents = nil;
    ///编辑
    if(self.merchantModel){
            contents = @[self.merchantModel.type,
                        self.merchantModel.company_name?:@"",
                        self.merchantModel.contact_name?:@"",
                        self.merchantModel.contact_telephone?:@"",
                        @"",
                        self.merchantModel.open_bank?:@"",
                        self.merchantModel.open_bank_detail?:@"",
                        self.merchantModel.open_bank_user?:@"",
                        self.merchantModel.account_no?:@""];
        self.dataDcitionry[@"type"] = [self.merchantModel.type isEqualToString:@"代理商"] ? @"AGENT":@"MERCHANT";
        self.dataDcitionry[@"open_bank"] = self.merchantModel.open_bank?:@"";
        self.dataDcitionry[@"merchant_id"] = self.merchantModel.modelid?:@"";

    }
    for (int i = 0; i < titles.count; i++){
        PMChannelTextField *textField = [[PMChannelTextField alloc] init];
        textField.title = titles[i];
        textField.key = keys[i];
        textField.placeholder = placeholders[i];
        if(contents){
            textField.textField.text = contents[i];
        }
        
        [self.view addSubview:textField];
        [textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(15);
            make.right.mas_equalTo(-15);
            make.height.mas_equalTo(50);
            if(lastMerchantView){
                make.top.mas_equalTo(lastMerchantView.mas_bottom).mas_offset(10);
            } else {
                make.top.mas_equalTo(5);
            }
        }];
        lastMerchantView = textField;
        [merchantViews addObject:lastMerchantView];
        NSString *title = titles[i];
        if([title containsString:@"商户类型"]){
            merchantTypeView = textField;
        }
        if([title containsString:@"开户银行"]){
            depositBankView = textField;
        }
    }

    
    ///!!!:商户类型
    merchantTypeView.textField.userInteractionEnabled = NO;
    UIButton *merchantTypeBtn = [UIButton lz_buttonTitle:@"" titleColor:COLOR_textColor fontSize:17];
    [merchantTypeBtn setImage:k_imageName(@"pm_next") forState:UIControlStateNormal];
    [merchantTypeView addSubview:merchantTypeBtn];
    [merchantTypeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.top.mas_equalTo(merchantTypeView.textField);
    }];
    merchantTypeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [merchantTypeBtn SG_imagePositionStyle:SGImagePositionStyleRight spacing:5];
    merchantTypeBtn.tag = 100;
    [merchantTypeBtn addTarget:self action:@selector(selectClick:) forControlEvents:UIControlEventTouchUpInside];

    
    ///!!!:开户银行
    depositBankView.textField.userInteractionEnabled = NO;
    UIButton *depositBankBtn = [UIButton lz_buttonTitle:@"" titleColor:COLOR_textColor fontSize:17];
    [depositBankBtn setImage:k_imageName(@"pm_next") forState:UIControlStateNormal];
    [depositBankView addSubview:depositBankBtn];
    [depositBankBtn mas_makeConstraints:^(MASConstraintMaker *make) {
       make.left.right.bottom.top.mas_equalTo(depositBankView.textField);
    }];
    depositBankBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [depositBankBtn SG_imagePositionStyle:SGImagePositionStyleRight spacing:5];
    depositBankBtn.tag = 101;
    [depositBankBtn addTarget:self action:@selector(selectClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [contentView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(lastMerchantView.mas_bottom).mas_offset(34);
    }];
  

    ///!!!: 完成
    UIButton *finishBtn = [UIButton lz_buttonTitle:@"完成" titleColor:COLOR_appColor fontSize:15];
    finishBtn.frame = CGRectMake(0, 0, 44, 44);
    [self initBarItem:finishBtn withType:1];
    @weakify(self);
    [[finishBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);
        
        if([self.dataDcitionry[@"type"] length] == 0){
            showDelayedDismissTitle(@"请选择商户类型", nil);
            return;
        }
        //获取必填的
        for (int i = 0; i < merchantViews.count; i++){
            PMChannelTextField *channelView = merchantViews[i];

            if([channelView.title containsString:@"企业名称"]
               ||[channelView.title containsString:@"联系人"]
               ||[channelView.title containsString:@"联系电话"]
               ||[channelView.title containsString:@"登录密码"]){
                if([channelView.textField.text length] == 0){
                    showDelayedDismissTitle(placeholders[i], nil);
                    return;
                }
            }
            self.dataDcitionry[channelView.key] = channelView.textField.text;
        }
          
        [self.viewModel editMerchantInfo:self.dataDcitionry];
    }];
    

    /**
   merchant_id: f04e85fc-81d7-4563-9edf-d866fe089173
    company_name: 吕布1
    open_bank_user: 吕布21312
    open_bank: 招商银行
    open_bank_detail: 中国支行1231
    account_no: 12312888888888812312
    contact_name: 貂蝉12
    contact_telephone: 110119120
    loginpwd: 123456
    type: MERCHANT
    single: true
     */
    
    
}


- (void)selectClick:(UIButton *)sender{
    
    [self.view endEditing:YES];
    
    switch (sender.tag) {
        case 100:
        {
            NSArray *subTitles = @[@"普通商户",@"代理商"];
            NSArray *keys = @[@"MERCHANT",@"AGENT"];

            [LZToolView showAlertType:LZAlertTypeActionSheet withTitle:@"" message:@"" cancel:@"取消" sure:@"" objectDict:@{@"subTitles":subTitles} cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
                NSInteger index = [object intValue];
                [sender setTitle:subTitles[index] forState:UIControlStateNormal];
                [sender SG_imagePositionStyle:SGImagePositionStyleRight spacing:5];
                self.dataDcitionry[@"type"] = keys[index];
                
            }];
        }
            break;
        case 101:
        {
            
            NSDictionary *publicDictionary =  [[NSDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Public"ofType:@"plist"]];
            NSArray *subTitles = publicDictionary[@"BankNames"];
            [LZToolView showAlertType:LZAlertTypeActionSheet withTitle:@"" message:@"" cancel:@"取消" sure:@"" objectDict:@{@"subTitles":subTitles} cancelBlock:^(id  _Nullable object) {} sureBlock:^(id  _Nullable object) {
                NSInteger index = [object intValue];
                [sender setTitle:subTitles[index] forState:UIControlStateNormal];
                [sender SG_imagePositionStyle:SGImagePositionStyleRight spacing:5];
                self.dataDcitionry[@"open_bank"] = subTitles[index];
            }];

        }
            break;
            
        default:
            break;
    }
    
    
}

- (PMMerchantManagerViewModel *)viewModel{
    if(!_viewModel){
        _viewModel = [[PMMerchantManagerViewModel alloc] init];
        _viewModel.reloadSubject = self.subject;
    }
    return _viewModel;;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
