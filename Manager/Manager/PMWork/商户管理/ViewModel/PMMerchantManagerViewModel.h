//
//  PMMerchantManagerViewModel.h
//  Manager
//
//  Created by lvzhao on 2020/8/1.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZBaseViewModel.h"
#import "LZTableDataModel.h"
#import "PMMerchantManagerModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PMMerchantManagerViewModel : LZBaseViewModel
//表格数据源
@property (nonatomic,strong) LZTableDataModel *dataModel;
//
@property (nonatomic,strong) NSMutableDictionary *paramsDictionary;

///获取商户列表
- (void)getMerchantList:(BOOL)isFrist;

///商户开户
- (void)editMerchantInfo:(NSDictionary *)dataDictionary;

///审核商户
- (void)auditMerchantInfo:(NSDictionary *)dataDictionary;


///禁用/启用
- (void)disableMerchantInfo:(NSDictionary *)dataDictionary;

///删除
- (void)deleteMerchantInfo:(NSDictionary *)dataDictionary;

///更新余额
- (void)updateAmountMerchantInfo:(NSDictionary *)dataDictionary;


///操作金额
- (void)transferAmountMerchantInfo:(NSDictionary *)dataDictionary;


@end

NS_ASSUME_NONNULL_END
