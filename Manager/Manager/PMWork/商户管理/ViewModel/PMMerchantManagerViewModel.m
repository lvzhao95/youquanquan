//
//  PMMerchantManagerViewModel.m
//  Manager
//
//  Created by lvzhao on 2020/8/1.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMMerchantManagerViewModel.h"
@interface  PMMerchantManagerViewModel()

@property (nonatomic,assign) NSInteger pageIndex;

@end

@implementation PMMerchantManagerViewModel
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self createTradeTableData];
    }
    return self;
}

//创建表格
- (void)createTradeTableData{
    
    [self dataModel];
    /// top数据
    self.dataModel.topLeftDataModel = [[LZTableRightSubDataModel alloc] init];
    self.dataModel.topLeftDataModel.titleString = @"商户号";
    self.dataModel.topLeftDataModel.param = @"merchant_no";
    self.dataModel.topLeftDataModel.sortStatus = 0;
    
    NSArray *titles = @[@"类型",@"公司名称",@"可用余额",@"冻结金额",@"商户联系人",@"商户联系电话",@"审核状态",@"申请时间",@"审核时间"];
    NSArray *itemWidthArray = @[@(80),@(120),@(80),@(80),@(130),@(130),@(80),@(140),@(140)];
    NSArray *params = @[@"type",@"",@"amount_useable",@"amount_freeze",@"",@"",@"status",@"create_date",@"audit_date"];


    NSMutableArray *topSubDataArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < titles.count; i++) {
        LZTableRightSubDataModel *subDataModel = [[LZTableRightSubDataModel alloc]init];
        subDataModel.titleString = titles[i];
        subDataModel.itemHeight = self.dataModel.rowHeight;
        subDataModel.param = params[i];
        if([titles[i] isEqualToString:@"公司名称"]||
           [titles[i] isEqualToString:@"商户联系人"]||
           [titles[i] isEqualToString:@"商户联系电话"]){
            subDataModel.sortStatus = -1;
        }
        
        [topSubDataArray addObject:subDataModel];
    }
    self.dataModel.leftWidth = 100;
    [self.dataModel.topDataModel.classNames addObjectsFromArray:@[@"PMCustomSortCell"]];
    self.dataModel.topDataModel.setDataMethodName = @"setModelObject:";
    self.dataModel.topDataModel.itemModelArray = topSubDataArray;
    self.dataModel.topDataModel.columnWidthArray = itemWidthArray;
    self.dataModel.totalItemWidth = [[itemWidthArray valueForKeyPath:@"@sum.floatValue"] integerValue];

}


//获取商户列表
- (void)getMerchantList:(BOOL)isFrist{
    
    if(isFrist){
        self.pageIndex = 1;
        self.dataModel.leftDataArray = @[];
        self.dataModel.dataArray = @[];
        [self.dataArray removeAllObjects];
    }

    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{@"page":@(self.pageIndex),
                                                                                    @"size":kPageSize,
                                                                                    @"is_parent":@(1)}];
    [params addEntriesFromDictionary:self.paramsDictionary];
    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kGetMerchantList params:params success:^(id  _Nullable responseObject) {
        @strongify(self);
        NSArray *dataArray = responseObject[@"list"];
        NSMutableArray * leftTableDataArray = [[NSMutableArray alloc]init];
        NSMutableArray * rowDataArray = [[NSMutableArray alloc]init];
            
        for (int i = 0; i < dataArray.count; i++){
            PMMerchantManagerModel *merchantModel = [PMMerchantManagerModel modelWithJSON:dataArray[i]];
            [self.dataArray addObject:merchantModel];
            // 左侧列表数据
            LZTableLeftDataModel * model = [[LZTableLeftDataModel alloc]init];
            model.titleString = merchantModel.merchant_no;
            [leftTableDataArray addObject:model];

            //右侧列表数据
            LZTableRightDataModel *rightDataModel = [[LZTableRightDataModel alloc]init];
            rightDataModel.itemHeight = self.dataModel.rowHeight;

            ///右侧数据源
            NSMutableArray * rightItemArray = [[NSMutableArray alloc]init];

            NSArray *detail = @[merchantModel.type?:@"",
                                merchantModel.company_name?:@"",
                                merchantModel.amount_useable?:@"",
                                merchantModel.amount_freeze?:@"",
                                merchantModel.contact_name?:@"",
                                merchantModel.contact_telephone?:@"",
                                merchantModel.status?:@"",
                                merchantModel.create_date?:@"",
                                merchantModel.audit_date ? :@""];

            for ( int j = 0; j < detail.count; j++) {
                LZTableRightSubDataModel *itemModel = [[LZTableRightSubDataModel alloc]init];
                itemModel.titleString = detail[j];
                //特殊处理
                if([itemModel.titleString isEqualToString:@"普通商户"]
                   ||[itemModel.titleString isEqualToString:@"待审核"]){
                        itemModel.textColor = UIColorHex(0x0091FF);

                } else if ([itemModel.titleString isEqualToString:@"审核通过"]
                           ||[itemModel.titleString isEqualToString:@"代理商"]
                           ||[itemModel.titleString isEqualToString:@"已禁用"]
                           ){
                    itemModel.textColor = UIColorHex(0xFA6400);
                } else if([itemModel.titleString isEqualToString:@"审核未通过"]){
                    itemModel.textColor = UIColor.redColor;
                }
                
                itemModel.itemHeight = self.dataModel.rowHeight;
                [rightItemArray addObject:itemModel];
            }
            rightDataModel.columnWidthArray = self.dataModel.topDataModel.columnWidthArray;
            rightDataModel.itemModelArray = rightItemArray;
            [rowDataArray addObject:rightDataModel];

            rightDataModel.setDataMethodName = @"setModelObject:";
            [rightDataModel.classNames addObjectsFromArray:@[@"PMCustomPayCell"]];
        }

        NSMutableArray *mutableLeftDataArray = [[NSMutableArray alloc] initWithArray:self.dataModel.leftDataArray];
        NSMutableArray *mutableRowDataArray = [[NSMutableArray alloc] initWithArray:self.dataModel.dataArray];

        [mutableLeftDataArray addObjectsFromArray:leftTableDataArray];
        [mutableRowDataArray addObjectsFromArray:rowDataArray];

        self.dataModel.leftDataArray = mutableLeftDataArray;
        self.dataModel.dataArray = mutableRowDataArray;
        ///回调
        if(dataArray.count < [kPageSize intValue]){
            [self.reloadSubject sendNext:@(0)];

        } else {
            self.pageIndex ++;
            [self.reloadSubject sendNext:@(1)];
        }
        } failure:^(NSError * _Nullable error) {
            @strongify(self);
            [self.reloadSubject sendNext:@(1)];
        }];
}

///商户开户
- (void)editMerchantInfo:(NSDictionary *)dataDictionary{
    
    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kEditMerchant params:dataDictionary success:^(id  _Nullable responseObject) {
        if([responseObject[kResultStatus] isEqualToString:kCode]){
            showDelayedDismissTitle(@"成功", nil);
              dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                  @strongify(self);
                  [self.reloadSubject sendNext:@(1)];
                  [LZTool.currentViewController.navigationController popViewControllerAnimated:YES];
              });
        }
      
    } failure:^(NSError * _Nullable error) {} isLoading:YES isFailTip:YES];
}

///审核商户
- (void)auditMerchantInfo:(NSDictionary *)dataDictionary{
    
    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kAuditMerchant params:dataDictionary success:^(id  _Nullable responseObject) {
        if([responseObject[kResultStatus] isEqualToString:kCode]){
            showDelayedDismissTitle(@"提交成功", nil);
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                @strongify(self);
                [self getMerchantList:YES];
            });
        }
    } failure:^(NSError * _Nullable error) {} isLoading:YES isFailTip:YES];
}

///禁用/启用
- (void)disableMerchantInfo:(NSDictionary *)dataDictionary{
    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kDisableMerchant params:dataDictionary success:^(id  _Nullable responseObject) {
        if([responseObject[kResultStatus] isEqualToString:kCode]){
            showDelayedDismissTitle(@"成功", nil);
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                @strongify(self);
                [self getMerchantList:YES];
            });
        }
    } failure:^(NSError * _Nullable error) {} isLoading:YES isFailTip:YES];
}


///删除
- (void)deleteMerchantInfo:(NSDictionary *)dataDictionary{
    
    @weakify(self);
    [LZNetworkingManager lz_request:@"post" url:kDeleteMerchant params:dataDictionary success:^(id  _Nullable responseObject) {
        if([responseObject[kResultStatus] isEqualToString:kCode]){
            showDelayedDismissTitle(@"成功", nil);
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                @strongify(self);
                [self getMerchantList:YES];
            });
        }
    } failure:^(NSError * _Nullable error) {} isLoading:YES isFailTip:YES];

}

///更新余额
- (void)updateAmountMerchantInfo:(NSDictionary *)dataDictionary{
    
    @weakify(self);
  [LZNetworkingManager lz_request:@"post" url:kUpdateMerchantAmount params:dataDictionary success:^(id  _Nullable responseObject) {
      if([responseObject[kResultStatus] isEqualToString:kCode]){
          dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
              @strongify(self);
              [self getMerchantList:YES];
          });
      }
  } failure:^(NSError * _Nullable error) {} isLoading:YES isFailTip:YES];

    
}


///操作金额
- (void)transferAmountMerchantInfo:(NSDictionary *)dataDictionary{
    
    @weakify(self);
     [LZNetworkingManager lz_request:@"post" url:kTransferAmount params:dataDictionary success:^(id  _Nullable responseObject) {
         if([responseObject[kResultStatus] isEqualToString:kCode]){
             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                 @strongify(self);
                 [self getMerchantList:YES];
             });
         }
     } failure:^(NSError * _Nullable error) {} isLoading:YES isFailTip:YES];
    
    
}

- (LZTableDataModel *)dataModel{
    if(!_dataModel){
        _dataModel = [[LZTableDataModel alloc] init];
        _dataModel.leftDataArray = @[];
        _dataModel.dataArray = @[];
    }
    return _dataModel;
}

- (NSMutableDictionary *)paramsDictionary{
    if(!_paramsDictionary){
        _paramsDictionary = [[NSMutableDictionary alloc] init];
    }
    return _paramsDictionary;
}
@end
