//
//  PMWorkViewController.m
//  Manager
//
//  Created by lvzhao on 2020/7/29.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMWorkViewController.h"
#import "LZOptionView.h"
#import "PMPDDManagerViewController.h"

@interface PMWorkViewController ()

@end

@implementation PMWorkViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = UIColor.whiteColor;
    [self setupView];
    
    [PMPublicManager sharedInstance];

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[PMPublicManager sharedInstance] createPublicData];

}

- (void)setupView{
    
    NSArray *titles = @[@"交易管理",@"商户管理",@"财务分析",@"收银测试",@"通道管理",@"刷手管理"];
    NSArray *imageNames = @[@"pm_jiaoyi",@"pm_shanghu",@"pm_caiwu",@"pm_huanying",@"pm_tongdao",@"pm_shuashou"];
    
    //每个Item宽高
    CGFloat H = 102;
    CGFloat W = kScreenWidth / 3.0;
    //每行列数
    NSInteger rank = 3;
    //每列间距
    CGFloat rankMargin = 0;
    //每行间距
    CGFloat rowMargin = 0;
    //Item索引 ->根据需求改变索引
    NSUInteger index = titles.count;
    
    CGFloat top = 0;
    for (int i = 0 ; i< index; i++) {
        UIButton *detailBtn = [UIButton lz_buttonTitle:titles[i] titleColor:COLOR_textColor fontSize:15];
        [detailBtn setImage:k_imageName(imageNames[i]) forState:UIControlStateNormal];
       //Item Y轴
        NSUInteger Y = (i / rank) * (H +rowMargin);
        
        CGFloat X = (i % rank) * (W + rankMargin);
        //Item X轴
        [self.view addSubview:detailBtn];
        detailBtn.tag = 100 + i;
        [detailBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(X);
            make.top.mas_equalTo(top + Y);
            make.width.mas_equalTo(W);
            make.height.mas_equalTo(H);
        }];
        [detailBtn  SG_imagePositionStyle:SGImagePositionStyleTop spacing:10];
        [detailBtn addTarget:self action:@selector(workManagerClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    UIButton *clearBtn = [UIButton lz_buttonTitle:@"拼多多通道管理" titleColor:COLOR_textColor fontSize:17];
    clearBtn.adjustsImageWhenHighlighted = NO;
    [clearBtn setImage:k_imageName(@"pm_pinduoudo") forState:UIControlStateNormal];
    [self.view addSubview:clearBtn];
    [clearBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(204);
        make.left.mas_equalTo(20);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(50);
    }];
    clearBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [clearBtn  SG_imagePositionStyle:SGImagePositionStyleDefault spacing:8];
    
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pm_next"]];
    [clearBtn addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(clearBtn.mas_centerY);
        make.right.mas_equalTo(-15);
        make.size.mas_equalTo(imageView.image.size);
    }];
    
    @weakify(self);
    [[clearBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);
        PMPDDManagerViewController *pddManagerVC = [[PMPDDManagerViewController alloc] init];
        [self.navigationController pushViewController:pddManagerVC animated:YES];
        
      
        
    }];
    
}

///管理
- (void)workManagerClick:(UIButton *)sender{
    
    

    NSArray *className = @[@"PMDealViewController",@"PMMerchantManagerViewController",@"PMFinancialViewController",@"PMPDDAddAccountViewController",@"PMChannelViewController",@"PMPersonManagerViewController"];
//
    NSLog(@"send tag = %ld",(long)sender.tag);
    Class baseVc = NSClassFromString(className[sender.tag-100]);
    LZBaseViewController *vc = [[baseVc alloc] init];
    [LZTool.currentViewController.navigationController pushViewController:vc animated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
