//
//  AppDelegate.m
//  Manager
//
//  Created by lvzhao on 2020/7/29.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "AppDelegate.h"
#import "LZBaseTabBarViewController.h"
#import "PMLoginViewController.h"
#import "IQKeyboardManager.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
      self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
      self.window.backgroundColor = UIColor.whiteColor;
      [self.window makeKeyAndVisible];
    
      //!!!!:本地化
     [self initLocal];
      //进入登录
    if(k_userToken.length > 0){
      LZBaseTabBarViewController *rootNav = [[LZBaseTabBarViewController alloc] init];
      self.window.rootViewController = rootNav;
    } else {
        PMLoginViewController *loginVC = [[PMLoginViewController alloc] init];
        self.window.rootViewController = loginVC;
    }
    return YES;
}


- (void)initLocal{
    //键盘相关
      [IQKeyboardManager sharedManager].enable = YES;                             //使用IQKeyboard
      [IQKeyboardManager sharedManager].enableAutoToolbar = NO;                   // 控制是否显示键盘上的工具条
      [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;         //当键盘弹起时，点击背景收起键盘

    
    [LZNetworkingManager setAutoLog:YES];
}

/**
通道管理 编辑费率, 必要传的参数是那些
 


 
 */



@end
