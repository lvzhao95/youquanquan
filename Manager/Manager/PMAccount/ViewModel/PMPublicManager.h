//
//  PMPublicManager.h
//  Manager
//
//  Created by lvzhao on 2020/8/3.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PMPublicManager : NSObject

@property (nonatomic,strong) NSMutableArray *channelArray;
@property (nonatomic,strong) NSMutableArray *channelBankArray;

@property (nonatomic,strong) NSMutableArray *merchantArray;

@property (nonatomic,strong) NSMutableArray *personArray;

+ (PMPublicManager*)sharedInstance;

//公共数据
- (void)createPublicData;





@end

NS_ASSUME_NONNULL_END
