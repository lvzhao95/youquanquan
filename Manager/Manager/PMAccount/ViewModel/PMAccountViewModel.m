//
//  PMAccountViewModel.m
//  Manager
//
//  Created by lvzhao on 2020/7/29.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMAccountViewModel.h"
#import "LZBaseTabBarViewController.h"

@implementation PMAccountViewModel


///登录
- (void)loginAccount:(NSString *)account password:(NSString *)password{
    
    NSString *loginType = @"username";
    NSDictionary *params = @{@"loginType":loginType,
                             @"loginName":account?:@"",
                             @"password":password?:@"",};
    

    [LZNetworkingManager lz_request:@"post" url:klogin params:params
         success:^(id  _Nullable responseObject) {
        NSDictionary *dataDcit = responseObject;
        NSString *accessToken = dataDcit[@"accessToken"];
        
        LZUserDetailModel *detailModel = [[LZUserDetailModel alloc] init];
        detailModel.Token = accessToken;
        [LZToolCache saveUserInfo:detailModel];
        
        LZBaseTabBarViewController *tababrVC = [[LZBaseTabBarViewController alloc] init];
        UIWindow *window = [UIApplication sharedApplication].delegate.window;
        window.rootViewController = tababrVC;
        
    } failure:^(NSError * _Nullable error) {} isLoading:YES isFailTip:YES];
}
@end
