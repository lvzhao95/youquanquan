//
//  PMAccountViewModel.h
//  Manager
//
//  Created by lvzhao on 2020/7/29.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "LZBaseViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PMAccountViewModel : LZBaseViewModel

///登录
- (void)loginAccount:(NSString *)account password:(NSString *)password;

@end

NS_ASSUME_NONNULL_END
