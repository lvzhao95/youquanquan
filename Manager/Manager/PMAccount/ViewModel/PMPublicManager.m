//
//  PMPublicManager.m
//  Manager
//
//  Created by lvzhao on 2020/8/3.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMPublicManager.h"
#import "LZOptionModel.h"

@implementation PMPublicManager
static PMPublicManager* sharedInstance = nil;
+ (PMPublicManager*)sharedInstance{
    // lazy instantiation
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[PMPublicManager alloc] init];
    });
    return sharedInstance;
}



- (instancetype)init
{
    self = [super init];
    if (self) {

        self.channelArray = [[NSMutableArray alloc] init];
        self.channelBankArray = [[NSMutableArray alloc] init];
        self.merchantArray = [[NSMutableArray alloc] init];
        self.personArray = [[NSMutableArray alloc] init];

        [self createPublicData];
    }
    return self;
}



//公共数据
- (void)createPublicData{
    
    YYCache *cache = [YYCache cacheWithName:@"PublicData"];
    
    {
        id responseObject = [cache objectForKey:[NSString stringWithFormat:@"%@%@%@",kGetPayChannel,@"is_transfer",@"1"]];
        if(responseObject){
            if([responseObject[kResultStatus] isEqualToString:kCode]){
                NSArray *dataArray = responseObject[@"channels"];
                [self.channelBankArray removeAllObjects];
                for (int i = 0; i < dataArray.count; i++){
                    NSDictionary *dictionary = dataArray[i];
                    LZOptionModel *optionModel = [LZOptionModel new];
                    optionModel.detail = dictionary[@"id"];
                    optionModel.key = @"";
                    optionModel.title = dictionary[@"channel_name"];
                    [self.channelBankArray addObject:optionModel];
                }
            }
        } else {
            [LZNetworkingManager lz_request:@"post" url:kGetPayChannel params:@{@"is_transfer":@(1)} success:^(id  _Nullable responseObject) {

                [cache setObject:responseObject forKey:[NSString stringWithFormat:@"%@%@%@",kGetPayChannel,@"is_transfer",@"1"]];
                if([responseObject[kResultStatus] isEqualToString:kCode]){
                    NSArray *dataArray = responseObject[@"channels"];
                    [self.channelBankArray removeAllObjects];
                    for (int i = 0; i < dataArray.count; i++){
                        NSDictionary *dictionary = dataArray[i];
                        LZOptionModel *optionModel = [LZOptionModel new];
                        optionModel.detail = dictionary[@"id"];
                        optionModel.key = @"";
                        optionModel.title = dictionary[@"channel_name"];
                        [self.channelBankArray addObject:optionModel];
                    }
                }
            } failure:^(NSError * _Nullable error) {}];
        }
    }
    
    
    

    

    
    {
        
        id responseObject = [cache objectForKey:[NSString stringWithFormat:@"%@%@%@",kGetPayChannel,@"is_transfer",@"0"]];
        if(responseObject){
             if([responseObject[kResultStatus] isEqualToString:kCode]){
                NSArray *dataArray = responseObject[@"channels"];
                [self.channelArray removeAllObjects];
                for (int i = 0; i < dataArray.count; i++){
                    NSDictionary *dictionary = dataArray[i];
                    LZOptionModel *optionModel = [LZOptionModel new];
                    optionModel.detail = dictionary[@"id"];
                    optionModel.key = @"channel";
                    optionModel.title = dictionary[@"channel_name"];
                    [self.channelArray addObject:optionModel];
                }
            }
        } else {
            [LZNetworkingManager lz_request:@"post" url:kGetPayChannel params:@{@"is_transfer":@(0)} success:^(id  _Nullable responseObject) {

                [cache setObject:responseObject forKey:[NSString stringWithFormat:@"%@%@%@",kGetPayChannel,@"is_transfer",@"0"]];
                if([responseObject[kResultStatus] isEqualToString:kCode]){
                    NSArray *dataArray = responseObject[@"channels"];
                    [self.channelArray removeAllObjects];
                    for (int i = 0; i < dataArray.count; i++){
                        NSDictionary *dictionary = dataArray[i];
                        LZOptionModel *optionModel = [LZOptionModel new];
                        optionModel.detail = dictionary[@"id"];
                        optionModel.key = @"channel";
                        optionModel.title = dictionary[@"channel_name"];
                        [self.channelArray addObject:optionModel];
                    }
                }
            } failure:^(NSError * _Nullable error) {}];
        }
    }
        
    {
        id responseObject = [cache objectForKey:[NSString stringWithFormat:@"%@%@%@",kGetMerchantList,@"status",@"NORMAL"]];
        if(responseObject){
            NSArray *dataArray = responseObject[@"list"];
            [self.merchantArray removeAllObjects];
            for (int i = 0; i < dataArray.count; i++){
                NSDictionary *dictionary = dataArray[i];
                LZOptionModel *optionModel = [LZOptionModel new];
                optionModel.detail = dictionary[@"id"];
                optionModel.key = @"merchant_id";
                optionModel.title = dictionary[@"company_name"];
                [self.merchantArray addObject:optionModel];
            }
            
        } else {
            NSDictionary *params = @{@"page":@(1),@"size":@"999",@"status":@"NORMAL"};
            [LZNetworkingManager lz_request:@"post" url:kGetMerchantList params:params success:^(id  _Nullable responseObject) {
                [cache setObject:responseObject forKey:[NSString stringWithFormat:@"%@%@%@",kGetMerchantList,@"status",@"NORMAL"]];

                NSArray *dataArray = responseObject[@"list"];
                [self.merchantArray removeAllObjects];
                for (int i = 0; i < dataArray.count; i++){
                    NSDictionary *dictionary = dataArray[i];
                    LZOptionModel *optionModel = [LZOptionModel new];
                    optionModel.detail = dictionary[@"id"];
                    optionModel.key = @"merchant_id";
                    optionModel.title = dictionary[@"company_name"];
                    [self.merchantArray addObject:optionModel];
                }
            } failure:^(NSError * _Nullable error) {}];
            
        }
    }
    
    {
        id responseObject = [cache objectForKey:[NSString stringWithFormat:@"%@%@%@",kMemberGetList,@"sortColumn",@"nickname"]];
        if(responseObject){
            if([responseObject[kResultStatus] isEqualToString:kCode]){
                NSArray *dataArray = responseObject[@"list"];
                [self.personArray removeAllObjects];
                for (int i = 0; i < dataArray.count; i++){
                    NSDictionary *dictionary = dataArray[i];
                    LZOptionModel *optionModel = [LZOptionModel new];
                    optionModel.detail = dictionary[@"id"];
                    optionModel.key = @"member_id";
                    optionModel.title= [NSString stringWithFormat:@"%@ %@",dictionary[@"nickname"],dictionary[@"mobile"]];
                    [self.personArray addObject:optionModel];
                }
            }
        } else {
            [LZNetworkingManager lz_request:@"post" url:kMemberGetList params:@{@"sortColumn":@"nickname",@"sortOrder":@"asc"} success:^(id  _Nullable responseObject) {
                [cache setObject:responseObject forKey:[NSString stringWithFormat:@"%@%@%@",kMemberGetList,@"sortColumn",@"nickname"]];
                if([responseObject[kResultStatus] isEqualToString:kCode]){
                   NSArray *dataArray = responseObject[@"list"];
                   [self.personArray removeAllObjects];
                   for (int i = 0; i < dataArray.count; i++){
                       NSDictionary *dictionary = dataArray[i];
                       LZOptionModel *optionModel = [LZOptionModel new];
                       optionModel.detail = dictionary[@"id"];
                       optionModel.key = @"member_id";
                       optionModel.title= [NSString stringWithFormat:@"%@ %@",dictionary[@"nickname"],dictionary[@"mobile"]];
                       [self.personArray addObject:optionModel];
                   }
               }
           } failure:^(NSError * _Nullable error) {}];
        
        }
    }
   
    
   
}




@end
