//
//  PMLoginViewController.m
//  Manager
//
//  Created by lvzhao on 2020/7/29.
//  Copyright © 2020 吕VV. All rights reserved.
//

#import "PMLoginViewController.h"
#import "PMAccountViewModel.h"

@interface PMLoginViewController ()

@property (nonatomic,strong) PMAccountViewModel *viewModel;


@end

@implementation PMLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = COLOR_nav;
    [self setupUI];
    
}


- (void)setupUI{
    
    ///!!!:
    UIView *backgroundView = [[UIView alloc] init];
    backgroundView.backgroundColor = UIColorHex(0x1D1D1D);
    [self.view addSubview:backgroundView];
    [backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(270);
    }];
    
    
    UIButton *logoBtn = [UIButton lz_buttonTitle:@"友 圈 圈 支 付" titleColor:UIColor.whiteColor fontSize:20];
    logoBtn.titleLabel.font = PingFangSC_M(20);
    [backgroundView addSubview:logoBtn];
    [logoBtn setImage:k_imageName(@"pm_logo") forState:UIControlStateNormal];
    [logoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(backgroundView.mas_centerX);
        make.centerY.mas_equalTo(backgroundView.mas_centerY);
        make.width.mas_equalTo(140);
        make.height.mas_equalTo(145);
    }];
    [logoBtn SG_imagePositionStyle:SGImagePositionStyleTop spacing:15];
    
    ///!!!:用户名
    UITextField *accountTextField = [[UITextField alloc] init];
    accountTextField.placeholder = @"请输入帐户名或邮箱地址";
    accountTextField.lz_placeholderColor = COLOR_placeholderColor;
    accountTextField.font = PingFangSC(17);
    accountTextField.keyboardType = UIKeyboardTypeASCIICapable;
    accountTextField.textColor = COLOR_textColor;
    [self.view addSubview:accountTextField];
    [accountTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(45);
        make.right.mas_equalTo(-45);
        make.top.mas_equalTo(backgroundView.mas_bottom).mas_offset(40);
        make.height.mas_equalTo(48);
    }];
    
    UIView *accountLineView = [[UIView alloc] init];
    accountLineView.backgroundColor = COLOR_cellLine;
    [accountTextField addSubview:accountLineView];
    [accountLineView mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.mas_equalTo(0);
         make.right.mas_equalTo(0);
         make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(0.8);
     }];

    ///!!!:密码
    UITextField *psdTextField = [[UITextField alloc] init];
    psdTextField.placeholder = @"请输入登录密码";
    psdTextField.keyboardType = UIKeyboardTypeASCIICapable;
    psdTextField.lz_placeholderColor = COLOR_placeholderColor;
    psdTextField.font = PingFangSC(17);
    psdTextField.textColor = COLOR_textColor;
    psdTextField.secureTextEntry = YES;
    psdTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    UIButton *psdBtn = [psdTextField valueForKey:@"_clearButton"];
    UIImage *clearNameImage = [[UIImage imageNamed:@"pm_clear"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [psdBtn setImage:clearNameImage forState:UIControlStateNormal];
    [self.view addSubview:psdTextField];
    [psdTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(45);
        make.right.mas_equalTo(- 45 - 48);
        make.top.mas_equalTo(accountTextField.mas_bottom).mas_offset(20);
        make.height.mas_equalTo(48);
    }];
  

    UIButton *eyeBtn = [UIButton lz_buttonImageName:@"pm_un_eye" backgroundImageName:@""];
    [eyeBtn setImage:k_imageName(@"pm_eye") forState:UIControlStateSelected];
    [self.view addSubview:eyeBtn];
    [eyeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(psdTextField.mas_right);
        make.right.mas_equalTo(-45);
        make.height.mas_equalTo(48);
        make.centerY.mas_equalTo(psdTextField.mas_centerY);
    }];

    [[eyeBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        x.selected = !x.selected;
        psdTextField.secureTextEntry = !x.selected;
    }];

    
    
    
    UIView *psdLineView = [[UIView alloc] init];
    psdLineView.backgroundColor = COLOR_cellLine;
    [psdTextField addSubview:psdLineView];
    [psdLineView mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.mas_equalTo(0);
         make.right.mas_equalTo(48);
         make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(0.8);
     }];
    
    
    ///!!!:登录
    UIButton *loginBtn = [UIButton lz_buttonTitle:@"登录" titleColor:UIColor.whiteColor fontSize:18];
    loginBtn.backgroundColor = COLOR_appColor;
    loginBtn.cornerRadius = 6;
    [self.view addSubview:loginBtn];
    [loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(45);
        make.right.mas_equalTo(-45);
        make.top.mas_equalTo(psdTextField.mas_bottom).mas_offset(35);
        make.height.mas_equalTo(48);
    }];
    
    accountTextField.text = @"gz1";
    psdTextField.text = @"123456";
    
    @weakify(self);
    [[loginBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);
        NSString *account = accountTextField.text;
        NSString *password = psdTextField.text;
        
        if(account.length == 0){
            showDelayedDismissTitle(accountTextField.placeholder, nil);
            return;
        }
        if(password.length == 0){
            showDelayedDismissTitle(psdTextField.placeholder, nil);
            return;
        }
        [self.viewModel loginAccount:account password:password];
        
    }];
}


- (PMAccountViewModel *)viewModel{
    if(!_viewModel){
        _viewModel = [[PMAccountViewModel alloc] init];
    }
    return _viewModel;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
